# Yaml
Quando estiver alterando arquivos Yaml é sempre importante verificar como está configurado o Tab. *O yaml funciona com base em espaços*, sempre que editar o arquivo yaml verifique se não foi inserido tabs ao invés de espaços.

# Configuração do NuGet 
 
1. Este projeto utiliza algumas bibliotecas .NET proprietárias da WKM, e para obtê-las é necessário adicionar uma fonte de pacotes adicional nas configurações do NuGet. Para isto, basta acessar as configurações de Package Sources do NuGet e incluir o endereço: http://nexus.wkm.com.br/repository/nuget-wkm/ 

# Frontend
## Configuração de endpoint da Api
- A configuração se encontra dentro de Constants Api 
## Gerando uma nova chamada ao servidor
## Rodando a aplicação
- Rodar o seguinte comando na pasta frontend
  `npm install`
- Rodar o seguinte comando na pasta frontend
  `npm start`
# Backend
## Configuração
É necessário instalar o dotnet core 2.0 sdk para rodar o projeto. 
- ConnectionStrings: Copiar a connectionString.Example e substituir o nome para connectionString.yaml, alterando a configuração de usuário, senha e servidor contidas nela. A configuração fica no seguinte caminhoo backend/server/configuration/connectionStrings.yaml
- Elastic Search: backend/server/configuration/elasticSearch.yaml
## Plugins
Para criar um plugin é necessário criar um novo projeto com todas as funcionalidades desejadas. 
## Elastic search
Caso o plugin tenha documento no elastic searcher que seja buscável é necessário incluir alterar a configuração do elastic na pasta backend/server/configuration/elastic.yaml
- name: Nome arbitrário do plugin
- searchResultType: O Tipo de resultado que será utilizado para matches de widgets e ações relacionadas. Normalmente o tipo de entidade com que o plugin vai trabalhar, como produto, endereço, etc...
- index: Índice no elastic search no qual será buscado os itens
- elasticType: Tipo no elastic search dentro do índice
- objectType: Tipo do objeto para qual é mapeado o documento do elastic searcher
- mapper: Classe de mapeamento do objeto do documento do elastic search para uma interface comum de resultados
- queryFactory: Factory que será utilizado para gerar a query de acordo com o documento. Normalmente é utilizado a query factory genérica.

## Regerando api para integração com o WMS

* Obter o swagger file gerado pelo WMS-Cadastro através da api http://<domínio:porta>/swagger/docs/v1

* Instalar o autorest

  `npm install -g autorest`

* Executar a seguinte linha de comando 

  `autorest --input-file=<nome_do_arquivo_salvo> --csharp --output-folder=<pasta_destino> --namespace=integrations.wmsrx.api`

* Copiar resultado para projeto de integrações em api