# Configurando Host

1. Criar site no IIS
2. Alterar .Net CLR version do application pool para none
3. Instalar dotnet core 2.0 hosting
4. Verificar se o windows está atualizado, caso não esteja há possibilidades da aplicação não subir por falta de um redistribuível do c++ não estar instalado. Esse erro será indicado pela seguinte mensagem no event log: api-ms-win-crt-runtime-l1-1-0.dll is missing; Nesse caso é necessário instalar o Visual Studio C++ 2015 Redistributable.
5. É necessário utilizar a configuração de redirect de cookie para a aplicação caso ela seja instalada como sub domain. Caso seja instalada como subdiretório deverá ser alterada as urls e rotas do frontend e verificado as urls de redirecionamento do elastic search, é possível que apenas uma reindexação funcione, uma vez que o wms também vai estar sob um diretório e a indexação deverá gerar as urls com o subdiretório.
6. Criar a pasta 'C:\Windows\system32\config\systemprofile\AppData\Local\ASP.NET\DataProtection-Keys' e dar permissão ao usuário IIS_IUSRS

# Configurando Web Deploy
**É importante seguir a ordem abaixo**

1. Habilitar a feature Management service em Web Server > Management Tools
2. Instalar o Web deploy e Web Deploy Hosting no Web Platform Installer
3. Reiniciar o serviço do IIS através dos comandos 

   `net stop WAS`

   `net start W3SVC`

4. No root do IIS > Management Service parar serviço na aba direita
5. Criar regra de permissão para acesso remoto para o IP da máquina que irá fazer o deploy ou um range de IP. Por exemplo para rede local a range de IPs ficaria 192.168.42.0 com máscara 255.255.255.0.
6. Reiniciar Management Service na aba direita
7. No root do IIS > IIS Manage Users criar usuário utilizado exclusivamente para deploy.
8. No site criado > IIs Manage Permissions adicionar o usuário criado na etapa anterior
9. Com o botão direito em cima do site > Deploy > Configure Web Deploy alterar o primeiro campo de usuário para o usuário criado e permissionado nas etapas anteriores. Alterar a url de deploy para url pública e porta liberadas para o servidor. A porta deve ser a mesma que está configurada no root do IIS > Management Service. 
10. Dar permissão total ao usuário do IIS na pasta da aplicação. O usuário do IIS normalmente é IIS_IUSRS.

# Publicando
Para publicar para o IIS basta publicar a pasta localmente que o publish já cria automaticamente o web.config. É importante notar que para hosts com IIS é importante que no program.cs esteja configurado o UseIISIntegration. No caso do web deploy é necessário configurar o usuário e senha configurados para o usuário de web deploy no IIS na seção "Configurando Web Deploy"

A senha configurada para deploy é 'qBh#6W:tt no usuário AccuinventoryDeploy (Necessário alterar senha e usuário para gerenciador de senhas)

# Elastic Search

1. Criar indices através do script de criação de indices
2. Alterar a url do elastic search no arquivo elastic.yaml para apontar para o servidor de elastic search

# Subdomínios

1. Alterar o appsettings[cookiDomain] do WMS para o domínio no qual ficara hospedado os serviços 
2. Alterar o authentication.yaml do Accuinventory com o mesmo domínio
3. Alterar as urls de apis.ts e routes.ts do Accuinventory frontend para as urls com subdomínio
4. Alterar o web.config do servidor do wms para incluir a regra de Url Rewrite de outbound incluída na raiz do projeto. Esse rewrite vai reescrever o cookie Domain que é setado automaticamente pelo Proxy Reverso, fazendo assim que a resposta com setCookie aponte para o domínio correto.
5. Incluir a aplicação Accuinventory Backend como aplicação dentro do website Accuinventory Frontend, permitindo assim que os dois compartilhem cookies. **É necessário atualizar a url do frontend para apontar para o diretório relativo**.