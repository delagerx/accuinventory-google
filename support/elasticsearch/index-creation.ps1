$elasticServer = "192.168.42.79:9201";
Invoke-WebRequest -Method DELETE -Uri "http://$elasticServer/accuinventory-search-addresses?pretty"
Invoke-WebRequest -Method DELETE -Uri "http://$elasticServer/accuinventory-search-lots?pretty"
Invoke-WebRequest -Method DELETE -Uri "http://$elasticServer/accuinventory-search-products?pretty"
Invoke-WebRequest -Method DELETE -Uri "http://$elasticServer/accuinventory-search-lpns?pretty"
Invoke-WebRequest -Method DELETE -Uri "http://$elasticServer/accuinventory-search-actions?pretty"

$addressSettings = @"
{
	"settings" : {
        "index" : {
            "number_of_shards" : 1,
            "number_of_replicas" : 1
        }
    },
	"mappings": {
		"address": {
            "properties": {
                "completeaddress": { 
                    "type": "text",
                    "index":"analyzed",
                    "fields" : {
                        "raw" : {
                            "index" : "not_analyzed",
                            "type" : "string"
                        }
                    }
                }
            }
		}
    }
}
"@;
Invoke-WebRequest -Method PUT -Uri "http://$elasticServer/accuinventory-search-addresses?pretty" -Body $addressSettings

