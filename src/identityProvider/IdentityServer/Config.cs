﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace IdentityServer
{
    public class Config
    {
        public const int TwoYears = 63072000;
        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("accuinventoryApi", "Accuinventory API")
                {
                    UserClaims = {"name"}
                }
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {
                new Client
                {
                    ClientId = "app.accuinventory",
                    ClientName = "Accuinventory Launcher",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AccessTokenLifetime = TwoYears,
                    IdentityTokenLifetime = TwoYears,

                    RedirectUris = { "http://localhost:3000/app/callback" },
                    PostLogoutRedirectUris = { "http://localhost:3000/app/" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "accuinventoryApi"
                    },
                    
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowedCorsOrigins = new List<string>()
                    {
                        "http://localhost:3000"
                    },
                    AllowAccessTokensViaBrowser = true,
                    ClientUri = "http://localhost:3000",
                    RequireConsent = false,
                },
                new Client
                {
                    ClientId = "wms",
                    ClientName = "Wms",
                    AllowedGrantTypes = GrantTypes.Implicit,

                    RedirectUris = { "http://localhost:5001/" },
                    PostLogoutRedirectUris = { "http://localhost:5001/Home/Index" },
                    ClientUri = "http://localhost:5001",
                    AccessTokenLifetime = TwoYears,
                    IdentityTokenLifetime = TwoYears,

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "accuinventoryApi"
                    },
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                }
            };
        }
    }
}