﻿using Dapper;
using Microsoft.Extensions.Logging;
using orders.widgets.orderOverview.dto;
using plugins.sdk.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace orders.widgets.orderOverview
{
    public interface IOrderWmsRepository
    {
        Task<OrderOverviewQueryResponse> FetchOrderAsync(string id);
    }

    public class OrderWmsRepository : IOrderWmsRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;
        private readonly ILogger<OrderWmsRepository> _logger;

        public OrderWmsRepository(IWmsDatabaseConnectionFactory connectionFactory, ILogger<OrderWmsRepository> logger)
        {
            _connectionFactory = connectionFactory;
            _logger = logger;
        }

        public async Task<OrderOverviewQueryResponse> FetchOrderAsync(string id)
        {
            try
            {
                using (var connection = _connectionFactory.Make())
                {
                    connection.Open();

                    OrderOverviewQueryBuilder queryBuilder = new OrderOverviewQueryBuilder(id);

                    WmsOrder selectOrderQueryResult = (await connection.QueryAsync<WmsOrder>(queryBuilder.SelectOrderQuery)).FirstOrDefault();

                    if (selectOrderQueryResult == null)
                        return null;

                    IEnumerable<WmsOrderVolume> selectOrderVolumeQueryResult = (await connection.QueryAsync<WmsOrderVolume>(queryBuilder.SelectOrderVolumesQuery));
                    IEnumerable<WmsOrderItem> selectOrderItemQueryResult = (await connection.QueryAsync<WmsOrderItem>(queryBuilder.SelectOrderItemsQuery));
                    WmsOrderStages selectOrderStagesQueryResult = (await connection.QueryAsync<WmsOrderStages>(queryBuilder.SelectOrderHistoryQuery)).FirstOrDefault();
                    WmsOrderShipmentInformation selectOrderShipmentInformationQueryResult = (await connection.QueryAsync<WmsOrderShipmentInformation>(queryBuilder.SelectOrderShipmentInformationQuery)).FirstOrDefault();

                    return new OrderOverviewQueryResponse(
                        selectOrderQueryResult, 
                        selectOrderItemQueryResult, 
                        selectOrderVolumeQueryResult, 
                        selectOrderStagesQueryResult,
                        selectOrderShipmentInformationQueryResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    }
}
