﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orders.widgets.orderOverview
{
    public class OrderOverviewQueryBuilder
    {
        public string Id { get; }

        public int? OrderItemsMaxResults { get; }

        public int? OrderVolumesMaxResults { get; }

        public string SelectOrderQuery
        {
            get
            {
                return
                    $"SELECT p.cod_pedido AS Id, e.razao_social AS Entidade, sp.situacao AS Situacao, p.cod_prenota AS Picking, op.descricao AS Operacao, sop.descricao AS Suboperacao, ori.descricao AS Origem, " +
                    $"(select count(pdi.cod_pedido) FROM pedido_item pdi WHERE pdi.cod_pedido = p.cod_pedido) AS NumItens " +
                    $"FROM pedido p " +
                    $"LEFT JOIN entidade e ON p.cod_entidade = e.cod_entidade " +
                    $"LEFT JOIN vw_situacao_pedidos sp ON sp.cod_pedido = p.cod_pedido " +
                    $"LEFT JOIN def_operacao op ON op.operacao = p.cod_operacao_logistica " +
                    $"LEFT JOIN def_suboperacao sop ON sop.cod_suboperacao = p.cod_suboperacao " +
                    $"LEFT JOIN def_origem_ped ori ON ori.cod_origem_ped = p.cod_origem_ped " +
                    $"WHERE p.cod_pedido = {Id};";
            }
        }

        public string SelectOrderItemsQuery
        {
            get
            {
                string limitExpression = OrderItemsMaxResults.HasValue ? $"TOP {OrderItemsMaxResults}" : string.Empty;

                return
                    $"SELECT {limitExpression} pr.cod_produto AS CodigoProduto, pr.descricao AS DescricaoProduto, pr.digito AS DigitoProduto, pi.quantidade AS Quantidade, pi.separado AS Separado " +
                    $"FROM pedido_item pi " +
                    $"LEFT JOIN produto pr ON pi.cod_produto = pr.cod_produto " +
                    $"WHERE pi.cod_pedido = {Id};";
            }
        }

        public string SelectOrderVolumesQuery
        {
            get
            {
                string limitExpression = OrderVolumesMaxResults.HasValue ? $"TOP {OrderVolumesMaxResults}" : string.Empty;

                return
                    $"SELECT {limitExpression} pv.volume AS Volume, tcx.descricao AS TipoCaixa, pv.checkout AS Checkout, pvs.situacao AS Situacao, 'Automático' AS Tipo " +
                    $"FROM pedido_volume pv " +
                    $"LEFT JOIN def_tipo_caixa_fechada tcx on pv.Tipo_caixa = tcx.cod_tipo " +
                    $"LEFT JOIN vw_pedido_volume_situacao pvs ON pvs.cod_pedido = pv.cod_pedido AND pvs.volume = pv.Volume " +
                    $"WHERE pv.cod_pedido = {Id};";
            }
        }

        public string SelectOrderHistoryQuery
        {
            get
            {
                return
                    $"SELECT " +
                    $" p.[digitacao] AS Digitacao " +
                    $",p.[termino_digitacao] AS TerminoDigitacao " +
                    $",p.[data_liberacao] AS DataLiberacao " +
                    $",p.[prenota] AS PreNota " +
                    $",p.[encerramento_PN] AS EncerramentoPn " +
                    $",p.[faturamento] AS Faturamento " +
                    $",p.[data_exportacao] AS DataExportacao " +
                    $",p.[dataExportacaoTMS] AS DataExportacaoTms " +
                    $",p.[data_solicitacao_cancelamento] AS DataSolitacaoCancelamento " +
                    $",p.[data_confirmacao_cancelamento] AS DataConfirmacaoCancelamento " +
                    $",p.[data_finalizacao_prenota] AS DataFinalizacaoPrenota " +
                    $",p.[data_saida_transp] AS DataSaidaTransportadora " +
                    $"FROM pedido p " +
                    $"WHERE p.cod_pedido = {Id};";
            }
        }

        public string SelectOrderShipmentInformationQuery
        {
            get
            {
                return
                    $"SELECT  " +
                    $"ajudante.fantasia AS Ajudante, " +
                    $"re.hora_chegada AS Chegada, " +
                    $"cp.cidade AS Cidade, " +
                    $"tr.hora_entrega AS Entrega, " +
                    $"cp.sigla_estado AS Estado, " +
                    $"motorista.fantasia AS Motorista, " +
                    $"re.placa AS Veiculo, " +
                    $"re.hora_saida AS Saida, " +
                    $"transportador.fantasia AS Transportador, " +
                    $"CASE WHEN pe.id_pedido_ender IS NOT NULL THEN pe.endereco ELSE ee.endereco END AS endereco, " +
                    $"CASE WHEN pe.id_pedido_ender IS NOT NULL THEN pe.bairro ELSE ee.bairro END AS bairro, " +
                    $"CASE WHEN pe.id_pedido_ender IS NOT NULL THEN pe.cep ELSE ee.cep END AS cep, " +
                    $"CASE WHEN pe.id_pedido_ender IS NOT NULL THEN pe.cod_rota ELSE ee.cod_rota END AS cod_rota " +
                    $"FROM pedido_romaneio pr " +
                    $"LEFT JOIN pedido p ON p.cod_pedido = pr.cod_pedido " +
                    $"LEFT JOIN entidade e ON p.cod_entidade = e.cod_entidade " +
                    $"LEFT JOIN entidade_ender ee ON e.cod_entidade = ee.cod_entidade AND ee.principal = -1 " +
                    $"LEFT JOIN cep cp ON ee.cep = cp.cep  " +
                    $"LEFT JOIN pedido_ender pe ON pe.cod_pedido = pr.cod_pedido " +
                    $"LEFT JOIN romaneio r ON pr.cod_romaneio = r.cod_romaneio " +
                    $"LEFT JOIN vw_wmsoper_titulo titulo ON pr.cod_pedido = titulo.cod_pedido " +
                    $"LEFT JOIN titulo_romaneio tr ON pr.cod_romaneio = tr.cod_romaneio AND titulo.cod_titulo = tr.cod_titulo " +
                    $"LEFT JOIN entidade transportador ON r.cod_transportador = transportador.cod_entidade " +
                    $"LEFT JOIN romaneio_entrega re ON r.cod_entrega = re.cod_entrega " +
                    $"LEFT JOIN entidade motorista ON re.cod_motorista = motorista.cod_entidade " +
                    $"LEFT JOIN entidade ajudante ON re.cod_ajudante = ajudante.cod_entidade " +
                    $"WHERE pr.cod_pedido = {Id};";
            }
        }

        public OrderOverviewQueryBuilder(string id)
        {
            Id = id;
        }

        public OrderOverviewQueryBuilder(string id, int? orderItemsMaxResults, int? orderVolumesMaxResults) : this(id)
        {
            OrderItemsMaxResults = orderItemsMaxResults;
            OrderVolumesMaxResults = orderVolumesMaxResults;
        }
    }
}
