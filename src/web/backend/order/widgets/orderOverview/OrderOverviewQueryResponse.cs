﻿using System;
using System.Collections.Generic;
using System.Text;
using orders.widgets.orderOverview.dto;
using System.Linq;

namespace orders.widgets.orderOverview
{
    public class OrderOverviewQueryResponse
    {
        public Order Order { get; set; }

        public OrderShipmentInformation OrderShipmentInformation { get; set; }

        public IEnumerable<OrderItem> OrderItems { get; set; } = new List<OrderItem>();

        public IEnumerable<OrderVolume> OrderVolumes { get; set; } = new List<OrderVolume>();

        public IEnumerable<OrderStage> OrderHistory { get; set; } = new List<OrderStage>();

        public OrderOverviewQueryResponse(
            WmsOrder wmsOrder, 
            IEnumerable<WmsOrderItem> wmsOrderItemList, 
            IEnumerable<WmsOrderVolume> wmsOrderVolumeList, 
            WmsOrderStages wmsOrderStages,
            WmsOrderShipmentInformation wmsOrderShipmentInformation)
        {
            Order = new Order(wmsOrder);
            OrderItems = wmsOrderItemList.Select(wmsOrderItem => new OrderItem(wmsOrderItem));
            OrderVolumes = wmsOrderVolumeList.Select(wmsOrderVolume => new OrderVolume(wmsOrderVolume));
            OrderHistory = new OrderStages(wmsOrderStages).AsOrderHistory();

            if (wmsOrderShipmentInformation != null)
                OrderShipmentInformation = new OrderShipmentInformation(wmsOrderShipmentInformation);
        }
    }

    public class Order
    {
        public string Id { get; set; }

        public string LegacyCode { get; set; }

        public string MovementOrder { get; set; }

        public string Entity { get; set; }

        public string Operation { get; set; }

        public string Picking { get; set; }

        public string SubOperation { get; set; }

        public string Status { get; set; }

        public string Origin { get; set; }

        public int ItemsQuantity { get; set; }

        public Order(WmsOrder wmsOrder)
        {
            Id = wmsOrder.Id;
            // CodLegado = selectOrderQueryResult.;
            Entity = wmsOrder.Entidade;
            Status = wmsOrder.Situacao;
            Operation = wmsOrder.Operacao;
            Picking = wmsOrder.Picking;
            SubOperation = wmsOrder.Suboperacao;
            Origin = wmsOrder.Origem;
            ItemsQuantity = wmsOrder.NumItens;
        }
    }

    public class OrderShipmentInformation
    {
        public string Assistant { get; set; }

        public string District { get; set; }

        public string ZipCode { get; set; }

        public DateTime? Arrive { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public DateTime? Delivery { get; set; }

        public string State { get; set; }

        public string Driver { get; set; }

        public string Vehicle { get; set; }

        public string Route { get; set; }

        public DateTime? Departure { get; set; }

        public string Conveyor { get; set; }

        public OrderShipmentInformation(WmsOrderShipmentInformation wmsOrderShipmentInformation)
        {
            Assistant = wmsOrderShipmentInformation.Ajudante;
            District = wmsOrderShipmentInformation.Bairro;
            ZipCode = wmsOrderShipmentInformation.Cep;
            Arrive = wmsOrderShipmentInformation.Chegada;
            City = wmsOrderShipmentInformation.Cidade;
            Address = wmsOrderShipmentInformation.Endereco;
            Delivery = wmsOrderShipmentInformation.Entrega;
            State = wmsOrderShipmentInformation.Estado;
            Driver = wmsOrderShipmentInformation.Motorista;
            Vehicle = wmsOrderShipmentInformation.Veiculo;
            Route = wmsOrderShipmentInformation.Rota;
            Departure = wmsOrderShipmentInformation.Saida;
            Conveyor = wmsOrderShipmentInformation.Transportador;
        }
    }

    public class OrderItem
    {
        public string ProductCode { get; set; }

        public string ProductDescription { get; set; }

        public string ProductDigit { get; set; }

        public int Quantity { get; set; }

        public int Collected { get; set; }


        public OrderItem(WmsOrderItem wmsOrderItem)
        {
            ProductCode = wmsOrderItem.CodigoProduto;
            ProductDescription = wmsOrderItem.DescricaoProduto;
            ProductDigit = wmsOrderItem.DigitoProduto;
            Quantity = wmsOrderItem.Quantidade;
            Collected = wmsOrderItem.Separado;
        }
    }

    public class OrderVolume
    {
        public string Volume { get; set; }

        public string BoxType { get; set; }

        public string Checkout { get; set; }

        public string Location { get; set; }

        public string Type { get; set; }

        public OrderVolume(WmsOrderVolume wmsOrderVolume)
        {
            Checkout = wmsOrderVolume.Checkout;
            Location = wmsOrderVolume.Situacao;
            Type = wmsOrderVolume.Tipo;
            BoxType = wmsOrderVolume.TipoCaixa;
            Volume = wmsOrderVolume.Volume;
        }
    }

    public class OrderStage
    {
        public string Key { get; set; }
        public DateTime Timestamp { get; set; }

        public OrderStage(string key, DateTime timestamp)
        {
            Key = key;
            Timestamp = timestamp;
        }
    }

    public class OrderStages
    {
        public DateTime? Typing { get; set; }
        public DateTime? TypingEnd { get; set; }
        public DateTime? Release { get; set; }
        public DateTime? PreNote { get; set; }
        public DateTime? PreNoteClosure { get; set; }
        public DateTime? Billing { get; set; }
        public DateTime? Exportation { get; set; }
        public DateTime? TmsExportation { get; set; }
        public DateTime? CancellationRequest { get; set; }
        public DateTime? CancellationConfirmation { get; set; }
        public DateTime? PreNoteFinalization { get; set; }
        public DateTime? DepartureFromTheCarrier { get; set; }

        public OrderStages(WmsOrderStages wmsOrderStages)
        {
            Typing = wmsOrderStages.Digitacao;
            TypingEnd = wmsOrderStages.TerminoDigitacao;
            Release = wmsOrderStages.DataLiberacao;
            PreNote = wmsOrderStages.PreNota;
            PreNoteClosure = wmsOrderStages.EncerramentoPn;
            Billing = wmsOrderStages.Faturamento;
            Exportation = wmsOrderStages.DataExportacao;
            TmsExportation = wmsOrderStages.DataExportacaoTms;
            CancellationRequest = wmsOrderStages.DataSolitacaoCancelamento;
            CancellationConfirmation = wmsOrderStages.DataConfirmacaoCancelamento;
            PreNoteFinalization = wmsOrderStages.DataFinalizacaoPrenota;
            DepartureFromTheCarrier = wmsOrderStages.DataSaidaTransportadora;
        }

        public IEnumerable<OrderStage> AsOrderHistory()
        {
            IList<OrderStage> history = new List<OrderStage>();

            if (Typing.HasValue)
                history.Add(new OrderStage(nameof(Typing), Typing.Value));

            if (TypingEnd.HasValue)
                history.Add(new OrderStage(nameof(TypingEnd), TypingEnd.Value));

            if (Release.HasValue)
                history.Add(new OrderStage(nameof(Release), Release.Value));

            if (PreNote.HasValue)
                history.Add(new OrderStage(nameof(PreNote), PreNote.Value));

            if (PreNoteClosure.HasValue)
                history.Add(new OrderStage(nameof(PreNoteClosure), PreNoteClosure.Value));

            if (Billing.HasValue)
                history.Add(new OrderStage(nameof(Billing), Billing.Value));

            if (Exportation.HasValue)
                history.Add(new OrderStage(nameof(Exportation), Exportation.Value));

            if (TmsExportation.HasValue)
                history.Add(new OrderStage(nameof(TmsExportation), TmsExportation.Value));

            if (CancellationRequest.HasValue)
                history.Add(new OrderStage(nameof(CancellationRequest), CancellationRequest.Value));

            if (CancellationConfirmation.HasValue)
                history.Add(new OrderStage(nameof(CancellationConfirmation), CancellationConfirmation.Value));

            if (PreNoteFinalization.HasValue)
                history.Add(new OrderStage(nameof(PreNoteFinalization), PreNoteFinalization.Value));

            if (DepartureFromTheCarrier.HasValue)
                history.Add(new OrderStage(nameof(DepartureFromTheCarrier), DepartureFromTheCarrier.Value));

            return history.OrderBy(orderStage => orderStage.Timestamp);
        }
    }
}
