﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace orders.widgets.orderOverview
{
    [Route("[controller]")]
    public class OrderOverviewController : Controller
    {
        private readonly IOrderWmsRepository _orderOverviewRepository;

        public OrderOverviewController(IOrderWmsRepository addressOverviewRepository)
        {
            _orderOverviewRepository = addressOverviewRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderOverview(string id)
        {
            var order = await _orderOverviewRepository.FetchOrderAsync(id);

            if (order == null)
                return NotFound();

            return Ok(order);
        }
    }
}
