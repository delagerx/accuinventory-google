﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orders.widgets.orderOverview.dto
{
    public class WmsOrderItem
    {
        public string CodigoProduto { get; set; }

        public string DescricaoProduto { get; set; }

        public string DigitoProduto { get; set; }

        public int Quantidade { get; set; }

        public int Separado { get; set; }
    }
}
