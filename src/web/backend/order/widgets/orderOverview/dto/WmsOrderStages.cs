﻿using System;

namespace orders.widgets.orderOverview.dto
{
    public class WmsOrderStages
    {
        public DateTime? Digitacao { get; set; }
        public DateTime? TerminoDigitacao { get; set; }
        public DateTime? DataLiberacao { get; set; }
        public DateTime? PreNota { get; set; }
        public DateTime? EncerramentoPn { get; set; }
        public DateTime? Faturamento { get; set; }
        public DateTime? DataExportacao { get; set; }
        public DateTime? DataExportacaoTms { get; set; }
        public DateTime? DataSolitacaoCancelamento { get; set; }
        public DateTime? DataConfirmacaoCancelamento { get; set; }
        public DateTime? DataFinalizacaoPrenota { get; set; }
        public DateTime? DataSaidaTransportadora { get; set; }
    }
}