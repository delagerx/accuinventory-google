﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orders.widgets.orderOverview.dto
{
    public class WmsOrderShipmentInformation
    {
        public string Ajudante { get; set; }

        public string Bairro { get; set; }

        public string Cep { get; set; }

        public DateTime? Chegada { get; set; }

        public string Cidade { get; set; }

        public string Endereco { get; set; }

        public DateTime? Entrega { get; set; }

        public string Estado { get; set; }

        public string Motorista { get; set; }

        public string Veiculo { get; set; }

        public string Rota { get; set; }

        public DateTime? Saida { get; set; }

        public string Transportador { get; set; }
    }
}
