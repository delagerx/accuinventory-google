﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orders.widgets.orderOverview.dto
{
    public class WmsOrderVolume
    {
        public string Volume { get; set; }

        public string TipoCaixa { get; set; }

        public string Checkout { get; set; }

        public string Situacao { get; set; }

        public string Tipo { get; set; }
    }
}
