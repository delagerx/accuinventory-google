﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orders.widgets.orderOverview.dto
{
    public class WmsOrder
    {
        public string Id { get; set; }

        public string CodLegado { get; set; }

        public string Entidade { get; set; }

        public int NumItens { get; set; }

        public string Operacao { get; set; }

        public string Picking { get; set; }

        public string Origem { get; set; }

        public string Situacao { get; set; }

        public string Suboperacao { get; set; }        
    }
}
