﻿using System;
using System.Collections.Generic;
using System.Text;
using plugins.sdk.search.elastic.attributes;

namespace orders.search
{
    public class OrderDocument
    {
        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string Id { get; set; }

        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string PrefixedField { get; set; }

        [ElasticPrefixedSearchField]
        public string EntityName { get; set; }

        [ElasticPrefixedSearchField]
        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }
    }
}
