﻿using plugins.sdk.search.elastic;

namespace orders.search
{
    public class OrderDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var order = ElasticDocumentSerializer.DeserializeInfos<OrderDocument>(serializedAction);

            string label = $"{order.Id} - {order.EntityName}";

            return new MappingResult(label, SearchResultSubType.None, LogisticOperationDocument.AllLOs, new { id = order.Id, name = order.Id, entityName = order.EntityName });
        }
    }
}
