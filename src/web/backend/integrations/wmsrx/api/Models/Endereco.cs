// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace integrations.wmsrx.api.Models
{
    using integrations.wmsrx;
    using integrations.wmsrx.api;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class Endereco
    {
        /// <summary>
        /// Initializes a new instance of the Endereco class.
        /// </summary>
        public Endereco()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Endereco class.
        /// </summary>
        public Endereco(TipoArmazenamentoEndereco defTipoArmazenamentoEndereco = default(TipoArmazenamentoEndereco), int? idEnderecoDiv = default(int?), string text = default(string), IList<Endereco> node = default(IList<Endereco>), int? idDef = default(int?), DefEndereco defEndereco = default(DefEndereco), int? idEnderecoAgrupamentoKpi = default(int?), DefEnderecoAgrupamentoKpi defEnderecoAgrumapentoKpi = default(DefEnderecoAgrupamentoKpi), int? idLoteClassificacao = default(int?), DefLoteClassificacao loteClassificacao = default(DefLoteClassificacao), int? idDefEnderecoSetor = default(int?), EnderecoSetor enderecoSetor = default(EnderecoSetor), int? idTipoVolume = default(int?), TipoVolumeEndereco defTipoVolumeEndereco = default(TipoVolumeEndereco), string idEnderecoCompleto = default(string), int? codSituacaoEndereco = default(int?), DefSituacaoEndereco defSituacaoEndereco = default(DefSituacaoEndereco))
        {
            DefTipoArmazenamentoEndereco = defTipoArmazenamentoEndereco;
            IdEnderecoDiv = idEnderecoDiv;
            Text = text;
            Node = node;
            IdDef = idDef;
            DefEndereco = defEndereco;
            IdEnderecoAgrupamentoKpi = idEnderecoAgrupamentoKpi;
            DefEnderecoAgrumapentoKpi = defEnderecoAgrumapentoKpi;
            IdLoteClassificacao = idLoteClassificacao;
            LoteClassificacao = loteClassificacao;
            IdDefEnderecoSetor = idDefEnderecoSetor;
            EnderecoSetor = enderecoSetor;
            IdTipoVolume = idTipoVolume;
            DefTipoVolumeEndereco = defTipoVolumeEndereco;
            IdEnderecoCompleto = idEnderecoCompleto;
            CodSituacaoEndereco = codSituacaoEndereco;
            DefSituacaoEndereco = defSituacaoEndereco;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "DefTipoArmazenamentoEndereco")]
        public TipoArmazenamentoEndereco DefTipoArmazenamentoEndereco { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_endereco_div")]
        public int? IdEnderecoDiv { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "node")]
        public IList<Endereco> Node { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_def")]
        public int? IdDef { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "DefEndereco")]
        public DefEndereco DefEndereco { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_endereco_agrupamento_kpi")]
        public int? IdEnderecoAgrupamentoKpi { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "DefEnderecoAgrumapentoKpi")]
        public DefEnderecoAgrupamentoKpi DefEnderecoAgrumapentoKpi { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_lote_classificacao")]
        public int? IdLoteClassificacao { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "LoteClassificacao")]
        public DefLoteClassificacao LoteClassificacao { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_def_endereco_setor")]
        public int? IdDefEnderecoSetor { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "EnderecoSetor")]
        public EnderecoSetor EnderecoSetor { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_tipo_volume")]
        public int? IdTipoVolume { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "DefTipoVolumeEndereco")]
        public TipoVolumeEndereco DefTipoVolumeEndereco { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_endereco_completo")]
        public string IdEnderecoCompleto { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_situacao_endereco")]
        public int? CodSituacaoEndereco { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "DefSituacaoEndereco")]
        public DefSituacaoEndereco DefSituacaoEndereco { get; set; }

    }
}
