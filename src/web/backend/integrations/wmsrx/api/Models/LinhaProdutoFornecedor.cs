// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace integrations.wmsrx.api.Models
{
    using integrations.wmsrx;
    using integrations.wmsrx.api;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class LinhaProdutoFornecedor
    {
        /// <summary>
        /// Initializes a new instance of the LinhaProdutoFornecedor class.
        /// </summary>
        public LinhaProdutoFornecedor()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the LinhaProdutoFornecedor class.
        /// </summary>
        public LinhaProdutoFornecedor(int? idLinhaProduto = default(int?), int? codEntidade = default(int?), LinhaProduto linhaProduto = default(LinhaProduto))
        {
            IdLinhaProduto = idLinhaProduto;
            CodEntidade = codEntidade;
            LinhaProduto = linhaProduto;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_linha_produto")]
        public int? IdLinhaProduto { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_entidade")]
        public int? CodEntidade { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "LinhaProduto")]
        public LinhaProduto LinhaProduto { get; set; }

    }
}
