// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace integrations.wmsrx.api.Models
{
    using integrations.wmsrx;
    using integrations.wmsrx.api;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class TipoArmazenamentoEndereco
    {
        /// <summary>
        /// Initializes a new instance of the TipoArmazenamentoEndereco class.
        /// </summary>
        public TipoArmazenamentoEndereco()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the TipoArmazenamentoEndereco class.
        /// </summary>
        public TipoArmazenamentoEndereco(int? idTipoArmazenamentoEndereco = default(int?), string descricao = default(string))
        {
            IdTipoArmazenamentoEndereco = idTipoArmazenamentoEndereco;
            Descricao = descricao;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_tipo_armazenamento_endereco")]
        public int? IdTipoArmazenamentoEndereco { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "descricao")]
        public string Descricao { get; set; }

    }
}
