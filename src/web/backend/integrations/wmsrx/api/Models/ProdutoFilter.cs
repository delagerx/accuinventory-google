// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace integrations.wmsrx.api.Models
{
    using integrations.wmsrx;
    using integrations.wmsrx.api;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class ProdutoFilter
    {
        /// <summary>
        /// Initializes a new instance of the ProdutoFilter class.
        /// </summary>
        public ProdutoFilter()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ProdutoFilter class.
        /// </summary>
        public ProdutoFilter(int? codProduto = default(int?), string descricao = default(string), int? idLinhaProduto = default(int?), int? codGrupo = default(int?), int? codSituacao = default(int?), int? codSubgrupo = default(int?), int? codigoOperacaoLogistica = default(int?), int? page = default(int?), int? pageSize = default(int?), string sortColumn = default(string), string sortOrder = default(string), bool? closed = default(bool?), bool? isPartial = default(bool?), bool? export = default(bool?))
        {
            CodProduto = codProduto;
            Descricao = descricao;
            IdLinhaProduto = idLinhaProduto;
            CodGrupo = codGrupo;
            CodSituacao = codSituacao;
            CodSubgrupo = codSubgrupo;
            CodigoOperacaoLogistica = codigoOperacaoLogistica;
            Page = page;
            PageSize = pageSize;
            SortColumn = sortColumn;
            SortOrder = sortOrder;
            Closed = closed;
            IsPartial = isPartial;
            Export = export;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_produto")]
        public int? CodProduto { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "descricao")]
        public string Descricao { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id_linha_produto")]
        public int? IdLinhaProduto { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_grupo")]
        public int? CodGrupo { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_situacao")]
        public int? CodSituacao { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cod_subgrupo")]
        public int? CodSubgrupo { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "CodigoOperacaoLogistica")]
        public int? CodigoOperacaoLogistica { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "Page")]
        public int? Page { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "PageSize")]
        public int? PageSize { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "SortColumn")]
        public string SortColumn { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "SortOrder")]
        public string SortOrder { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "Closed")]
        public bool? Closed { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "IsPartial")]
        public bool? IsPartial { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "Export")]
        public bool? Export { get; set; }

    }
}
