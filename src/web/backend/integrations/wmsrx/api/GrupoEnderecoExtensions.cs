// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace integrations.wmsrx.api
{
    using integrations.wmsrx;
    using Models;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Extension methods for GrupoEndereco.
    /// </summary>
    public static partial class GrupoEnderecoExtensions
    {
            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='filtercodigo'>
            /// </param>
            /// <param name='filterdescricao'>
            /// </param>
            /// <param name='filterpage'>
            /// </param>
            /// <param name='filterpageSize'>
            /// </param>
            /// <param name='filtersortColumn'>
            /// </param>
            /// <param name='filtersortOrder'>
            /// </param>
            /// <param name='filterclosed'>
            /// </param>
            /// <param name='filterisPartial'>
            /// </param>
            /// <param name='filterexport'>
            /// </param>
            public static object Get(this IGrupoEndereco operations, int? filtercodigo = default(int?), string filterdescricao = default(string), int? filterpage = default(int?), int? filterpageSize = default(int?), string filtersortColumn = default(string), string filtersortOrder = default(string), bool? filterclosed = default(bool?), bool? filterisPartial = default(bool?), bool? filterexport = default(bool?))
            {
                return operations.GetAsync(filtercodigo, filterdescricao, filterpage, filterpageSize, filtersortColumn, filtersortOrder, filterclosed, filterisPartial, filterexport).GetAwaiter().GetResult();
            }

            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='filtercodigo'>
            /// </param>
            /// <param name='filterdescricao'>
            /// </param>
            /// <param name='filterpage'>
            /// </param>
            /// <param name='filterpageSize'>
            /// </param>
            /// <param name='filtersortColumn'>
            /// </param>
            /// <param name='filtersortOrder'>
            /// </param>
            /// <param name='filterclosed'>
            /// </param>
            /// <param name='filterisPartial'>
            /// </param>
            /// <param name='filterexport'>
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<object> GetAsync(this IGrupoEndereco operations, int? filtercodigo = default(int?), string filterdescricao = default(string), int? filterpage = default(int?), int? filterpageSize = default(int?), string filtersortColumn = default(string), string filtersortOrder = default(string), bool? filterclosed = default(bool?), bool? filterisPartial = default(bool?), bool? filterexport = default(bool?), CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.GetWithHttpMessagesAsync(filtercodigo, filterdescricao, filterpage, filterpageSize, filtersortColumn, filtersortOrder, filterclosed, filterisPartial, filterexport, null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='filter'>
            /// </param>
            public static object Post(this IGrupoEndereco operations, GrupoEnderecoFilter filter)
            {
                return operations.PostAsync(filter).GetAwaiter().GetResult();
            }

            /// <param name='operations'>
            /// The operations group for this extension method.
            /// </param>
            /// <param name='filter'>
            /// </param>
            /// <param name='cancellationToken'>
            /// The cancellation token.
            /// </param>
            public static async Task<object> PostAsync(this IGrupoEndereco operations, GrupoEnderecoFilter filter, CancellationToken cancellationToken = default(CancellationToken))
            {
                using (var _result = await operations.PostWithHttpMessagesAsync(filter, null, cancellationToken).ConfigureAwait(false))
                {
                    return _result.Body;
                }
            }

    }
}
