﻿using integrations.wmsrxReportingServices.api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace integrations.wmsrxReportingServices.api
{
    public interface IWmsRxReportingServicesClient
    {
        Task<IList<WmsRxReport>> FetchIndexableReports(CancellationToken cancellationToken = default(CancellationToken));
    }

    public class WmsRxReportingServicesClient : IWmsRxReportingServicesClient
    {
        public Task<IList<WmsRxReport>> FetchIndexableReports(CancellationToken cancellationToken = default(CancellationToken))
        {
            IList<WmsRxReport> reports;
            using (StreamReader file = File.OpenText("mocks/reports/reportApiResponse.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                reports = (IList<WmsRxReport>)serializer.Deserialize(file, typeof(List<WmsRxReport>));
            }
            return Task.FromResult(reports);
        }
    }
}
