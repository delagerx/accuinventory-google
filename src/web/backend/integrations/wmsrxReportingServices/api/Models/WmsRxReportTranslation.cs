﻿using System;
using System.Collections.Generic;
using System.Text;

namespace integrations.wmsrxReportingServices.api.Models
{
    public class WmsRxReportTranslation
    {
        public WmsRxReportTranslation()
        {

        }

        public WmsRxReportTranslation(string culture = default(string), string translation = default(string))
        {
            Culture = culture;
            Translation = translation;
        }

        public string Culture { get; private set; }

        public string Translation { get; private set; }
    }
}