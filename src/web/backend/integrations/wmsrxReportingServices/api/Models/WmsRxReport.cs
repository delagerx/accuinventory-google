﻿namespace integrations.wmsrxReportingServices.api.Models
{
    public class WmsRxReport
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string Path { get; set; }
    }
}