﻿using System;
using System.Collections.Generic;
using System.Text;

namespace integrations.wmsrxReportingServices.api.Models
{
    public class WmsRxReportClaim
    {
        public WmsRxReportClaim()
        {

        }

        public WmsRxReportClaim(string type = default(string), string value = default(string), string composedKey = default(string))
        {
            Type = type;
            Value = value;
            ComposedKey = composedKey;
        }

        public string Type { get; private set; }

        public string Value { get; private set; }

        public string ComposedKey { get; private set; }
    }
}