﻿namespace integrations
{
    public class IntegrationOptions
    {
        public string Wmsrx { get; set; }
        public string Reports { get; set; }
    }
}
