﻿namespace tasks.widgets.taskDetail
{
    public class TaskQueryBuilder
    {
	    public static string SelectTaskQuery =>
            @"
                SELECT om.id_mov as Id, 
                       ds.definicao as Status, 
                	   om.cod_ordem as Code,  
                	   doo.descricao as Operation, 
                	   de.descricao as Equipment,
                	   e.fantasia as Operator,
                	   des.descricao as Sector,
                	   om.prioridade as Priority,
                	   om.descricao as Description
                  FROM ordem_mov om INNER JOIN 
                       def_situacao ds ON om.cod_situacao = ds.cod_situacao INNER JOIN 
                	   def_operacao_ordem doo ON om.id_operacao = doo.id_operacao LEFT JOIN 
                	   def_equipamento de ON om.id_equipamento = de.id_equipamento LEFT JOIN 
                	   entidade e ON om.cod_operador = e.cod_entidade LEFT JOIN 
                	   def_endereco_setor des ON om.id_setor = des.id_def_endereco_setor
				 WHERE om.id_mov = @taskId
			";
    }
}
