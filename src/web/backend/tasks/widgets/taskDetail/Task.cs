﻿namespace tasks.widgets.taskDetail
{
    public class Task
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Operation { get; set; }
        public string Status { get; set; }
        public string Equipment { get; set; }
        public string Operator { get; set; }
        public string Sector { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }

    }

}