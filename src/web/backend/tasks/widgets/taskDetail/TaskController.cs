﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace tasks.widgets.taskDetail
{
    [Route("[controller]")]
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;

        public TaskController(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }
        [HttpGet("{id}/info")]
        public async Task<IActionResult> GetTaskInfo(string id)
        {
            return Ok(await _taskRepository.GetTaskById(id));
        }
    }
}
