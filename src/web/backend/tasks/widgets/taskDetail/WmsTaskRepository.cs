﻿using System.Linq;
using System.Threading.Tasks;
using Dapper;
using plugins.sdk.database;

namespace tasks.widgets.taskDetail
{
    public interface ITaskRepository
    {
        Task<Task> GetTaskById(string taskId);
    }

    public class WmsTask
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Status { get; set; }
        public string Operation { get; set; }
        public string Equipment { get; set; }
        public string Operator { get; set; }
        public string Sector { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
    }

    public class WmsTaskRepository : ITaskRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;

        public WmsTaskRepository(IWmsDatabaseConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<Task> GetTaskById(string taskId)
        {
            using (var connection = _connectionFactory.Make())
            {
                var task = (await connection.QueryAsync<WmsTask>(TaskQueryBuilder.SelectTaskQuery, new {taskId})).FirstOrDefault();
              
                if (task == null)
                {
                    return null;
                }
                return new Task {
                    Id = task.Id,
                    Status = task.Status,
                    Code = task.Code,
                    Operation = task.Operation,
                    Equipment = task.Equipment,
                    Operator = task.Operator,
                    Sector = task.Sector,
                    Priority = task.Priority,
                    Description = task.Description
                    };
            }
        }
    }
}
