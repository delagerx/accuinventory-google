﻿using plugins.sdk.search.elastic;

namespace tasks.search
{
    public class TaskDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var task = ElasticDocumentSerializer.DeserializeInfos<TaskDocument>(serializedAction);

            return new MappingResult($"{task.Code} - {task.Operation} - {task.Status}", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new { id = task.Id, code = task.Code, status = task.Status, operation = task.Operation });
        }
    }
}
