﻿using plugins.sdk.search.elastic.attributes;

namespace tasks.search
{
    public class TaskDocument
    {
        [ElasticIgnoreOnSearch]
        public string Id { get; set; }

        [ElasticBoostOnSearch(5)]
        public string Code { get; set; }

        [ElasticBoostOnSearch(4)]
        public string Status { get; set; }

        [ElasticBoostOnSearch(5)]
        public string Operation { get; set; }

        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }





    }
}
