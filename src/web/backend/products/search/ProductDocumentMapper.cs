using plugins.sdk.search.elastic;

namespace products.search
{
    public class ProductDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var product = ElasticDocumentSerializer.DeserializeInfos<ProductDocument>(serializedAction);

            return new MappingResult($"{product.Code} - {product.Name}", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new {id = product.Id, name = product.Name});
        }
    }
}