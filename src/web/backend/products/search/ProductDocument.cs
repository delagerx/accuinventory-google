﻿using plugins.sdk.search.elastic.attributes;

namespace products.search
{
    public class ProductDocument
    {
        [ElasticIgnoreOnSearch]
        public string Id { get; set; }

        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string Code { get; set; }

        [ElasticBoostOnSearch(4)]
        [ElasticPrefixedSearchField]
        public string Barcode { get; set; }

        [ElasticBoostOnSearch(4)]
        [ElasticPrefixedSearchField]
        public string PrefixedField { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

        [ElasticPrefixedSearchField]
        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }
    }
}