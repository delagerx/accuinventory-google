﻿using System.Threading.Tasks;

namespace products.widgets.productDetail
{
    public interface IProductRepository
    {
        Task<ProductInfoQueryResponse> GetByIdAsync(string id, int codigoOperacaoLogistica);
    }
}