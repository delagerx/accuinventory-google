﻿using System.Collections.Generic;

namespace products.widgets.productDetail
{
    public class ProductInfoQueryResponse
    {
        public Product Product { get; set; }

        public IEnumerable<ProductBarcode> ProductBarcodes { get; set; } = new List<ProductBarcode>();

        public IEnumerable<ProductStock> ProductStocks { get; set; } = new List<ProductStock>();

    }

    public class Product
    {
        public string Id { get; set; }

        public double AvailableStock {
            get
            {
                return TotalStock - StockHeld;
            }
        }

        public string Curve { get; set; }

        public string Digit { get; internal set; }

        public string Divergence { get; set; }

        public string GenericName { get; set; }

        public string Name { get; set; }

        public double StockHeld { get; set; }

        public double TotalStock { get; set; }
    }

    public class ProductBarcode
    {
        public int Id { get; set; }

        public string Barcode { get; set; }

        public bool Main { get; set; }

        public double Height { get; set; }

        public double Length { get; set; }

        public double Quantity { get; set; }

        public string Type { get; set; }

        public double Weight { get; set; }

        public double Width { get; set; }
    }

    public class ProductStock
    {
        public double Minimum { get; set; }

        public double Maximum { get; set; }

        public string Region { get; set; }

        public double Stock { get; set; }
    }
}