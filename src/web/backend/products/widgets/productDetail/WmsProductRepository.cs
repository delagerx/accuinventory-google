﻿using System.Linq;
using System.Threading.Tasks;
using Dapper;
using plugins.sdk.database;
using System.Collections.Generic;
using System;

namespace products.widgets.productDetail
{
    public class WmsDivergence
    {
        public string div { get; set; }
    }

    public class WmsProduct
    {
        public string Curva { get; set; }

        public string Digito { get; set; }

        public string Descricao { get; set; }

        public double EstoqueRetido { get; set; }

        public double EstoqueTotal { get; set; }

        public string NomeGenerico { get; set; }
    }

    public class WmsProductBarcode
    {
        public double Altura { get; set; }

        public string CodigoBarra { get; set; }

        public double Comprimento { get; set; }

        public double Largura { get; set; }

        public double Quantidade{ get; set; }

        public double Peso { get; set; }

        public bool Principal { get; set; }

        public string Tipo { get; set; }
    }

    public class WmsProductStock
    {
        public string Descricao { get; set; }

        public double Minimo { get; set; }

        public double Maximo { get; set; }

        public double Estoque { get; set; }
    }

    public class WmsProductRepository : IProductRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;

        public WmsProductRepository(IWmsDatabaseConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<ProductInfoQueryResponse> GetByIdAsync(string id, int codigoOperacaoLogistica)
        {
            try
            {
                using (var connection = _connectionFactory.Make())
                {
                    connection.Open();

                    WmsProduct selectProductQueryResult = (await connection.QueryAsync<WmsProduct>(WmsProductSqlQuery.SELECT_PRODUCT, new { id, codigoOperacaoLogistica })).FirstOrDefault();
                    WmsDivergence selectDivergenceQueryResult = (await connection.QueryAsync<WmsDivergence>(WmsProductSqlQuery.SELECT_DIVERGENCE, new { id, codigoOperacaoLogistica })).FirstOrDefault();
                    IEnumerable<WmsProductBarcode> selectProductBarcodesQueryResult = (await connection.QueryAsync<WmsProductBarcode>(WmsProductSqlQuery.SELECT_PRODUCT_BARCODES, new { id }));
                    IEnumerable<WmsProductStock> selectStockByRegionQueryResult = (await connection.QueryAsync<WmsProductStock>(WmsProductSqlQuery.SELECT_STOCK_BY_REGION, new { id, codigoOperacaoLogistica }));

                    if (selectProductQueryResult == null)
                        return null;

                    var queryResponse = new ProductInfoQueryResponse()
                    {
                        Product = new Product
                        {
                            Id = id,
                            Curve = selectProductQueryResult.Curva,
                            Digit = selectProductQueryResult.Digito,
                            Divergence = selectDivergenceQueryResult?.div,
                            GenericName = selectProductQueryResult.NomeGenerico,
                            Name = selectProductQueryResult.Descricao,
                            StockHeld = selectProductQueryResult.EstoqueRetido,
                            TotalStock = selectProductQueryResult.EstoqueTotal
                        },
                        ProductBarcodes = selectProductBarcodesQueryResult.Select(productBarcode =>
                            new ProductBarcode
                            {
                                Barcode = productBarcode.CodigoBarra,
                                Height = productBarcode.Altura,
                                Length = productBarcode.Comprimento,
                                Main = productBarcode.Principal,
                                Quantity = productBarcode.Quantidade,
                                Type = productBarcode.Tipo,
                                Weight = productBarcode.Peso,
                                Width = productBarcode.Largura
                            }),
                        ProductStocks = selectStockByRegionQueryResult.Select(productStock =>
                            new ProductStock
                            {
                                Maximum = productStock.Maximo,
                                Minimum = productStock.Minimo,
                                Region = productStock.Descricao,
                                Stock = productStock.Estoque
                            })
                    };

                    return queryResponse;
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }

    public class WmsProductSqlQuery
    {
        public static string SELECT_DIVERGENCE =
            "EXEC [dbo].[stp_wms_divergencia_inv] @codigoOperacaoLogistica, @id, NULL, NULL";

        public static string SELECT_PRODUCT =
            $"SELECT p.descricao as Descricao, p.digito AS Digito, p.generico as NomeGenerico, c.curva as Curva, pe.estoque AS EstoqueTotal, pe.retencao AS EstoqueRetido " +
            $"FROM produto p " +
            $"LEFT JOIN produto_estoque pe ON pe.cod_produto = p.cod_produto AND pe.cod_operacao_logistica = @codigoOperacaoLogistica " +
            $"LEFT JOIN produto_curva pc ON pc.cod_produto = p.cod_produto " +
            $"LEFT JOIN def_curva c ON c.cod_curva = pc.cod_curva " +
            $"WHERE p.cod_produto=@id ";

        public static string SELECT_STOCK_BY_REGION =
            $"SELECT dtae.descricao AS Descricao, PA.minimo AS Minimo, PA.MAXIMO AS Maximo, sum(ISNULL(lest.estoque, 0)) AS Estoque " +
            $"FROM produto p " +
            $"INNER JOIN produto_tipo_armazenagem_endereco pa ON p.cod_produto = pa.cod_produto " +
            $"INNER JOIN def_tipo_armazenamento_endereco dtae ON pa.id_tipo_armazenamento_endereco = dtae.id_tipo_armazenamento_endereco " +
            $"LEFT JOIN " + 
            $"(SELECT le.cod_produto, le.cod_operacao_logistica, e.id_tipo_Armazenamento, sum(ISNULL(le.estoque, 0)) AS Estoque " +
            $"FROM vw_lote_estoque le " +
            $"INNER JOIN endereco e ON le.id_endereco = e.id_endereco " +
            $"GROUP BY le.cod_produto, le.cod_operacao_logistica, e.id_tipo_Armazenamento) AS lest ON lest.cod_produto = p.cod_produto " +
            $"AND lest.id_tipo_Armazenamento = pa.id_tipo_armazenamento_endereco " +
            $"AND lest.cod_operacao_logistica = @codigoOperacaoLogistica " +
            $"WHERE p.cod_produto = @id " +
            $"GROUP BY dtae.descricaO, PA.minimo, PA.MAXIMO ";

        public static string SELECT_PRODUCT_BARCODES =
            $"SELECT cf.cod_barra AS CodigoBarra, cf.principal AS Principal, cf.quantidade AS Quantidade, tcf.descricao AS Tipo, cf.altura AS Altura, cf.largura AS Largura, cf.comprimento as Comprimento, cf.peso AS Comprimento " +
            $"FROM caixa_fechada cf " +
            $"LEFT JOIN def_tipo_caixa_fechada tcf ON tcf.cod_tipo = cf.cod_tipo " +
            $"WHERE cod_produto = @id ";
    }
}
