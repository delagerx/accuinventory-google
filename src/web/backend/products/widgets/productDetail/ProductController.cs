﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace products.widgets.productDetail
{
    [Route("[controller]")]
    public class ProductInfoController : Controller
    {
        private readonly IProductRepository _productRepository;

        public ProductInfoController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductInfo(string id)
        {
            // para testar estoque
            // id = "5754";

            // para testar gráfico
            // id = "19";

            int codigoOperacaoLogistica = 1;

            var product = await _productRepository.GetByIdAsync(id, codigoOperacaoLogistica);

            if (product == null)
                return NotFound();

            return Ok(product);
        }
    }
}
