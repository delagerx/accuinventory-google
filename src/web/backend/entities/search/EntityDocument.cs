﻿using System.Collections.Generic;
using plugins.sdk.search.elastic.attributes;

namespace entities.search
{
    public class EntityDocument
    {
        [ElasticIgnoreOnSearch]
        public string Id { get; set; }

        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string Code { get; set; }

        [ElasticBoostOnSearch(3)]
        public string Name { get; set; }
        
        [ElasticBoostOnSearch(3)]
        public string FantasyName { get; set; }

        [ElasticBoostOnSearch(2)]
        public string EntityStatus { get; set; }

        [ElasticBoostOnSearch(2)]
        public string EntityType { get; set; }

        [ElasticPrefixedSearchField]
        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }

        [ElasticIgnoreOnSearch]
        public string ProductName { get; set; }
    }
}
