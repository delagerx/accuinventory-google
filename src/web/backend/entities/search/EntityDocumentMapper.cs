﻿using plugins.sdk.search.elastic;

namespace entities.search
{
    public class EntityDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var entity = ElasticDocumentSerializer.DeserializeInfos<EntityDocument>(serializedAction);

            return new MappingResult($"{entity.Code} - {entity.Name} ({entity.EntityType})", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new { id = entity.Id, code = entity.Code, name = entity.Name });
        }
    }
}
