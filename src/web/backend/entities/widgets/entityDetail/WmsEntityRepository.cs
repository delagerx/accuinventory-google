﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Nest;
using plugins.sdk.database;

namespace entities.widgets.entityDetail
{
    public interface IEntityRepository
    {
        Task<EntityInfo> GetEntityById(string entityId);
    }

    public class WmsEntity
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Nome { get; set; }
        public string Fantasia { get; set; }
        public string Situacao { get; set; }
        public string Tipo { get; set; }
    }
    
    public class WmsEntityRepository : IEntityRepository
    {
        public const string Query = "SELECT cod_entidade as id, CONCAT(cod_entidade, digito) as code, razao_social as nome, fantasia, s.descricao as Situacao, t.descricao as Tipo FROM entidade e INNER JOIN def_tipo_entidade t on e.cod_tipo = t.cod_tipo INNER JOIN def_situacao_entidade s on e.cod_situacao = s.cod_situacao WHERE cod_entidade = @entityId";
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;

        public WmsEntityRepository(IWmsDatabaseConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<EntityInfo> GetEntityById(string entityId)
        {
            using (var connection = _connectionFactory.Make())
            {
                var selectEntityQueryResult = (await connection.QueryAsync<WmsEntity>(Query, new { entityId = entityId })).ToList();
                if (selectEntityQueryResult.Length() == 0)
                {
                    return null;
                }
                var entity = selectEntityQueryResult.First();

                return new EntityInfo(entity.Id, entity.Code, entity.Nome, entity.Fantasia, entity.Situacao, entity.Tipo);
            }
        }
    }
}
