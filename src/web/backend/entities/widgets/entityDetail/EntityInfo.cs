﻿using System;
using System.Collections.Generic;

namespace entities.widgets.entityDetail
{
    public class EntityInfo
    {
        public EntityInfo(string id, string code, string name, string fantasyName, string status, string type)
        {
            Id = id;
            Code = code;
            Name = name;
            FantasyName = fantasyName;
            Status = status;
            Type = type; 
        }

        
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FantasyName { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
    }
}