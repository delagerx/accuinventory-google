﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace entities.widgets.entityDetail
{
    [Route("[controller]")]
    public class EntityController : Controller
    {
        private readonly IEntityRepository _entityRepository;

        public EntityController(IEntityRepository entityRepository)
        {
            _entityRepository = entityRepository;
        }
        [HttpGet("{id}/info")]
        public async Task<IActionResult> GetEntityInfo(string id)
        {
            return Ok(await _entityRepository.GetEntityById(id));
        }
    }
}
