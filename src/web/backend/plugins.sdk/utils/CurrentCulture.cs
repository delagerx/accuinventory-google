﻿namespace plugins.sdk.utils
{
    public class Culture
    {
        public string Name { get; }

        public Culture(string name)
        {
            Name = name;
        }

        public static implicit operator string(Culture culture)
        {
            return culture.Name;
        }

        public static implicit operator Culture(string culture)
        {
            return new Culture(culture);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
