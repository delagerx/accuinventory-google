﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;
using LanguageExt;
using plugins.sdk.search.elastic.attributes;
using plugins.sdk.search.elastic.configuration;

namespace plugins.sdk.utils
{
    public static class ElasticFieldMapper
    {
        public static IEnumerable<SearchFieldCache> GetFieldForType(Type baseType)
        {
            var props = IterateProps(baseType, baseType.Name);
            return props.Fold(ImmutableList<SearchFieldCache>.Empty,
                (accumulator, item) =>
                    MapResult(item, accumulator));
        }

        private static ImmutableList<SearchFieldCache> MapResult(Option<SearchFieldCache> item, ImmutableList<SearchFieldCache> accumulator)
        {
            return item
                .Some(accumulator.Add)
                .None(() => accumulator);
        }

        private static IEnumerable<Option<SearchFieldCache>> IterateProps(Type parentType, string parentName)
        {
            var childProperties = parentType.GetProperties();
            foreach (var childProperty in childProperties)
            {
                var isIgnored = childProperty.GetCustomAttribute<ElasticIgnoreOnSearchAttribute>() != null;
                if (isIgnored)
                    yield return Option<SearchFieldCache>.None;
                else
                {
                    foreach (var p in MapChildProperty(parentName, childProperty)) yield return p;
                }
            }
        }

        private static IEnumerable<Option<SearchFieldCache>> MapChildProperty(string parentName, PropertyInfo childProperty)
        {
            var childPropertyName = childProperty.Name;
            if (IsComplexType(childProperty.PropertyType))
            {
                var genericsNestedType = GetGenericsNestedType(childProperty.PropertyType);
                var nestedParentName = MakeParentName(parentName, childPropertyName, childProperty.PropertyType);
                foreach (var prop in IterateProps(genericsNestedType, nestedParentName)
                )
                    yield return prop;
            }
            else
                yield return MakeField(parentName, childPropertyName, childProperty);
        }

        private static string MakeParentName(string parentName, string childName, Type childPropertyType)
        {
            if (typeof(IDictionary).IsAssignableFrom(childPropertyType))
                return $"{parentName}.{childName}.*";
            return $"{parentName}.{childName}";
        }

        private static SearchFieldCache MakeField(string parentName, string childPropertyName, MemberInfo memberInfo)
        {
            var fieldAttribute = memberInfo.GetCustomAttribute<ElasticFieldNameOnSearch>();
            var fieldName = fieldAttribute != null ? fieldAttribute.Name : childPropertyName.ToLowerInvariant();
            var boost = memberInfo.GetCustomAttribute<ElasticBoostOnSearchAttribute>();
            var propertyName = boost == null 
                ? $"{parentName.ToLowerInvariant()}.{fieldName}"
                : $"{parentName.ToLowerInvariant()}.{fieldName}^{boost.Value}";
            var trimmedPropertyName = RemoveBaseTypeName(propertyName);
            var isFuzzy = memberInfo.GetCustomAttribute<ElasticFuzzyAttribute>() != null;
            var isPrefixed = memberInfo.GetCustomAttribute<ElasticPrefixedSearchField>() != null;
            if (!isPrefixed)
                isFuzzy = true;
            return new SearchFieldCache(trimmedPropertyName, isFuzzy, isPrefixed);
        }

        private static string RemoveBaseTypeName(string property)
        {
            return property.Remove(0, property.IndexOf('.') + 1);
        }

        private static bool IsComplexType(Type type)
        {
            return !type.IsValueType && type != typeof(string);
        }

        private static Type GetGenericsNestedType(Type type)
        {
            if (typeof(IDictionary).IsAssignableFrom(type) && type.IsGenericType)
            {
                return type.GenericTypeArguments[1];
            }
            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                if (type.IsArray)
                    return type.GetElementType();
                else if(type.IsGenericType)
                    return type.GenericTypeArguments[0];
            }
            return type;
        }
    }
}
