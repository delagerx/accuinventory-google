﻿using backend.application.domain.user.entities;

namespace plugins.sdk.permissions
{
    public static class PermissionHelper
    {
        public static string MakeIndexeablePermission(string type, string value)
        {
            return $"{type}.{value}";
        }
    }
}
