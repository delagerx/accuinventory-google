﻿using System.Data.SqlClient;

namespace plugins.sdk.database
{
    public interface IWmsDatabaseConnectionFactory
    {
        SqlConnection Make();
    }

    public class WmsWmsDatabaseConnectionFactory : IWmsDatabaseConnectionFactory
    {
        private readonly string _connectionString;

        public WmsWmsDatabaseConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection Make()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
