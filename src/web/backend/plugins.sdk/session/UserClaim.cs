﻿namespace backend.application.domain.user.entities
{
    public class UserClaim
    {
        public UserClaim(string type, string value)
        {
            Type = type;
            Value = value;
        }

        public string Type { get; }
        public string Value { get; }
        public string Concatenated => $"{Type}.{Value}".ToLower();
        public bool Equals(string permission) => string.Equals(Concatenated, permission.ToLower());
    }
}