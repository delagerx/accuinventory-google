﻿using System.Collections.Generic;

namespace backend.application.domain.user.entities
{
    public class UserPermissions
    {
        public UserPermissions(IEnumerable<UserClaim> claims)
        {
            Claims = claims;
        }

        public IEnumerable<UserClaim> Claims { get; }

    }
}
