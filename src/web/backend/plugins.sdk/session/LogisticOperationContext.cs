﻿using LanguageExt;

namespace plugins.sdk.session
{
    public class LogisticOperationContext
    {
        public Option<string> Id { get; set; }
    }
}