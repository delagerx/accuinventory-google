﻿using backend.application.domain.user.entities;

namespace plugins.sdk.session
{
    public class UserContext
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public UserPermissions UserPermissions { get; set; }
        public string Id { get; set; }
        public LogisticOperation[] LogisticOperations { get; set; }
    }
}