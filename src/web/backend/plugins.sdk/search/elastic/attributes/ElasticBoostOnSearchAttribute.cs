﻿using System;

namespace plugins.sdk.search.elastic.attributes
{
    public class ElasticBoostOnSearchAttribute : Attribute
    {
        public double Value { get; }

        public ElasticBoostOnSearchAttribute(double value)
        {
            Value = value;
        }
    }
}
