﻿using System;

namespace plugins.sdk.search.elastic.attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ElasticIgnoreOnSearchAttribute : Attribute
    {
    }
}