﻿using System;

namespace plugins.sdk.search.elastic.attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ElasticFieldNameOnSearch : Attribute
    {
        public string Name { get; }

        public ElasticFieldNameOnSearch(string name)
        {
            Name = name;
        }
    }
}