namespace plugins.sdk.search.elastic
{
    public class MatchProbability
    {
        public const double NoMatchValue = 0.0;
        public const double EqualValue = 1.0;
        public double Value { get; }
        public bool Match => Value != NoMatchValue;

        public MatchProbability(double value)
        {
            Value = value;
        }

        public static MatchProbability NoMatch()
        {
            return new MatchProbability(NoMatchValue);
        }

        public static implicit operator MatchProbability((double, bool) value)
        {
            return new MatchProbability(value.Item1);
        }

        public static implicit operator MatchProbability(double value)
        {
            return new MatchProbability(value);
        }

        public static MatchProbability Equal()
        {
            return new MatchProbability(EqualValue);
        }
    }
}