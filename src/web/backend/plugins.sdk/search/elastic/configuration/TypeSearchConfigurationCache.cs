﻿using System;

namespace plugins.sdk.search.elastic.configuration
{
    public class TypeSearchConfigurationCache
    {
        public TypeSearchConfigurationCache(Type type, SearchFieldCache[] cachedSearchFields, bool isInternationalizable, bool isPermissionable, string elasticType)
        {
            Type = type;
            CachedSearchFields = cachedSearchFields;
            IsInternationalizable = isInternationalizable;
            IsPermissionable = isPermissionable;
            ElasticType = elasticType;
        }

        public Type Type { get; }
        public string ElasticType { get; }
        public SearchFieldCache[] CachedSearchFields { get; }
        public bool IsInternationalizable { get; }
        public bool IsPermissionable { get; }
    }
}