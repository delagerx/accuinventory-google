﻿using System.Collections.Generic;
using Nest;

namespace plugins.sdk.search.elastic.configuration
{
    public class SearchFieldCache
    {
        public SearchFieldCache(string fieldName, bool isFuzzy, bool isPrefixed)
        {
            FieldName = fieldName;
            IsFuzzy = isFuzzy;
            IsPrefixed = isPrefixed;
        }

        public string FieldName { get; }
        public bool IsFuzzy { get; }
        public bool IsPrefixed { get; }
    }
}