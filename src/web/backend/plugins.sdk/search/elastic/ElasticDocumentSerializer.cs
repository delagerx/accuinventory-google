using Newtonsoft.Json;

namespace plugins.sdk.search.elastic
{
    public static class ElasticDocumentSerializer
    {
        public static T DeserializeInfos<T>(string serializedInformations)
        {
            return JsonConvert.DeserializeObject<T>(serializedInformations);
        }
    }
}