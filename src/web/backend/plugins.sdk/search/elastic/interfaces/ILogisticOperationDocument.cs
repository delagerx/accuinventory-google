﻿namespace plugins.sdk.search.elastic
{
    public interface ILogisticOperationDocument
    {
        string LogisticOperation { get; }
    }
}