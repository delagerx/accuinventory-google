namespace plugins.sdk.search.elastic
{
    public interface IDocumentActionMapper
    {
        MappingResult Map(string serializedAction);
    }
}