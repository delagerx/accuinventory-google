using LanguageExt;

namespace plugins.sdk.search.elastic.interfaces
{
    public interface IPermissionableMappedDocument
    {
        Option<string[]> Permissions { get; }
    }
}