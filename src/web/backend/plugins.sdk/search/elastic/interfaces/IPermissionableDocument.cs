﻿using System.Collections.Generic;

namespace plugins.sdk.search.elastic
{
    public interface IPermissionableDocument
    {
        IEnumerable<string> Permissions { get; }
    }
}