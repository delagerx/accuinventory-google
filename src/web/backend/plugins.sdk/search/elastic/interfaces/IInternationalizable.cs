﻿using System.Collections.Generic;

namespace plugins.sdk.search.elastic
{
    public interface IInternationalizable<T>
    {
        Dictionary<string, T> Translations { get; }
    }
}
