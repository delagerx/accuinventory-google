using System.Collections.Generic;
using Nest;

namespace plugins.sdk.search.elastic.interfaces
{
    public interface IDocumentQueryFactory
    {
        QueryContainer MakeQuery(ElasticQueryContext queryContext);
    }
}