﻿
using System;

namespace plugins.sdk.search.elastic
{
    public class LogisticOperationDocument
    {
        public static string LogisticOperationField = nameof(ILogisticOperationDocument.LogisticOperation).ToLower();

        public static string AllLOs = "all";
    }
}
