﻿namespace plugins.sdk.search.elastic
{
    public class SearchResultSubType
    {
        public SearchResultSubType(string type)
        {
            Type = type;
        }

        public string Type { get; }

        public static implicit operator string(SearchResultSubType actionSubType)
        {
            return actionSubType.Type;
        }

        public static implicit operator SearchResultSubType(string actionSubType)
        {
            return new SearchResultSubType(actionSubType);
        }

        public static SearchResultSubType None => new SearchResultSubType("None");

        public override bool Equals(object obj)
        {
            return obj is SearchResultSubType type &&
                   Type == type.Type;
        }

        protected bool Equals(SearchResultSubType other)
        {
            return string.Equals(Type, other.Type);
        }

        public override int GetHashCode()
        {
            return (Type != null ? Type.GetHashCode() : 0);
        }
    }
}