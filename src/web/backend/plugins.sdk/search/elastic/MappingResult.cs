using System.Collections.Generic;
using LanguageExt;
using plugins.sdk.search.elastic.interfaces;

namespace plugins.sdk.search.elastic
{
    public class MappingResult : IPermissionableMappedDocument
    {
        public string Label { get; }
        public SearchResultSubType SubType { get; }
        public dynamic ContextProps { get; }
        public string LogisticOperation { get; }
        public Option<string[]> Permissions { get; }

        public MappingResult(string label, SearchResultSubType subType, string logisticOperation, dynamic contextProps = null, string[] permissions = null)
        {
            Label = label;
            SubType = subType;
            LogisticOperation = logisticOperation;
            Permissions = permissions;
            ContextProps = contextProps;
        }

        public void Deconstruct(out string label, out SearchResultSubType subType, out string logisticOperation, out dynamic customValue, out Option<string[]> permissions)
        {
            label = Label;
            subType = SubType;
            customValue = ContextProps;
            logisticOperation = LogisticOperation;
            permissions = Permissions;
        }
    }
}