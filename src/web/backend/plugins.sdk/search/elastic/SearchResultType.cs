﻿namespace plugins.sdk.search.elastic
{
    public enum SearchResultType
    {
        Action,
        Address,
        Product,
        Order,
        Task,
        Lot,
        Lpn,
        Entity
    }
}