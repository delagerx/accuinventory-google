﻿using plugins.sdk.search.elastic.configuration;
using plugins.sdk.utils;

namespace plugins.sdk.search.elastic
{
    public class ElasticQueryContext
    {
        public ElasticQueryContext(TypeSearchConfigurationCache typeSearchConfigurationCache, string query, Culture culture)
        {
            TypeSearchConfigurationCache = typeSearchConfigurationCache;
            Query = query;
            Culture = culture;
        }

        public string Query { get; }
        public TypeSearchConfigurationCache TypeSearchConfigurationCache { get; }
        public Culture Culture { get; }
    }
}