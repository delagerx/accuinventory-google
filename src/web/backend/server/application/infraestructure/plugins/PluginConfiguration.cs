﻿namespace backend.application.infraestructure.plugins
{
    public class PluginConfiguration
    {
        public string Assembly { get; set; }
        public string Bundle { get; set; }
        public string StartupClass { get; set; }
    }
}
