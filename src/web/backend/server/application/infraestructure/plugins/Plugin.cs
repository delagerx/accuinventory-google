﻿using System.Reflection;
using Microsoft.AspNetCore.Hosting;

namespace backend.application.infraestructure.plugins
{
    public class Plugin
    {
        public PluginConfiguration Configuration { get; set; }
        public Assembly Assembly { get; set; }
        public IStartup Startup { get; set; }
    }
}