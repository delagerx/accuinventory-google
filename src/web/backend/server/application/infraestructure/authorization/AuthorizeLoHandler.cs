﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using plugins.sdk.session;

namespace backend.application.infraestructure.authorization
{
    public class HasAccessToLogisticOperationRequirement : IAuthorizationRequirement { }
    public class AuthorizeLoHandler : AuthorizationHandler<HasAccessToLogisticOperationRequirement>
    {
        private readonly UserContext _userContext;

        public AuthorizeLoHandler(UserContext userContext)
        {
            _userContext = userContext;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasAccessToLogisticOperationRequirement requirement)
        {
            context.Succeed(requirement);
            var authorizationContext = context.Resource as AuthorizationFilterContext;
            var currentOl = authorizationContext?.HttpContext.GetRouteValue("lo") as string;
            if (_userContext.LogisticOperations.Any(x => x.Id.ToString() == currentOl))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }
            return Task.CompletedTask;
        }
    }
}
