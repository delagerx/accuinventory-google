﻿namespace backend.application.infraestructure.authorization
{
    public class Policies
    {
        public const string HasLOAccess = "HasAcessToLogisticOperation";
    }
}
