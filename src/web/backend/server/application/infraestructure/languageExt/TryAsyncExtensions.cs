﻿using System.Threading.Tasks;
using LanguageExt;

namespace backend.application.infraestructure.languageExt
{
    public static class TryAsyncExtensions
    {
        public static Task<Result<TE>> CascadeFail<T, TE>(this TryAsyncSuccContext<T, Result<TE>> succContext)
        {
            return succContext.Fail(exc => new Result<TE>(exc).AsTask());
        }

        public static Task<Task<Result<TE>>> CascadeFail<T, TE>(this TryAsyncSuccContext<T, Task<Result<TE>>> succContext)
        {
            return succContext.Fail(exc => new Result<TE>(exc).AsTask());
        }

        public static Task<OptionalResult<TE>> CascadeFail<T, TE>(this TryAsyncSuccContext<T, OptionalResult<TE>> succContext)
        {
            return succContext.Fail(exc => new OptionalResult<TE>(exc).AsTask());
        }

        public static Task<Task<OptionalResult<TE>>> CascadeFail<T, TE>(this TryAsyncSuccContext<T, Task<OptionalResult<TE>>> succContext)
        {
            return succContext.Fail(exc => new OptionalResult<TE>(exc).AsTask());
        }
    }
}
