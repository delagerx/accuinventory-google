﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using backend.application.domain.user.services;
using IdentityModel;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace backend.application.infraestructure.middlewares
{
    public class LoadUserContextFilter
    {
        public static async Task OnActionExecutionAsync(TokenValidatedContext context)
        {
            var userService = (IUserContextService)context.HttpContext.RequestServices.GetService(typeof(IUserContextService));
            var email = (context.Principal.Identity as ClaimsIdentity).Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name).Value;
            await userService.LoadUserContext(email);
        }
    }
}
