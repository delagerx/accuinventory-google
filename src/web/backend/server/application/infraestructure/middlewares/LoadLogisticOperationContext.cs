﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using plugins.sdk.session;

namespace backend.application.infraestructure.middlewares
{
    public class LoadLogisticOperationContext : IAsyncActionFilter
    {
        private readonly LogisticOperationContext _logisticOperationContext;

        public LoadLogisticOperationContext(LogisticOperationContext logisticOperationContext)
        {
            _logisticOperationContext = logisticOperationContext;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var logisticOperation = context.HttpContext.GetRouteValue("lo") as string;
            _logisticOperationContext.Id = !string.IsNullOrEmpty(logisticOperation)
                ? logisticOperation
                : null;
            await next();
        }
    }
}