namespace backend.application.infraestructure.elasticSearch
{
    public class ElasticSearchOptions
    {
        public string Uri { get; set; }
        public ElasticDocumentConfiguration[] Documents { get; set; }
        public bool TraceRequest { get; set; }
    }
}