﻿using System;
using System.IO;
using System.Threading;
using backend.application.domain.search.entities;
using LanguageExt;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;

namespace backend.application.infraestructure.elasticSearch
{
    public interface IElasticClientAdapter
    {
        TryAsync<ISearchResponse<T>> TrySearchAsync<T>(BoolQuery query, Func<SearchDescriptor<T>, SearchDescriptor<T>> searchDescriptor, CancellationToken cancellationToken = default) 
            where T : class;
    }

    public class ElasticClientAdapter : IElasticClientAdapter
    {
        private IElasticClient ElasticClient { get; }
        private readonly IOptions<ElasticSearchOptions> _elasticOptions;
        private readonly ILogger _logger;
        private readonly FilterLogisticOperationQueryVisitor _queryVisitor;

        public ElasticClientAdapter(IElasticClient elasticClient, IOptions<ElasticSearchOptions> elasticOptions, ILogger<ElasticClientAdapter> logger, FilterLogisticOperationQueryVisitor queryVisitor)
        {
            ElasticClient = elasticClient;
            _logger = logger;
            _queryVisitor = queryVisitor;
            _elasticOptions = elasticOptions;
        }

        public TryAsync<ISearchResponse<T>> TrySearchAsync<T>(BoolQuery query, Func<SearchDescriptor<T>, SearchDescriptor<T>> searchDescriptor, CancellationToken cancellationToken = default)
            where T : class
        {
            return async () =>
            {
                _queryVisitor.Visit(query);
                var result = await ElasticClient
                    .SearchAsync<T>(search => 
                        searchDescriptor(search)
                        .Query(q => query)
                        , cancellationToken);
                return HandleResult(result);
            };
        }

        private Result<ISearchResponse<T>> HandleResult<T>(ISearchResponse<T> result)
            where T : class
        {
            if (_elasticOptions.Value.TraceRequest)
            {
                var request = new StreamReader(new MemoryStream(result.ApiCall.RequestBodyInBytes)).ReadToEnd();
                _logger.LogTrace($"The following request took {result.Took}: \n {request}");
            }
            if (result.IsValid)
                return new Result<ISearchResponse<T>>(result);
            else
            {
                _logger.LogError(
                    $"Failed to execute search on elastic search due to the following errors: {result.DebugInformation}");
                return new Result<ISearchResponse<T>>(result.OriginalException);
            }
        }
    }
}
