using System.Collections.Generic;
using plugins.sdk.search.elastic;

namespace backend.application.infraestructure.elasticSearch
{
    public class ElasticDocumentConfiguration
    {
        public string Name { get; set; }
        public string Index { get; set; }
        public IEnumerable<ElasticDocumentTypeConfiguration> Types { get; set; }
    }

    public class ElasticDocumentTypeConfiguration
    {
        public string ElasticType { get; set; }
        public string ObjectType { get; set; }
        public SearchResultType SearchResultType { get; set; }
        public string Mapper { get; set; }
        public string QueryFactory { get; set; }
    }
}