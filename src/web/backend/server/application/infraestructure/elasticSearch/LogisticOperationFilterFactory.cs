﻿using Nest;
using plugins.sdk.search.elastic;
using plugins.sdk.session;

namespace backend.application.infraestructure.elasticSearch
{
    public interface ILogisticOperationFilterFactory
    {
        QueryContainer MakeFilter();
    }

    public class LogisticOperationFilterFactory : ILogisticOperationFilterFactory
    {
        private readonly LogisticOperationContext _loContext;

        public LogisticOperationFilterFactory(LogisticOperationContext loContext)
        {
            _loContext = loContext;
        }
        public QueryContainer MakeFilter()
        {
            var logisticOperationFilterValue =
                _loContext.Id
                    .Some(logisticOperationId => (QueryContainer)new TermsQuery()
                    {
                        Field = LogisticOperationDocument.LogisticOperationField,
                        Terms = new[] { logisticOperationId, LogisticOperationDocument.AllLOs }
                    })
                    .None(() => new TermQuery()
                    {
                        Field = LogisticOperationDocument.LogisticOperationField,
                        Value = LogisticOperationDocument.AllLOs,
                    });
            return logisticOperationFilterValue;
        }
    }
}