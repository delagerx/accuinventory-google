﻿using System;
using System.Linq;
using Nest;

namespace backend.application.infraestructure.elasticSearch
{
    public class FilterLogisticOperationQueryVisitor : QueryVisitor
    {
        private readonly ILogisticOperationFilterFactory _logisticOperationFilterFactory;

        public FilterLogisticOperationQueryVisitor(ILogisticOperationFilterFactory logisticOperationFilterFactory)
        {
            _logisticOperationFilterFactory = logisticOperationFilterFactory;
        }
 
        public override void Visit(IBoolQuery query)
        {
            var loFilter = _logisticOperationFilterFactory.MakeFilter();
            if (query.Filter != null)
            {
                var filters = query
                    .Filter
                    .ToList();
                filters.Add(loFilter);
                query.Filter = filters;
            }
            else
            {
                query.Filter = new[] {loFilter};
            }
        }
    }
}