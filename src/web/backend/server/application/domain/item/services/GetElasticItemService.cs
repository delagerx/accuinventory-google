﻿using System;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.vos;
using backend.application.infraestructure.elasticSearch;
using backend.application.infraestructure.languageExt;
using LanguageExt;
using Nest;

namespace backend.application.domain.item.services
{
    public interface IGetIndexedItemService
    {
        TryOptionAsync<SuggestionItem> TryGetItem(string type, string id);
    }

    public class GetElasticSearchIndexedItemService : IGetIndexedItemService
    {
        private readonly ConfiguredDocumentsList _configuredDocuments;
        private readonly IElasticClientAdapter _elasticClientAdapter;
        private readonly ISearchIndexedItensResultMapperService _mapperService;

        public GetElasticSearchIndexedItemService(ConfiguredDocumentsList configuredDocuments, IElasticClientAdapter elasticClientAdapter, ISearchIndexedItensResultMapperService mapperService)
        {
            _configuredDocuments = configuredDocuments;
            _elasticClientAdapter = elasticClientAdapter;
            _mapperService = mapperService;
        }

        public TryOptionAsync<SuggestionItem> TryGetItem(string type, string id)
        {
            var configuredDocument = _configuredDocuments.Configurations.Find(x => x.SearchResultType.ToString().ToLower() == type.ToLower());
            return () =>
            {
                return configuredDocument
                    .Some(configuration => ExecuteSearch(id, configuration))
                    .None(OptionalResult<SuggestionItem>.None.AsTask());
            };
        }

        private async Task<OptionalResult<SuggestionItem>> ExecuteSearch(string id, IndexedItemDocumentType configuration)
        {
            var queryContainer = MakeQueryContainer(id);
            var trySearch = _elasticClientAdapter.TrySearchAsync<object>(
                queryContainer,
                MakeSearchDescriptor(configuration)
            );
            return await MapTrySearchResult(trySearch);
        }

        private static Func<SearchDescriptor<object>, SearchDescriptor<object>> MakeSearchDescriptor(IndexedItemDocumentType configuration)
        {
            return search =>
                search.Index(configuration.IndexName)
                .Type(Types.All);
        }

        private static BoolQuery MakeQueryContainer(string id)
        {
            return new BoolQuery()
            {
                Filter = new [] { (QueryContainer)new IdsQuery()
                {
                    Values = new[] { new Id(id) }
                }}
            };
        }

        private async Task<OptionalResult<SuggestionItem>> MapTrySearchResult(TryAsync<ISearchResponse<object>> tryFetchResult)
        {
            return await await tryFetchResult.Succ(async searchResult =>
            {
                var mappedResult = await _mapperService.MapElasticResult(searchResult);
                return new OptionalResult<SuggestionItem>(mappedResult.FirstOrDefault());
            }).CascadeFail();
        }
    }
}
