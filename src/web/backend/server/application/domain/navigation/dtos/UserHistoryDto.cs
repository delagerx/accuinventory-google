using backend.application.domain.navigation.vos;

namespace backend.application.domain.navigation.dtos
{
    public class UserHistoryDto
    {
        public Link Link { get; set; }
        public int Hits { get; set; }
    }
}