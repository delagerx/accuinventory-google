using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.navigation.dtos;
using backend.application.domain.navigation.entities;
using backend.application.domain.navigation.vos;
using backend.application.persistence;
using Core.EntityFrameworkCore;
using Core.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace backend.application.domain.navigation.persistence
{
    public class UserHistoryOptions
    {
        public int HistoryFrequencyDaysToShow { get; set; }
    }

    public interface IUserHistoryRepository : ICrudRepository<long, UserHistory>
    {
        Task<IEnumerable<UserHistoryDto>> FrequentRegistries(string userId, int pageSize);
    }

    public class UserHistoryRepository : CrudRepository<long, UserHistory>, IUserHistoryRepository
    {
        public IOptions<UserHistoryOptions> Options { get; }

        public UserHistoryRepository(IOptions<UserHistoryOptions> options, Context context) : base(context)
        {
            Options = options;
        }

        public async Task<IEnumerable<UserHistoryDto>> FrequentRegistries(string userId, int pageSize)
        {
            var fromDate = DateTime.Now.Subtract(TimeSpan.FromDays(Options.Value.HistoryFrequencyDaysToShow));
            return await Items
                .Where(x => x.DateTime >= fromDate && x.UserId == userId)
                .GroupBy(x => new {x.Link.Url, x.Link.Name})
                .Select(x => new {Hits = x.Count(), Link = x.Key})
                .OrderByDescending(x => x.Hits)
                .Take(pageSize)
                .ToListAsync()
                .ContinueWith(task =>
                    task
                        .Result
                        .Select(r =>
                            new UserHistoryDto {Hits = r.Hits, Link = new Link {Name = r.Link.Name, Url = r.Link.Url}})
                );
        }
    }
}