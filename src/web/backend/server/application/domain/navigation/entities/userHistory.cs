using System;
using backend.application.domain.navigation.vos;
using Core.Persistence;

namespace backend.application.domain.navigation.entities
{
    public class UserHistory : IEntity<long>
    {
        public virtual Link Link { get; set; }
        public DateTime DateTime { get; set; } = DateTime.UtcNow;
        public string UserId { get; set; }

        public UserHistory(string userId, Link link)
        {
            UserId = userId;
            Link = link;
        }

        public UserHistory()
        {
        }

        public long Id { get; set; }
    }
}