namespace backend.application.domain.navigation.vos
{
    public class Link
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string IconKey { get; set; }
    }
}