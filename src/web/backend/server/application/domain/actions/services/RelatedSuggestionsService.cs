﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.search.vos;
using LanguageExt;
using Microsoft.Extensions.Logging;
using plugins.sdk.search.elastic;

namespace backend.application.domain.actions.services
{
    public class RelatedSuggestionsService : IRelatedSuggestionsService
    {
        private readonly IRelatedActionsClient _relatedActionsClient;
        private readonly IDocumentPermissionService _permissionService;

        public RelatedSuggestionsService(IRelatedActionsClient relatedActionsClient, IDocumentPermissionService permissionService)
        {
            _relatedActionsClient = relatedActionsClient;
            _permissionService = permissionService;
        }

        public async Task<IList<SuggestionItem>> GetAndBindRelatedSuggestions(IEnumerable<SuggestionItem> resultItems)
        {
            var groupedItensByType = resultItems
                .GroupBy(x => x.Type)
                .ToList();

            var bindRelatedActionsTasks =
                groupedItensByType
                    .Where(ItemIsNotAnAction)
                    .Select(GetRelatedSuggestionAndBindToSuggestion);

            var actionItems = groupedItensByType.FirstOrDefault(x => x.Key == SearchResultType.Action);

            return await Task.WhenAll(bindRelatedActionsTasks)
                .ContinueWith(task =>
                    actionItems != null
                        ? MergeItems(task, actionItems)
                        : ReturnItems(task));
        }

        private static List<SuggestionItem> ReturnItems(Task<IEnumerable<SuggestionItem>[]> task)
        {
            return task.Result.SelectMany(x => x).ToList();
        }

        private static List<SuggestionItem> MergeItems(Task<IEnumerable<SuggestionItem>[]> task, IGrouping<SearchResultType, SuggestionItem> actionResultItems)
        {
            return task.Result.SelectMany(x => x).Concat(actionResultItems).ToList();
        }

        private static bool ItemIsNotAnAction(IGrouping<SearchResultType, SuggestionItem> x)
        {
            return x.Key != SearchResultType.Action;
        }

        private async Task<IEnumerable<SuggestionItem>> GetRelatedSuggestionAndBindToSuggestion(IGrouping<SearchResultType, SuggestionItem> group)
        {
            var actions = await _relatedActionsClient.TryFetchActionForType(group.Key)();
            var bindedActions = HandleTryFetchResult(group.ToList(), actions);
            return bindedActions;
        }

        private IEnumerable<SuggestionItem> HandleTryFetchResult(List<SuggestionItem> userSearchResultItems, Result<IEnumerable<RelatedActionMappingResult>> actionFetchResult)
        {
            return actionFetchResult
                .Match(
                    Succ: actions => BindRelatedSuggestionsToSuggestion(userSearchResultItems, actions),
                    Fail: error => userSearchResultItems
                );
        }

        private IEnumerable<SuggestionItem> BindRelatedSuggestionsToSuggestion(List<SuggestionItem> userSearchResultItems, IEnumerable<RelatedActionMappingResult> relatedActions)
        {
            return userSearchResultItems.Select(suggestion =>
            {
                
                var suggestionRelatedActions = relatedActions.Select(relatedAction =>
                    {
                        suggestion.ContextProps["url"] = relatedAction.Url;
                        var hasPermission = _permissionService.HasPermissions(relatedAction);
                        return new RelatedSuggestion(relatedAction.Label, suggestion.ContextProps, SearchResultType.Action, relatedAction.ActionType.ToString(), hasPermission);
                    }
                );
                return new SuggestionItem(
                    suggestion.Label,
                    suggestion.Type,
                    suggestion.SubType,
                    suggestion.Probability,
                    suggestion.HasPermission,
                    suggestion.ContextProps,
                    suggestionRelatedActions
                );
            });
        }
    }
}