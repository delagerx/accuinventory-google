﻿using System.Collections.Generic;
using System.Threading.Tasks;
using backend.application.domain.search.vos;
using Elasticsearch.Net;

namespace backend.application.domain.actions.services
{
    public interface IRelatedSuggestionsService
    {
        Task<IList<SuggestionItem>> GetAndBindRelatedSuggestions(IEnumerable<SuggestionItem> resultItems);
    }
}
