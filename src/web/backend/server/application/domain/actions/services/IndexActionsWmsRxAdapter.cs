﻿using System.Threading.Tasks;
using backend.application.domain.actions.entities;
using integrations.wmsrx.api;
using LanguageExt;

namespace backend.application.domain.actions.services
{
    public interface IIndexActionSourceAdapater
    {
        TryAsync<IndexeableAction[]> GetIndexeableActions();
    }

    public class IndexActionsWmsRxAdapter : IIndexActionSourceAdapater
    {
        private readonly IIndexedActions _indexedActionsApi;
        private readonly IWmsRxActionTranslator _translator;

        public IndexActionsWmsRxAdapter(IIndexedActions indexedActionsApi, IWmsRxActionTranslator translator)
        {
            _indexedActionsApi = indexedActionsApi;
            _translator = translator;
        }
        public TryAsync<IndexeableAction[]> GetIndexeableActions()
        {
            return async () =>
            {
                var result = await _indexedActionsApi.GetWithHttpMessagesAsync();
                return _translator.MapIndexeableActionFromWmsRx(result.Body);
            };
        }
    }
}
