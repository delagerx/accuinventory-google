﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.actions.entities;
using Elasticsearch.Net;
using Microsoft.Extensions.Logging;
using Nest;
using backend.application.domain.reports.index;

namespace backend.application.domain.actions.services
{
    public interface IIndexActionsService
    {
        Task IndexAllActions();
    }
    public class IndexActionsService : IIndexActionsService
    {
        private readonly IIndexActionSourceAdapater _actionsIndexSourceAdapter;
        private readonly IIndexReportSourceAdapter _indexReportSourceAdapter;
        private readonly IElasticClient _elasticClient;
        private readonly ILogger<IIndexActionsService> _logger;

        public IndexActionsService(
            IIndexActionSourceAdapater actionsIndexSourceAdapter,
            IIndexReportSourceAdapter indexReportSourceAdapter,
            IElasticClient elasticClient, 
            ILogger<IIndexActionsService> logger)
        {
            _actionsIndexSourceAdapter = actionsIndexSourceAdapter;
            _indexReportSourceAdapter = indexReportSourceAdapter;
            _elasticClient = elasticClient;
            _logger = logger;
        }
        public async Task IndexAllActions()
        {
            _logger.LogInformation("Starting wmsrx actions reindex");

            IEnumerable<IndexeableAction> actionsToBeIndexed = new List<IndexeableAction>();

            var tryFetchActions = _actionsIndexSourceAdapter.GetIndexeableActions();

            await tryFetchActions
                .Succ(actions => { actionsToBeIndexed = actionsToBeIndexed.Append(actions); })
                .Fail(err => throw new Exception("Failed to fetch actions from wms-cadastro", err));

            var tryFetchReports = _indexReportSourceAdapter.GetIndexeableReports();

            await tryFetchReports
                .Succ(actions => { actionsToBeIndexed = actionsToBeIndexed.Append(actions); })
                .Fail(err => throw new Exception("Failed to fetch reports from wms-cadastro", err));

            await IndexActions(actionsToBeIndexed.ToArray());
        }

        private async Task IndexActions(IndexeableAction[] items)
        {
            var indexName = "accuinventory-search-actions";
            await RecreateActionsIndex(indexName);
            PostData<object> postData = CreateActionsPostData(indexName, items);
            var bulkResut = await _elasticClient.LowLevel.BulkPutAsync<Stream>(postData);
            if (!bulkResut.Success)
                throw new Exception("Index bulk insert failed");
            _logger.LogInformation("Reindexed all wmsrx actions successfully");
        }

        private async Task RecreateActionsIndex(string indexName)
        {
            await _elasticClient.DeleteIndexAsync(indexName);
            await _elasticClient.CreateIndexAsync(indexName);
        }

        private static PostData<object> CreateActionsPostData(string indexName, IndexeableAction[] results)
        {
            List<object> mappedResults = InsertIndexRequestBeforeEachItem(indexName, results);
            return new PostData<object>(mappedResults);
        }

        private static List<object> InsertIndexRequestBeforeEachItem(string indexName, IndexeableAction[] results)
        {
            var mappedResults = results.Aggregate(new List<object>(), (list, action) =>
            {
                //Cada linha de inserção precisa que seja dito em qual indíce está sendo escrito,
                //por isso é inserido uma linha index {}. O nome do indice é determinado pelo indice
                //da url do post
                var type = action.GetType() == typeof(IndexeableAction) ? "actions" : "entityActions";
                list.Add(new {index = new { _index = indexName, _type = type}});
                list.Add(action);
                return list;
            });
            return mappedResults;
        }
    }
}
