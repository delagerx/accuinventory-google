﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.actions.entities;
using backend.application.domain.actions.search;
using backend.application.domain.search.services.elastic.documents.shared;
using backend.application.domain.search.vos;
using backend.application.infraestructure.elasticSearch;
using backend.application.infraestructure.languageExt;
using LanguageExt;
using Nest;
using plugins.sdk.search.elastic;

namespace backend.application.domain.actions.services
{
    public interface IRelatedActionsClient
    {
        TryAsync<IEnumerable<RelatedActionMappingResult>> TryFetchActionForType(SearchResultType type);
    }

    public class RelatedActionsClient : IRelatedActionsClient
    {
        private readonly IElasticClientAdapter _elasticClient;
        private readonly EntityActionMapper _mapper;

        public RelatedActionsClient(IElasticClientAdapter elasticClient, EntityActionMapper mapper)
        {
            _elasticClient = elasticClient;
            _mapper = mapper;
        }

        public TryAsync<IEnumerable<RelatedActionMappingResult>> TryFetchActionForType(
            SearchResultType type)
        {
            var filter = MakeQuery(type);
            var descriptor = MakeSearchDescriptor();
            var trySearchResult = _elasticClient.TrySearchAsync(filter, descriptor);
            return () => HandlTrySearchResult(trySearchResult);
        }

        private static Func<SearchDescriptor<object>, SearchDescriptor<object>> MakeSearchDescriptor()
        {
            return s =>
                s.Index("accuinventory-search-actions")
                    .Type(Types.AllTypes)
                    .RequestCache()
                    .From(0)
                    .Size(30);
        }

        private Task<Result<IEnumerable<RelatedActionMappingResult>>> HandlTrySearchResult(TryAsync<ISearchResponse<object>> trySearchResult)
        {
            return trySearchResult
                .Succ(MapDocuments)
                .CascadeFail();
        }

        private Result<IEnumerable<RelatedActionMappingResult>> MapDocuments(ISearchResponse<object> searchResult)
        {
            var mappedResults = searchResult
                .Hits
                .Select(x => _mapper.Map(x.Source.ToString()));
            return new Result<IEnumerable<RelatedActionMappingResult>>(mappedResults);
        }

        private BoolQuery MakeQuery(SearchResultType type)
        {
            var entityTypeField = nameof(EntityAction.EntityType).ToLower();
            var shouldShowField = nameof(EntityAction.ShouldShowOnResultItem).ToLower();          
            return new BoolQuery()
            {
                Filter = new[]
                {
                    (QueryContainer) new TermQuery()
                    {
                        Field = entityTypeField,
                        Value = type.ToString().ToLower()
                    },
                    new TermQuery()
                    {
                        Field = shouldShowField,
                        Value = true
                    }
                }
            };
        }
    }
}