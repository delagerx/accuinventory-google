﻿using System;
using System.Collections.Generic;
using System.Linq;
using backend.application.domain.actions.entities;
using integrations.wmsrx.api.Models;
using LanguageExt;
using plugins.sdk.permissions;
using ActionTranslation = backend.application.domain.actions.entities.ActionTranslation;

namespace backend.application.domain.actions.services
{
    public interface IWmsRxActionTranslator
    {
        IndexeableAction[] MapIndexeableActionFromWmsRx(IList<ActionResponseItem> indexedActions);
    }

    public class WmsRxActionTranslator : IWmsRxActionTranslator
    {
        public IndexeableAction[] MapIndexeableActionFromWmsRx(IList<ActionResponseItem> indexedActions)
        {
            return indexedActions.Select(MapResponseItem).ToArray();
        }

        private IndexeableAction MapResponseItem(ActionResponseItem responseItem)
        {
            var key = responseItem.Key;
            var translations = MapTranslations(responseItem.Translations);
            var permissions = MapPermissions(responseItem.Permissions);
            var url = responseItem.Url;
            var resourceTypeOption = MapActionResourceType(responseItem);
            return resourceTypeOption
                .Some(resourceType => MapToEntityAction(key, url, translations, permissions, resourceType))
                .None(() => MapToIndexeableAction(key, url, translations, permissions));
        }

        private static IndexeableAction MapToIndexeableAction(string key, string url, Dictionary<string, ActionTranslation> translations, ActionPermission[] permissions)
        {
            return new IndexeableAction(
                key, 
                url, 
                translations, 
                ActionType.WmsRxLink, 
                permissions.Select(x => PermissionHelper.MakeIndexeablePermission(x.Type, x.Value))
            );
        }

        private static IndexeableAction MapToEntityAction(string key, string url, Dictionary<string, ActionTranslation> translations, ActionPermission[] permissions, ActionResourceType resourceType)
        {
            var noPriority = 0;
            return new EntityAction(
                key, 
                url, 
                translations, 
                permissions.Select(x => PermissionHelper.MakeIndexeablePermission(x.Type, x.Value)), 
                ActionType.WmsRxLink, 
                resourceType.ToString(), 
                noPriority, 
                true
            );
        }

        private static Option<ActionResourceType> MapActionResourceType(ActionResponseItem responseItem)
        {
            switch (responseItem.ResourceType.ToLower())
            {
                case "product":
                    return ActionResourceType.Product;
                case "address":
                    return ActionResourceType.Address;
                case "lpn":
                    return ActionResourceType.Lpn;
                case "lot":
                    return ActionResourceType.Lot;
                default: return Option<ActionResourceType>.None;
            }
        }

        private Dictionary<string, ActionTranslation> MapTranslations(IEnumerable<integrations.wmsrx.api.Models.ActionTranslation> translations)
        {
            return translations.ToDictionary(x => x.Culture, x => new ActionTranslation(x.Translation));
        }

        private ActionPermission[] MapPermissions(IList<ActionClaim> claims)
        {
            return claims.Select(x => new ActionPermission(x.Type, x.Value)).ToArray();
        }
    }
}