﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.interfaces;
using plugins.sdk.session;

namespace backend.application.domain.actions.services
{
    public interface IDocumentPermissionService
    {
        bool HasPermissions(IPermissionableMappedDocument mappingResult);
    }

    public class DocumentPermissionService : IDocumentPermissionService
    {
        private readonly UserContext _userContext;

        public DocumentPermissionService(UserContext userContext)
        {
            _userContext = userContext;
        }

        public bool HasPermissions(IPermissionableMappedDocument mappingResult)
        {
            return mappingResult
                .Permissions
                .Some(CheckForPermissions)
                .None(() => true);
        }

        private bool CheckForPermissions(string[] permissions)
        {
            return permissions.All(documentPermission =>
                _userContext
                    .UserPermissions
                    .Claims
                    .Any(userPermission => 
                        userPermission.Equals(documentPermission)
                    )
                );
        }
    }
}
