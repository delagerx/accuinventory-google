﻿using System.Threading.Tasks;
using backend.application.domain.actions.services;

namespace backend.application.domain.actions.jobs
{
    public class IndexActionsJob
    {
        private readonly IIndexActionsService _indexActionService;

        public IndexActionsJob(IIndexActionsService indexActionService)
        {
            _indexActionService = indexActionService;
        }

        public async Task IndexActions()
        {
            await _indexActionService.IndexAllActions();
        }
    }
}
