﻿using System.Collections.Generic;
using System.Linq;
using backend.application.domain.actions.entities;
using backend.application.domain.search.vos;
using LanguageExt;
using plugins.sdk.search.elastic;
using plugins.sdk.utils;

namespace backend.application.domain.actions.search
{
    public interface IEntityActionMapper
    {
        RelatedActionMappingResult Map(string serializedAction);
    }

    public class EntityActionMapper : IEntityActionMapper
    {
        private readonly Culture _culture;

        public EntityActionMapper(Culture culture)
        {
            _culture = culture;
        }

        public RelatedActionMappingResult Map(string serializedAction)
        {
            var action = ElasticDocumentSerializer.DeserializeInfos<EntityAction>(serializedAction);

            var label = ((IDictionary<string, ActionTranslation>)action
                    .Translations)
                .TryGetValue(_culture.Name)
                .Some(x => x.Label)
                .None(() => action.Key);

            return new RelatedActionMappingResult(
                label, 
                action.Url, 
                action.Type,
                action.Permissions?.ToArray()
            );
        }
    }
}
