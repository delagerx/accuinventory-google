﻿using System.Collections.Generic;
using System.Linq;
using backend.application.domain.actions.entities;
using LanguageExt;
using plugins.sdk.search.elastic;
using plugins.sdk.utils;

namespace backend.application.domain.actions.search
{
    public class ActionDocumentMapper : IDocumentActionMapper
    {
        private readonly Culture _culture;

        public ActionDocumentMapper(Culture culture)
        {
            _culture = culture;
        }
        public MappingResult Map(string serializedAction)
        {
            var action = ElasticDocumentSerializer.DeserializeInfos<IndexeableAction>(serializedAction);

            var label = ((IDictionary<string, ActionTranslation>)action
                .Translations)
                .TryGetValue(_culture.Name)
                    .Some(x => x.Label)
                    .None(() => action.Key);
            return new MappingResult(label, action.Type.ToString(), LogisticOperationDocument.AllLOs, new { url = action.Url }, action.Permissions?.ToArray());
        }
    }
}