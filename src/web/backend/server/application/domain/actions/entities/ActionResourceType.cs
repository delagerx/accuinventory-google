﻿namespace backend.application.domain.actions.entities
{
    public enum ActionResourceType
    {
        Address,
        Product,
        Lot,
        Lpn,
        Order,
    }
}