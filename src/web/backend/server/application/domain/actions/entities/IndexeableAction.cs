﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.attributes;

namespace backend.application.domain.actions.entities
{
    public class IndexeableAction : IInternationalizable<ActionTranslation>, IPermissionableDocument,
        ILogisticOperationDocument
    {
        public IndexeableAction(string key, string url, Dictionary<string, ActionTranslation> translations,
            ActionType type, IEnumerable<string> permissions)
        {
            Key = key;
            Url = url;
            Translations = translations;
            Type = type;
            Permissions = permissions;
        }

        public string Key { get; }
        public string Url { get; }
        public ActionType Type { get; }        
        public Dictionary<string, ActionTranslation> Translations { get; }

        [ElasticIgnoreOnSearch]
        public IEnumerable<string> Permissions { get; }
        [ElasticIgnoreOnSearch]
        public string LogisticOperation { get; } = LogisticOperationDocument.AllLOs;
    }
}