﻿namespace backend.application.domain.actions.entities
{
    public enum ActionType
    {
        WmsRxLink,
        ExternalLink,
        InternalLink,
        EntityAction
    }
}