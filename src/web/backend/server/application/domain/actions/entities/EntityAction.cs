﻿using System.Collections.Generic;

namespace backend.application.domain.actions.entities
{
    public class EntityAction : IndexeableAction
    {
        public string EntityType { get; set; }
        public bool ShouldShowOnResultItem { get; set; }
        public int Priority { get; set; }

        public EntityAction(
            string key, 
            string url, 
            Dictionary<string, ActionTranslation> translations,
            IEnumerable<string> permissions,
            ActionType type,
            string entityType, 
            int priority, 
            bool shouldShowOnResultItem) : base(key, url, translations, type, permissions)
        {
            EntityType = entityType;
            Priority = priority;
            ShouldShowOnResultItem = shouldShowOnResultItem;
        }
    }
}
