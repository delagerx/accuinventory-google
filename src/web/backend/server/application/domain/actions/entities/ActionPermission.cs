﻿namespace backend.application.domain.actions.entities
{
    public class ActionPermission
    {
        public ActionPermission(string type, string value)
        {
            Type = type;
            Value = value;
        }

        public string Type { get; }
        public string Value { get; }
    }
}