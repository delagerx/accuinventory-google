﻿namespace backend.application.domain.actions.entities
{
    public class ActionTranslation
    {
        public ActionTranslation(string label)
        {
            Label = label;
        }

        public string Label { get; }
    }
}