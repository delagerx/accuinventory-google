using System.ComponentModel.DataAnnotations.Schema;
using Core.Persistence;
using Newtonsoft.Json;

namespace backend.application.domain.widgets
{
    public class UserFavoriteWidget : IEntity<string>
    {
        public string SerializedProps { get; set; } = "";

        [NotMapped]
        public dynamic Props
        {
            get => JsonConvert.DeserializeObject(SerializedProps);
            set => SerializedProps = JsonConvert.SerializeObject(value);
        }

        public string Type { get; set; }
        public string UserId { get; set; }
        public string Id { get; set; }
    }
}