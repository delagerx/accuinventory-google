using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.persistence;
using Core.EntityFrameworkCore;
using Core.Persistence;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace backend.application.domain.widgets
{
    public interface IUserFavoriteRepository : ICrudRepository<string, UserFavoriteWidget>
    {
        Task<IEnumerable<UserFavoriteWidget>> List(int count, string userId);
        Task AddOrUpdate(UserFavoriteWidget userFavoriteWidget);
    }

    public class UserFavoritesRepository : CrudRepository<string, UserFavoriteWidget>, IUserFavoriteRepository
    {
        public UserFavoritesRepository(Context context) : base(context)
        {
        }

        public async Task<IEnumerable<UserFavoriteWidget>> List(int count, string userId)
        {
            return await Items
                .Where(x => x.UserId == userId)
                .Take(count)
                .ToListAsync();
        }

        public async Task AddOrUpdate(UserFavoriteWidget userFavoriteWidget)
        {
            var existingUserFavorite = GetByPrimaryKey(userFavoriteWidget.Id);
            if (existingUserFavorite != null)
                await UpdateAsync(userFavoriteWidget);
            else
                await AddAsync(userFavoriteWidget);
        }
    }
}