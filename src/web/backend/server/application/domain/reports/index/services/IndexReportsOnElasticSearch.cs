﻿using Elasticsearch.Net;
using LanguageExt;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace backend.application.domain.reports.index
{
    public interface IIndexReportOnElasticSearch
    {
        Task Execute();
    }

    public class IndexReportsOnElasticSearch : IIndexReportOnElasticSearch
    {
        private readonly IFetchIndexableReportsFromWmsRx _fetchIndexableReportsFromWmsRx;
        private readonly IElasticClient _elasticClient;
        private readonly ILogger<IIndexReportOnElasticSearch> _logger;

        public IndexReportsOnElasticSearch(IFetchIndexableReportsFromWmsRx fetchIndexableReportsFromWmsRx, IElasticClient elasticClient, ILogger<IIndexReportOnElasticSearch> logger)
        {
            _fetchIndexableReportsFromWmsRx = fetchIndexableReportsFromWmsRx;
            _elasticClient = elasticClient;
            _logger = logger;
        }

        public async Task Execute()
        {
            _logger.LogInformation("Starting wmsrx reports reindex");

            var reports = await _fetchIndexableReportsFromWmsRx.FetchReports();
            await IndexReports(reports);            
        }

        private async Task IndexReports(IndexableReport[] items)
        {
            var indexName = "accuinventory-search-reports";

            await RecreateReportsIndex(indexName);

            PostData<object> postData = CreateReportsPostData(indexName, items);

            var bulkResut = await _elasticClient.LowLevel.BulkPutAsync<Stream>(postData);

            if (!bulkResut.Success)
                throw new Exception("Index bulk insert failed");

            _logger.LogInformation("Reindexed all wmsrx reports successfully");
        }

        private async Task RecreateReportsIndex(string indexName)
        {
            await _elasticClient.DeleteIndexAsync(indexName);

            await _elasticClient.CreateIndexAsync(indexName);
        }

        private static PostData<object> CreateReportsPostData(string indexName, IndexableReport[] results)
        {
            List<object> mappedResults = InsertIndexRequestBeforeEachItem(indexName, results);

            return new PostData<object>(mappedResults);
        }

        private static List<object> InsertIndexRequestBeforeEachItem(string indexName, IndexableReport[] results)
        {
            var mappedResults = results.Aggregate(new List<object>(), (list, report) =>
            {
                //Cada linha de inserção precisa que seja dito em qual indíce está sendo escrito,
                //por isso é inserido uma linha index {}. O nome do indice é determinado pelo indice
                //da url do post
                list.Add(new { index = new { _index = indexName, _type = "reports" } });

                list.Add(report);

                return list;
            });

            return mappedResults;
        }
    }
}
