﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.application.domain.reports.index.entities
{
    public class ReportTranslation
    {
        public ReportTranslation(string label)
        {
            Label = label;
        }

        public string Label { get; }
    }
}
