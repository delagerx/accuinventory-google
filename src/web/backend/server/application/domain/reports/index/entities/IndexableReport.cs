﻿using backend.application.domain.reports.index.entities;
using plugins.sdk.search.elastic.attributes;
using System.Collections.Generic;

namespace backend.application.domain.reports.index
{
    public class IndexableReport
    {
        public IndexableReport(string name, string path)
        {
            Name = name;
            Path = path;
        }

        public string Name { get; set; }

        public string Path { get; set; }

        public ReportParameterizationType ParameterizationType { get; set; }

        public Dictionary<string, ReportTranslation> Translations { get; }

        [ElasticIgnoreOnSearch]
        public IEnumerable<string> Permissions { get; }
    }
}
