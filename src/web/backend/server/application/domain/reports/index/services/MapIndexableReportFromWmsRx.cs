﻿using System.Collections.Generic;
using System.Linq;
using LanguageExt;
using backend.application.domain.reports.index;
using backend.application.domain.reports.index.entities;
using integrations.wmsrxReportingServices.api.Models;

namespace backend.application.domain.reports.services
{
    public interface IMapIndexableReportFromWmsRx
    {
        IndexableReport[] Execute(IList<WmsRxReport> indexableReports);
    }

    public class MapIndexableReportFromWmsRx : IMapIndexableReportFromWmsRx
    {
        public IndexableReport[] Execute(IList<WmsRxReport> wmsRxReport)
        {
            return wmsRxReport.Select(MapWmsRxReportIntoIndexableStructure).ToArray();
        }

        private IndexableReport MapWmsRxReportIntoIndexableStructure(WmsRxReport wmsRxReport)
        {
            string reportName = wmsRxReport.Name;
            string reportPath = wmsRxReport.Path;
            ReportParameterizationType reportParametrizationType = MapParameterizationTypeFromReportName();

            IndexableReport indexableReport = MapToIndexableReport(reportName, reportPath);

            return indexableReport;
        }

        private static IndexableReport MapToIndexableReport(string reportName, string reportPath)
        {
            return new IndexableReport(reportName, reportPath);
        }

        private ReportParameterizationType MapParameterizationTypeFromReportName()
        {
            return ReportParameterizationType.Parameterless;
        }
    }
}