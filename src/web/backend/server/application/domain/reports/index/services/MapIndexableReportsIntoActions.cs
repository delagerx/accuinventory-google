﻿using backend.application.domain.actions.entities;
using backend.application.domain.reports.services;
using integrations.wmsrxReportingServices.api;
using integrations.wmsrxReportingServices.api.Models;
using LanguageExt;
using System.Collections.Generic;
using System.Linq;
using integrations;
using Microsoft.Extensions.Options;

namespace backend.application.domain.reports.index
{
    public interface IMapIndexableReportsIntoActions
    {
        IndexeableAction[] Map(IndexableReport[] reportsToBeMapped);
    }

    public class MapIndexableReportsIntoActions : IMapIndexableReportsIntoActions
    {
        private readonly IntegrationOptions _integrationOptions;

        public MapIndexableReportsIntoActions(IOptions<IntegrationOptions> integrationOptions)
        {
            _integrationOptions = integrationOptions.Value;
        }

        public IndexeableAction[] Map(IndexableReport[] reportsToBeMapped)
        {
            IList<IndexeableAction> mappedIndexableAction = new List<IndexeableAction>();

            foreach (IndexableReport indexableReport in reportsToBeMapped)
            {
                Dictionary<string, ActionTranslation> mappedTranslations = new Dictionary<string, ActionTranslation>();

                var reportUrl = _integrationOptions.Reports.Replace(":report", indexableReport.Path);

                var resourceType = GetEntityType(indexableReport.Name);
                resourceType.IfSome(type =>
                {
                    var relatedAction = CreateRelatedAction(indexableReport, reportUrl, mappedTranslations, type);
                    mappedIndexableAction.Add(relatedAction);
                });

                var action = CreateAction(indexableReport, reportUrl, mappedTranslations);
                mappedIndexableAction.Add(action);
            }

            return mappedIndexableAction.ToArray();
        }

        private static IndexeableAction CreateAction(IndexableReport indexableReport, string reportUrl,
            Dictionary<string, ActionTranslation> mappedTranslations)
        {
            var indexableAction = new IndexeableAction(
                indexableReport.Name,
                reportUrl,
                mappedTranslations,
                ActionType.ExternalLink,
                indexableReport.Permissions);
            return indexableAction;
        }

        private static IndexeableAction CreateRelatedAction(IndexableReport indexableReport, string reportUrl,
            Dictionary<string, ActionTranslation> mappedTranslations, ActionResourceType type)
        {
            bool shouldShowOnResultItem = true;
            int priority = 0;
            var indexableAction = new EntityAction(
                indexableReport.Name,
                reportUrl,
                mappedTranslations,
                indexableReport.Permissions,
                ActionType.ExternalLink,
                type.ToString(),
                priority, 
                shouldShowOnResultItem);
            return indexableAction;
        }

        private Option<ActionResourceType> GetEntityType(string name)
        {
            name = name.ToLower();
            if (name.Contains("produto"))
            {
                return ActionResourceType.Product;
            }
            if (name.Contains("pedido"))
            {
                return ActionResourceType.Order;
            }
            if (name.Contains("lote"))
            {
                return ActionResourceType.Lot;
            }
            if (name.Contains("endereço") || name.Contains("endereco"))
            {
                return ActionResourceType.Address;
            }
            if (name.Contains("lpn"))
            {
                return ActionResourceType.Lpn;
            }
            return Option<ActionResourceType>.None;
        }
    }
}
