﻿using System;
using System.Collections.Generic;
using System.Text;

namespace backend.application.domain.reports.index
{
    public enum ReportParameterizationType
    {
        Dynamic,
        Periodic,
        Parameterless
    }
}
