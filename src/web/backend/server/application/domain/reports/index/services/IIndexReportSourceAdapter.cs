﻿using backend.application.domain.actions.entities;
using LanguageExt;

namespace backend.application.domain.reports.index
{
    public interface IIndexReportSourceAdapter
    {
        TryAsync<IndexeableAction[]> GetIndexeableReports();
    }

    public class IndexReportSourceAdapter : IIndexReportSourceAdapter
    {
        private readonly IFetchIndexableReportsFromWmsRx _fetchIndexableReportsFromWmsRx;
        private readonly IMapIndexableReportsIntoActions _mapIndexableReportsIntoActionsService;

        public IndexReportSourceAdapter(
            IFetchIndexableReportsFromWmsRx fetchIndexableReportsFromWmsRx,
            IMapIndexableReportsIntoActions mapIndexableReportsIntoActionsService
            )
        {
            _fetchIndexableReportsFromWmsRx = fetchIndexableReportsFromWmsRx;
            _mapIndexableReportsIntoActionsService = mapIndexableReportsIntoActionsService;
        }

        public TryAsync<IndexeableAction[]> GetIndexeableReports()
        {
            return async () =>
            {
                var tryFetchReports = await _fetchIndexableReportsFromWmsRx.FetchReports();
                return _mapIndexableReportsIntoActionsService.Map(tryFetchReports);
            };
        }
    }
}