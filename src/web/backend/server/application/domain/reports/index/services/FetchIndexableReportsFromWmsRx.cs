﻿using backend.application.domain.reports.services;
using integrations.wmsrxReportingServices.api;
using integrations.wmsrxReportingServices.api.Models;
using LanguageExt;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.application.domain.reports.index
{
    public interface IFetchIndexableReportsFromWmsRx
    {
        Task<IndexableReport[]> FetchReports();
    }

    public class FetchIndexableReportsFromWmsRx : IFetchIndexableReportsFromWmsRx
    {
        private readonly IWmsRxReportingServicesClient _wmsRxReportingServicesClient;
        private readonly IMapIndexableReportFromWmsRx _mapIndexableReportFromWmsRx;

        public FetchIndexableReportsFromWmsRx(IWmsRxReportingServicesClient wmsRxReportingServicesClient, IMapIndexableReportFromWmsRx mapIndexableReportFromWmsRx)
        {
            _wmsRxReportingServicesClient = wmsRxReportingServicesClient;
            _mapIndexableReportFromWmsRx = mapIndexableReportFromWmsRx;
        }

        public async Task<IndexableReport[]> FetchReports()
        {
            IList<WmsRxReport> result = await _wmsRxReportingServicesClient.FetchIndexableReports();

            return _mapIndexableReportFromWmsRx.Execute(result);
        }
    }
}
