using System.Collections.Generic;

namespace backend.application.domain.search.vos
{
    public class UserSearchResult
    {
        public IEnumerable<SuggestionItem> Items { get; }
        public long Total { get; }

        public UserSearchResult(long total, IEnumerable<SuggestionItem> items)
        {
            Total = total;
            Items = items;
        }
    }
}