using backend.application.domain.actions.entities;
using LanguageExt;
using plugins.sdk.search.elastic.interfaces;

namespace backend.application.domain.search.vos
{
    public class RelatedActionMappingResult : IPermissionableMappedDocument
    {
        public RelatedActionMappingResult(string label, string url, ActionType actionType, Option<string[]> permissions)
        {
            Label = label;
            Url = url;
            ActionType = actionType;
            Permissions = permissions;
        }

        public string Label { get; }
        public string Url { get; }
        public ActionType ActionType { get; }
        public Option<string[]> Permissions { get; }
    }
}