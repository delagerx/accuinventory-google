using Microsoft.AspNetCore.Routing;
using plugins.sdk.search.elastic;

namespace backend.application.domain.search.vos
{
    public interface ISuggestion
    {
        string Label { get; }
        RouteValueDictionary ContextProps { get; }
        SearchResultType Type { get; }
        SearchResultSubType SubType { get; }
    }
}