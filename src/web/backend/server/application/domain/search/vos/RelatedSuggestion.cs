using Microsoft.AspNetCore.Routing;
using plugins.sdk.search.elastic;

namespace backend.application.domain.search.vos
{
    public class RelatedSuggestion : ISuggestion
    {
        public RelatedSuggestion(string label, dynamic contextProps, SearchResultType type, SearchResultSubType subType, bool hasPermission)
        {
            Label = label;
            ContextProps = contextProps;
            Type = type;
            SubType = subType;
            HasPermission = hasPermission;
        }

        public string Label { get; }
        public RouteValueDictionary ContextProps { get; }
        public SearchResultType Type { get; }
        public SearchResultSubType SubType { get; }
        public bool HasPermission { get; }
    }
}