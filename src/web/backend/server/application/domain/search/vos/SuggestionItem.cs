using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using plugins.sdk.search.elastic;

namespace backend.application.domain.search.vos
{
    public class SuggestionItem : ISuggestion
    {
        public string Label { get; }
        public RouteValueDictionary ContextProps { get; }
        public IEnumerable<RelatedSuggestion> RelatedSuggestions { get; }
        public SearchResultType Type { get; }
        public SearchResultSubType SubType { get; }
        public MatchProbability Probability { get; }
        public bool HasPermission { get; }

        public SuggestionItem(string label, SearchResultType type, SearchResultSubType subType, MatchProbability probability, bool hasPermission, dynamic contextProps = null, IEnumerable<RelatedSuggestion> relatedActions = null)
        {
            Label = label;
            Type = type;
            SubType = subType;
            Probability = probability;
            HasPermission = hasPermission;
            ContextProps = new RouteValueDictionary(contextProps);
            RelatedSuggestions = relatedActions ?? new RelatedSuggestion[0];
        }
    }
}