﻿using System.Threading;

namespace backend.application.domain.search.entities
{
    public class SearchQuery
    {
        public SearchQuery(string value, int page, int pageSize, CancellationToken cancellationToken)
        {
            Value = value;
            Page = page;
            PageSize = pageSize;
            CancellationToken = cancellationToken;
        }

        public string Value { get; }
        public int Page { get; }
        public int PageSize { get; }
        public CancellationToken CancellationToken { get; }
    }
}
