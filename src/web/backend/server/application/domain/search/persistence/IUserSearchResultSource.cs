using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using backend.application.domain.search.vos;

namespace backend.application.domain.search.persistence
{
    public interface IUserSearchResultSource
    {
        Task<UserSearchResult> Find(string query, int page, int pageSize, CancellationToken cancellationToken);
    }
}