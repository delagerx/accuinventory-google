using System;
using System.Linq;
using System.Threading;
using backend.application.domain.search.entities;
using backend.application.domain.search.services.elastic.configuration;
using backend.application.infraestructure.elasticSearch;
using LanguageExt;
using Nest;

namespace backend.application.domain.search.services.elastic
{
    public interface IQueryIndexedItemsService
    {
        TryAsync<ISearchResponse<object>> Query(SearchQuery query);
    }

    public class QueryIndexedItemsService : IQueryIndexedItemsService
    {
        private readonly ElasticConfiguredQueryFactories _descriptors;
        private readonly IElasticClientAdapter _elasticClient;
        private readonly IElasticQueryContextFactory _contextFactory;
        

        public QueryIndexedItemsService(ElasticConfiguredQueryFactories descriptors, IElasticClientAdapter elasticClient, IElasticQueryContextFactory contextFactory)
        {
            _descriptors = descriptors;
            _elasticClient = elasticClient;
            _contextFactory = contextFactory;
        }
        public TryAsync<ISearchResponse<object>> Query(SearchQuery query)
        {
            var boolQuery = new BoolQuery()
            {
                Should = _descriptors
                    .Descriptors
                    .Select(x => x.QueryFactory.MakeQuery(_contextFactory.Make(query.Value, x.TypeSearchConfigurationCache))),
            };
            
            return _elasticClient.TrySearchAsync(
                boolQuery,
                MakeSearchDescriptor(query),
                query.CancellationToken);
        }

        private static Func<SearchDescriptor<object>, SearchDescriptor<object>> MakeSearchDescriptor(SearchQuery query)
        {
            return search =>
                search.Index("accuinventory-search-*")
                    .Type(Types.All)
                    .From((query.Page - 1) * query.PageSize)
                    .Size(query.PageSize)
                    .MinScore(0.5);
        }
    }
}