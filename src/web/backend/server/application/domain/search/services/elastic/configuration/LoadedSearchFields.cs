﻿using System;
using System.Collections.Generic;
using plugins.sdk.search.elastic.configuration;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class CachedSearchConfiguration
    {
        public CachedSearchConfiguration(Dictionary<Type, TypeSearchConfigurationCache> typesSearchConfigurationCache)
        {
            TypesSearchConfigurationCache = typesSearchConfigurationCache;
        }

        public Dictionary<Type, TypeSearchConfigurationCache> TypesSearchConfigurationCache {get;}
    }
}