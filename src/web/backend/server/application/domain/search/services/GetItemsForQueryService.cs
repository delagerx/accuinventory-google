using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using backend.application.domain.search.entities;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.vos;
using backend.application.infraestructure.languageExt;
using LanguageExt;
using Nest;

namespace backend.application.domain.search.services
{
    public interface IGetItemsForQueryService
    {
        TryAsync<UserSearchResult> TryFind(SearchQuery query);
    }

    public class GetItemsForQueryService : IGetItemsForQueryService
    {
        private readonly IQueryIndexedItemsService _queryIndexedItemsService;
        private readonly ISearchIndexedItensResultMapperService _searchIndexedItensResultMapperService;

        public GetItemsForQueryService(IQueryIndexedItemsService queryIndexedItemsService, ISearchIndexedItensResultMapperService searchIndexedItensResultMapperService)
        {
            _queryIndexedItemsService = queryIndexedItemsService;
            _searchIndexedItensResultMapperService = searchIndexedItensResultMapperService;
        }

        public TryAsync<UserSearchResult> TryFind(SearchQuery query)
        {
            var tryQuery = _queryIndexedItemsService.Query(query);
            return async () => await MapResults(tryQuery);
        }

        private async Task<Result<UserSearchResult>> MapResults(TryAsync<ISearchResponse<object>> tryQuery)
        {
            return await await tryQuery
                .Succ(async results =>
                {
                    IEnumerable<SuggestionItem> mappedResults =
                        await _searchIndexedItensResultMapperService.MapElasticResult(results);
                    var userResult = new UserSearchResult(results.Total, mappedResults);
                    return new Result<UserSearchResult>(userResult);
                }).CascadeFail();
        }
    }
}