using System.Collections.Generic;

namespace backend.application.domain.search.services.elastic
{
    public class ConfiguredDocumentsList
    {
        public ConfiguredDocumentsList(IEnumerable<IndexedItemDocumentType> configurations)
        {
            Configurations = configurations;
        }

        public IEnumerable<IndexedItemDocumentType> Configurations { get; }
    }
}