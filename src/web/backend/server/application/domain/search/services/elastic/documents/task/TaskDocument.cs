using System;
using plugins.sdk.search.elastic.attributes;

namespace backend.application.domain.search.services.elastic.documents.task
{
    public class TaskDocument
    {
        [ElasticPrefixedSearchField]
        public string Id { get; set; }
        public string Operator { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}