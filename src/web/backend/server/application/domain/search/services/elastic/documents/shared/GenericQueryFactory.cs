﻿using System.Collections.Generic;
using System.Linq;
using Nest;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.interfaces;

namespace backend.application.domain.search.services.elastic.documents.shared
{
    public class GenericQueryFactory : IDocumentQueryFactory
    {
        private readonly IPrefixQueryFactory _prefixQueryFactory;
        private readonly IFuzzyQueryFactory _fuzzyQueryFactory;

        public GenericQueryFactory(
            IPrefixQueryFactory prefixQueryFactory,
            IFuzzyQueryFactory fuzzyQueryFactory
            )
        {
            _prefixQueryFactory = prefixQueryFactory;
            _fuzzyQueryFactory = fuzzyQueryFactory;
        }
        public QueryContainer MakeQuery(ElasticQueryContext context)
        {
            return new BoolQuery()
            {
                Should = Query(context),
                Filter = new[] { MakeTypeFilter(context) }
            };
        }

        private IEnumerable<QueryContainer> Query(ElasticQueryContext context)
        {
            return new[]
            {
                _prefixQueryFactory.Make(context),
                _fuzzyQueryFactory.Make(context)
            };
        }

        private QueryContainer MakeTypeFilter(ElasticQueryContext context)
        {

            var filter = new TermQuery()
            {
                Field = "_type",
                Value = context.TypeSearchConfigurationCache.ElasticType
            };
            return filter;
        }
    }
}