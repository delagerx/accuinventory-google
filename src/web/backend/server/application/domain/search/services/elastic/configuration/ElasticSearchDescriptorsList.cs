using System.Collections.Generic;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticSearchDescriptorsList
    {
        public ElasticSearchDescriptorsList(IEnumerable<ElasticSearchDescriptorInstance> descriptors)
        {
            Descriptors = descriptors;
        }

        public IEnumerable<ElasticSearchDescriptorInstance> Descriptors { get; }
    }
}