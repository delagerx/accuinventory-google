using backend.application.domain.search.vos;
using plugins.sdk.search.elastic;

namespace backend.application.domain.search.services.elastic.documents.task
{
    public class TaskDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var task = ElasticDocumentSerializer.DeserializeInfos<TaskDocument>(serializedAction);

            return new MappingResult($"Task: {task.Description} | Operator: {task.Operator} | Date: {task.Date}", LogisticOperationDocument.AllLOs, SearchResultSubType.None);
        }
    }
}