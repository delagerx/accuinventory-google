using backend.application.domain.user.entities;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.configuration;
using plugins.sdk.utils;

namespace backend.application.domain.search.services.elastic
{
    public interface IElasticQueryContextFactory
    {
        ElasticQueryContext Make(string query, TypeSearchConfigurationCache cache);
    }

    public class ElasticQueryContextFactory : IElasticQueryContextFactory
    {
        private readonly Culture _culture;

        public ElasticQueryContextFactory(Culture culture)
        {
            _culture = culture;
        }
        public ElasticQueryContext Make(string query, TypeSearchConfigurationCache cache)
        {
            return new ElasticQueryContext(cache, query, _culture);
        }
    }
}