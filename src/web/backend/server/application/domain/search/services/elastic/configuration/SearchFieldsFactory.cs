﻿using System;
using System.Linq;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.configuration;
using plugins.sdk.utils;
using StructureMap;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class QueryFieldsCacheFactory
    {
        public static CachedSearchConfiguration Make(IContext context)
        {
            var documentList = context.GetInstance<ConfiguredDocumentsList>();
            var cachedFieldsPerType = documentList
                .Configurations
                .Select(MakeCacheForType)
                .ToDictionary(x => x.Type, x => x);
            return new CachedSearchConfiguration(cachedFieldsPerType);
        }

        private static TypeSearchConfigurationCache MakeCacheForType(IndexedItemDocumentType documentCfg)
        {
            var isInternationalizable = DocumentImplementsInternationalizable(documentCfg.ObjectType);
            var isPermissionable = DocumentImplementsPermissionable(documentCfg.ObjectType);
            var fields = ElasticFieldMapper.GetFieldForType(documentCfg.ObjectType).ToArray();
            return new TypeSearchConfigurationCache(documentCfg.ObjectType, fields, isInternationalizable, isPermissionable, documentCfg.ElasticType);
        }

        private static bool DocumentImplementsInternationalizable(Type objectType)
        {
            return objectType
                .GetInterfaces()
                .Any(x =>
                    x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IInternationalizable<>));
        }

        private static bool DocumentImplementsPermissionable(Type objectType)
        {
            return typeof(IPermissionableDocument).IsAssignableFrom(objectType);
        }
    }
}
