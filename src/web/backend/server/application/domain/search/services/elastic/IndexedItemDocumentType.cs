using System;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.interfaces;

namespace backend.application.domain.search.services.elastic
{
    public class IndexedItemDocumentType
    {
        public IndexedItemDocumentType(SearchResultType searchResultType, string indexName, string elasticType, Type mapper, Type searchDescriptor, Type objectType)
        {
            SearchResultType = searchResultType;
            IndexName = indexName;
            ElasticType = elasticType;
            if (!typeof(IDocumentActionMapper).IsAssignableFrom(mapper))
                throw new ArgumentException(
                    $"Mapper type must be assignable from {nameof(IDocumentActionMapper)}");
            Mapper = mapper;
            if (!typeof(IDocumentQueryFactory).IsAssignableFrom(searchDescriptor))
                throw new ArgumentException(
                    $"SearchDescriptor type must be assignable from {nameof(IDocumentQueryFactory)}");
            SearchDescriptor = searchDescriptor;
            ObjectType = objectType;
        }
        public SearchResultType SearchResultType { get; }
        public string IndexName { get; }
        public string ElasticType { get; }
        public Type ObjectType { get; }
        public Type Mapper { get; }
        public Type SearchDescriptor { get; }
    }
}