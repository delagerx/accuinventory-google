﻿using System.Linq;
using Nest;
using plugins.sdk.utils;

namespace backend.application.domain.search.services.elastic.documents.shared
{
    public interface ITranslationQueryFactory
    {
        string[] HandleInternationalization(string[] properties, bool isInternationalizable);
    }

    public class TranslationQueryFactory : ITranslationQueryFactory
    {
        private readonly Culture _culture;

        public TranslationQueryFactory(Culture culture)
        {
            _culture = culture;
        }
        public string[] HandleInternationalization(string[] properties, bool isInternationalizable)
        {
            if (isInternationalizable)
                properties = properties.Select(x => x.Replace("translations.*", $"translations.{_culture}")).ToArray();
            return properties;
        }
    }
}