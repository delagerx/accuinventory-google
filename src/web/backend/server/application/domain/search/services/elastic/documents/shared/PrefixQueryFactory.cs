﻿using System.Linq;
using Nest;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.attributes;

namespace backend.application.domain.search.services.elastic.documents.shared
{
    public interface IPrefixQueryFactory
    {
        QueryContainer Make(ElasticQueryContext context);
    }

    public class PrefixQueryFactory : IPrefixQueryFactory
    {
        private readonly ITranslationQueryFactory _translationQueryFactory;

        public PrefixQueryFactory(ITranslationQueryFactory translationQueryFactory)
        {
            _translationQueryFactory = translationQueryFactory;
        }
        public QueryContainer Make(ElasticQueryContext context)
        {
            var prefixedFields = context
                .TypeSearchConfigurationCache
                .CachedSearchFields
                .Where(x => x.IsPrefixed);
            var fields = prefixedFields.Select(x => x.FieldName).ToArray();
            var translatedFields =
                _translationQueryFactory.HandleInternationalization(fields, context.TypeSearchConfigurationCache.IsInternationalizable);
            return new MultiMatchQuery()
            {
                Fields = translatedFields,
                Query = context.Query,
                Lenient = true,
                Type = TextQueryType.PhrasePrefix,
                Boost = 3
            };
        }
    }
}