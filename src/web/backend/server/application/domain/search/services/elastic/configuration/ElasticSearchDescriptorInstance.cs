using plugins.sdk.search.elastic.configuration;
using plugins.sdk.search.elastic.interfaces;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticSearchDescriptorInstance
    {
        public ElasticSearchDescriptorInstance(string indexName, TypeSearchConfigurationCache typeSearchConfigurationCache, IDocumentQueryFactory queryFactory)
        {
            IndexName = indexName;
            TypeSearchConfigurationCache = typeSearchConfigurationCache;
            QueryFactory = queryFactory;
        }

        public string IndexName { get; }
        public TypeSearchConfigurationCache TypeSearchConfigurationCache { get; }
        public IDocumentQueryFactory QueryFactory { get; }
    }
}