using System.Collections.Generic;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticConfiguredQueryFactories
    {
        public ElasticConfiguredQueryFactories(IEnumerable<ElasticQueryFactoryInstance> descriptors)
        {
            Descriptors = descriptors;
        }

        public IEnumerable<ElasticQueryFactoryInstance> Descriptors { get; }
    }
}