﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.actions.services;
using backend.application.domain.search.services.elastic.configuration;
using backend.application.domain.search.vos;
using Nest;
using plugins.sdk.search.elastic;
using plugins.sdk.session;

namespace backend.application.domain.search.services.elastic
{
    public interface ISearchIndexedItensResultMapperService
    {
        Task<IList<SuggestionItem>> MapElasticResult(ISearchResponse<object> result);
    }

    public class SearchIndexedItensResultMapperService : ISearchIndexedItensResultMapperService
    {
        private readonly ElasticConfiguredResultMappers _mappers;
        private readonly IRelatedSuggestionsService _relatedSuggestionsService;
        private readonly IDocumentPermissionService _documentPermissionService;

        public SearchIndexedItensResultMapperService(
            ElasticConfiguredResultMappers mappers, 
            IRelatedSuggestionsService relatedSuggestionsService,
            IDocumentPermissionService documentPermissionService
        )
        {
            _mappers = mappers;
            _relatedSuggestionsService = relatedSuggestionsService;
            _documentPermissionService = documentPermissionService;
        }
        public async Task<IList<SuggestionItem>> MapElasticResult(ISearchResponse<object> result)
        {
            var mappingTasks = result
                .Hits
                .GroupBy(x => x.Index)
                .Select(MapIndexDocuments);
            return await Task
                .WhenAll(mappingTasks)
                .ContinueWith(task => 
                    task.Result?.SelectMany(x => x).ToList() ?? new List<SuggestionItem>()
                );
        }

        private async Task<IList<SuggestionItem>> MapIndexDocuments(IGrouping<string, IHit<object>> group)
        {
            var mapperConfiguration = _mappers.Mappers
                .Find(x => x.IndexName == group.Key);
            return await mapperConfiguration.Some(m =>
            {
                var mapper = m.Mapper;
                var mappedResults = group.Select(hit =>
                    MapDocumentToSuggestion(hit, mapper, m.SearchResultType)
                );
                return _relatedSuggestionsService.GetAndBindRelatedSuggestions(mappedResults);
            }).None(() => ((IList<SuggestionItem>)new List<SuggestionItem>()).AsTask());
        }

        private SuggestionItem MapDocumentToSuggestion(IHit<object> hit, IDocumentActionMapper mapper, SearchResultType searchResultType)
        {
            var mappingResult = mapper.Map(hit.Source.ToString());
            var hasPermissions = _documentPermissionService.HasPermissions(mappingResult);
            var suggestion = new SuggestionItem(
                mappingResult.Label, 
                searchResultType, 
                mappingResult.SubType, 
                hit.Score, 
                hasPermissions, 
                mappingResult.ContextProps
            );
            suggestion.ContextProps["lo"] = mappingResult.LogisticOperation;
            return suggestion;
        }
    }
}
