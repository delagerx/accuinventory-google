﻿using System.Linq;
using Nest;
using plugins.sdk.search.elastic;

namespace backend.application.domain.search.services.elastic.documents.shared
{
    public interface IFuzzyQueryFactory
    {
        QueryContainer Make(ElasticQueryContext context);
    }

    public class FuzzyQueryFactory : IFuzzyQueryFactory
    {
        private readonly ITranslationQueryFactory _translationQueryFactory;

        public FuzzyQueryFactory(ITranslationQueryFactory translationQueryFactory)
        {
            _translationQueryFactory = translationQueryFactory;
        }
        public QueryContainer Make(ElasticQueryContext context)
        {
            var fuzzyFields = context
                .TypeSearchConfigurationCache
                .CachedSearchFields
                .Where(x => x.IsFuzzy);
            var fields = fuzzyFields.Select(x => x.FieldName).ToArray();
            var translatedFields = 
                _translationQueryFactory.HandleInternationalization(fields, context.TypeSearchConfigurationCache.IsInternationalizable);
            return new MultiMatchQuery()
            {
                Fields = translatedFields,
                Query = context.Query,
                //Permite queries com outros tipos de dados. Por exemplo: int num campo de string.
                Lenient = true,
                Fuzziness = Fuzziness.Auto,
                PrefixLength = 2,
                //Aumenta o score se as palavras esiverem juntas
                Slop = 75
            };
        }
    }
}