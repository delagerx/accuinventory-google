using plugins.sdk.search.elastic;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticSearchMapperInstance
    {
        public ElasticSearchMapperInstance(string indexName, SearchResultType searchResultType, IDocumentActionMapper mapper)
        {
            IndexName = indexName;
            SearchResultType = searchResultType;
            Mapper = mapper;
        }

        public string IndexName { get; }
        public SearchResultType SearchResultType { get; }
        public IDocumentActionMapper Mapper { get; }
    }
}