using System.Collections.Generic;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticConfiguredResultMappers
    {
        public ElasticConfiguredResultMappers(IEnumerable<ElasticResultMapperInstance> mappers)
        {
            Mappers = mappers;
        }

        public IEnumerable<ElasticResultMapperInstance> Mappers { get; }
    }
}