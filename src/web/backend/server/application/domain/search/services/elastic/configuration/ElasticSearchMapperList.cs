using System.Collections.Generic;

namespace backend.application.domain.search.services.elastic.configuration
{
    public class ElasticSearchMapperList
    {
        public ElasticSearchMapperList(IEnumerable<ElasticSearchMapperInstance> mappers)
        {
            Mappers = mappers;
        }

        public IEnumerable<ElasticSearchMapperInstance> Mappers { get; }
    }
}