﻿using System.Collections.Generic;

namespace backend.application.domain.user.persistence.model
{
    public class Entity
    {
        public int EntityCode { get; set; }
        public string Name { get; set; }
        public virtual User User { get; set; }
        public string UserId { get; set; }
        public virtual IEnumerable<EntityLogisticOperation> LogisticOperations { get; set; }
    }

    public class EntityLogisticOperation
    {
        public int PrincipalEntityCode { get; set; }
        public Entity Principal { get; set; }
        public int LogisticOperationEntityCode { get; set; }
        public Entity LogisticOperation { get; set; }
    }
}