﻿using System;
using System.Collections.Generic;

namespace backend.application.domain.user.persistence.model
{
    public class UserNotification
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Messsage { get; set; }
        public UserNotificationType Type { get; set; }
        public int IdSender { get; set; }
        public Entity Sender { get; set; }
        public ICollection<UserNotificationRecipient> Recipients { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}