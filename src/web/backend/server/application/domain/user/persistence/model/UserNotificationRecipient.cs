﻿using System;

namespace backend.application.domain.user.persistence.model
{
    public class UserNotificationRecipient
    {
        public int IdRecipient { get; set; }
        public Entity Recipient { get; set; }
        public int IdNotification { get; set; }
        public UserNotification Notification { get; set; }
        public DateTime? OpenedAt { get; set; }
    }
}