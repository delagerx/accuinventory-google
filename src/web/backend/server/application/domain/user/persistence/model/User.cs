﻿using System.Collections.Generic;

namespace backend.application.domain.user.persistence.model
{
    public class User
    {
        public string Id { get; set; } 
        public string Email { get; set; }
        public virtual IEnumerable<AspNetUserClaims> Claims { get; set; }
        public virtual Entity Entity { get; set; }
    }

    public enum UserNotificationType
    {
        Default = 1,
        Success = 2,
        Danger = 3,
        Warning = 4,
        Information = 5,
    }
}