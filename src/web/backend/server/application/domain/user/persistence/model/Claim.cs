﻿namespace backend.application.domain.user.persistence.model
{
    public class Claim
    {
        public long Id { get; set; }
        public string Tipo { get; set; }
        public string Valor { get; set; }
        public string LabelTipo { get; set; }
        public string LabelValor { get; set; }
        public bool Todos { get; set; }
    }
}
