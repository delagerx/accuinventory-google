﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.user.persistence.model;
using backend.application.persistence;
using Core.EntityFrameworkCore;
using Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace backend.application.domain.user.persistence
{
    public interface INotificationRepository
    {
        Task<ICollection<UserNotificationRecipient>> GetUserNotReadNotifications(string recipient);
        Task SaveNotifications(IEnumerable<UserNotificationRecipient> notifications);
    }

    public class NotificationRepository : INotificationRepository
    {
        private readonly WmsRxContext _context;

        public NotificationRepository(WmsRxContext context)
        {
            _context = context;
        }

        public async Task<ICollection<UserNotificationRecipient>> GetUserNotReadNotifications(string recipient)
        {
            recipient = recipient.ToLower();
            return await _context
                .NotificationRecipients
                .Include(x => x.Recipient)
                .Include(x => x.Notification)
                .Where(notificationRecipient =>
                    notificationRecipient.Recipient.Name.ToLower() == recipient
                    && notificationRecipient.OpenedAt == null
                )
                .OrderByDescending(x => x.Notification.CreatedAt)
                .ToListAsync();
        }

        public Task SaveNotifications(IEnumerable<UserNotificationRecipient> notifications)
        {
            _context.UpdateRange(notifications);
            return _context.SaveChangesAsync();
        }
    }
}