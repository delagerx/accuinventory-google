﻿namespace backend.application.domain.user.persistence.model
{
    public class AspNetUserClaims
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}