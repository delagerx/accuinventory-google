﻿using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.user.entities;
using backend.application.domain.user.persistence.model;
using backend.application.persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace backend.application.domain.user.persistence
{
    public interface IUserRepository
    {
        Task<UserPermissions> GetUserPermissions(string id);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserById(string userId);
    }

    public class UserRepository : IUserRepository
    {
        private readonly WmsRxContext _context;

        public UserRepository(WmsRxContext context)
        {
            _context = context;
        }
        public async Task<User> GetUserByEmail(string email)
        {
            return await _context
                .Users
                .Include(x => x.Entity)
                .ThenInclude(x => x.LogisticOperations)
                .ThenInclude(x => x.LogisticOperation)
                .Where(x => x.Email == email).FirstAsync();
        }
        public async Task<UserPermissions> GetUserPermissions(string id)
        {
            var claims = await _context
                .UserClaims
                .Where(x => x.User.Id == id)
                .Select(x => new UserClaim(x.ClaimType, x.ClaimValue))
                .ToListAsync();
            return new UserPermissions(claims);
        }

        public async Task<User> GetUserById(string userId)
        {
            var user = await _context
                .Users
                .Include(x => x.Entity)
                .ThenInclude(x => x.LogisticOperations)
                .ThenInclude(x => x.LogisticOperation)
                .Where(x => x.Id == userId)
                .FirstAsync();
            return user;
        }
    }
}
