﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.user.persistence;
using backend.application.domain.user.persistence.model;
using plugins.sdk.session;

namespace backend.application.domain.user.services
{
    public interface IGetUserNotificationService
    {
        Task<UserNotificationInfoDto> GetCurrentUserNotifications();
    }

    public class GetUserNotificationService : IGetUserNotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly UserContext _userContext;

        public GetUserNotificationService(INotificationRepository notificationRepository, UserContext userContext)
        {
            _notificationRepository = notificationRepository;
            _userContext = userContext;
        }

        public async Task<UserNotificationInfoDto> GetCurrentUserNotifications()
        {
            var notifications = (await _notificationRepository
                .GetUserNotReadNotifications(_userContext.Name))
                .Select(n => new UserNotificationDto()
                {
                    Id = n.Notification.Id,
                    Type = n.Notification.Type,
                    Title = n.Notification.Title,
                    Sender = n.Notification.Sender.Name,
                    CreatedAt = n.Notification.CreatedAt,
                    Message = n.Notification.Messsage,
                })
                .ToList();
            return new UserNotificationInfoDto()
            {
                NewNotifications = notifications,
                NotificationCount = notifications.Count,
            };
        }
    }

    public class UserNotificationInfoDto
    {
        public IEnumerable<UserNotificationDto> NewNotifications { get; set; }
        public int NotificationCount { get; set; }
    }

    public class UserNotificationDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public UserNotificationType Type { get; set; }
        public string Sender { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}