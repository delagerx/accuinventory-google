﻿using backend.application.domain.user.entities;
using LanguageExt;
using Microsoft.Extensions.Caching.Memory;

namespace backend.application.domain.user.services
{
    public interface ICachePermissionService
    {
        Option<UserPermissions> GetPermissions(string userName);
        void SavePermissions(string userId, UserPermissions userPermissions);
    }

    public class CachePermissionService : ICachePermissionService
    {
        private readonly IMemoryCache _memoryCache;

        public CachePermissionService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public Option<UserPermissions> GetPermissions(string userName)
        {
            var cacheFound = _memoryCache.TryGetValue(userName, out var cachedPermissions);
            return cacheFound ? (UserPermissions)cachedPermissions : null;
        }

        public void SavePermissions(string userId, UserPermissions userPermissions)
        {
            #if !DEBUG
            _memoryCache.Set(userId, userPermissions);
            #endif
        }
    }
}
