﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.user.entities;
using backend.application.domain.user.persistence;
using plugins.sdk.session;

namespace backend.application.domain.user.services
{
    public interface IUserContextService
    {
        Task LoadUserContext(string email);
    }

    public class UserContextService : IUserContextService
    {
        private readonly IGetUserPermissionsService _permissionService;
        private readonly UserContext _userContext;
        private readonly IUserRepository _userRepository;

        public UserContextService(
            IGetUserPermissionsService permissionService, 
            UserContext userContext, 
            IUserRepository userRepository)
        {
            _permissionService = permissionService;
            _userContext = userContext;
            _userRepository = userRepository;
        }

        public async Task LoadUserContext(string email)
        {
            var user = await _userRepository.GetUserByEmail(email);
            var permissions = await _permissionService.GetUserPermissions(email);
            var logisticOperations = user
                .Entity
                .LogisticOperations
                .Select(x => new LogisticOperation(){Name = x.LogisticOperation.Name, Id = x.LogisticOperation.EntityCode})
                .ToArray();
            _userContext.Id = user.Id;
            _userContext.Email = email;
            _userContext.UserPermissions = permissions;
            _userContext.LogisticOperations = logisticOperations;
            _userContext.Name = user.Entity.Name;
        }
    }
}
