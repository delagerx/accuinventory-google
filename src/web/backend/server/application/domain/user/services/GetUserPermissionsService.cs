﻿using System.Threading.Tasks;
using backend.application.domain.user.entities;
using backend.application.domain.user.persistence;

namespace backend.application.domain.user.services
{
    public interface IGetUserPermissionsService
    {
        Task<UserPermissions> GetUserPermissions(string userName);
    }

    public class GetUserPermissionsService : IGetUserPermissionsService
    {
        private readonly ICachePermissionService _cachePermissionService;
        private readonly IUserRepository _userRepository;

        public GetUserPermissionsService(ICachePermissionService cachePermissionService, IUserRepository userRepository)
        {
            _cachePermissionService = cachePermissionService;
            _userRepository = userRepository;
        }
        public async Task<UserPermissions> GetUserPermissions(string id)
        {
            var cachedPermissions = _cachePermissionService.GetPermissions(id);

            return await cachedPermissions
                .Some(Task.FromResult)
                .None(async () => await GetPermissionsAndCache(id));
        }

        private async Task<UserPermissions> GetPermissionsAndCache(string id)
        {
            var userPermissions =  await _userRepository.GetUserPermissions(id);
            _cachePermissionService.SavePermissions(id, userPermissions);
            return userPermissions;
        }
    }
}
