﻿using System;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.user.persistence;
using plugins.sdk.session;

namespace backend.application.domain.user.services
{
    public interface IReadNotificationService
    {
        Task ReadNotifications(int[] notifications);
    }

    public class ReadNotificationService : IReadNotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly UserContext _userContext;

        public ReadNotificationService(INotificationRepository notificationRepository, UserContext userContext)
        {
            _notificationRepository = notificationRepository;
            _userContext = userContext;
        }

        public async Task ReadNotifications(int[] notifications)
        {
            var currentDate = DateTime.Now;
            var userNotifications = await _notificationRepository.GetUserNotReadNotifications(_userContext.Name);
            foreach (var notification in userNotifications)
            {
                if (notifications.Any(x => x == notification.IdNotification))
                {
                    notification.OpenedAt = currentDate;
                }
            }
            await _notificationRepository.SaveNotifications(userNotifications);
        }
    }
}