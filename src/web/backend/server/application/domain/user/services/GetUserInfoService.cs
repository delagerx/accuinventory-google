﻿using System.Linq;
using System.Threading;
using backend.application.domain.user.entities;
using backend.application.domain.user.persistence;
using backend.application.domain.user.persistence.model;
using System.Threading.Tasks;
using plugins.sdk.session;

namespace backend.application.domain.user.services
{
    public interface IGetUserInfoService
    {
        Task<UserDto> GetUserInfo();
    }

    public class GetUserInfoService : IGetUserInfoService
    {
        private readonly UserContext _userContext;
        private readonly IUserRepository _userRepository;
        
        public GetUserInfoService(UserContext userContext, IUserRepository userRepository)
        {
            _userContext = userContext;
            _userRepository = userRepository;
        }

        public Task<UserDto> GetUserInfo()
        {
            var logisticOperations = _userContext
                .LogisticOperations
                .Select(x => new LogisticOperationDto()
                {
                    Id = x.Id.ToString(),
                    Name = x.Name,
                });
            var user = new UserDto()
            {
                Name = _userContext.Name,
                Email = _userContext.Email,
                UserPreferredLanguage = Thread.CurrentThread.CurrentCulture.Name,
                LogisticOperations = logisticOperations.ToArray()
            };
            return Task.FromResult(user);
        }
    }

    public class UserDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserPreferredLanguage { get; set; }
        public LogisticOperationDto[] LogisticOperations { get; set; } = new LogisticOperationDto[0];
    }

    public class LogisticOperationDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
