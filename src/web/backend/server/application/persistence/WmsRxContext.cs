﻿using System;
using backend.application.domain.user.entities;
using backend.application.domain.user.persistence.model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace backend.application.persistence
{
    public class WmsRxContext : DbContext
    {
        private readonly ILoggerFactory _loggerFactory;
        public DbSet<AspNetUserClaims> UserClaims { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<UserNotification> Notifications { get; set; }
        public DbSet<UserNotificationRecipient> NotificationRecipients { get; set; }

        public WmsRxContext(DbContextOptions<WmsRxContext> options, ILoggerFactory loggerFactory) : base(options)
        {
            _loggerFactory = loggerFactory;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseLoggerFactory(_loggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetUserClaims>().ToTable("AspNetUserClaims");
            modelBuilder.Entity<AspNetUserClaims>().HasKey(x => x.Id).HasName("Id");
            modelBuilder.Entity<AspNetUserClaims>().Property(x => x.UserId).HasColumnName("UserId");
            modelBuilder.Entity<AspNetUserClaims>().Property(x => x.ClaimType).HasColumnName("ClaimType");
            modelBuilder.Entity<AspNetUserClaims>().Property(x => x.ClaimValue).HasColumnName("ClaimValue");


            modelBuilder.Entity<User>().ToTable("AspNetUsers");
            modelBuilder.Entity<User>().HasKey(x => x.Id).HasName("Id");
            modelBuilder.Entity<User>().Property(x => x.Email).HasColumnName("email");
            modelBuilder.Entity<User>().HasMany(x => x.Claims).WithOne(x => x.User).HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Entity>().ToTable("entidade");
            modelBuilder.Entity<Entity>().Property(x => x.UserId).HasColumnName("identityId");
            modelBuilder.Entity<Entity>().Property(x => x.Name).HasColumnName("fantasia");
            modelBuilder.Entity<Entity>().HasKey(x => x.EntityCode);
            modelBuilder.Entity<Entity>().Property(x => x.EntityCode).HasColumnName("cod_entidade");
            modelBuilder.Entity<Entity>().HasOne(x => x.User).WithOne().HasForeignKey<Entity>(x => x.UserId);
            //Needs to be declared after entity mapping, otherwise it doesn't get the foreign key column name right
            modelBuilder.Entity<User>().HasOne(x => x.Entity).WithOne(x => x.User);

            modelBuilder.Entity<EntityLogisticOperation>().ToTable("entidade_operacao_logistica");
            modelBuilder.Entity<EntityLogisticOperation>().Property(x => x.PrincipalEntityCode).HasColumnName("cod_entidade");
            modelBuilder.Entity<EntityLogisticOperation>().Property(x => x.LogisticOperationEntityCode).HasColumnName("cod_operacao_logistica");
            modelBuilder.Entity<EntityLogisticOperation>()
                .HasKey(t => new { EntityCode = t.PrincipalEntityCode, t.LogisticOperationEntityCode });
            modelBuilder.Entity<EntityLogisticOperation>()
                .HasOne(x => x.Principal)
                .WithMany()
                .HasForeignKey(x => x.PrincipalEntityCode);
            modelBuilder.Entity<EntityLogisticOperation>()
                .HasOne(x => x.LogisticOperation)
                .WithMany()
                .HasForeignKey(x => x.LogisticOperationEntityCode);
            modelBuilder.Entity<Entity>().HasMany(x => x.LogisticOperations).WithOne(x => x.Principal);

            modelBuilder.Entity<Claim>().ToTable("claim");
            modelBuilder.Entity<Claim>().HasKey(x => x.Id).HasName("id_claim");
            modelBuilder.Entity<Claim>().Property(x => x.Tipo).HasColumnName("tipo");
            modelBuilder.Entity<Claim>().Property(x => x.Valor).HasColumnName("valor");
            modelBuilder.Entity<Claim>().Property(x => x.LabelTipo).HasColumnName("label_tipo");
            modelBuilder.Entity<Claim>().Property(x => x.LabelValor).HasColumnName("label_valor");
            modelBuilder.Entity<Claim>().Property(x => x.Todos).HasColumnName("todos");

            modelBuilder.Entity<UserNotification>().ToTable("notificacao");
            modelBuilder.Entity<UserNotification>().HasKey(x => x.Id).HasName("id_notificacao");
            modelBuilder.Entity<UserNotification>().Property(x => x.Id).HasColumnName("id_notificacao");
            modelBuilder.Entity<UserNotification>().Property(x => x.CreatedAt).HasColumnName("data_criacao");
            modelBuilder.Entity<UserNotification>().Property(x => x.Title).HasColumnName("titulo");
            modelBuilder.Entity<UserNotification>().Property(x => x.Messsage).HasColumnName("mensagem");
            modelBuilder.Entity<UserNotification>().Property(x => x.Type).HasColumnName("id_tipo");
            modelBuilder.Entity<UserNotification>().Property(x => x.IdSender).HasColumnName("cod_entidade_remetente");
            modelBuilder.Entity<UserNotification>().HasOne(x => x.Sender).WithMany().HasForeignKey(x => x.IdSender);

            modelBuilder.Entity<UserNotificationRecipient>().ToTable("notificacao_destinatario");
            modelBuilder.Entity<UserNotificationRecipient>().Property(x => x.IdNotification).HasColumnName("id_notificacao");
            modelBuilder.Entity<UserNotificationRecipient>().Property(x => x.IdRecipient).HasColumnName("cod_entidade");
            modelBuilder.Entity<UserNotificationRecipient>().Property(x => x.OpenedAt).HasColumnName("data_abertura");
            modelBuilder.Entity<UserNotificationRecipient>()
                .HasKey(t => new { t.IdRecipient, t.IdNotification});
            modelBuilder.Entity<UserNotificationRecipient>()
                .HasOne(x => x.Notification)
                .WithMany()
                .HasForeignKey(x => x.IdNotification);
            modelBuilder.Entity<UserNotificationRecipient>()
                .HasOne(x => x.Recipient)
                .WithMany()
                .HasForeignKey(x => x.IdRecipient);
            modelBuilder.Entity<UserNotification>().HasMany(x => x.Recipients).WithOne(x => x.Notification);
        }
    }
}
