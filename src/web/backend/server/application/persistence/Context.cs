using backend.application.domain.navigation.entities;
using backend.application.domain.user.persistence.model;
using backend.application.domain.widgets;
using Microsoft.EntityFrameworkCore;

namespace backend.application.persistence
{
    public class Context : DbContext
    {
        public DbSet<UserHistory> UserHistory { get; set; }
        public DbSet<UserFavoriteWidget> UserFavorites { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        {
            Database.Migrate();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserHistory>().OwnsOne(c => c.Link);
        }
    }
}