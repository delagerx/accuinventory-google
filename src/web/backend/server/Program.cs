﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace backend
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Api Acccuinventory";
            BuildWebHost(args).Run();
        }

        private static string MakeConfigurationFolderPath(string file)
        {
            return $"configurations{Path.DirectorySeparatorChar}{file}.yaml";
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var environmentDependentsConfigurations = new[] {
                        "appsettings",
                        "connectionStrings",
                        "elasticsearch",
                        "integrations",
                        "authentication"
                    };
                    var otherConfigurations = new[] {
                        "userhistory",
                        "plugins"
                    };

                    var configBuilder = config
                        .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                        .AddEnvironmentVariables();
                    foreach(var configFile in otherConfigurations.Concat(environmentDependentsConfigurations))
                    {
                        configBuilder.AddYamlFile($"{MakeConfigurationFolderPath($"{configFile}")}", true, true);
                    }
                    foreach(var environmentConfig in environmentDependentsConfigurations)
                    {
                        configBuilder.AddYamlFile($"{MakeConfigurationFolderPath($"{environmentConfig}.{hostingContext.HostingEnvironment.EnvironmentName}")}", true, true);
                    }
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.ClearProviders();
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                })
                .UseIISIntegration()
                .UseUrls("http://*:5000/")
                .Build();
        }
    }
}