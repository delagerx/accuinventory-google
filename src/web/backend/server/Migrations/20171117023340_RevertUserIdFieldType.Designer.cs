﻿// <auto-generated />
using backend.application.persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace backend.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20171117023340_RevertUserIdFieldType")]
    partial class RevertUserIdFieldType
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("backend.application.domain.navigation.entities.UserHistory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTime");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.ToTable("UserHistory");
                });

            modelBuilder.Entity("backend.application.domain.widgets.UserFavoriteWidget", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("SerializedProps");

                    b.Property<string>("Type");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.ToTable("UserFavorites");
                });

            modelBuilder.Entity("backend.application.domain.navigation.entities.UserHistory", b =>
                {
                    b.OwnsOne("backend.application.domain.navigation.vos.Link", "Link", b1 =>
                        {
                            b1.Property<long>("UserHistoryId");

                            b1.Property<string>("IconKey");

                            b1.Property<string>("Name");

                            b1.Property<string>("Url");

                            b1.ToTable("UserHistory");

                            b1.HasOne("backend.application.domain.navigation.entities.UserHistory")
                                .WithOne("Link")
                                .HasForeignKey("backend.application.domain.navigation.vos.Link", "UserHistoryId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
