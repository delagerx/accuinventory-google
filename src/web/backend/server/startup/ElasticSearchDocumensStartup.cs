﻿using System;
using System.Collections.Generic;
using System.Linq;
using backend.application.domain.search.services.elastic;
using backend.application.infraestructure.elasticSearch;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using StructureMap;

namespace backend.startup
{
    public static class ElasticSearchDocumentsStartup
    {
        public static ConfiguredDocumentsList ElasticDocumentConfigurationListFactory(IContext provider)
        {
            var documents = provider.GetInstance<IOptions<ElasticSearchOptions>>().Value.Documents;
            var logger = provider.GetInstance<ILoggerFactory>();
            var documentsConfigurations = documents.SelectMany(x => MapSourceConfiguration(logger.CreateLogger("SearchResultSourceStartup"), x)).ToList();
            return new ConfiguredDocumentsList(documentsConfigurations);
        }

        private static IEnumerable<IndexedItemDocumentType> MapSourceConfiguration(ILogger logger, ElasticDocumentConfiguration searchSource)
        {         
            foreach (var type in searchSource.Types)
            {
                var mapperType = LoadType(logger, type.Mapper);
                var searchDescriptorType = LoadType(logger, type.QueryFactory);
                var objectType = LoadType(logger, type.ObjectType);
                yield return new IndexedItemDocumentType(
                    searchResultType: type.SearchResultType,
                    indexName: searchSource.Index,
                    elasticType: type.ElasticType,
                    mapper: mapperType,
                    searchDescriptor: searchDescriptorType,
                    objectType: objectType
                );
            }
        }

        private static Type LoadType(ILogger logger, string type)
        {
            try
            {
                return Type.GetType(type);
            }
            catch (Exception)
            {
                logger.LogError($"Error while loading a document search result source of type: {type}");
                throw;
            }
        }
    }
}