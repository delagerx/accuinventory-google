﻿using System;
using System.Linq;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.services.elastic.configuration;
using Nest;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.configuration;
using plugins.sdk.search.elastic.interfaces;
using StructureMap;
using StructureMap.Pipeline;

namespace backend.startup
{
    public static class ElasticSearchQueriesStartup
    {
        
        public static ElasticConfiguredResultMappers ResultMapperListFactory(IContext provider)
        {
            var documents = provider.GetInstance<ConfiguredDocumentsList>();
            var mappers = documents.Configurations.Select(x => ResultMapperFactory(provider, x));
            return new ElasticConfiguredResultMappers(mappers);
        }

        public static ElasticConfiguredQueryFactories QueryFactoriesListFactory(IContext provider)
        {
            var documents = provider.GetInstance<ConfiguredDocumentsList>();
            var searchDescriptors = documents.Configurations.Select(x => QueryFactoryCreator(provider, x));
            return new ElasticConfiguredQueryFactories(searchDescriptors);
        }

        private static ElasticQueryFactoryInstance QueryFactoryCreator(IContext provider, IndexedItemDocumentType cfg)
        {
            var descriptor = (IDocumentQueryFactory) provider.GetInstance(cfg.SearchDescriptor);
            var cachedSearchConfiguration = provider.GetInstance<CachedSearchConfiguration>();
            var searchConfigurationCache = cachedSearchConfiguration.TypesSearchConfigurationCache[cfg.ObjectType];
            return new ElasticQueryFactoryInstance(cfg.IndexName, searchConfigurationCache, descriptor);
        }

        private static ElasticResultMapperInstance ResultMapperFactory(IContext provider, IndexedItemDocumentType cfg)
        {
            var mapper = (IDocumentActionMapper)provider.GetInstance(cfg.Mapper);
            return new ElasticResultMapperInstance(cfg.IndexName, cfg.SearchResultType, mapper);
        }
    }
}