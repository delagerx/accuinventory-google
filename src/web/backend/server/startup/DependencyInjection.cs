﻿using System;
using System.Reflection;
using backend.application.domain.navigation.persistence;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.services.elastic.configuration;
using backend.application.infraestructure.elasticSearch;
using backend.application.infraestructure.middlewares;
using backend.application.infraestructure.plugins;
using Core.EntityFrameworkCore;
using Core.Persistence;
using integrations;
using integrations.wmsrx.api;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using plugins.sdk.database;
using plugins.sdk.session;
using StructureMap;
using UserContext = plugins.sdk.session.UserContext;

namespace backend.startup
{
    public static class DependencyInjection
    {
        public static IServiceProvider ConfigureDependencyInjection(IConfiguration configuration, IServiceCollection services, Plugin[] plugins)
        {
            services.Configure<ElasticSearchOptions>(configuration.GetSection("elastic"));
            services.Configure<UserHistoryOptions>(configuration.GetSection("userhistory"));
            services.Configure<IntegrationOptions>(configuration.GetSection("integrations"));

            services.AddSingleton(plugins);
            var wmsRxConnection = configuration.GetConnectionString("wmsrx");

            var container = new Container();

            container.Configure(cfg =>
            {
                cfg.Scan(s =>
                {
                    s.Assembly(Assembly.GetExecutingAssembly());
                    foreach (var plugin in plugins)
                    {
                        s.Assembly(plugin.Assembly);
                    }
                    s.Assembly(Assembly.GetAssembly(typeof(IIndexedActions)));
                    s.RegisterConcreteTypesAgainstTheFirstInterface();
                    s.SingleImplementationsOfInterface();
                    s.WithDefaultConventions();
                });
                cfg.For(typeof(IRepository<,>)).Use(typeof(Repository<,>));
                cfg.For(typeof(ICrudRepository<,>)).Use(typeof(CrudRepository<,>));
                cfg.For<IWmsDatabaseConnectionFactory>().Use("DatabaseConnectionFactory",
                    provider => new WmsWmsDatabaseConnectionFactory(wmsRxConnection));
                cfg.For<UserContext>().ContainerScoped();
                cfg.For<LogisticOperationContext>().ContainerScoped();
                ElasticDocumentSearcherConfiguration(cfg);
                ConfigureWmsRxClient(configuration, cfg);
            });
            container.Populate(services);
            return container.GetInstance<IServiceProvider>();
        }

        private static void ConfigureWmsRxClient(IConfiguration configuration, ConfigurationExpression cfg)
        {
            cfg.For<WMSCadastro>().Use(() => new WMSCadastro(new Uri(configuration.GetSection("integrations:wmsrx").Value)));
        }

        private static void ElasticDocumentSearcherConfiguration(ConfigurationExpression cfg)
        {
            cfg.For<ConnectionSettings>().Singleton().Use(provider => 
                ElasticSearchClientStartup.MakeConnectionSettings(provider));
            cfg.For(typeof(IElasticClient)).Singleton().Use("ElasticClientFactory", 
                ElasticSearchClientStartup.ElasticClientFactory);
            cfg.For<CachedSearchConfiguration>().Singleton().Use("CachedSearchFieldsFactory", QueryFieldsCacheFactory.Make);
            cfg.For<ConfiguredDocumentsList>().Singleton().Use("ConfiguredDocuments", 
                ElasticSearchDocumentsStartup.ElasticDocumentConfigurationListFactory);
            cfg.For<ElasticConfiguredResultMappers>().Use("ElasticMappers", ElasticSearchQueriesStartup.ResultMapperListFactory);
            cfg.For<ElasticConfiguredQueryFactories>().Use("ElasticDescriptors", 
                ElasticSearchQueriesStartup.QueryFactoriesListFactory);
        }
    }
}