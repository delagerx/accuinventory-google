﻿using System;
using backend.application.infraestructure.elasticSearch;
using Elasticsearch.Net;
using Microsoft.Extensions.Options;
using Nest;
using Newtonsoft.Json;
using StructureMap;

namespace backend.startup
{
    public static class ElasticSearchClientStartup
    {
        public static ConnectionSettings MakeConnectionSettings(IContext provider)
        {
            var cfg = provider.GetInstance<IOptions<ElasticSearchOptions>>().Value;
            var uri = new Uri(cfg.Uri);

            var connectionSettings = new ConnectionSettings(uri);

            connectionSettings.DefaultFieldNameInferrer(x => x.ToLower());
            connectionSettings.DisableDirectStreaming(cfg.TraceRequest);

            return connectionSettings;
        }

        public static IElasticClient ElasticClientFactory(IContext provider)
        {
            var settings = provider.GetInstance<ConnectionSettings>();
            return new ElasticClient(settings);
        }
    }
}