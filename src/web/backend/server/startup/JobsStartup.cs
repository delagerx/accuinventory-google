﻿using System;
using backend.application.domain.actions.services;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using backend.application.domain.reports;
using backend.application.domain.reports.index;

namespace backend.startup
{
    public static class JobsStartup
    {
        public static void StartJobs(IServiceProvider provider)
        {
            IndexActionsJob(provider);
        }

        public static void IndexActionsJob(IServiceProvider provider)
        {
            var service = provider.GetService<IIndexActionsService>();
            RecurringJob.AddOrUpdate("IndexActions", () => service.IndexAllActions(), Cron.Daily);
        }

        public static void IndexReportsJob(IServiceProvider provider)
        {
            var service = provider.GetService<IIndexReportOnElasticSearch>();

            RecurringJob.AddOrUpdate("IndexReports", () => service.Execute(), Cron.Daily);
        }
    }
}
