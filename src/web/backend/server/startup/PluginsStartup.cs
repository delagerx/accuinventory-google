﻿using System;
using System.Collections.Generic;
using System.Reflection;
using backend.application.infraestructure.plugins;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace backend.startup
{
    public static class PluginsStartup
    {
        public static Plugin[] LoadAllPlugins(IConfiguration configuration)
        {
            var plugins = configuration.GetSection("plugins").Get<PluginConfiguration[]>();
            var assemblies = new List<Plugin>();
            foreach (var plugin in plugins)
            {
                var assembly = Assembly.Load(plugin.Assembly);
                var startup = GetStartupType(plugin, assembly);
                assemblies.Add(new Plugin()
                {
                    Assembly = assembly,
                    Configuration = plugin,
                    Startup = startup
                });
            }
            return assemblies.ToArray();
        }

        private static IStartup GetStartupType(PluginConfiguration plugin, Assembly assembly)
        {
            try
            {
                var startupType = string.IsNullOrEmpty(plugin.StartupClass)
                    ? assembly.GetType($"{assembly.GetName().Name}.{nameof(Startup)}", false)
                    : assembly.GetType(plugin.StartupClass, false);
                IStartup startup = null;
                if (startupType != null)
                    startup = (IStartup) Activator.CreateInstance(startupType);
                return startup;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void RunAllConfigureServices(Plugin[] plugins, IServiceCollection services)
        {
            foreach (var plugin in plugins)
            {
                plugin.Startup?.ConfigureServices(services);
            }
        }

        public static void RunAllConfigure(Plugin[] plugins, IApplicationBuilder app)
        {
            foreach (var plugin in plugins)
            {
                plugin.Startup?.Configure(app);
            }
        }
    }
}
