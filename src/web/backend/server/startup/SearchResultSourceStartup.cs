﻿using System.Linq;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.services.elastic.configuration;
using plugins.sdk.search.elastic;
using plugins.sdk.search.elastic.interfaces;
using StructureMap;

namespace backend.startup
{
    public static class SearchResultSourceStartup
    {
        
        public static ElasticSearchMapperList SearchMapperListFactory(IContext provider)
        {
            var documents = provider.GetInstance<ConfiguredDocumentsList>();
            var mappers = documents.Configurations.Select(x => SearchMapperFactory(provider, x));
            return new ElasticSearchMapperList(mappers);
        }

        public static ElasticSearchDescriptorsList SearchDescriptorsListFactory(IContext provider)
        {
            var documents = provider.GetInstance<ConfiguredDocumentsList>();
            var searchDescriptors = documents.Configurations.Select(x => SearchDescriptorFactory(provider, x));
            return new ElasticSearchDescriptorsList(searchDescriptors);
        }

        private static ElasticSearchDescriptorInstance SearchDescriptorFactory(IContext provider, IndexedItemDocumentType cfg)
        {
            var descriptor = (IDocumentQueryFactory) provider.GetInstance(cfg.SearchDescriptor);
            var cachedSearchConfiguration = provider.GetInstance<CachedSearchConfiguration>();
            var searchConfigurationCache = cachedSearchConfiguration.TypesSearchConfigurationCache[cfg.ObjectType];
            return new ElasticSearchDescriptorInstance(cfg.IndexName, searchConfigurationCache, descriptor);
        }

        private static ElasticSearchMapperInstance SearchMapperFactory(IContext provider, IndexedItemDocumentType cfg)
        {
            var mapper = (IDocumentActionMapper)provider.GetInstance(cfg.Mapper);
            return new ElasticSearchMapperInstance(cfg.IndexName, cfg.SearchResultType, mapper);
        }
    }
}