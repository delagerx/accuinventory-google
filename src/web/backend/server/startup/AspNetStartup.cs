using System;
using backend.application.infraestructure.middlewares;
﻿using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using backend.application.domain.user.services;
using backend.application.infraestructure.authorization;
using backend.application.infraestructure.plugins;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.IdentityModel.Tokens;

namespace backend.startup
{
    public static class AspNetStartup
    {
        public static void ConfigureServices(IConfiguration configuration, IServiceCollection services, Plugin[] plugins)
        {
            var mvcBuilder = services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
                config.Filters.AddService<LoadLogisticOperationContext>();
            });
            services.AddRouting();
            AddPlugins(mvcBuilder, plugins);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = configuration.GetSection("integrations:login").Value;
                    options.RequireHttpsMetadata = false;
                    options.Events = new JwtBearerEvents()
                    {
                        OnTokenValidated = LoadUserContextFilter.OnActionExecutionAsync
                    };
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidAudiences = new[] {"app.accuinventory"}
                    };
                });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("HasAcessToLogisticOperation",
                    policy => policy.Requirements.Add(new HasAccessToLogisticOperationRequirement()));
            });
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            services.AddMemoryCache();
            services.AddResponseCaching(options =>
            {
                options.UseCaseSensitivePaths = true;
                options.MaximumBodySize = 1024;
            });
        }


        private static void AddPlugins(IMvcBuilder mvcBuilder, Plugin[] plugins)
        {
            foreach (var plugin in plugins)
            {
                mvcBuilder.AddApplicationPart(plugin.Assembly);
            }
        }

        public static void Configure(IConfiguration configuration, IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}