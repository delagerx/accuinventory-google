﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using backend.application.domain.search.entities;
using backend.application.domain.search.services;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.vos;
using backend.application.infraestructure.authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using plugins.sdk.search.elastic;

namespace backend.Controllers
{
    public class SuggestionItemDto
    {
        public string Label { get; set; }
        public object ContextProps { get; set; }
        public IEnumerable<RelatedSuggestionDto> RelatedSuggestions { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public SearchResultType Type { get; set; }

        public string SubType { get; set; }

        public double Probability { get; set; }
        public bool HasPermission { get; set; }
    }

    public class RelatedSuggestionDto
    {
        public string Label { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public SearchResultType Type { get; set; }
        public object ContextProps { get; set; }
        public string SubType { get; set; }
        public bool HasPermission { get; set; }
    }
    public class SearchResponseDto
    {
        public IEnumerable<SuggestionItemDto> Items { get; set; }
        public long Total { get; set; }
    }

    public class SearchController : Controller
    {
        private readonly IGetItemsForQueryService _searchService;

        public SearchController(IGetItemsForQueryService searchService)
        {
            _searchService = searchService;
        }
        [HttpGet("lo/{lo:int}/search")]
        [HttpGet("/search")]
        public async Task<IActionResult> Search( 
            string query,
            CancellationToken cancellationToken,
            [FromQuery] int page = 1,
            [FromQuery] int pageSize = 10)
        {
            if (string.IsNullOrEmpty(query) || page <= 0 || pageSize < 10)
                return BadRequest();
            var searchQuery = new SearchQuery(query, page, pageSize, cancellationToken);
            var result = await _searchService.TryFind(searchQuery)();
            return result.Match(
                Succ: MapResultAndReturnOk,
                Fail: exc => throw exc
            );
        }

        private IActionResult MapResultAndReturnOk(UserSearchResult result)
        {
            return Ok(new SearchResponseDto(){Items = result.Items.Select(MapActionToDto), Total = result.Total});
        }

        public static SuggestionItemDto MapActionToDto(SuggestionItem searchResultItem)
        {
            return new SuggestionItemDto
            {
                Label = searchResultItem.Label,
                Probability = searchResultItem.Probability.Value,
                Type = searchResultItem.Type,
                ContextProps = searchResultItem.ContextProps,
                SubType = searchResultItem.SubType,
                HasPermission = searchResultItem.HasPermission,
                RelatedSuggestions = searchResultItem.RelatedSuggestions.Select(x => new RelatedSuggestionDto()
                {
                    SubType = x.SubType,
                    Label = x.Label,
                    Type = x.Type,
                    ContextProps = x.ContextProps,
                    HasPermission = x.HasPermission
                })
            };
        }
    }
}