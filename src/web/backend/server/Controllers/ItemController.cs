﻿using System;
using System.Threading.Tasks;
using backend.application.domain.item.services;
using backend.application.domain.search.vos;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("lo/{lo}/[controller]")]
    public class ItemController : Controller
    {
        [HttpGet("{type}/{id}")]
        public async Task<IActionResult> Get(string type, string id, [FromServices] IGetIndexedItemService service)
        {
            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            var tryGetItemResult = await service.TryGetItem(type, id)();
            return tryGetItemResult.Match(
                Some: MapResultAndReturnOk,
                None: NotFound,
                Fail: exc => throw exc
            );
        }

        private IActionResult MapResultAndReturnOk(SuggestionItem suggestionItem)
        {
            return Ok(SearchController.MapActionToDto(suggestionItem));
        }
    }
}
