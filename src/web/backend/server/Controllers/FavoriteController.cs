using System.Threading.Tasks;
using backend.application.domain.navigation.vos;
using backend.application.domain.user.entities;
using backend.application.domain.widgets;
using Microsoft.AspNetCore.Mvc;
using plugins.sdk.session;

public class FavoriteDto
{
    public Link Link { get; set; }
}

[Route("[controller]")]
public class WidgetController : Controller
{
    private readonly UserContext _userContext;

    public WidgetController(UserContext userContext)
    {
        _userContext = userContext;
    }
    [HttpPost]
    public async Task<ActionResult> SaveFavorite([FromBody] UserFavoriteWidget userFavoriteWidget,
        [FromServices] IUserFavoriteRepository repository)
    {
        if (string.IsNullOrEmpty(userFavoriteWidget.Id) || string.IsNullOrEmpty(userFavoriteWidget.SerializedProps) ||
            string.IsNullOrEmpty(userFavoriteWidget.Type)) return BadRequest();

        userFavoriteWidget.UserId = _userContext.Id;

        await repository.AddOrUpdate(userFavoriteWidget);

        await repository.CommitAsync();

        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> RemoveFavorite(string id, [FromServices] IUserFavoriteRepository repository)
    {
        if (string.IsNullOrEmpty(id))
            return BadRequest();

        var widget = await repository.GetByPrimaryKeyAsync(id);
        if (widget != null)
            await repository.RemoveAsync(widget);

        await repository.CommitAsync();

        return Ok();
    }

    [HttpGet]
    public async Task<ActionResult> ListFavorites([FromServices] IUserFavoriteRepository repository)
    {
        return Ok(await repository.List(50, _userContext.Id));
    }
}