﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using backend.application.domain.user.services;
using Microsoft.AspNetCore.Mvc;
using plugins.sdk.session;

namespace backend.Controllers
{
    [Route("[controller]")]
    public class UserController : Controller
    {
        [HttpGet(nameof(CurrentUser))]
        public async Task<IActionResult> CurrentUser([FromServices] IGetUserInfoService userInfoService)
        {
            return Ok(await userInfoService.GetUserInfo());
        }

        [HttpGet(nameof(CurrentUser) + "/" + nameof(Notifications))]
        public async Task<IActionResult> Notifications([FromServices] IGetUserNotificationService notificationService)
        {
            return Ok(await notificationService.GetCurrentUserNotifications());
        }

        [HttpPost(nameof(CurrentUser) + "/" + nameof(Notifications) + "/read")]
        public async Task<IActionResult> ReadNotifications([FromServices] IReadNotificationService readNotificationService, [FromBody]ReadNotificationsDto request)
        {
            await readNotificationService.ReadNotifications(request.Notifications);
            return Ok();
        }
    }

    public class ReadNotificationsDto
    {
        public int[] Notifications { get; set; }
    }
}
