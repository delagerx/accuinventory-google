using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.application.domain.navigation.dtos;
using backend.application.domain.navigation.entities;
using backend.application.domain.navigation.persistence;
using backend.application.domain.navigation.vos;
using backend.application.domain.user.entities;
using Microsoft.AspNetCore.Mvc;
using plugins.sdk.session;

namespace backend.Controllers
{
    public class LinkDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public bool Save { get; set; }
    }
    [Route("[controller]")]
    public class NavigationController : Controller
    {
        private readonly UserContext _userContext;

        public NavigationController(UserContext userContext)
        {
            _userContext = userContext;
        }
        [HttpGet("[action]")]
        public async Task<ActionResult> Redirect([FromQuery] LinkDto link, [FromServices] IUserHistoryRepository crudRepository)
        {
            if (string.IsNullOrEmpty(link.Url) || string.IsNullOrEmpty(link.Name)) return BadRequest();
            if (link.Save)
            {
                await crudRepository.AddAsync(new UserHistory(_userContext.Id, new Link()
                {
                    Name = link.Name,
                    IconKey = link.Icon,
                    Url = link.Url
                }));

                await crudRepository.CommitAsync();
            }

            return Redirect(link.Url);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult> UserHistory([FromServices] IUserHistoryRepository repository)
        {
            var defaultPageSize = 8;
            IEnumerable<UserHistoryDto> results =
                await repository.FrequentRegistries(_userContext.Id, defaultPageSize);
            IEnumerable<UserHistoryResponse> mappedResults =
                results.Select(x => new UserHistoryResponse {Hits = x.Hits, IconKey = x.Link.IconKey, Name = x.Link.Name, Url = x.Link.Url});
            return Ok(mappedResults);
        }

        public class UserHistoryResponse
        {
            public string Name { get; set; }
            public string IconKey { get; set; }
            public string Url { get; set; }
            public int Hits { get; set; }
        }
    }
}