﻿using System;
using backend.application.domain.actions.services;
using backend.application.infraestructure.middlewares;
using backend.application.infraestructure.plugins;
using backend.application.persistence;
using backend.startup;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace backend
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options => options.AutomaticAuthentication = true);
            var plugins = PluginsStartup.LoadAllPlugins(_configuration);
            HangFireStartup.ConfigureServices(_configuration, services);
            AspNetStartup.ConfigureServices(_configuration, services, plugins);
            CulturesStartup.ConfigureServices(_configuration, services);
            DatabaseStartup.ConfigureServices(_configuration, services);
            SwaggerStartup.ConfigureServices(_configuration, services);
            PluginsStartup.RunAllConfigureServices(plugins, services);
            return DependencyInjection.ConfigureDependencyInjection(_configuration, services, plugins);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseAuthentication();
                //app.UseResponseCaching();
            }

            var context = app.ApplicationServices.GetService(typeof(Context));
            SwaggerStartup.Configure(_configuration, app, env, loggerFactory);
            CulturesStartup.Configure(_configuration, app, env, loggerFactory);
            AspNetStartup.Configure(_configuration, app, env, loggerFactory);

            HangFireStartup.Configure(_configuration, app, env, loggerFactory);
            JobsStartup.IndexActionsJob(app.ApplicationServices);
            PluginsStartup.RunAllConfigure((Plugin[])app.ApplicationServices.GetService(typeof(Plugin[])), app);
        }
    }
}