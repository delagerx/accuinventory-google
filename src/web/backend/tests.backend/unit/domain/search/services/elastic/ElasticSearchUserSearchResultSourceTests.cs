﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using backend.application.domain.search.services;
using backend.application.domain.search.services.elastic;
using backend.application.domain.search.vos;
using Moq;
using Nest;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit2;
using tests.backend.infraestructure;
using Xunit;

namespace tests.backend.unit.domain.search.services.elastic
{
    public class ElasticSearchUserSearchResultSourceTests
    {
        [Theory, AutoMoqData]
        public async Task ShouldCallSearcherAndMapper(
            string query,
            CancellationToken cancellationToken,
            IList<SuggestionItem> mappingResult,
            ISearchResponse<object> searchResponse,
            [Frozen]Mock<IQueryIndexedItemsService> searcher,
            [Frozen]Mock<ISearchIndexedItensResultMapperService> mapper,
            GetItemsForQueryService sut,
            Fixture fixture)
        {
            searcher.Setup(x => x.Query(query, 0, 10, cancellationToken)).Returns(Task.FromResult(searchResponse));
            mapper.Setup(x => x.MapElasticResult(searchResponse)).Returns(Task.FromResult(mappingResult));

            var result = await sut.TryFind(query, 0, 10, cancellationToken);

            searcher.Verify(x => x.Query(query, 0, 10, cancellationToken));
            mapper.Verify(x => x.MapElasticResult(searchResponse));
            Assert.Equal(mappingResult, result.Items);
            Assert.Equal(searchResponse.Total, result.Total);
        }
    }
}

