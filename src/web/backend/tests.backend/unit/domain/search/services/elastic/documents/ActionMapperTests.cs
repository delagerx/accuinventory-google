﻿using System.Collections.Generic;
using backend.application.domain.actions.entities;
using backend.application.domain.actions.search;
using Newtonsoft.Json;
using plugins.sdk.utils;
using Xunit;

namespace tests.backend.domain.search.services.elastic.documents
{
    public class ActionMapperTests
    {
        private readonly string _serializedAction;
        private const string EnUs = "enUs";
        private const string PtBr = "ptBr";

        public ActionMapperTests()
        {
            var translations = new Dictionary<string, ActionTranslation>()
            {
                {EnUs, new ActionTranslation(EnUs)},
                {PtBr, new ActionTranslation(PtBr)}
            };
            var indexedAction = new IndexeableAction("teste", "teste", translations, new ActionPermission[]{}, ActionType.ExternalLink);
            _serializedAction = JsonConvert.SerializeObject(indexedAction);
        }
        [Fact]
        public void ShouldTranslate()
        {
            var enUsMapper = new ActionDocumentMapper(new Culture(EnUs));
            var enUsResult = enUsMapper.Map(_serializedAction);
            Assert.Equal(enUsResult.Label, EnUs);
            var ptBrMapper = new ActionDocumentMapper(new Culture(PtBr));
            var ptBrResult = ptBrMapper.Map(_serializedAction);
            Assert.Equal(ptBrResult.Label, PtBr);
        }

        [Fact]
        public void ShouldDessarializeTypeAndUrl()
        {
            var mapper = new ActionDocumentMapper(new Culture(EnUs));
            var result = mapper.Map(_serializedAction);
            Assert.Equal(result.SubType, ActionType.ExternalLink.ToString());
            Assert.Equal(result.ContextProps, "teste");
        }
    }
}
