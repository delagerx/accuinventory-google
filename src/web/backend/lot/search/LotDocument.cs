﻿using System.Collections.Generic;
using plugins.sdk.search.elastic.attributes;

namespace lot.search
{
    public class LotDocument
    {
        [ElasticIgnoreOnSearch]
        public string Id { get; set; }
        [ElasticBoostOnSearch(5)]
        public string Name { get; set; }

        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }

        public string ProductName { get; set; }
    }
}
