﻿using plugins.sdk.search.elastic;

namespace lot.search
{
    public class LotDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var lot = ElasticDocumentSerializer.DeserializeInfos<LotDocument>(serializedAction);

            return new MappingResult($"{lot.Name} - {lot.ProductName}", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new { id = lot.Id, name = lot.Name });
        }
    }
}
