﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Nest;
using plugins.sdk.database;

namespace lot.widgets.lotDetail
{
    public interface ILotRepository
    {
        Task<Lot> GetLotById(string lotId);
    }

    public class WmsLot
    {
        public string Nome { get; set; }
        public string DescricaoProduto { get; set; }
        public string IdProduto { get; set; }
        public string DigitoProduto { get; set; }
        public string Embalagem { get; set; }
        public string Situacao { get; set; }
        public DateTime DataValidade { get; set; }
        public DateTime DataFabricacao { get; set; }
        public DateTime DataCadastro { get; set; }
    }

    public class WmsLotStock
    {
        public double Estoque { get; set; }
        public string Endereco { get; set; }
        public string IdEndereco { get; set; }
        public string NumeroLpn { get; set; }
        public string IdLpn { get; set; }
        public string SituacaoEndereco { get; set; }
    }
    
    public class WmsLotRepository : ILotRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;

        public WmsLotRepository(IWmsDatabaseConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<Lot> GetLotById(string lotId)
        {
            using (var connection = _connectionFactory.Make())
            {
                var lot = (await connection.QueryAsync<WmsLot>(LotQueryBuilder.SelectLotQuery, new { lotId = lotId })).FirstOrDefault();
                var lotStock = (await connection.QueryAsync<WmsLotStock>(LotQueryBuilder.SelectLotStockQuery, new { lotId = lotId })).ToList();
                if (lot == null)
                {
                    return null;
                }
                return new Lot {
                    ProductDescription = lot.DescricaoProduto,
                    ProductId = lot.IdProduto,
                    ManufacturedDate = lot.DataFabricacao,
                    RegistrationDate = lot.DataCadastro,
                    ShelfLife = lot.DataValidade,
                    Name = lot.Nome,
                    Packing = lot.Embalagem,
                    Status = lot.Situacao,
                    Stocks = lotStock.Select(x => new LotStock()
                    {
                        Address = x.Endereco,
                        AddressId = x.IdEndereco,
                        LpnId = x.IdLpn,
                        LpnNumber = x.NumeroLpn,
                        Stock = x.Estoque,
                        Status = x.SituacaoEndereco,
                    })
                };
            }
        }
    }
}
