﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace lot.widgets.lotDetail
{
    [Route("[controller]")]
    public class LotController : Controller
    {
        private readonly ILotRepository _lotRepository;

        public LotController(ILotRepository lotRepository)
        {
            _lotRepository = lotRepository;
        }
        [HttpGet("{id}/info")]
        public async Task<IActionResult> GetLotInfo(string id)
        {
            return Ok(await _lotRepository.GetLotById(id));
        }
    }
}
