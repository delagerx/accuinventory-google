﻿using System;
using System.Collections.Generic;

namespace lot.widgets.lotDetail
{
    public class Lot
    {
        public string Name { get; set; }
        public string ProductId { get; set; }
        public string ProductDescription { get; set; }
        public string Packing { get; set; }
        public string Status { get; set; }

        public DateTime ShelfLife { get; set; }
        public DateTime ManufacturedDate { get; set; }
        public DateTime RegistrationDate { get; set; }
        public IEnumerable<LotStock> Stocks { get; set; }
    }

    public class LotStock
    {
        public double Stock { get; set; }
        public string AddressId { get; set; }
        public string Address { get; set; }
        public string LpnId { get; set; }
        public string LpnNumber { get; set; }
        public string Status { get;set; }
    }
}