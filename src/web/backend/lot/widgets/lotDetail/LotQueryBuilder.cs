﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lot.widgets.lotDetail
{
    public class LotQueryBuilder
    {
	    public static string SelectLotQuery =>
			@"
				select 
					l.lote as Nome, 
					l.cod_produto as IdProduto, 
					p.descricao as DescricaoProduto, 
					p.digito AS DigitoProduto, 
					l.validade AS DataValidade ,
					l.data_fabricacao as DataFabricacao,
					l.data_cadastro as DataCadastro,
					l.emb_compra as Embalagem,
					dsl.descricao as Situacao
				from lote l 
				inner join produto p on p.cod_produto = l.cod_produto 
				inner join def_situacao_lote dsl on dsl.cod_situacao = l.cod_situacao
				where l.id_lote = @lotId
			";

	    public static string SelectLotStockQuery =>
			@"
				select lel.estoque as Estoque, e.endereco_completo as Endereco, e.id_endereco as IdEndereco, lpn.numero as NumeroLpn, lpn.id_LPN as IdLpn, dse.descricao as SituacaoEndereco
				from lote l
				inner join Lote_estoque_LPN lel on lel.id_lote = l.id_lote
				inner join lpn on lpn.id_LPN = lel.id_LPN
				inner join endereco e on e.id_endereco = lpn.id_endereco
				inner join def_situacao_endereco dse on dse.cod_situacao_endereco = e.cod_situacao_endereco
				where l.id_lote = @lotId
			";

    }
}
