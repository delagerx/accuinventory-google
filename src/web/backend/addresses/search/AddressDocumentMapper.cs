using plugins.sdk.search.elastic;

namespace addresses.search
{
    public class AddressDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var address = ElasticDocumentSerializer.DeserializeInfos<AddressDocument>(serializedAction);

            return new MappingResult($"{address.CompleteAddress}", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new {id = address.Id, name = address.CompleteAddress});
        }
    }
}