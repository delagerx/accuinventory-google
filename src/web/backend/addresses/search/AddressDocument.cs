﻿using plugins.sdk.search.elastic.attributes;

namespace addresses.search
{
    public class AddressDocument
    {
        public string Id { get; set; }
        [ElasticBoostOnSearch(2)]
        public string Classification { get; set; }

        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string CompleteAddress { get; set; }
        public string StorageType { get; set; }

        [ElasticPrefixedSearchField]
        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }
    }
}