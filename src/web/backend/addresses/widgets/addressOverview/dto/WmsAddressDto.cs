﻿using System;
using System.Collections.Generic;
using System.Text;

namespace addresses.widgets.addressOverview.dto
{
    public class WmsAddressDto
    {
        public string Id { get; set; }

        public string Classificacao { get; set; }

        public string EnderecoCompleto { get; set; }

        public string Situacao { get; set; }

        public string TipoAcesso { get; set; }

        public string TipoArmazenamento { get; set; }

        public string TipoVolume { get; set; }
    }
}
