﻿using System;
using System.Collections.Generic;
using System.Text;

namespace addresses.widgets.addressOverview.dto
{
    public class WmsAddressStockDto
    {
        public string IdProduto { get; set; }

        public string DescricaoProduto { get; set; }

        public string DigitoProduto { get; set; }

        public string IdLote { get; set; }

        public string Lote { get; set; }

        public double Quantidade { get; set; }
    }
}
