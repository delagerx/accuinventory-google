﻿using System;
using System.Collections.Generic;
using System.Text;

namespace addresses.widgets.addressOverview
{
    public class AddressOverviewQueryBuilder
    {
        public string Id { get; }

        public int? AddressStocksMaxResults { get; }

        public string SelectAddressQuery
        {
            get
            {
                return 
                    $"SELECT e.id_endereco AS Id, lc.descricao AS Classificacao, e.endereco_completo AS EnderecoCompleto, se.descricao AS Situacao, acesso.descricao AS TipoAcesso, tae.descricao AS TipoArmazenamento, tve.descricao AS TipoVolume " +
                    $"FROM endereco e " +
                    $"LEFT JOIN def_lote_classificacao lc ON lc.id_lote_classificacao = e.id_lote_classificacao " +
                    $"LEFT JOIN def_situacao_endereco se ON se.cod_situacao_endereco = e.cod_situacao_endereco " +
                    $"LEFT JOIN def_tipo_acesso_endereco acesso ON acesso.id_tipo_acesso_endereco = e.id_tipo_acesso_endereco " +
                    $"LEFT JOIN def_tipo_armazenamento_endereco tae ON tae.id_tipo_armazenamento_endereco = e.id_tipo_Armazenamento " +
                    $"LEFT JOIN def_tipo_volume_endereco tve ON tve.id_tipo_volume_endereco = e.id_tipo_volume " +
                    $"WHERE e.id_endereco = {Id} ";
            }
        }

        public string SelectAddressStocksQuery
        {
            get
            {
                string limitExpression = AddressStocksMaxResults.HasValue ? $"TOP {AddressStocksMaxResults}" : string.Empty;

                return
                    $"SELECT {limitExpression} p.cod_produto AS IdProduto, p.digito AS DigitoProduto, p.descricao AS DescricaoProduto, l.id_lote AS IdLote, l.lote AS Lote, le.estoque AS Quantidade " +
                    $"FROM vw_lote_estoque le " +
                    $"JOIN produto p ON p.cod_produto = le.cod_produto " +
                    $"JOIN lote l ON l.id_lote = le.id_lote " +
                    $"WHERE le.id_endereco = {Id} " +
                    $"ORDER BY p.descricao ";
            }
        }


        public AddressOverviewQueryBuilder(string id)
        {
            Id = id;
        }

        public AddressOverviewQueryBuilder(string id, int? addressStocksMaxResults) : this(id)
        {
            AddressStocksMaxResults = addressStocksMaxResults;
        }
    }
}
