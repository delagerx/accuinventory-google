﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace addresses.widgets.addressOverview
{
    [Route("[controller]")]
    public class AddressOverviewController : Controller
    {
        private readonly IAddressOverviewWmsRepository _wmsRepository;

        public AddressOverviewController(IAddressOverviewWmsRepository wmsRepository)
        {
            _wmsRepository = wmsRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAddressOverview(string id)
        {
            AddressOverviewQueryResponse address = await _wmsRepository.FetchAddressAsync(id);

            if (address == null)
                return NotFound();

            return Ok(address);
        }
    }
}
