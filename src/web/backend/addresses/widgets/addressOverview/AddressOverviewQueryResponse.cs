﻿using addresses.widgets.addressOverview.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace addresses.widgets.addressOverview
{
    public class AddressOverviewQueryResponse
    {
        public Address Address { get; set; }

        public IEnumerable<AddressStock> AddressStocks { get; set; } = new List<AddressStock>();

        public double StorageLevel { get; set; }

        public AddressOverviewQueryResponse()
        {
        }

        public AddressOverviewQueryResponse(WmsAddressDto wmsAddress, IEnumerable<WmsAddressStockDto> wmsAddressStocks) : this()
        {
            Address = new Address
            {
                Id = wmsAddress.Id,
                AccessType = wmsAddress.TipoAcesso,
                CompleteAddress = wmsAddress.EnderecoCompleto,
                Classification = wmsAddress.Classificacao,
                Status = wmsAddress.Situacao,
                StorageType = wmsAddress.TipoArmazenamento,
                VolumeType = wmsAddress.TipoVolume
            };

            AddressStocks = wmsAddressStocks.Select(addressStock =>
                new AddressStock
                {
                    ProductDescription = addressStock.DescricaoProduto,
                    ProductId = addressStock.IdProduto,
                    ProductVerifyingDigit = addressStock.DigitoProduto,
                    LotCode = addressStock.Lote,
                    LotId = addressStock.IdLote,
                    Quantity = addressStock.Quantidade
                }
            );
        }
    }

    public class Address
    {
        public string Id { get; set; }

        public string AccessType { get; set; }

        public string Classification { get; set; }

        public string CompleteAddress { get; set; }

        public string Status { get; set; }

        public string StorageType { get; set; }

        public string VolumeType { get; set; }
    }

    public class AddressStock
    {
        public string ProductId { get; set; }

        public string ProductDescription { get; set; }

        public string ProductVerifyingDigit { get; set; }

        public string LotId { get; set; }

        public string LotCode { get; set; }

        public double Quantity { get; set; }
    }
}
