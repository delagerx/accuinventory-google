﻿using addresses.widgets.addressOverview;
using Microsoft.Extensions.Logging;
using plugins.sdk.database;
using System;
using System.Threading.Tasks;
using System.Linq;
using Dapper;
using System.Collections.Generic;
using addresses.widgets.addressOverview.dto;

namespace addresses.widgets.addressOverview
{
    public interface IAddressOverviewWmsRepository
    {
        Task<AddressOverviewQueryResponse> FetchAddressAsync(string id, int? addressStocksMaxResults = null);
    }

    public class AddressOverviewWmsRepository : IAddressOverviewWmsRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;
        private readonly ILogger<AddressOverviewWmsRepository> _logger;

        public AddressOverviewWmsRepository(IWmsDatabaseConnectionFactory connectionFactory, ILogger<AddressOverviewWmsRepository> logger)
        {
            _connectionFactory = connectionFactory;
            _logger = logger;
        }

        public async Task<AddressOverviewQueryResponse> FetchAddressAsync(string id, int? addressStocksMaxResults = null)
        {
            try
            {
                using (var connection = _connectionFactory.Make())
                {
                    connection.Open();

                    AddressOverviewQueryBuilder queryBuilder = new AddressOverviewQueryBuilder(id, addressStocksMaxResults);

                    WmsAddressDto selectAddressQueryResult = (await connection.QueryAsync<WmsAddressDto>(queryBuilder.SelectAddressQuery)).FirstOrDefault();
                    IEnumerable<WmsAddressStockDto> selectProductsStockQueryResult = (await connection.QueryAsync<WmsAddressStockDto>(queryBuilder.SelectAddressStocksQuery));

                    if (selectAddressQueryResult == null)
                        return null;

                    return new AddressOverviewQueryResponse(selectAddressQueryResult, selectProductsStockQueryResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    }
}
