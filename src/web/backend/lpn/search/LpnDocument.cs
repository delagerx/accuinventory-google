﻿using System;
using System.Collections.Generic;
using System.Text;
using plugins.sdk.search.elastic.attributes;

namespace lpn.search
{
    public class LpnDocument
    {
        public string Id { get; set; }
        [ElasticBoostOnSearch(5)]
        [ElasticPrefixedSearchField]
        public string Number { get; set; }
        [ElasticBoostOnSearch(3)]
        [ElasticPrefixedSearchField]
        public string PrefixedField { get; set; }

        [ElasticPrefixedSearchField]
        [ElasticBoostOnSearch(5)]
        public string ObjectType { get; set; }
    }
}
