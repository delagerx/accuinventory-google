﻿using plugins.sdk.search.elastic;

namespace lpn.search
{
    public class LpnDocumentMapper : IDocumentActionMapper
    {
        public MappingResult Map(string serializedAction)
        {
            var lpn = ElasticDocumentSerializer.DeserializeInfos<LpnDocument>(serializedAction);

            return new MappingResult($"{lpn.Number}", SearchResultSubType.None, LogisticOperationDocument.AllLOs, new { id = lpn.Id, name = lpn.Number });
        }
    }
}
