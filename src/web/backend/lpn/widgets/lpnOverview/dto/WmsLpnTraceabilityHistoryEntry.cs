﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lpn.widgets.lpnOverview.dto
{
    public class WmsLpnTraceabilityHistoryEntry
    {
        public string LpnOrigemId { get; set; }
        public string LpnDestinoId { get; set; }
        public string Responsavel { get; set; }
        public DateTime Data { get; set; }
        public double QtdMovimento { get; set; }
    }
}
