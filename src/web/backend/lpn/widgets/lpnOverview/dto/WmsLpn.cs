﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lpn.widgets.lpnOverview.dto
{
    public class WmsLpn
    {
        public string Id { get; set; }

        public string Endereco { get; set; }

        public DateTime DataCadastro { get; set; }

        public string IdEndereco { get; set; }

        public string IdLpnPai { get; set; }

        public string Numero { get; set; }

        public string NumeroLpnPai { get; set; }

        public string Situacao { get; set; }

        public string Tipo { get; set; }

        public string OperacaoLogistica { get; set; }
    }
}
