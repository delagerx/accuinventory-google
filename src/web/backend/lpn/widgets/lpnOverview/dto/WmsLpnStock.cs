﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lpn.widgets.lpnOverview.dto
{
    public class WmsLpnStock
    {
        public string IdProduto { get; set; }

        public string IdLote { get; set; }

        public string Lote { get; set; }

        public string DescricaoProduto { get; set; }

        public string DigitoProduto { get; set; }

        public double Quantidade { get; set; }

        public DateTime Validade { get; set; }
    }
}
