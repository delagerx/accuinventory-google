﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace lpn.widgets.lpnOverview
{
    [Route("[controller]")]
    public class LpnOverviewController : Controller
    {
        private readonly ILpnWmsRepository _lpnOverviewRepository;

        public LpnOverviewController(ILpnWmsRepository addressOverviewRepository)
        {
            _lpnOverviewRepository = addressOverviewRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetLpnOverview(string id)
        {
            var lpn = await _lpnOverviewRepository.GetByIdAsync(id);

            if (lpn == null)
                return NotFound();

            return Ok(lpn);
        }
    }
}
