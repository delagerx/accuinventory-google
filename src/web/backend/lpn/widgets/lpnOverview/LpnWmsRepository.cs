﻿using Dapper;
using lpn.widgets.lpnOverview.dto;
using Microsoft.Extensions.Logging;
using plugins.sdk.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lpn.widgets.lpnOverview
{
    public class LpnWmsRepository : ILpnWmsRepository
    {
        private readonly IWmsDatabaseConnectionFactory _connectionFactory;
        private readonly ILogger<LpnWmsRepository> _logger;

        public LpnWmsRepository(IWmsDatabaseConnectionFactory connectionFactory, ILogger<LpnWmsRepository> logger)
        {
            _connectionFactory = connectionFactory;
            _logger = logger;
        }

        public async Task<LpnOverviewQueryResponse> GetByIdAsync(string id)
        {
            try
            {
                using (var connection = _connectionFactory.Make())
                {
                    connection.Open();

                    WmsLpn selectLpnQueryResult = (await connection.QueryAsync<WmsLpn>(WmsLpnSqlQuery.SELECT_LPN, new { id })).FirstOrDefault();
                    IEnumerable<WmsLpnStock> selectLpnStocksQueryResult = (await connection.QueryAsync<WmsLpnStock>(WmsLpnSqlQuery.SELECT_LPN_STOCKS, new { id }));
                    var ordersCount = await connection.ExecuteScalarAsync<int>(WmsLpnSqlQuery.SELECT_RELATED_MOVORDERS, new {id});
                    var lpnTraceabilityQueryResult =
                        await connection.QueryAsync<WmsLpnTraceabilityHistoryEntry>(WmsLpnSqlQuery.SELECT_LPN_TRACEABILITY_HISTORY,
                            new {id});

                    if (selectLpnQueryResult == null)
                        return null;

                    return new LpnOverviewQueryResponse
                    {
                        Lpn = new Lpn
                        {
                            Id = selectLpnQueryResult.Id,
                            Address = selectLpnQueryResult.Endereco,
                            AddressId = selectLpnQueryResult.IdEndereco,
                            CreatedDate = selectLpnQueryResult.DataCadastro,
                            Number = selectLpnQueryResult.Numero,
                            ParentLpnId = selectLpnQueryResult.IdLpnPai,
                            ParentLpnNumber = selectLpnQueryResult.NumeroLpnPai,                            
                            Status = selectLpnQueryResult.Situacao,
                            Type = selectLpnQueryResult.Tipo,
                            LogisticOperation = selectLpnQueryResult.OperacaoLogistica,
                            ActiveMovementOrders = ordersCount,
                        },
                        LpnStocks = selectLpnStocksQueryResult.Select(lpnStock =>
                            new LpnStock
                            {
                                ProductDescription = lpnStock.DescricaoProduto,
                                ProductId = lpnStock.IdProduto,
                                ProductDigit = lpnStock.DigitoProduto,
                                LotCode = lpnStock.Lote,
                                LotId = lpnStock.IdLote,
                                Quantity = lpnStock.Quantidade,
                                ShelfLife = lpnStock.Validade
                            }),
                        LpnTraceabilityHistory = lpnTraceabilityQueryResult.Select(historyEntry => 
                            new LpnTraceabilityHistoryEntry
                            {
                                User = historyEntry.Responsavel,
                                Timestamp = historyEntry.Data,
                                FlowDirection = historyEntry.LpnDestinoId == historyEntry.LpnOrigemId 
                                    ? LpnFlowDirection.Stagnant 
                                    : historyEntry.LpnOrigemId == id 
                                        ? LpnFlowDirection.Source
                                        : LpnFlowDirection.Destiny,
                                MovementQuantity = historyEntry.QtdMovimento
                            })
                    };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }
    }

    public interface ILpnWmsRepository
    {
        Task<LpnOverviewQueryResponse> GetByIdAsync(string id);
    }

    public class WmsLpnSqlQuery
    {
        public static string SELECT_LPN =
            $"SELECT l.id_LPN AS Id, l.numero AS Numero, l.data_cadastro AS DataCadastro, e.id_endereco AS IdEndereco, e.endereco_completo AS Endereco, dtl.descricao AS Tipo, dsl.descricao AS Situacao, lpai.id_LPN AS IdLpnPai, lpai.numero AS NumeroLpnPai, ent.fantasia as operacaoLogistica " +
            $"FROM LPN l " +
            $"LEFT JOIN endereco e ON e.id_endereco = l.id_endereco " +
            $"LEFT JOIN def_tipo_lpn dtl ON dtl.id_def_tipo_lpn = l.id_def_tipo_lpn " +
            $"LEFT JOIN def_situacao_lpn dsl ON dsl.id_def_situacao_lpn = l.id_def_situacao_lpn " +
            $"LEFT JOIN LPN lpai ON lpai.id_LPN = l.id_LPN_pai " +
            $"LEFT JOIN Entidade ent ON ent.cod_entidade = l.cod_operacao_logistica " +
            $"WHERE l.id_LPN = @id;";

        public static string SELECT_LPN_STOCKS =
            $"SELECT p.cod_produto AS IdProduto, p.descricao AS DescricaoProduto, p.digito AS DigitoProduto, l.id_lote AS IdLote, l.lote AS Lote, lel.estoque AS Quantidade, l.validade AS Validade " +
            $"FROM Lote_estoque_LPN lel  " +
            $"JOIN lote l ON l.id_lote = lel.id_lote  " +
            $"JOIN produto p ON p.cod_produto = l.cod_produto " +
            $"WHERE lel.id_LPN = @id " +
            $"ORDER BY p.descricao ";

        public static string SELECT_RELATED_MOVORDERS =
            $"SELECT COUNT(*) FROM ORDEM_MOV_ITEM WHERE ID_LPN = @id";

        public static string SELECT_LPN_TRACEABILITY_HISTORY = 
            $"SELECT id_LPN_Origem as LpnOrigemId, id_LPN_destino as LpnDestinoId, qtd_movimento as QtdMovimento, responsavel as Responsavel, data_movimento as Data " +
            $"FROM lote_estoque_LPN_movimento " + 
            $"WHERE id_LPN_origem = @id or id_LPN_destino = @id";
    }
}
