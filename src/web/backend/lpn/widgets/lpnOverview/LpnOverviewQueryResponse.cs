﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace lpn.widgets.lpnOverview
{
    public class LpnOverviewQueryResponse
    {
        public Lpn Lpn { get; set; }

        public IEnumerable<LpnStock> LpnStocks { get; set; } = new List<LpnStock>();

        public IEnumerable<LpnTraceabilityHistoryEntry> LpnTraceabilityHistory { get; set; } = new List<LpnTraceabilityHistoryEntry>();
    }

    public class Lpn
    {
        public string Id { get; set; }

        public string Address { get; set; }

        public string AddressId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string LogisticOperation { get; set; }

        public string Number { get; set; }

        public string ParentLpnId { get; set; }

        public string ParentLpnNumber { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public int ActiveMovementOrders { get; set; }
    }

    public class LpnStock
    {
        public string ProductId { get; set; }

        public string ProductDescription { get; set; }

        public string ProductDigit { get; set; }

        public string LotId { get; set; }

        public string LotCode { get; set; }

        public double Quantity { get; set; }

        public DateTime ShelfLife { get; set; }
    }

    public class LpnTraceabilityHistoryEntry
    {
        public DateTime Timestamp { get; set; }
        public string User { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public LpnFlowDirection FlowDirection { get; set; }
        public double MovementQuantity { get; set; }
    }

    public enum LpnFlowDirection
    {
        Source,
        Destiny,
        Stagnant,
    }
}
