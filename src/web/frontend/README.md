Subtituir o conteúdo node_modules/bin/webpack.cmd com as seguintes linhas. Essa alteraçao é necessária para aumentar a disponibilidade de memória para o webpack, permitindo assim o build com minificação que em alguns casos chega a 2gb de memória (Investigar).

@if EXIST "%~dp0\node.exe" (
"%~dp0\node.exe --max_old_space_size=4096 " "%~dp0..\webpack\bin\webpack.js" %*
) ELSE (
@SETLOCAL
@set PATHEXT=%PATHEXT:;.JS;=;%
node --max_old_space_size=4096 "%~dp0..\webpack\bin\webpack.js" %*
)