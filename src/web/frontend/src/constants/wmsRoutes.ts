import settings from 'settings';
export interface WmsRouteLocale {
  WMS_ROUTE_SETTINGS: string;
  WMS_ROUTE_NOTIFICATION: string;
  WMS_ROUTE_LOGIN: string;
  WMS_ROUTE_SEARCH_MOVEMENT_ORDER: string;
  WMS_ROUTE_EDIT_PRODUCT: string;
}

export interface WmsRoute {
  url: string;
  name: keyof WmsRouteLocale;
}

export interface WmsRoutes {
  settings: WmsRoute;
  notifications: WmsRoute;
  login: WmsRoute;
  searchMovementOrder: WmsRoute;
  editProduct: WmsRoute;
}

export const wmsRoutes: WmsRoutes = {
  settings: {
    url: `${settings.wmsUrl}#comm=openSettings`,
    name: 'WMS_ROUTE_SETTINGS',
  },
  notifications: {
    url: `${settings.wmsUrl}/Notificacao`,
    name: 'WMS_ROUTE_NOTIFICATION',
  },
  login: {
    url: `${settings.wmsHome}`,
    name: 'WMS_ROUTE_LOGIN',
  },
  searchMovementOrder: {
    url: `${settings.wmsUrl}/OrdemMovimentacao?(lpn=:lpn)#comm=filter`,
    name: 'WMS_ROUTE_SEARCH_MOVEMENT_ORDER',
  },
  editProduct: {
    url: `${settings.wmsUrl}/Produto/Detalhes/:id`,
    name: 'WMS_ROUTE_EDIT_PRODUCT',
  },
};
