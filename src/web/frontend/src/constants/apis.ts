import settings from 'settings';
const logisticOperationUrl = `${settings.baseApiUrl}/lo/:lo`;
export const SearchApi = `${logisticOperationUrl}/search`;
export const GetItemApi = `${logisticOperationUrl}/item/:type/:id`;
export const RedirectApi = `${settings.baseApiUrl}/navigation/redirect`;
export const UserHistoryApi = `${settings.baseApiUrl}/navigation/userHistory`;
export const WidgetsApi = `${settings.baseApiUrl}/widget`;
export const CurrentUserApi = `${settings.baseApiUrl}/user/currentUser`;
export const CurrentUserNotificationsApi = `${settings.baseApiUrl}/user/currentUser/notifications`;
export const ReadNotificationsApi = `${settings.baseApiUrl}/user/currentUser/notifications/read`;

export const baseApiUrl = settings.baseApiUrl;
