import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './infraestructure/redux/store';
import configureRealize from 'config/realize.config';
import { AppRouter } from 'application/router/container/router';
import { createHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import { configureHttpClient } from 'services/httpClient';

import './assets/sass/bundle.scss';
import '../node_modules/realizejs/src/css/styles.scss';
import 'typeface-roboto';
import {OidcProvider, processSilentRenew} from 'redux-oidc';
import {userManager} from './services/login';

const history = createHistory();
const store = configureStore(history);
configureRealize(store);
const syncedHistory = syncHistoryWithStore(history, store);
configureHttpClient(store);

processSilentRenew();

ReactDOM.render(
    <Provider store={store}>
        <OidcProvider store={store} userManager={userManager}>
          <AppRouter history={syncedHistory}/>
        </OidcProvider>
    </Provider>,
  document.getElementById('root'),
);
