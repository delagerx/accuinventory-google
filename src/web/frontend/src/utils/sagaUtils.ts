import axios, { CancelTokenStatic } from 'axios';
import { CANCEL } from 'redux-saga';

export const makeCancelableCall = (decoratedFun: (cancelToken: any) => Promise<any>) => {
    return () => {
        const cancelToken = axios.CancelToken;
        const source = cancelToken.source();
        const promise = decoratedFun(source.token);
        promise[CANCEL] = () => source.cancel('Saga cancelled');
        return promise;
    };
};
