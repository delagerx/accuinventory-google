import { Action } from 'redux-actions';
import { Dispatch } from 'react-redux';

export interface EventDefs<TEvent> {
    name: string;
    creator: (payload: TEvent) => Action<TEvent>;
    dispatcher: (dispatch: Dispatch<any>) => (payload: TEvent) => void;
}

export function createEventDefs<TEvent>(eventType: string): EventDefs<TEvent> {
    const creator = (payload: TEvent) => ({ type: eventType, payload });
    const dispatcher = (dispatch) => (payload: TEvent) => dispatch(creator(payload));
    return {
      creator: creator,
      dispatcher: dispatcher,
      name: eventType,
    };
}
