export function formatString(str: string, data: any) {
    data = data || {};
    Object.keys(data).forEach((key: string) => {
        if (!key || key === '') {
          return;
        }
        const stringToReplace = ':' + key;
        str = str.replace(stringToReplace, data[key]);
    });
    str = removeUnusedParameters(str);
    str = removeOptionalParametersParenthesis(str);
    return str;
}

function removeUnusedParameters(str: string) {
  return str.replace(/:[a-z]+/, '');
}

function removeOptionalParametersParenthesis(str: string) {
  str = str.replace('(', '');
  str = str.replace(')', '');
  return str;
}
