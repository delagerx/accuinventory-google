import { Errors } from '../services/locales/errors';
import { call, cancelled, put, takeLatest } from 'redux-saga/effects';
import { EventDefs, createEventDefs } from './eventCreator';
import { Action } from 'redux-actions';
import { delay } from 'redux-saga';
import { translateError } from 'services/locales/i18n';
import { changeRoute } from 'application/navigation/services/changeRouteService';
import {ErrorEvent} from '../application/application/errors/events/errorEvent';
export interface ApiEventDefs<TEvent, TEventSuccess> {
    com: EventDefs<TEvent>;
    req: EventDefs<TEvent>;
    succ: EventDefs<TEventSuccess>;
    fail: EventDefs<ErrorEvent>;
}

export interface ApiOptions<TEvent> {
    apiCall: ApiCall<TEvent>;
    debounceTime?: number;
    redirectOnError?: boolean;
    redirectToCorrespondingResponse?: boolean;
}

export interface SagaEventDefs<TEvent, TEventSuccess> extends ApiEventDefs<TEvent, TEventSuccess> {
    saga: any;
}

export type ApiCall<TEvent> = (payload: TEvent) => Iterator<any>;

export function apiReduxBoilerplateCreator<TEvent, TEventSuccess>(
  eventPrefix: string,
  apiOptions: ApiOptions<TEvent>): SagaEventDefs<TEvent, TEventSuccess> {
    const command = createEventDefs<TEvent>(`command/${eventPrefix}`);
    const failedDefs = createEventDefs<ErrorEvent>(`${eventPrefix}_FAILED`);
    const successDefs = createEventDefs<TEventSuccess>(`${eventPrefix}_SUCCEEDED`);
    const requestedDefs = createEventDefs<TEvent>(`${eventPrefix}_REQUESTED`);
    const defs = {
        com: command,
        req: requestedDefs,
        succ: successDefs,
        fail: failedDefs,
    };
    const saga = createSaga<TEvent, TEventSuccess>(apiOptions, defs);
    return {
        ...defs,
        saga: saga,
    };
}

function makeDebounce(time: number) {
    return delay(time);
}

function createSaga<TEvent, TEventSuccess>(
    apiOptions: ApiOptions<TEvent>,
    defs: ApiEventDefs<TEvent, TEventSuccess>) {
    function* saga(action: Action<TEvent>) {
        const payload = action.payload;
        try {
            if (apiOptions.debounceTime) {
                yield makeDebounce(apiOptions.debounceTime);
            }
            yield put(defs.req.creator(payload));
            const result = yield call(() => apiOptions.apiCall(payload));
            yield put(defs.succ.creator(result));
        } catch (e) {
            yield* handleApiError<TEvent, TEventSuccess>(e, defs, apiOptions);
        }
        if (yield cancelled()) {
            yield put<Action<void>>({type: defs.fail.name});
        }
    }
    return function*() {
        yield takeLatest(defs.com.name, saga);
    };
}
function* handleApiError<TEvent, TEventSuccess>(
  e: any,
  defs: ApiEventDefs<TEvent, TEventSuccess>,
  apiOptions: ApiOptions<TEvent>) {

  const message: keyof Errors = 'UNKNOW_ERROR';
  yield put(defs.fail.creator({ error: translateError(message), exception: e }));
  const isUnhandledException = !e.response || (e.response && !e.response.status);
  const shouldRedirectOnAnyError = apiOptions.redirectOnError || apiOptions.redirectToCorrespondingResponse;
  if (isUnhandledException) {
      if (shouldRedirectOnAnyError) {
        return yield changeRoute('Error');
      }
      return;
  }
  const apiResponseStatus = e.response.status;
  if (apiResponseStatus === 404 && apiOptions.redirectToCorrespondingResponse) {
      yield changeRoute('NotFound');
  }
}
