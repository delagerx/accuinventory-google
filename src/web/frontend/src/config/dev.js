export const settings = {
  baseUrl: 'http://localhost:5000',
  baseApiUrl: 'http://localhost:5000',
  loginUrl: 'http://localhost:5003',
  wmsUrl: 'http://localhost:5001/ol-:lo',
  wmsHome: 'http://localhost:5001',
};
