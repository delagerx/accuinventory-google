export const settings = {
  baseUrl: 'http://app.accuinventory.hmg:5002',
  baseApiUrl: 'http://app.accuinventory.hmg:5002/api',
  loginUrl: 'http://wms.accuinventory.hmg:5002/',
  wmsUrl: 'http://wms.accuinventory.hmg:5002/ol-:lo',
  wmsHome: 'http://wms.accuinventory.hmg:5002',
};
