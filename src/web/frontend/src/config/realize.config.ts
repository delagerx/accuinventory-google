import * as realize from 'realizejs';
import {config as realizeConfig, HttpOptions} from 'realizejs';
import * as locales from 'services/locales';
import * as cookies from 'browser-cookies';
import {setLocale} from 'i18n';

function configureHttpClient(store: any) {
    const defaultHttpClient = realizeConfig.httpClient;
    realizeConfig.httpClient = (url: string, options: HttpOptions) => {
        options.headers = {
            TenantId: store
                .getState()
                .session
                .tenant
                .id
        };
        return defaultHttpClient(url, options);
    };
}

export default function(store: any) {
    configureHttpClient(store);

    for (const property in locales) {
        if (locales.hasOwnProperty(property)) {
            realize
                .i18n
                .registerLocale(locales[property].locale, locales[property].name.toLowerCase());
        }
    }

    const cookieLang = cookies.get('_culture');
    const userLang = navigator.language || navigator.userLanguage;
    const language = cookieLang || userLang;
    setLocale(language);
    realize.setConfig({
        grid: {
            paginationOnTop: false,
            pagination: {
                param: 'p',
                perPageParam: 'per_page',
                perPage: 20,
                window: 4,
                type: 'default',
                perPageOptions: [
                    {
                        name: '10',
                        value: 10,
                    }, {
                        name: '20',
                        value: 20,
                    }, {
                        name: '50',
                        value: 50,
                    },
                ],
            },
        },
    });
}
