export const settings = {
  baseUrl: 'http://app.accuinventory.com',
  baseApiUrl: 'http://app.accuinventory.com/api',
  loginUrl: 'http://wms.accuinventory.com/',
  wmsUrl: 'http://wms.accuinventory.com/ol-:lo',
  wmsHome: 'http://wms.accuinventory.com',
};
