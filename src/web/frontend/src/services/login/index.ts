import { createUserManager } from 'redux-oidc';
import {callbackRouterUrlPattern, rootUrlWithoutLo} from '../routes/routeUrls';
import settings from 'settings';

const oidcSettings = {
  authority: settings.loginUrl,
  client_id: 'app.accuinventory',
  redirect_uri: `${window.location.protocol}//${window.location.host}${callbackRouterUrlPattern}`,
  post_logout_redirect_uri: `${window.location.protocol}//${window.location.host}${rootUrlWithoutLo}`,
  response_type: 'id_token token',
  scope: 'openid profile accuinventoryApi',
  filterProtocolClaims: true,
  loadUserInfo: true,
};

export function isAuthenticated(user: any) {
  return user && !user.expired;
}

export const userManager = createUserManager(oidcSettings);
