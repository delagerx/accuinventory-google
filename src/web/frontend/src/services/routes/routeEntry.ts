import { RouteName } from './routeNames';
export interface RouteEntry {
    name: RouteName;
    routerUrlPattern: string;
    icon: string;
}
