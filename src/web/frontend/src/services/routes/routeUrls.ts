export const rootUrl = '/app/(lo/:lo/)';
export const rootUrlWithoutLo = '/app/';

export const homeUrl = `${rootUrlWithoutLo}home`;

export const homeRouterUrlPattern = `${rootUrl}home`;
export const callbackRouterUrlPattern = `${rootUrlWithoutLo}callback`;

export const searchUrl = `${rootUrl}search`;
export const searchRouterUrlPattern = `${rootUrl}search*`;

export const itemRouterUrlPattern = `${rootUrl}item/:type/:id`;

export const notFoundRouterUrlPattern = `${rootUrl}notFound`;
export const errorRouterUrlPattern = `${rootUrlWithoutLo}error`;
