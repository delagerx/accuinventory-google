import { RouteEntry } from './routeEntry';
import * as routeUrls from './routeUrls';

interface Routes {
    [key: string]: RouteEntry;
}

export const Routes: Routes = {
    home: {
        name: 'Home',
        routerUrlPattern: routeUrls.homeRouterUrlPattern,
        icon: 'home',
    },
    search: {
        name: 'Search',
        routerUrlPattern: routeUrls.searchRouterUrlPattern,
        icon: 'search',
    },
    item: {
        name: 'Item',
        routerUrlPattern: routeUrls.itemRouterUrlPattern,
        icon: 'work',
    },
    notFound: {
        name: 'NotFound',
        routerUrlPattern: routeUrls.notFoundRouterUrlPattern,
        icon: '',
    },
    error: {
      name: 'Error',
      routerUrlPattern: routeUrls.errorRouterUrlPattern,
      icon: '',
  },
};
