import { RouteName } from './routeNames';
import { RouteEntry } from './routeEntry';
import { Routes } from './routes';

export const getRouteByType = (action: RouteName): RouteEntry => {
  const routeKey = Object.keys(Routes).find(x => Routes[x].name === action);
  return Routes[routeKey];
};
