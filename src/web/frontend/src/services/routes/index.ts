import { searchUrl } from './routeUrls';
import { LogisticOperationId } from '../../application/application/session/entities/logisticOperation';
import { formatString } from '../../utils/stringFormatter';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import * as queryString from 'query-string';

import { SelectedItem } from '../../application/selectedItem/entities/selectedItem';
import { RouteEntry } from './routeEntry';
import { RouteName } from './routeNames';
import { Routes } from './routes';
import { getRouteByType } from './getRouteByType';

export function getSearchQueryFromQueryString(searchQueryString: string): SearchQuery {
    const searchQuery: SearchQuery = queryString.parse(searchQueryString);
    searchQuery.page = parseInt(searchQuery.page.toString(), 10);
    searchQuery.pageSize = parseInt(searchQuery.pageSize.toString(), 10);
    return searchQuery;
}

export function makeUrl(routeName: RouteName, lo: LogisticOperationId, data?: any) {
    const route = getRouteByType(routeName).routerUrlPattern;
    return formatString(route, {...data, lo});
}

export function makeSearchRoute(search: SearchQuery, lo: LogisticOperationId): string {
    const searchQueryString = queryString.stringify(search);
    const url = formatString(searchUrl, {lo});
    return `${url}?${searchQueryString}`;
}

export function makeItemRoute(selectedItem: SelectedItem): string {
    const url = formatString(Routes.item.routerUrlPattern, {...selectedItem});
    return url;
}
