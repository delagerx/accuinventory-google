import axios, {AxiosRequestConfig} from 'axios';
import {Store} from 'redux';
import {RootState} from '../../infraestructure/redux/reducers/rootReducer';
import {currentLanguage, translateError} from 'i18n';
import {Errors} from '../locales/errors';
import {errorEventCreator} from '../../application/application/errors/events/errorEvent';

const httpClient = axios.create({
  timeout: 10000,
});

export const configureHttpClient = (store: Store<RootState>) => {
  httpClient.interceptors.request.use(addCultureParam);
  httpClient.interceptors.request.use(request => addToken(request, store));
  httpClient.interceptors.response.use(
    (response) => response,
    response => failResponseInterceptor(response, store),
  );
};

function addToken(request: AxiosRequestConfig, store: Store<RootState>) {
  const token = store.getState().oidc.user.id_token;
  request.headers.Authorization = `Bearer ${token}`;
  return request;
}

function addCultureParam(request: AxiosRequestConfig) {
  request.params
    ? request.params.culture = currentLanguage
    : request.params = {culture: currentLanguage};
  return request;
}

function failResponseInterceptor(error: any, store: Store<RootState>): any {
    let message: keyof Errors = 'UNKNOW_ERROR';
    const apiResponseStatus = error.response.status;
    switch (apiResponseStatus) {
      case 401: {
        message = 'UNAUTHORIZED';
        return;
      }
      case 403: {
        message = 'FORBIDDEN';
        break;
      }
      case  404: {
        message = 'NOT_FOUND';
        break;
      }
      case 500: {
        message = 'SERVER_ERROR';
        break;
      }
      case 502: {
        message = 'SERVER_OFFLINE';
        break;
      }
    }
    store.dispatch(errorEventCreator(translateError(message), error));
    if (error.response) {
      if (error.response.status < 200 || error.response.status >= 300) {
        throw error;
      }
    }
    return error;
}

export default httpClient;
