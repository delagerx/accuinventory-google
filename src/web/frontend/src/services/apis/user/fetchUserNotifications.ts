import {CurrentUserNotificationsApi} from 'constants/apis';
import httpClient from '../../httpClient';
import {UserNotificationsDto} from './dtos/userNotificationsDto';
export function fetchCurrentUserNotifications(): Promise<UserNotificationsDto> {
  return httpClient.get(CurrentUserNotificationsApi).then(response => response.data);
}
