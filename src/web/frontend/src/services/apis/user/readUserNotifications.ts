import {ReadNotificationsApi} from 'constants/apis';
import httpClient from '../../httpClient';
export function readUserNotifications(notifications: number[]): Promise<any> {
  return httpClient
    .post(ReadNotificationsApi, {notifications});
}
