import { LogisticOperation } from 'application/application/session/entities/logisticOperation';
import { UserDto } from './dtos/userDto';
import { CurrentUserApi } from 'constants/apis';
import httpClient from '../../httpClient';
import { User } from 'application/application/session/entities/user';
export function fetchCurrentUserInfo(): Promise<User> {
    return httpClient.get(CurrentUserApi).then(response => mapUserDtoToUser(response.data));
}

function mapUserDtoToUser(userDto: UserDto): User {
  return {
    name: userDto.name,
    email: userDto.email,
    function: '',
    preferredLanguage: userDto.userPreferredLanguage,
    logisticOperations: userDto.logisticOperations as LogisticOperation[],
  };
}
