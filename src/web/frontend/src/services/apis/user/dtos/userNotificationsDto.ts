import {UserNotificationItem} from 'application/application/session/entities/UserNotificationItem';

export interface UserNotificationsDto {
  newNotifications: UserNotificationItem[];
  notificationCount: number;
}
