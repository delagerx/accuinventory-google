export interface UserDto {
  name: string;
  email: string;
  userPreferredLanguage: string;
  logisticOperations: LogisticOperationDto[];
}

export interface LogisticOperationDto {
  id: string;
  name: string;
}
