import { AxiosResponse } from 'axios';
import { UserFavoritedWidget } from './dtos/userFavoriteWidget';
import { WidgetsApi as url } from 'constants/apis';
import httpClient from 'services/httpClient';

export const saveFavoriteApi = (widget: UserFavoritedWidget): Promise<AxiosResponse> => {
  return httpClient.post(url, widget);
};

export const removeFavoriteApi = (id: string): Promise<AxiosResponse> => {
  return httpClient.delete(url + `/${id}`);
};

export const getUserFavoriteWidgets = (): Promise<UserFavoritedWidget[]> => {
  return httpClient.get(url).then(x => x.data);
};
