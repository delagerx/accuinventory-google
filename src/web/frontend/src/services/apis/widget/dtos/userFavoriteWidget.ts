import { WidgetSerializableProps } from 'application/widget/entities/widgetSerializableProps';
export interface UserFavoritedWidget  {
  id: string;
  props: WidgetSerializableProps;
  type: string;
}
