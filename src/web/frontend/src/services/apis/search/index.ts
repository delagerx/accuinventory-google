import { formatString } from '../../../utils/stringFormatter';
import { LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import { SearchResultApiDto } from './dtos/UserAction';
import { SearchApi } from 'constants/apis';
import httpClient from 'services/httpClient';
import { SearchRequested } from 'application/search/events/autoCompleteFetchRequested';

export { SearchResultItemApiDto } from './dtos/UserAction';

export interface SearchFilter {
    query: string;
    page: number;
    pageSize: number;
    logisticOperation: LogisticOperationId;
}

function makeRequestParameters(search: SearchRequested, cancelToken?: any) {
    return {
        params: search,
        cancelToken: cancelToken,
    };
}

export function searchApi(search: SearchFilter, cancelToken?: any): Promise<SearchResultApiDto> {
    const apiUrl = formatString(SearchApi, {lo: search.logisticOperation});
    return httpClient
        .get(apiUrl, makeRequestParameters(search, cancelToken))
        .then(x => x.data);
}
