import { SuggestionSubType } from 'application/navigation/entities/suggestionSubType';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { SuggestionContextProps } from 'application/navigation/entities/suggestionContextProps';

export type SearchResultApiDto = {
    total: number;
    items: SearchResultItemApiDto[];
};

export type SearchResultItemApiDto = {
    label: string;
    contextProps: SuggestionContextProps;
    relatedSuggestions: RelatedSuggestionApiDto[];
    type: SuggestionType;
    subType: SuggestionSubType;
    probability: number;
    hasPermission: boolean;
};

export type RelatedSuggestionApiDto = {
    label: string;
    type: SuggestionType;
    contextProps: SuggestionContextProps;
    subType: SuggestionSubType;
    hasPermission: boolean;
};
