import { HistoryResponseItem } from './dtos/HistoryResponseItem';
import { HistoryEntry } from './dtos/HistoryEntry';
import { UserHistoryApi } from 'constants/apis';
import httpClient from 'services/httpClient';

export const fetchUserHistory = (): Promise<HistoryResponseItem[]> => {
    return httpClient.get(UserHistoryApi)
        .then(x => x.data);
};
