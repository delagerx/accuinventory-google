import { RouteName } from 'services/routes/routeNames';
export interface HistoryResponseItem {
    name: RouteName;
    url: string;
    iconKey: string;
    hits: number;
}
