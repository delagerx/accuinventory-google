export interface HistoryEntry {
    name: string;
    url: string;
    iconKey: string;
}
