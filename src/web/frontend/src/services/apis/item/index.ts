import { GetItemApi } from 'constants/apis';
import httpClient from 'services/httpClient';
import { formatString } from 'utils/stringFormatter';

import { SearchResultItemApiDto } from '../search/index';
import { SuggestionType } from 'application/navigation/entities/suggestionType';

export interface GetItemFilter {
    id: string;
    type: SuggestionType;
    logisticOperation: string;
}

function makeGetItemUrl(itemFilter: GetItemFilter) {
    return formatString(GetItemApi, {
      ...itemFilter,
      lo: itemFilter.logisticOperation,
    });
}

export function getItemApi(itemFilter: GetItemFilter): Promise<SearchResultItemApiDto> {
    return httpClient
        .get(makeGetItemUrl(itemFilter))
        .then(x => x.data);
}
