export interface Errors {
    NOT_FOUND: string;
    UNAUTHORIZED: string;
    FORBIDDEN: string;
    SERVER_ERROR: string;
    UNKNOW_ERROR: string;
    SERVER_OFFLINE: string;
    FAILED_TO_CONTACT_LOGIN_SERVER: string;
}
