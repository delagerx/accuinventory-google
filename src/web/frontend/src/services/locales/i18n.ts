import { Errors } from './errors';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { Locales } from './locales';
import * as Realize from 'realizejs';
import * as locales from 'services/locales';

export let currentLanguage = '';

export function translateError(error: keyof Errors) {
    return i18n(error);
}

export function translateSuggestionType(type: SuggestionType) {
    return i18n(type.toUpperCase());
}

export function i18n(key: keyof Locales) {
    return Realize.i18n.t(key);
}

export function setLocale(locale: string) {
  currentLanguage = isLanguageRegistred(locale)
    ? locale.toLowerCase()
    : 'en-us';
  Realize.i18n.setLocale(currentLanguage);
}

export function getCurrentLanguage() {
    return currentLanguage;
}

export function isLanguageRegistred(language: string) {
  return Object.keys(locales).some(x => locales[x].name.toLowerCase() === language.toLowerCase());
}

export default i18n;
