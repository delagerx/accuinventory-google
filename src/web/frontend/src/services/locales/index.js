import pt_br from './localeFiles/pt-br';
import en_us from './localeFiles/en-us';
export const ptBr = {
        "name": "pt-BR",
        "locale": pt_br
};
export const en = {
    "name": "en",
    "locale": en_us
};
export const enUs = {
  "name": "en-US",
  "locale": en_us
};
