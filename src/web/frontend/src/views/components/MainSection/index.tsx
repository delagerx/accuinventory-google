import * as React from 'react';
import { connect } from 'react-redux';

export const MainSection = (props) => {
  return (
      <main className="main-section">
        {props.children}
      </main>
    );
};
