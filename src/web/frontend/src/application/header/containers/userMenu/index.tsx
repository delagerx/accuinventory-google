import { UserProfileProps, UserProfileComponent } from '../../components/userProfile';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { LogisticOperation, LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import { UserActionListComponent } from 'application/header/components/userProfile/userActionList';
import { logisticOperationChangedDispatcher } from 'application/application/session/events/changeLogisticOperation';

interface Props extends UserProfileProps {
  logisticOperations?: LogisticOperation[];
  changeLogisticOperation?: (id: LogisticOperationId) => void;
}

@connect(mapStateToProps, mapDispatchToProps)
export class UserMenu extends React.Component<Props> {
    state = {
      anchorEl: null,
      open: false,
    };

    handleClick = event => {
      this.setState({ open: true, anchorEl: event.currentTarget });
    }

    handleRequestClose = () => {
      this.setState({ open: false });
    }

    render() {
        return (
          <div className="user-profile">
            <UserProfileComponent {...this.props} onClick={this.handleClick}/>
            <UserActionListComponent
              logisticOperations={this.props.logisticOperations}
              isOpen={this.state.open}
              anchorEl={this.state.anchorEl}
              handleRequestClose={this.handleRequestClose}
              changeLogisticOperation={this.props.changeLogisticOperation}
            />
          </div>
        );
    }
}

function mapStateToProps(state: RootState): Partial<Props> {
  return {
      userName: state.session.user.name,
      company: state.session.currentLogisticOperation.name,
      profilePic: 'https://vb.northsearegion.eu/files/theme/default-user-icon-profile.png',
      logisticOperations: state.session.user.logisticOperations,
  };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
  return {
    changeLogisticOperation: logisticOperationChangedDispatcher(dispatch),
  };
}
