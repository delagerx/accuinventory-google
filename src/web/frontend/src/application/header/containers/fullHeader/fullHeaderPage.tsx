import { MainSection } from 'views/components/MainSection/index';
import * as React from 'react';
import { FullHeader } from './index';
import { LoadingBar } from 'react-redux-loading-bar';

interface Props {
    children?: any;
}
export function FullHeaderPage(props: Props) {
    return (
        <div>
            <FullHeader/>
            <LoadingBar/>
            <MainSection>
                {props.children}
            </MainSection>
        </div>
    );
}
