import { SearchBox } from 'application/search/containers/searchBox';
import * as React from 'react';

import { Notifications } from '../notifications/index';
import { UserMenu } from '../userMenu/index';
import { Logo } from 'application/shared/containers/logo/logo';
import { Menu } from 'application/header/containers/menu';

export class FullHeader extends React.Component<any, any> {

    render() {
        return (
            <header className="header-expanded">
                <div className="container">
                    <div className="row">
                        <div className="col s12 l8">
                            <Logo/>
                        </div>
                        <div className="col s12 l4 hide-on-med-and-down">
                            <div className="right">
                                <Notifications/>
                                <UserMenu/>
                            </div>
                        </div>
                        </div>
                        <div className="row">
                            <div className="col s12">
                                <SearchBox/>
                            </div>
                        </div>
                        <Menu/>
                    </div>
                </header>
        );
    }
}
