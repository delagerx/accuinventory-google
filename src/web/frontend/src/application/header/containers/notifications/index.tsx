import * as React from 'react';
import { NotificationDropdown } from './notificationDropdown';
import {connect, Dispatch} from 'react-redux';
import {getNotifications} from 'application/application/session/events/eventHandlers';
import {UserNotificationItem} from 'application/application/session/entities/UserNotificationItem';
import {
  ReadNotificationCommand,
  readUserNotificationDefs,
} from '../../../application/session/events/readNotificationsCommand';

interface Props {
    count?: number;
    notifications?: UserNotificationItem[];
    saveReadNotifications?: (command: ReadNotificationCommand) => void;
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
  return {
    saveReadNotifications: readUserNotificationDefs.com.dispatcher(dispatch),
  };
}

@connect(getNotifications, mapDispatchToProps)
export class Notifications extends React.Component<Props, any> {
  state = {
    anchorEl: null,
    open: false,
  };

  constructor(props: Props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = event => {
    this.setState({ open: true, anchorEl: event.currentTarget });
    const notificationsId = this
      .props
      .notifications
      .filter(x => !x.isRead)
      .map(x => x.id);
    const command = {notificationsIds: notificationsId};
    this.props.saveReadNotifications(command);
  }

  handleRequestClose = () => {
    this.setState({ open: false });
  }

  renderNotificationDropdown(items: UserNotificationItem[], handleClose: any) {
    return(
      <NotificationDropdown
        items={items}
        handleRequestClose={handleClose}
        anchorEl={this.state.anchorEl}
        isOpen={this.state.open}
      />
    );
  }

  render() {
    const hasNotification = this.props.count > 0;
    const notificationIcon = hasNotification ? 'notifications_active' : 'notifications';
    const badgeCount = hasNotification
      ? <span className="notification-alert-amount">{this.props.count}</span>
      : <span/>;
    return (
        <div className="notification">
            <i className="material-icons notification-alert-icon" onClick={this.handleClick}>{notificationIcon}</i>
          {badgeCount}
            {this.renderNotificationDropdown(this.props.notifications, this.handleRequestClose)}
        </div>
    );
  }
}
