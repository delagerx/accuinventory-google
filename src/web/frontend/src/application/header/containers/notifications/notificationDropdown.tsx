import * as React from 'react';
import {UserNotificationItem} from 'application/application/session/entities/UserNotificationItem';
import {WmsLinkComponent} from '../../../navigation/containers/wmsLink';
import Menu, {MenuItem} from 'material-ui/Menu';
import {NotificationList} from './notificationList';
import i18n from 'i18n';

interface Props {
  handleRequestClose: (event: any) => void;
  anchorEl: any;
  isOpen: boolean;
  items: UserNotificationItem[];
}

export class NotificationDropdown extends React.Component<Props> {
  render() {
    return <Menu
      id="simple-menu"
      className="notification-list"
      anchorEl={this.props.anchorEl}
      open={this.props.isOpen}
      onRequestClose={this.props.handleRequestClose}
      anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
    >
      <NotificationList
        key="notification-list"
        notifications={this.props.items}
      />
      <MenuItem
        key="menu-item-allNotifications"
        className="notification-list-item notification-list-showAll"
        onClick={this.props.handleRequestClose}
      >
        <WmsLinkComponent
          route={'notifications'}
        >
          <div>{i18n('NOTIFICATIONS_SHOW_ALL')}</div>
        </WmsLinkComponent>
      </MenuItem>
    </Menu>;
  }
}
