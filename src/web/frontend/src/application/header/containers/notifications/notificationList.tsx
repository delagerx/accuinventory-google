import * as React from 'react';
import {UserNotificationItem} from 'application/application/session/entities/UserNotificationItem';
import {NotificationItem} from './notificationItem';
import i18n from 'i18n';

interface Props {
  notifications: UserNotificationItem[];
}

export class NotificationList extends React.Component<Props> {
  renderEmptyList() {
    return (
      <div className="notification-list-empty">
        <i className="material-icons">notifications</i>
        <span>{i18n('NOTIFICATIONS_EMPTY')}</span>
      </div>
    );
  }

  renderNotificationList() {
    const itens = this
      .props
      .notifications
      .map((notification, i) => (
        <NotificationItem
          key={i}
          item={notification}
        />
    ));
    return (
      <div>{itens}</div>
    );
  }

  render() {
    return this.props.notifications.length > 0
      ? this.renderNotificationList()
      : this.renderEmptyList();
  }
}
