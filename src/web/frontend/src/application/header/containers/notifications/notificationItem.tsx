import * as React from 'react';
import {MenuItem} from 'material-ui/Menu';
import {UserNotificationItem} from '../../../application/session/entities/UserNotificationItem';
import {DateTimeComponent} from '../../../application/components/datetime';
import {UserNotificationType} from '../../../application/session/entities/UserNotificationType';

interface Props {
  item: UserNotificationItem;
}

export class NotificationItem extends React.Component<Props> {
  render() {
    const icon = UserNotificationType[this.props.item.type].toLowerCase();
    return (
      <MenuItem
        key="menu-item-settings"
        className="notification-list-card"
        divider={true}
        disableRipple={true}
        disableGutters={true}
        dense={true}
      >
        <div>
          <div className="notification-list-icon">
            <i className={`material-icons notification-item-${icon}`}>{icon}</i>
          </div>
          <div className="notification-list-content">
            <div className="notification-list-content-title">{this.props.item.title}</div>
            <div className="notification-list-content-message">{this.props.item.message}</div>
            <div className="notification-list-content-date">
              <DateTimeComponent
                dateAsString={this.props.item.createdAt}
                showTime={true}
              />
            </div>
          </div>
        </div>
      </MenuItem>
    );
  }
}
