import { SearchBox } from 'application/search/containers/searchBox';
import { Logo } from 'application/shared/containers/logo/logo';
import { UserMenu } from '../userMenu/index';
import * as React from 'react';

export class CompactHeader extends React.Component <any, any> {
    render() {
        return (
            <header className="header-compressed">
              <div className="container">
                <div className="row">
                    <div className="col s12 m5 l2">
                        <Logo/>
                    </div>
                    <div className="col s12 m7 l6">
                        <div className="compact-header-searchbox">
                            <SearchBox/>
                        </div>
                    </div>
                    <div className="col s12 m2 l4 hide-on-med-and-down">
                      <div className="right">
                        <UserMenu/>
                      </div>
                    </div>
                </div>
              </div>
            </header>
        );
    }
}
