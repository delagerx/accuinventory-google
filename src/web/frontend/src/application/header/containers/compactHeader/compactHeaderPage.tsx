import { MainSection } from 'views/components/MainSection/index';
import * as React from 'react';
import { CompactHeader } from './index';
import { LoadingBar } from 'react-redux-loading-bar';

interface Props {
    children?: any;
}
export function CompactHeaderPage(props: Props) {
    return (
        <div>
            <CompactHeader/>
            <LoadingBar/>
            <MainSection>
                {props.children}
            </MainSection>
        </div>
    );
}
