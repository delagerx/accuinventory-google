import * as React from 'react';

export function MenuButtom() {
    return (
        <button
            type="button"
            data-activates="call-menu-mobile"
            className="call-menu-mobile"
        >
            <i className="material-icons" aria-hidden="true">menu</i>
        </button>
    );
}
