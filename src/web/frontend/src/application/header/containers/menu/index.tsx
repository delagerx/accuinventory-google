import * as React from 'react';





export class Menu extends React.Component {
    render() {
        return (
            <aside id="call-menu-mobile" className="side-nav sidebar hide-on-large-only">
            <div className="user-profile">
                <a className="close-menu-mobile">
                    <i className="material-icons">highlight_off</i>
                </a>
                <div className="user-profile-header">
                    <div className="user-profile-avatar">
                        <img
                            src="https://www1-lw.xda-cdn.com/files/2012/04/618px-Over_9000_Vector_by_Vernacular.jpg"
                            alt=""
                        />
                    </div>
                    <div className="user-profile-infos">
                        <p className="user-profile-name">
                            <span>Alex Barboza</span>
                        </p>
                        <p className="user-profile-address">Delage Hospital Privado</p>
                    </div>
                </div>
                <div className="user-profile-body">
                    <ul>
                        <li>
                            <a href="#!">
                                <i className="material-icons">person</i>
                                Editar Perfil</a>
                        </li>
                        <li>
                            <a href="#!">
                                <i className="material-icons">star</i>
                                Gerenciar Favoritos</a>
                        </li>
                        <li>
                            <a href="#!">
                                <i className="material-icons">exit_to_app</i>
                                Desconectar</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="menu-sidebar">
                <ul className="collapsible collapsible-accordion">
                    <li className="first-level">
                        <a className="collapsible-header" href="#!" title="">Item Menu Nível Único</a>
                    </li>
                    <li className="first-level">
                        <a className="collapsible-header">Item Menu Nível 1<i className="right material-icons">arrow_drop_down</i>
                        </a>
                        <div className="collapsible-body">
                            <ul role="menu">
                                <li>
                                    <a href="#!" title="">Item Menu Nível 2</a>
                                </li>
                                <li>
                                    <a href="#!" title="">Item Menu Nível 2</a>
                                </li>
                                <li>
                                    <a href="#!" title="">Item Menu Nível 2</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li className="first-level">
                        <a className="collapsible-header" href="#!" title="">Item Menu Nível Único</a>
                    </li>
                </ul>
            </div>
        </aside>
        );
    }
}
