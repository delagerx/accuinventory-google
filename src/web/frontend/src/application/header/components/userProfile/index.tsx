import * as React from 'react';

export interface UserProfileProps {
  userName?: string;
  company?: string;
  profilePic?: string;
}

interface Props extends UserProfileProps {
  onClick: (event: any) => void;
}

export class UserProfileComponent extends React.Component<Props> {
  render() {
    return (
      <a
        role="button"
        className="dropdown-button"
        href="#!"
        onClick={this.props.onClick}
      >
      <div className="user-profile-avatar">
        <img
          src={this.props.profilePic}
          alt=""
        />
      </div>
      <div className="user-profile-infos">
        <p className="user-profile-name">
          <span>{this.props.userName}</span>
          <i className="material-icons">arrow_drop_down</i>
        </p>
        <p className="user-profile-address">
          {this.props.company}
        </p>
      </div>
      </a>
    );
  }
}
