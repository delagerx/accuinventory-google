import * as React from 'react';
import {userManager} from '../../../../services/login';
import i18n from 'i18n';

export class LogoutButton extends React.Component<any> {
  logout() {
    userManager.signoutRedirect();
    return false;
  }
  render() {
    return (
      <a href="#" role="button" onClick={this.logout}>
        {i18n('GENERAL_LOGOUT')}
      </a>
    );
  }
}
