import { WmsLinkComponent } from '../../../navigation/containers/wmsLink';
import { LogisticOperation, LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import Menu, { MenuItem } from 'material-ui/Menu';
import * as React from 'react';
import i18n from 'i18n';
import { UserLogisticOperationItemComponent } from 'application/header/components/userProfile/userLogisticOperationItem';
import { ListSubheader } from 'material-ui';
import {LogoutButton} from './logoutButton';

interface Props {
    logisticOperations: LogisticOperation[];
    isOpen: boolean;
    anchorEl: any;
    handleRequestClose: () => void;
    changeLogisticOperation: (id: LogisticOperationId) => void;
}

export class UserActionListComponent extends React.Component<Props> {
    constructor(props: Props) {
      super(props);
      this.renderItem = this.renderItem.bind(this);
    }
    renderItem(logisticOperation: LogisticOperation) {
      const {id} = logisticOperation;
      return (
        <UserLogisticOperationItemComponent
          key={`menu-item-lo-${id}`}
          handleClose={this.props.handleRequestClose}
          logisticOperation={logisticOperation}
          changeLogisticOperation={this.props.changeLogisticOperation}
        />
      );
    }
    render() {
        const logisticOperations =
            this.props.logisticOperations.map(this.renderItem);
        return(
            <Menu
                id="simple-menu"
                className="user-menu"
                anchorEl={this.props.anchorEl}
                open={this.props.isOpen}
                onRequestClose={this.props.handleRequestClose}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'left'}}
            >
                <ListSubheader className="user-menu-divider">{i18n('USER_MENU_AVAILABLE_LO')}</ListSubheader>
                {logisticOperations}
                <ListSubheader className="user-menu-divider">{i18n('USER_MENU_AVAILABLE_OPTIONS')}</ListSubheader>
                <MenuItem
                    key="menu-item-settings"
                    className="user-menu-item"
                    onClick={this.props.handleRequestClose}
                >
                    <WmsLinkComponent
                      route={'settings'}
                    />
                </MenuItem>
                <MenuItem
                    key="menu-item-logout"
                    className="user-menu-item"
                    onClick={this.props.handleRequestClose}
                >
                    <LogoutButton/>
                </MenuItem>
            </Menu>
        );
    }
}
