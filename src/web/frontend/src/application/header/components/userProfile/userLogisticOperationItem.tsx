import { LogisticOperation, LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import * as React from 'react';
import Menu, { MenuItem } from 'material-ui/Menu';

export interface Props {
  logisticOperation: LogisticOperation;
  changeLogisticOperation: (id: LogisticOperationId) => void;
  handleClose: () => void;
}

export class UserLogisticOperationItemComponent extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.changeLogisticOperation(this.props.logisticOperation.id);
    this.props.handleClose();
  }
  render() {
    const {id, name} = this.props.logisticOperation;
    return(
      <MenuItem
        key={`menu-item-lo-${id}`}
        className="user-menu-item"
        onClick={this.handleClick}
      >
        <span>{name}</span>
      </MenuItem>
    );
  }
}
