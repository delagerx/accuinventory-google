import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { ProductInfo } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const ProductVisualizeConfiguration: WidgetDefinition = {
    name: 'ProductWidget',
    type: 'Product',
    component: ProductInfo,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: props.name,
            subtitle: i18n('PRODUCT'),
            expandable: false,
            actions: [],
        };
    },
};
