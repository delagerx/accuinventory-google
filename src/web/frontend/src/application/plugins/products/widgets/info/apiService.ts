import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { ProductQueryResponse } from './productQueryResponse';

export function fetchProductInfo(id: string): Promise<ProductQueryResponse>{
    return httpClient.get(baseApiUrl + `/productInfo/${id}`).then(x => x.data);
}
