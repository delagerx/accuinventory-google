import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

//TODO: Mover dependência para assíncrono
import { Bar } from 'react-chartjs-2';
import { ProductStock } from './productQueryResponse';

interface Props {
    productStock: ProductStock[];
}

export function ProductStockChart(props: Props) {

    const chartData = {
        labels: [],
        datasets: [],
    };

    const currentStock = [];
    const maximumStock = [];
    const minimumStock = [];
    props.productStock.forEach(stock => {
        const maxValue = stock.maximum;

        chartData.labels.push(stock.region);

        maximumStock.push(stock.maximum / maxValue);
        minimumStock.push(stock.minimum / maxValue);
        currentStock.push(stock.stock / maxValue);
    });

    const options = {
        tooltips: {
            enabled: false,
        },
        hover: {
            mode: null,
        },
        scales: {
            xAxes: [
                {
                    stacked: true,
                    barThickness: 50,
                    gridLines: {
                        display: false,
                    },
                },
            ],
            yAxes: [
                { display: false },
            ],
      },
    };

    chartData.datasets.push({
            label: 'Estoque mínimo',
            backgroundColor: 'rgba(255,179,194,1)',
            borderColor: 'rgba(255,179,194,1)',
            borderWidth: 1,
            data: minimumStock,
            stack: 'limits',
        });

    chartData.datasets.push({
            label: 'Estoque máximo',
            backgroundColor: 'rgba(194,179,255,1)',
            borderWidth: 1,
            data: maximumStock,
            stack: 'limits',
        });

    chartData.datasets.push({
            label: 'Estoque atual',
            backgroundColor: 'rgba(179,255,194,1)',
            borderColor: 'rgba(179,255,194,1)',
            borderWidth: 1,
            data: currentStock,
        });

    return (
        <div className="graphic">
            <Bar data={chartData} options={options} />
        </div>
    );
}
