export interface ProductQueryResponse {
    product: Product;
    productBarcodes: ProductBarcode[];
    productStocks: ProductStock[];
}

export interface Product {
    id: string;
    availableStock: number;
    curve: string;
    divergence: string;
    digit: string;
    genericName: string;
    name: string;
    stockHeld: number;
    totalStock: number;
}

export interface ProductBarcode {
    id: string;
    barcode: string;
    height: number;
    length: number;
    main: boolean;
    quantity: number;
    type: string;
    weight: number;
    width: number;
}

export interface ProductStock {
    maximum: number;
    minimum: number;
    region: string;
    stock: number;
}
