import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { TableComponent } from '../../../../application/components/table/index';
import { ProductBarcode } from './productQueryResponse';

interface Props {
    productBarCodes: ProductBarcode[];
}

export function ProductBarcodeList(props: Props) {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(props.productBarCodes);

    return (
        <TableComponent
          title={i18n('BAR_CODES')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
}

function createTableColumnsProps() {
    return {
        barCode: {
            title: i18n('PRODUCT_BARCODE'),
        },
        quantity: {
            title: i18n('PRODUCT_QUANTITY'),
        },
        type: {
            title: i18n('PRODUCT_BARCODE_TYPE'),
        },
        principal: {
            title: i18n('PRODUCT_BARCODE_PRINCIPAL'),
        },
    };
}

function createTableRowsProps(productBarCodeList: ProductBarcode[]) {
    const tableRowsProps = productBarCodeList.map((productBarCode, rowIndex) => {
        return {
            barCode: {
                value: productBarCode.barcode,
            },
            quantity: {
                value: productBarCode.quantity,
            },
            type: {
                value: productBarCode.type,
            },
            principal: {
                value: productBarCode.main ? i18n('YES') : i18n('NO'),
            },
        };
    });

    return tableRowsProps;
}
