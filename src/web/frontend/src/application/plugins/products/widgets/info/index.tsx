import * as React from 'react';
import i18n from 'i18n';

import { WaitFetch } from '../../../../shared/components/waitFetch';
import { WmsLinkComponent } from 'application/navigation/containers/wmsLink';
import { fetchProductInfo } from './apiService';
import { ProductBarcodeList } from './productBarCodeList';
import { ProductDetails } from './productDetails';
import { ProductStockChart } from './productStockChart';
import { Product, ProductBarcode, ProductStock } from './productQueryResponse';

export interface Props {
    product: Product;
    productBarcodes: ProductBarcode[];
    productStocks: ProductStock[];
}

const Component = (props: Props) => {
    const chart = !!props.productStocks && props.productStocks.length > 0
      ? rederProductStockChart(props.productStocks)
      : renderEmptyChart(props.product.id);
    return (
        <div className="row card-content">
            <div className="col s12 l7 xl5">
                {renderProductDetails(props.product)}
                {renderProductBarcodeList(props.productBarcodes)}
            </div>
            <div className="col s12 l5 xl7">
                {chart}
            </div>
        </div>
    );
};

function renderProductDetails(props: Product) {
    return(<ProductDetails {...props} />);
}

function renderProductBarcodeList(props: ProductBarcode[]) {
    return(<ProductBarcodeList productBarCodes={props} />);
}

function rederProductStockChart(props: ProductStock[]) {
    return (<ProductStockChart productStock={props} />);
}

function renderEmptyChart(productId: string) {
    return (
        <div className="empty-chart">
            <img src="/assets/img/empty_chart.png" alt=""/>
            <div className="caption">
                <p>{i18n('WIDGET_PRODUCT_NO_MIN_MAX_STOCK_CONFIGURED')}</p>
                <br/>
                <WmsLinkComponent
                  text={i18n('WIDGET_PRODUCT_CONFIGURE_MAX_MIN_LABEL')}
                  route="editProduct"
                  payload={{id: productId}}
                />
            </div>
        </div>
    );
}

export class HOCProductInfo extends React.Component<any> {
    fetch(props: any) {
        return fetchProductInfo(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}

export const ProductInfo = HOCProductInfo;
