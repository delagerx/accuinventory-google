import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { ProductCodeComponent } from '../../../../application/components/productCode/index';
import { Product } from './productQueryResponse';

export function ProductDetails(props: Product) {
    return (
        <div>
            <div className="row">
                <div className="col s12 l6">
                    <div className="widget-body-infos">
                        <span className="name">{i18n('PRODUCT_COD')}</span>
                        <span className="description">
                            <ProductCodeComponent productCode={props.id} productDigit={props.digit} />
                        </span>
                    </div>
                </div>
                <div className="col s12 l6">
                    <div className="widget-body-infos">
                        <span className="name">{i18n('PRODUCT_GENERIC_NAME')}</span>
                        <span className="description">{props.genericName}</span>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col s12 l6">
                    <div className="widget-body-infos">
                        <span className="name">{i18n('PRODUCT_STOCK')}</span>
                        <span className="description">{props.availableStock} / {props.totalStock}</span>
                    </div>
                </div>
                <div className="col s12 l6">
                    <div className="widget-body-infos">
                        <span className="name">{i18n('PRODUCT_CURVE')}</span>
                        <span className="description">{props.curve || 'N/D'}</span>
                    </div>
                </div>
            </div>
        </div>
    );
}
