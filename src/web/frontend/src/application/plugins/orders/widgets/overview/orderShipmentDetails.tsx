import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import { Order, OrderShipmentInformation } from './orderOverviewQueryResponse';
import { DateTimeComponent } from '../../../../application/components/datetime/index';

export interface Props {
  orderShipmentInformation: OrderShipmentInformation;
}

export function OrderShipmentDetails(props: Props) {
  const shipmentInformation = props.orderShipmentInformation;

  return (
    <div>
      <div className="col s6 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_ADDRESS')}</span>
          <span className="description">{shipmentInformation.address || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_DISTRICT')}</span>
          <span className="description">{shipmentInformation.district || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_CITY')}</span>
          <span className="description">{shipmentInformation.city || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_STATE')}</span>
          <span className="description">{shipmentInformation.state || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_ZIP_CODE')}</span>
          <span className="description">{shipmentInformation.zipCode || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_CONVEYOR')}</span>
          <span className="description">{shipmentInformation.conveyor || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_ROUTE')}</span>
          <span className="description">{shipmentInformation.route || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_DEPARTURE')}</span>
          <span className="description">{renderDepartureAsFormattedDate(shipmentInformation)}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_DELIVERY')}</span>
          <span className="description">{renderDeliveryAsFormattedDate(shipmentInformation)}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_ARRIVE')}</span>
          <span className="description">{renderArriveAsFormattedDate(shipmentInformation)}</span>
        </div>
      </div>
      <div className="col s6 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_DRIVER')}</span>
          <span className="description">{shipmentInformation.driver || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_VEHICLE')}</span>
          <span className="description">{shipmentInformation.vehicle || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s6 l2">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SHIPIMENT_ASSISTANT')}</span>
          <span className="description">{shipmentInformation.assistant || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
    </div>
  );
}

function renderArriveAsFormattedDate(props: OrderShipmentInformation) {
  if (!props.arrive) {
    return i18n('GENERAL_NO_INFORMATION');
  }

  return (<DateTimeComponent dateAsObject={props.arrive} />);
}

function renderDeliveryAsFormattedDate(props: OrderShipmentInformation) {
  if (!props.delivery) {
    return i18n('GENERAL_NO_INFORMATION');
  }

  return (<DateTimeComponent dateAsObject={props.delivery} />);
}

function renderDepartureAsFormattedDate(props: OrderShipmentInformation) {
  if (!props.departure) {
    return i18n('GENERAL_NO_INFORMATION');
  }

  return (<DateTimeComponent dateAsObject={props.departure} />);
}
