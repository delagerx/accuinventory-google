import { itemSelectedDispatcher } from '../../../../selectedItem/events/itemSelected/index';
import * as realize from 'realizejs';
import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import {TimelineComponent, TimelineProps, TimelineEntryProps} from '../../../../application/components/timeline';
import { OrderStage } from './orderOverviewQueryResponse';

export interface Props {
  orderHistory: OrderStage[];
}

export function OrderStageList(props: Props) {
  const timelineEntries = buildTimelineProps(props.orderHistory);

  return (
    <TimelineComponent entries={timelineEntries} title={i18n('WIDGET_ORDER_STAGES')} />
  );
}

function buildTimelineProps(orderHistory: OrderStage[]) {

  return orderHistory.map((orderStage, orderStageIndex) => {
      const currentStage = orderHistory.length === orderStageIndex + 1;
      return createTimelineEntry(orderStage, currentStage);
  });
}

function createTimelineEntry(orderStage: OrderStage, currentStage: boolean) {
  const formattedStageKey = getI18nStageKey(orderStage);

  return {
    title: i18n(`WIDGET_ORDER_STAGE_${formattedStageKey}`),
    timestamp: orderStage.timestamp,
    isActive: currentStage,
    showTime: true,
  };
}

function getI18nStageKey(orderStage: OrderStage) {
  return orderStage.key.split(/(?=[A-Z])/).join('_').toUpperCase();
}
