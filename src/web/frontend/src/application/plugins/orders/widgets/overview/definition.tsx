import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { OrderOverview } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const OrderOverviewConfiguration: WidgetDefinition = {
    name: 'OrderWidget',
    type: 'Order',
    component: OrderOverview,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        const formattedTitle = props.entityName
            ? `${props.id} - ${props.entityName}`
            : props.id;

        return {
            title: formattedTitle,
            subtitle: i18n('ORDER'),
            expandable: false,
            actions: [],
        };
    },
};
