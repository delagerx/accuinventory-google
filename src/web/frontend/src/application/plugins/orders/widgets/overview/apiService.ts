import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { OrderOverviewQueryResponse } from './orderOverviewQueryResponse';

export function fetchOrderData(id: string): Promise<OrderOverviewQueryResponse>{
    return httpClient.get(baseApiUrl + `/orderOverview/${id}`).then(x => x.data);
}
