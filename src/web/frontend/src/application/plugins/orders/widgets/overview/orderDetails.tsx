import { OrderShipmentDetails } from './orderShipmentDetails';
import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import { Order, OrderShipmentInformation } from './orderOverviewQueryResponse';

export interface Props {
  order: Order;
}

export function OrderDetails(props: Props) {
  return (
    <div>
      {renderOrderInformation(props.order)}
    </div>
  );
}

function renderOrderInformation(props: Order) {
  return(
    <div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_PICKING')}</span>
          <span className="description">{props.picking || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_LEGACY_CODE')}</span>
          <span className="description">{props.legacyCode || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_OPERATION')}</span>
          <span className="description">{props.operation || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_SUBOPERATION')}</span>
          <span className="description">{props.subOperation || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_STATUS')}</span>
          <span className="description">{props.status || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_ORIGIN')}</span>
          <span className="description">{props.origin || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ORDER_ITEM_QUANTITY')}</span>
          <span className="description">{props.itemsQuantity || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
    </div>
  );
}
