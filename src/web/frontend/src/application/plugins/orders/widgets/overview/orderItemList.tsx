import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import { ProductCodeComponent } from '../../../../application/components/productCode/index';
import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import { TableComponent } from '../../../../application/components/table/index';
import { OrderItem } from './orderOverviewQueryResponse';

export interface Props {
    orderItems: OrderItem[];
}

export function OrderItemList(props: Props) {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(props.orderItems);

    return (
        <TableComponent
          title={i18n('ITEMS')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
}

function createTableColumnsProps() {
    return {
        code: {
            title: i18n('WIDGET_ORDER_ITEM_CODE'),
        },
        description: {
            title: i18n('WIDGET_ORDER_ITEM_DESCRIPTION'),
        },
        quantity: {
            title: i18n('WIDGET_ORDER_ITEM_QUANTITY'),
        },
        collected: {
            title: i18n('WIDGET_ORDER_ITEM_COLLECTED'),
        },
    };
}

function createTableRowsProps(orderItemList: OrderItem[]) {
    const tableRowsProps = orderItemList.map((orderItem, rowIndex) => {
        return {
            code: {
                value: renderProductCodeAsLink(orderItem),
            },
            description: {
                value: renderProductDescriptionAsLink(orderItem),
            },
            quantity: {
                value: orderItem.quantity,
            },
            collected: {
                value: orderItem.collected || i18n('GENERAL_NO_INFORMATION'),
            },
        };
    });

    return tableRowsProps;
}

function renderProductCodeAsLink(orderItem: OrderItem) {
    return(
        <ItemDetailsLink
            type={'Product'}
            contextProps={{id: orderItem.productCode}}
            text={orderItem.productDescription}
        >
            <ProductCodeComponent
                productCode={orderItem.productCode}
                productDigit={orderItem.productDigit}
            />
        </ItemDetailsLink>
    );
}

function renderProductDescriptionAsLink(orderItem: OrderItem) {
    return(
        <ItemDetailsLink
            type={'Product'}
            contextProps={{id: orderItem.productCode}}
            text={orderItem.productDescription}
        >
            {orderItem.productDescription}
        </ItemDetailsLink>
    );
}
