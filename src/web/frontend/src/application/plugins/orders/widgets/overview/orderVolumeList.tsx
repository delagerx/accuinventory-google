import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { TableComponent } from '../../../../application/components/table/index';
import { OrderVolume } from './orderOverviewQueryResponse';

export interface Props {
    orderVolumes: OrderVolume[];
}

export function OrderVolumeList(props: Props) {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(props.orderVolumes);

    return (
        <TableComponent
          title={i18n('VOLUMES')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
}

function createTableColumnsProps() {
    return {
        volume: {
            title: i18n('WIDGET_ORDER_VOLUME_VOLUME'),
        },
        boxType: {
            title: i18n('WIDGET_ORDER_VOLUME_BOX_TYPE'),
        },
        checkout: {
            title: i18n('WIDGET_ORDER_VOLUME_CHECKOUT'),
        },
        location: {
            title: i18n('WIDGET_ORDER_VOLUME_LOCATION'),
        },
        type: {
            title: i18n('WIDGET_ORDER_VOLUME_TYPE'),
        },
    };
}

function createTableRowsProps(orderVolumeList: OrderVolume[]) {
    const tableRowsProps = orderVolumeList.map((orderVolume, rowIndex) => {
        return {
            volume: {
                value: orderVolume.volume,
            },
            boxType: {
                value: orderVolume.boxType,
            },
            checkout: {
                value: orderVolume.checkout ? i18n('YES') : i18n('NO') || i18n('GENERAL_NO_INFORMATION'),
            },
            location: {
                value: orderVolume.location || i18n('GENERAL_NO_INFORMATION'),
            },
            type: {
                value: orderVolume.type || i18n('GENERAL_NO_INFORMATION'),
            },
        };
    });

    return tableRowsProps;
}
