import { WaitFetch } from 'application/shared/components/waitFetch';
import i18n from 'i18n';
import * as React from 'react';

import { ExpandableCard } from '../../../../application/components/expandable';
import { TabbedContent, TabProps, TabsContainerProps } from '../../../../application/components/tabbedContent';
import { fetchOrderData } from './apiService';
import { OrderDetails } from './orderDetails';
import { OrderItemList } from './orderItemList';
import { OrderStageList } from './orderStageList';
import { OrderVolumeList } from './orderVolumeList';
import { OrderShipmentDetails } from './orderShipmentDetails';
import { Order, OrderItem, OrderShipmentInformation, OrderStage, OrderVolume } from './orderOverviewQueryResponse';

export interface Props {
    order: Order;
    orderShipmentInformation: OrderShipmentInformation;
    orderItems: OrderItem[];
    orderVolumes: OrderVolume[];
    orderHistory: OrderStage[];
}

const Component = (props: Props) => {
    return (
        <ExpandableCard content={renderContent(props)} collapsedContent={renderCollapsedContent(props)} />
    );
};

function renderContent(props: Props) {
    return (
        <div className="row">
            <div className="col s12 m12 l8">
                <OrderDetails order={props.order} />
            </div>
            <div className="col s12 l3 offset-l1">
                <OrderStageList orderHistory={props.orderHistory} />
            </div>
        </div>
    );
}

function renderCollapsedContent(props: Props) {
    const volumesTabProps: TabProps = {
        label: i18n('VOLUMES'),
        tabContent: <OrderVolumeList orderVolumes={props.orderVolumes} />,
    };

    const itemsTabProps: TabProps = {
        label: i18n('ITEMS'),
        tabContent: <OrderItemList orderItems={props.orderItems} />,
    };

    const tabsContainerProps: TabsContainerProps = {
        tabs: [volumesTabProps, itemsTabProps],
    };

    return(
        <div className="row">
            <div className="col s12 l12">
                {!!props.orderShipmentInformation && renderOrderShipmentDetails(props)}
            </div>
            <div className="col s12 l12">
                <br />
                <TabbedContent {...tabsContainerProps} />
            </div>
        </div>
    );
}

function renderOrderShipmentDetails(props: Props) {
    return (
        <div className="row">
            <OrderShipmentDetails orderShipmentInformation={props.orderShipmentInformation} />
        </div>
    );
}

export class OrderOverview extends React.Component<any> {
    fetch(props: any) {
        return fetchOrderData(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}
