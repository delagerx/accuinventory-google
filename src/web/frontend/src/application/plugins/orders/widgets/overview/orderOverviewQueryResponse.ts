export interface OrderOverviewQueryResponse {
    order: Order;
}

export interface Order {
    id: string;
    entity: string;
    itemsQuantity: number;
    legacyCode: string;
    operation: string;
    origin: number;
    picking: string;
    status: string;
    subOperation: string;
}

export interface OrderItem {
    collected: string;
    productCode: string;
    productDescription: string;
    productDigit: string;
    quantity: string;
}

export interface OrderVolume {
    volume: string;
    boxType: string;
    checkout: string;
    location: string;
    type: string;
}

export interface OrderStage {
    key: string;
    timestamp: Date;
}

export interface OrderShipmentInformation {
    address: string;
    arrive: Date;
    assistant: string;
    city: string;
    conveyor: string;
    district: string;
    delivery: Date;
    departure: Date;
    driver: string;
    route: string;
    state: string;
    vehicle: string;
    zipCode: string;
}
