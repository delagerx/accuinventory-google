import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { EntityOverview } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const EntityOverviewConfiguration: WidgetDefinition = {
    name: 'EntityWidget',
    type: 'Entity',
    component: EntityOverview,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: `${props.code} - ${props.name}`,
            subtitle: i18n('ENTITY'),
            expandable: false,
            actions: [],
        };
    },
};
