import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { Entity } from './entity';

export function fetchEntityData(id: string): Promise<Entity>{
    return httpClient.get(baseApiUrl + `/entity/${id}/info`).then(x => x.data);
}
