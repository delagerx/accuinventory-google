import * as React from 'react';

import { Entity } from './entity';
import { fetchEntityData } from './apiService';
import { EntityDetails } from './entityDetails';
import { WaitFetch } from 'application/shared/components/waitFetch';

export interface Props extends Entity {
}

const Component = (props: Props) => {
    return (
        <div className="row card-content">
            <div className="col s12 m6 l6">
                <EntityDetails {...props} />
            </div>
        </div>
    );
};

export class EntityOverview extends React.Component<any> {
    fetch(props: any) {
        return fetchEntityData(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}
