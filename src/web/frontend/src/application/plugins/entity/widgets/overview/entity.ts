export interface Entity {
    id: string;
    coode: string;
    name: string;
    fantasyName: string;
    status: string;
    type: string;
}
