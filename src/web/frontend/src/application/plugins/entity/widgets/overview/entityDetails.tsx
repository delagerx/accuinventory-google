import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import { Props } from './';

export function EntityDetails(props: Props) {
  return (
    <ul className="row">
      <div className="col s12 l12">
        <div className="widget-body-infos">
          <span className="name">
            {i18n('ENTITY_FANTASY_NAME')}
          </span>
          <span className="description">{props.fantasyName}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('ENTITY_STATUS')}</span>
          <span className="description">{props.status}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('ENTITY_TYPE')}</span>
          <span className="description">{props.type}</span>
        </div>
      </div>
    </ul>
  );
}
