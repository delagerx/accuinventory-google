import * as React from 'react';
import i18n from 'i18n';
import { Props } from './';
import {Task} from './task';

export function TaskDetails(props: Task) {
  return (
    <div>
      {renderTaskInformation(props)}
    </div>
  );
}

function renderTaskInformation(props: Task) {
  return(
    <div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
            {i18n('WIDGET_TASKS_ORDER')}
          </span>
          <span className="description">{props.code}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
          {i18n('WIDGET_TASKS_OPERATION')}
          </span>
          <span className="description">{props.operation}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
          {i18n('WIDGET_TASKS_STATUS')}
          </span>
          <span className="description">{props.status}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
          {i18n('WIDGET_TASKS_DESCRIPTION')}
          </span>
          <span className="description">{props.description || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
          {i18n('WIDGET_TASKS_OPERATOR')}
          </span>
          <span className="description">{props.operator || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
            {i18n('WIDGET_TASKS_EQUIPMENT')}
          </span>
          <span className="description">{props.equipment || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
            {i18n('WIDGET_TASKS_PRIORITY')}
          </span>
          <span className="description">{props.priority || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l4">
        <div className="widget-body-infos">
          <span className="name">
          {i18n('WIDGET_TASKS_SECTOR')}
          </span>
          <span className="description">{props.sector || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
    </div>
  );
}
