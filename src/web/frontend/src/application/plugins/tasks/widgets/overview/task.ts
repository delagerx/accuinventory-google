export interface Task {
    id: string;
    code: string;
    status: string;
    operation: string;
    equipment: string;
    operator: string;
    sector: string;
    priority: string;
    description: string;
}
