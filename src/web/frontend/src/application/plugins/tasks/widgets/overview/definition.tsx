import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { TaskOverview } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const TaskOverviewConfiguration: WidgetDefinition = {
    name: 'TasksWidget',
    type: 'Task',
    component: TaskOverview,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: `${props.code} - ${props.operation} (${props.status})`,
            subtitle: i18n('TASK'),
            expandable: false,
            actions: [],
        };
    },
};
