import * as React from 'react';

import { Task } from './task';
import { fetchTaskData } from './apiService';
import { TaskDetails } from './taskDetails';
import { WaitFetch } from 'application/shared/components/waitFetch';

export interface Props extends Task {
}

const Component = (props: Props) => {
    return (
        <div className="row">
            <div className="col s12 m12 l12">
                <TaskDetails {...props} />
            </div>
        </div>
    );
};

export class TaskOverview extends React.Component<any> {
    fetch(props: any) {
        return fetchTaskData(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}
