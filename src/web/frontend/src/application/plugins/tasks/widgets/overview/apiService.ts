import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { Task } from './task';

export function fetchTaskData(id: string): Promise<Task>{
    return httpClient.get(baseApiUrl + `/task/${id}/info`).then(x => x.data);
}
