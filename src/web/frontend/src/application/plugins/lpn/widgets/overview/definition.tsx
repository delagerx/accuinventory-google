import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { LpnOverview } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const LpnOverviewConfiguration: WidgetDefinition = {
    name: 'LpnWidget',
    type: 'Lpn',
    component: LpnOverview,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: props.name || i18n('WIDGET_LPN_PARENT_LPN_WITHOUT_NUMBER'),
            subtitle: i18n('LPN'),
            expandable: false,
            actions: [],
        };
    },
};
