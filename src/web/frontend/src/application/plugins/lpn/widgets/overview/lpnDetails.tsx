import * as realize from 'realizejs';
import * as React from 'react';
import i18n from 'i18n';

import { WmsLinkComponent } from '../../../../navigation/containers/wmsLink';
import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import { DateTimeComponent } from '../../../../application/components/datetime/index';
import { Lpn } from './lpnOverviewQueryResponse';

export function LpnDetails(props: Lpn) {
  return (
    <div className="row">
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_ADDRESS')}</span>
          <span className="description">{renderLinkToAddress()}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_TYPE')}</span>
          <span className="description">{props.type}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_CREATED_DATE')}</span>
          <span className="description">{renderFormattedCreatedDate()}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_LOGISTIC_OPERATION')}</span>
          <span className="description">{props.logisticOperation || i18n('GENERAL_NO_INFORMATION')}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_PARENT_LPN_NUMBER')}</span>
          <span className="description">{renderLinkToParentLpn()}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_STATUS')}</span>
          <span className="description">{props.status}</span>
        </div>
      </div>
      <div className="col s12 l3">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_LPN_ACTIVE_MOVEMENT_ORDERS')}</span>
          <span className="description">{renderLinkToMovementOrders(props.activeMovementOrders, props.id)}</span>
        </div>
      </div>
    </div>
  );

  function renderFormattedCreatedDate() {
    if (!props.createdDate) {
      return i18n('GENERAL_NO_INFORMATION');
    }

    return (<DateTimeComponent dateAsObject={props.createdDate} />);
  }

  function renderLinkToParentLpn() {
    if (!props.parentLpnId) {
      return i18n('GENERAL_NO_INFORMATION');
    }

    const parentLpnNumber = props.parentLpnNumber || i18n('WIDGET_LPN_PARENT_LPN_WITHOUT_NUMBER');

    return (
      <ItemDetailsLink
        type={'Lpn'}
        contextProps={{id: props.parentLpnId}}
        text={parentLpnNumber}
      >
        {parentLpnNumber}
      </ItemDetailsLink>
    );
  }

  function renderLinkToMovementOrders(activeMovementOrder: number, lpnId: string) {
    const text = props.activeMovementOrders || i18n('GENERAL_NO_INFORMATION');
    if (props.activeMovementOrders) {
      return (
        <WmsLinkComponent
          route={'searchMovementOrder'}
          payload={{lpn: lpnId}}
          text={text}
        />
      );
    }
    return (
      <div>{text}</div>
    );
  }

  function renderLinkToAddress() {
    if (!props.addressId) {
      return i18n('GENERAL_NO_INFORMATION');
    }

    const address = props.address || i18n('GENERAL_NO_INFORMATION');

    return (
      <ItemDetailsLink
        type={'Address'}
        contextProps={{id: props.addressId}}
        text={address}
      >
        {address}
      </ItemDetailsLink>
    );
  }
}
