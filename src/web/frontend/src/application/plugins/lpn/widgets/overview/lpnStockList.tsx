import * as realize from 'realizejs';
import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { TableComponent } from '../../../../application/components/table/index';
import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import { DateTimeComponent } from '../../../../application/components/datetime/index';
import { ProductCodeComponent } from '../../../../application/components/productCode/index';
import { LpnStock } from './lpnOverviewQueryResponse';

export interface Props {
    lpnStocks: LpnStock[];
}

export function LpnStockList(props: Props) {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(props.lpnStocks);

    return(
        <TableComponent
          title={i18n('STOCK')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
}

function createTableColumnsProps() {
    return {
        productCode: {
            title: i18n('WIDGET_LPN_STOCK_PRODUCT_ID'),
        },
        productDescription: {
            title: i18n('WIDGET_LPN_STOCK_PRODUCT'),
        },
        lotCode: {
            title: i18n('WIDGET_LPN_STOCK_LOT'),
        },
        shelfLife: {
            title: i18n('WIDGET_LPN_STOCK_SHELF_LIFE'),
        },
        stockQuantity: {
            title: i18n('WIDGET_LPN_STOCK_QUANTITY'),
        },
    };
}

function createTableRowsProps(lpnStockList: LpnStock[]) {
    const tableRowsProps = lpnStockList.map((lpnStock, rowIndex) => {
        return {
            productCode: {
                value: renderProductCodeAsLink(lpnStock),
            },
            productDescription: {
                value: renderProductDescriptionAsLink(lpnStock),
            },
            lotCode: {
                value: renderLotCodeAsLink(lpnStock),
            },
            shelfLife: {
                value: renderFormattedShelfLife(lpnStock),
            },
            stockQuantity: {
                value: lpnStock.quantity,
            },
        };
    });

    return tableRowsProps;
}

function renderProductCodeAsLink(lpnStock: LpnStock) {
    return(
        <ItemDetailsLink
            type={'Product'}
            contextProps={{id: lpnStock.productId}}
            text={lpnStock.productDescription}
        >
            <ProductCodeComponent
                productCode={lpnStock.productId}
                productDigit={lpnStock.productDigit}
            />
        </ItemDetailsLink>
    );
}

function renderProductDescriptionAsLink(lpnStock: LpnStock) {
    return(
        <ItemDetailsLink
            type={'Product'}
            contextProps={{id: lpnStock.productId}}
            text={lpnStock.productDescription}
        >
            {lpnStock.productDescription}
        </ItemDetailsLink>
    );
}

function renderLotCodeAsLink(lpnStock: LpnStock) {
    const lotCode = lpnStock.lotCode || i18n('WIDGET_LPN_LOT_WITHOUT_NUMBER');

    return (
      <ItemDetailsLink
        type={'Lot'}
        contextProps={{id: lpnStock.lotId}}
        text={lotCode}
      >
        {lotCode}
      </ItemDetailsLink>
    );
}

function renderFormattedShelfLife(lpnStock: LpnStock) {
    if (!lpnStock.shelfLife) {
        return i18n('GENERAL_NO_INFORMATION');
    }

    return (
        <DateTimeComponent dateAsString={lpnStock.shelfLife} />
    );
}
