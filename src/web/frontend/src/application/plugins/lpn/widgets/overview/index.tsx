import * as React from 'react';

import { WaitFetch } from 'application/shared/components/waitFetch';
import { ExpandableCard } from '../../../../application/components/expandable';
import { fetchLpnData } from './apiService';
import { LpnDetails } from './lpnDetails';
import { LpnStockList } from './lpnStockList';
import { LpnTraceabilityHistory, LpnTraceabilityHistoryEntryProps } from './lpnTraceabilityHistory';
import { Lpn, LpnStock } from './lpnOverviewQueryResponse';

export interface Props {
    lpn: Lpn;
    lpnStocks: LpnStock[];
    lpnTraceabilityHistory: LpnTraceabilityHistoryEntryProps[];
}

const Component = (props: Props) => {
    return (
        <ExpandableCard content={renderContent(props)} collapsedContent={renderCollapsedContent(props)} />
    );
};

function renderContent(props: Props) {
    return (
        <div>
            <div className="col s12 m12 l12">
                <LpnDetails {...props.lpn} />
            </div>
        </div>
    );
}

function renderCollapsedContent(props: Props) {
    return(
        <div>
            <div className="col s12 m12 l8">
                {<LpnStockList lpnStocks={props.lpnStocks} />}
            </div>
            <div className="col s12 m12 l3 offset-l1">
                {<LpnTraceabilityHistory entries={props.lpnTraceabilityHistory} />}
            </div>
        </div>
    );
}

export class LpnOverview extends React.Component<any> {
    fetch(props: any) {
        return fetchLpnData(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}
