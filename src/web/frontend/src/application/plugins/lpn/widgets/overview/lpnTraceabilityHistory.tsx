import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { TimelineComponent, TimelineProps, TimelineEntryProps } from '../../../../application/components/timeline';

enum FlowDirection {
  Stagnant = 'Stagnant',
  Source = 'Source',
  Destiny = 'Destiny',
}
export interface LpnTraceabilityHistoryEntryProps {
  user: string;
  timestamp: Date;
  flowDirection: FlowDirection;
  movementQuantity: number;
}

interface Props {
  entries: LpnTraceabilityHistoryEntryProps[];
}

export function LpnTraceabilityHistory(props: Props) {
  const timelineEntries = props.entries.map(entry => {
    const flowDirection = entry.flowDirection === FlowDirection.Stagnant
    ? <i className="material-icons lpn_movement--stagnated">compare_arrows</i>
    : entry.flowDirection === FlowDirection.Source
      ? <i className="material-icons lpn_movement--source">chevron_left</i>
      : <i className="material-icons lpn_movement--destiny">chevron_right</i>;

    const timelineEntry: TimelineEntryProps = {
      title: renderHistoryEntry(entry),
      timestamp: entry.timestamp,
      isActive: false,
      icon: flowDirection,
    };
    return timelineEntry;
  });

  return (
    <TimelineComponent
      entries={timelineEntries}
      maxVisibleEntries={4}
      title={i18n('WIDGET_LPN_TRACEABILITY_HISTORY')}
      entryClassName=""
    />
  );
}

function renderHistoryEntry(props: LpnTraceabilityHistoryEntryProps) {
  return (
    <span>
      <div>
        <span className="history_entry--label">{i18n('GENERAL_QUANTITY')} </span>
        <span className="history_entry--value">{props.movementQuantity}</span>
      </div>
      <div>
        <span className="history_entry--label">{i18n('GENERAL_USER')} </span>
        <span className="history_entry--value">{props.user || i18n('GENERAL_NO_INFORMATION')}</span>
      </div>
    </span>
  );
}
