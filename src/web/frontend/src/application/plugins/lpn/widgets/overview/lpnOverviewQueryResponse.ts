export interface LpnOverviewQueryResponse {
    lpn: Lpn;
}

export interface Lpn {
    id: string;
    activeMovementOrders: number;
    address: string;
    addressId: string;
    createdDate: Date;
    logisticOperation: string;
    parentLpnId: string;
    parentLpnNumber: string;
    status: string;
    type: string;
    user: string;
    userRole: string;
}

export interface LpnStock {
    lotCode: string;
    lotId: string;
    productDescription: string;
    productDigit: string;
    productId: string;
    quantity: number;
    shelfLife: string;
}
