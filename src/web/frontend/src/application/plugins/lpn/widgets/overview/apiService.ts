import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { LpnOverviewQueryResponse } from './lpnOverviewQueryResponse';

export function fetchLpnData(id: string): Promise<LpnOverviewQueryResponse>{
    return httpClient.get(baseApiUrl + `/lpnOverview/${id}`).then(x => x.data);
}
