import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import i18n from 'i18n';
import * as React from 'react';

import { TableComponent } from '../../../../../application/components/table/index';
import { LotStock } from '../lotQueryResponse';

interface Props {
    readonly stocks: LotStock[];
}

export class ProductDetails extends React.Component<Props> {
  render() {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(this.props.stocks);

    return(
        <TableComponent
          title={i18n('PRODUCTS')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
  }
}

function createTableColumnsProps() {
    return {
        address: {
          title: i18n('ADDRESS_NAME'),
        },
        lpn: {
          title: i18n('LPN'),
        },
        stock: {
            title: i18n('PRODUCT_STOCK'),
        },
        status: {
            title: i18n('ADDRESS_STATUS'),
        },
    };
}

function createTableRowsProps(stockList: LotStock[]) {
    const tableRowsProps = stockList.map((stock, rowIndex) => {
        return {
            address: {
              value: renderAddressAsLink(stock.address, stock.addressId),
            },
            lpn: {
              value: renderLpnAsLink(stock.lpn, stock.lpnId),
            },
            stock: {
                value: stock.stock,
            },
            status: {
                value: stock.status,
            },
        };
    });

    return tableRowsProps;
}

function renderLpnAsLink(lpnNumber: string, lpnId: string) {
  const displayName = lpnNumber || i18n('WIDGET_LPN_PARENT_LPN_WITHOUT_NUMBER');
  return (
      <ItemDetailsLink
          type={'Lpn'}
          contextProps={{id: lpnId}}
          text={displayName}
      >
          {displayName}
      </ItemDetailsLink>
  );
}

function renderAddressAsLink(completeAddress: string, addressId: string) {
  return (
      <ItemDetailsLink
          type={'Address'}
          contextProps={{id: addressId}}
          text={completeAddress}
      >
          {completeAddress}
      </ItemDetailsLink>
  );
}
