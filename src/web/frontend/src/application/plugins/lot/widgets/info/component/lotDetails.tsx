import * as React from 'react';
import { LotInfo } from '../lotQueryResponse';
import i18n from 'i18n';
import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import { DateTimeComponent } from 'application/application/components/datetime';

export class LotDetails extends React.Component<LotInfo> {
  render() {
    return (
      <div>
          <div className="row">
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('PRODUCT_NAME')}</span>
                      <span className="description">
                        {renderProductAsLink(this.props.productDescription, this.props.productId)}
                      </span>
                  </div>
              </div>
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('PRODUCT_SHELF_LIFE')}</span>
                      <span className="description">
                        <DateTimeComponent dateAsString={this.props.shelfLife} />
                      </span>
                  </div>
              </div>
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('LOT_STATUS')}</span>
                      <span className="description">
                        {this.props.status}
                      </span>
                  </div>
              </div>
          </div>
          <div className="row">
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('LOT_MANUFACTURED_DATE')}</span>
                      <span className="description">
                        <DateTimeComponent dateAsString={this.props.manufacturedDate} />
                      </span>
                  </div>
              </div>
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('LOT_REGISTRATION_DATE')}</span>
                      <span className="description">
                        <DateTimeComponent dateAsString={this.props.registrationDate} />
                      </span>
                  </div>
              </div>
              <div className="col s12 l4">
                  <div className="widget-body-infos">
                      <span className="name">{i18n('LOT_PACKING')}</span>
                      <span className="description">
                        {this.props.packing || i18n('GENERAL_NO_INFORMATION')}
                      </span>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

function renderProductAsLink(productDescription: string, productId: string) {
  return (
      <ItemDetailsLink
          type={'Product'}
          contextProps={{id: productId}}
          text={productDescription}
      >
          {productDescription}
      </ItemDetailsLink>
  );
}
