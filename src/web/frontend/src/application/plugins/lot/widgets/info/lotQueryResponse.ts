export interface LotInfo {
  readonly name: string;
  readonly productId: string;
  readonly productDescription: string;
  readonly packing: string;
  readonly status: string;
  readonly shelfLife: string;
  readonly manufacturedDate: string;
  readonly registrationDate: string;
  readonly stocks: LotStock[];
}

export interface LotStock {
  readonly stock: number;
  readonly addressId: string;
  readonly address: string;
  readonly lpnId: string;
  readonly lpn: string;
  readonly status: string;
}
