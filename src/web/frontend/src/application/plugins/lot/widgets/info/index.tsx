import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';
import { fetchLotInfo } from './apiService';
import { LotWidget } from './component';
import { WaitFetch } from 'application/shared/components/waitFetch';

export class LotInfo extends React.Component<any> {
    fetch(props: any) {
        return fetchLotInfo(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={LotWidget}
            />
        );
    }
}
