import * as React from 'react';

import { LotDetails } from './lotDetails';
import { ProductDetails } from './productDetails';
import { LotInfo } from '../lotQueryResponse';

export function LotWidget(props: LotInfo) {
  return (
    <div className="row card-content">
        <div className="col s12 m12 l12">
            <LotDetails {...props}/>
            <ProductDetails stocks={props.stocks}/>
        </div>
    </div>
  );
};
