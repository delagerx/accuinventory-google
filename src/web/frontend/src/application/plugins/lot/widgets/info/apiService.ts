import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import {LotInfo} from './lotQueryResponse';

export function fetchLotInfo(name: string): Promise <LotInfo> {
    return httpClient.get(baseApiUrl + `/lot/${name}/info`).then(x => x.data);
}
