import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { LotInfo } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const LotInfoConfiguration: WidgetDefinition = {
    name: 'LotInfoWidget',
    type: 'Lot',
    component: LotInfo,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: props.name,
            subtitle: i18n('LOT'),
            expandable: false,
            actions: [],
        };
    },
};
