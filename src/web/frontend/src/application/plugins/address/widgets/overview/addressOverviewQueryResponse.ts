export interface AddressOverviewQueryResponse {
    address: Address;
    addressStocks: AddressStock[];
}

export interface Address {
    id: string;
    accessType: string;
    classification: string;
    completeAddress: string;
    status: string;
    storageType: string;
    volumeType: string;
}

export interface AddressStock {
    lotCode: string;
    lotId: string;
    productDescription: string;
    productId: string;
    productVerifyingDigit: string;
    quantity: number;
}
