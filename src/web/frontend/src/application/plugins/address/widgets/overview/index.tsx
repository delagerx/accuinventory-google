import * as React from 'react';

import { fetchAddressData } from './apiService';
import { WaitFetch } from 'application/shared/components/waitFetch';
import { AddressDetails } from './addressDetails';
import { AddressStockList } from './addressStockList';
import { Address, AddressStock } from './addressOverviewQueryResponse';

export interface Props {
    address: Address;
    addressStocks: AddressStock[];
}

const Component = (props: Props) => {
    return (
        <div className="row card-content">
            <div className="col s12 m6 l6">
                {renderAddressDetails(props.address)}
            </div>
            <div className="col s12 m6 l6">
                {renderAddressStockList(props.addressStocks)}
            </div>
        </div>
    );
};

function renderAddressDetails(props: Address) {
    return(<AddressDetails {...props} />);
}

function renderAddressStockList(props: AddressStock[]) {
    return(<AddressStockList addressStocks={props} />);
}

export class AddressOverview extends React.Component<any> {
    fetch(props: any) {
        return fetchAddressData(props.id);
    }
    render() {
        return (
            <WaitFetch
                {...this.props}
                fetch={this.fetch}
                render={Component}
            />
        );
    }
}
