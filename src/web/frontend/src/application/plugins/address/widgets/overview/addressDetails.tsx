import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { Address } from './addressOverviewQueryResponse';

export function AddressDetails(props: Address) {
  return (
    <ul className="row">
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">
            {i18n('WIDGET_ADDRESS_COMPLETE_ADDRESS')}
          </span>
          <span className="description">{props.completeAddress}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ADDRESS_STATUS')}</span>
          <span className="description">{props.status}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ADDRESS_STORAGE_TYPE')}</span>
          <span className="description">{props.storageType}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ADDRESS_CLASSIFICATION')}</span>
          <span className="description">{props.classification}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ADDRESS_ACCESS_TYPE')}</span>
          <span className="description">{props.accessType}</span>
        </div>
      </div>
      <div className="col s12 l6">
        <div className="widget-body-infos">
          <span className="name">{i18n('WIDGET_ADDRESS_VOLUME_TYPE')}</span>
          <span className="description">{props.volumeType}</span>
        </div>
      </div>
    </ul>
  );
}
