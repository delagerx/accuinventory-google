import * as React from 'react';
import i18n from 'i18n';
import * as style from './style.css';

import { ItemDetailsLink } from 'application/navigation/containers/itemDetailsLink';
import { TableComponent } from '../../../../application/components/table/index';
import { AddressStock } from './addressOverviewQueryResponse';

interface Props {
    addressStocks: AddressStock[];
}

export function AddressStockList(props: Props) {
    const columns = createTableColumnsProps();
    const rows = createTableRowsProps(props.addressStocks);

    return (
        <TableComponent
          title={i18n('STOCK')}
          columns={columns}
          items={rows}
          maxRowsDisplayed={10}
        />
    );
}

function createTableColumnsProps() {
    return {
        lotCode: {
            title: i18n('WIDGET_ADDRESS_STOCK_LOT'),
        },
        productDescription: {
            title: i18n('WIDGET_ADDRESS_STOCK_PRODUCT'),
        },
        stockQuantity: {
            title: i18n('WIDGET_ADDRESS_STOCK_QUANTITY'),
        },
    };
}

function createTableRowsProps(addressStockList: AddressStock[]) {
    const tableRowsProps = addressStockList.map((addressStock, rowIndex) => {
        return {
            lotCode: {
                value: renderLotCodeAsLink(addressStock),
            },
            productDescription: {
                value: renderProductDescriptionAsLink(addressStock),
            },
            stockQuantity: {
                value: addressStock.quantity,
            },
        };
    });

    return tableRowsProps;
}

function renderLotCodeAsLink(addressStock: AddressStock) {
    const lotCode = addressStock.lotCode || i18n('WIDGET_LPN_LOT_WITHOUT_NUMBER');

    return(
        <ItemDetailsLink
            type={'Lot'}
            contextProps={{id: addressStock.lotId}}
            text={lotCode}
        >
            {lotCode}
        </ItemDetailsLink>
    );
}

function renderProductDescriptionAsLink(addressStock: AddressStock) {
    return(
        <ItemDetailsLink
            type={'Product'}
            contextProps={{id: addressStock.productId}}
            text={addressStock.productDescription}
        >
            {addressStock.productDescription}
        </ItemDetailsLink>
    );
}
