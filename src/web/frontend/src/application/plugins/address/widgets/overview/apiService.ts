import httpClient from 'services/httpClient';
import { baseApiUrl } from 'constants/apis';
import { AddressOverviewQueryResponse } from './addressOverviewQueryResponse';

export function fetchAddressData(id: string): Promise<AddressOverviewQueryResponse> {
    return httpClient.get(baseApiUrl + `/addressOverview/${id}`).then(x => x.data);
}
