import { WidgetContextProps } from '../../../../widget/entities/widget';
import { ComponentDefinedProps } from '../../../../widget/entities/componentDefinedProps';
import { AddressOverview } from './index';
import { WidgetDefinition } from '../../../../widget/entities/widgetDefinition';
import i18n from 'i18n';

export const AddressOverviewConfiguration: WidgetDefinition = {
    name: 'AddressWidget',
    type: 'Address',
    component: AddressOverview,
    widgetPropsMapper: (props: WidgetContextProps): ComponentDefinedProps => {
        return {
            title: props.name,
            subtitle: i18n('ADDRESS'),
            expandable: false,
            actions: [],
        };
    },
};
