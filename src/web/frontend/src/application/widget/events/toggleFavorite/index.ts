import { WidgetState } from '../../entities/widgetState';
import { fromJS } from 'immutable';
import { Widget, WidgetKey } from '../../entities/widget';
import {Action, createAction, Reducer} from 'redux-actions';
import {Map} from 'immutable';

export const toggleFavoriteFailed = 'TOGGLE_FAVORITE_FAILED';
export const toggleFavoriteCommand = 'command/TOGGLE_FAVORITE_REQUESTED';
export interface ToggleFavoriteRequested {
    widget: Widget;
}

export const toggleFavoriteSucceeded = 'TOGGLE_FAVORITE_SUCCEEDED';
export interface ToggleFavoriteSucceeded {
    key: WidgetKey;
    favoriteStatus: boolean;
}

function updateWidget(widgetKey: WidgetKey, widgets: Map<WidgetKey, Widget>, newProps: Partial<Widget>): Map<WidgetKey, Widget>{
    return widgets.update(widgetKey, x => fromJS(x).mergeDeep(newProps).toJS());
}

function saveRequestedHandler(state: WidgetState, action: Action<ToggleFavoriteRequested>): WidgetState{
    return {
        ...state,
        widgets: updateWidget(action.payload.widget.key, state.widgets, ({transientProps: {isFavoriting: true}})),
    };
}

function toggleFavoriteHandler(state: WidgetState, action: Action<ToggleFavoriteSucceeded>): WidgetState{
  const favoriteStatus = action.payload.favoriteStatus;
  return {
      ...state,
      widgets: updateWidget(action.payload.key, state.widgets, ({transientProps: {isFavoriting: false}, internalProps: {favoriteStatus}})),
  };
}

function saveFailedHandler(state: WidgetState, action: Action<ToggleFavoriteRequested>): WidgetState{
    return {
        ...state,
        widgets: updateWidget(action.payload.widget.key, state.widgets, ({transientProps: {isFavoriting: false}})),
    };
}

export const handleSaveFailed = {[toggleFavoriteFailed]: saveFailedHandler};
export const handleToggleFavorite = {[toggleFavoriteSucceeded]: toggleFavoriteHandler};
export const handleSaveRequested = {[toggleFavoriteCommand]: saveRequestedHandler};

export const favoriteWidgetDispatcher = createAction<ToggleFavoriteRequested, Widget>(toggleFavoriteCommand, (widget: Widget) => ({widget}));

export function toggleFavoriteSucceededCreator(key: WidgetKey, favoriteStatus: boolean): Action<ToggleFavoriteSucceeded>{
    return {type: toggleFavoriteSucceeded, payload: {key, favoriteStatus}};
}
