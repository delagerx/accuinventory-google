import { WidgetState } from '../../entities/widgetState';
import {Action} from 'redux-actions';
import {Map} from 'immutable';
import {WidgetKey, Widget} from 'application/widget/entities/widget';

const widgetCreatedEvent = 'WIDGETS_CREATED';

export interface WidgetsCreated {
    widgets: Widget[];
}
export function widgetsCreatedEventCreator(widgets: Widget[]): Action<WidgetsCreated> {
    return {type: widgetCreatedEvent, payload: {widgets}};
}

function widgetsCreatedHandler(state: WidgetState, action: Action<WidgetsCreated>): WidgetState{
    return {
        ...state,
        widgets: Map<WidgetKey, Widget>(action.payload.widgets.map(w => [w.key, w])),
    };
}

export const handleWidgetsCreated = {[widgetCreatedEvent]: widgetsCreatedHandler};
