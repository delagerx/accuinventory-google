import { handleWidgetListUpdated } from './widgetList';
import { handleToggleFavorite } from './toggleFavorite';
import { handleSaveRequested, handleSaveFailed} from './toggleFavorite';
import { handleWidgetsCreated} from './widgetsCreated';
import {WidgetTransientProps} from '../entities/widgetTransientProps';
import { Widget, WidgetKey, WidgetMap } from '../entities/widget';
import { WidgetState } from '../entities/widgetState';
import { fromJS, Map } from 'immutable';
import {handleActions} from 'redux-actions';

const initialState: WidgetState = {
  widgets: Map<WidgetKey, Widget>(),
  widgetKeys: [],
};

export default handleActions<WidgetState, any>({
    ...handleSaveRequested,
    ...handleSaveFailed,
    ...handleToggleFavorite,
    ...handleWidgetsCreated,
    ...handleWidgetListUpdated,
}, initialState);
