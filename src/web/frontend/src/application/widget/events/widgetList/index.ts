import { type } from 'os';
import { WidgetContextProps, WidgetKey } from '../../entities/widget';
import { WidgetState } from '../../entities/widgetState';
import { WidgetType } from '../../entities/widgetType';
import {Action} from 'redux-actions';

const widgetListUpdated = 'WIDGET_LIST_UPDATE_REQUESTED';
export interface WidgetListUpdateRequested {
  widgetKeys: WidgetKey[];
}

function WidgetListUpdateRequestedHandler(state: WidgetState, action: Action<WidgetListUpdateRequested>): WidgetState {
  return {
    ...state,
    widgetKeys: action.payload.widgetKeys,
  };
}

export const handleWidgetListUpdated = {[widgetListUpdated]: WidgetListUpdateRequestedHandler};

export function widgetListUpdateRequestedCreator(widgetKeys: WidgetKey[]): Action<WidgetListUpdateRequested> {
  return {type: widgetListUpdated, payload: {widgetKeys}};
}
