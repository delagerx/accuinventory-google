import { Suggestion } from 'application/navigation/entities/suggestion';
import {Action} from 'redux-actions';
import { WidgetType } from 'application/widget/entities/widgetType';
import { WidgetContextProps } from 'application/widget/entities/widget';

export const updateWidgetListCommand = 'command/UPDATE_WIDGET_LIST';
export interface WidgetCreationRequest {
  type: WidgetType;
  contextProps: WidgetContextProps;
  actions: Suggestion[];
}
export interface UpdateWidgetListCommand {
  widgets: WidgetCreationRequest[];
}

export function updateWidgetListCommandCreator(widgets: WidgetCreationRequest[]): Action<UpdateWidgetListCommand> {
    return {type: updateWidgetListCommand, payload: {widgets: widgets}};
  }
