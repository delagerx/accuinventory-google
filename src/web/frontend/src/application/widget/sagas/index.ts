import { all } from 'redux-saga/effects';
import saveFavoriteSaga from './saveFavoriteSaga';
import updateWidgetList from './updateWidgetListSaga';

export default function* widgetSagas() {
    yield all([
      ...updateWidgetList(),
      ...saveFavoriteSaga(),
    ]);
  }
