import { UpdateWidgetListCommand, updateWidgetListCommand } from 'application/widget/commands/updateWidgetList';
import { Action } from 'redux-actions';
import { put, takeLatest } from 'redux-saga/effects';

import { WidgetCreationRequest } from '../commands/updateWidgetList/index';
import { Widget } from '../entities/widget';
import { widgetListUpdateRequestedCreator } from '../events/widgetList';
import { widgetsCreatedEventCreator } from '../events/widgetsCreated';
import { createWidgetsForType } from '../services/widgetService';

function createWidgets(widgetsToShow: WidgetCreationRequest[]) {
    const widgets = widgetsToShow.map(createWidgetsForType);
    return [].concat.apply([], widgets);
}

function * updateWidgetList(action: Action<UpdateWidgetListCommand>) {
    const widgets: Widget[] = createWidgets(action.payload.widgets);
    yield put(widgetsCreatedEventCreator(widgets));
    yield put(widgetListUpdateRequestedCreator(widgets.map(x => x.key)));
}

export default function* saga() {
    yield takeLatest(updateWidgetListCommand, updateWidgetList);
}
