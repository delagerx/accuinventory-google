import { Action } from 'redux-actions';
import { call, put, takeLatest } from 'redux-saga/effects';

import { toggleFavoriteCommand } from '../events/toggleFavorite';
import { ToggleFavoriteRequested, toggleFavoriteSucceededCreator } from '../events/toggleFavorite';
import { toggleFavoriteFailed } from '../events/toggleFavorite/index';
import { toggleFavorite } from '../services/favoriteService';
import {ErrorEvent} from '../../application/errors/events/errorEvent';

function* save(action: Action<ToggleFavoriteRequested>) {
   try {
        const favoriteStatus = !action.payload.widget.internalProps.favoriteStatus;
        yield call(() => toggleFavorite(action.payload.widget, favoriteStatus));
        yield put(toggleFavoriteSucceededCreator(action.payload.widget.key, favoriteStatus));
   } catch (e) {
        yield put<Action<ErrorEvent>>({type: toggleFavoriteFailed, payload: {error: e.message, exception: e}});
   }
}

export default function* saga(){
    yield takeLatest(toggleFavoriteCommand, save);
}
