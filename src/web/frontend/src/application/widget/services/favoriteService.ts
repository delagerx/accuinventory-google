import { UserFavoritedWidget } from '../../../services/apis/widget/dtos/userFavoriteWidget';
import {
    getUserFavoriteWidgets,
    removeFavoriteApi,
    saveFavoriteApi } from '../../../services/apis/widget';
import { availableWidgets } from '../../widget/services/availableWidgets';
import {Widget} from '../entities/widget';

export const toggleFavorite = (widget: Widget, favoriteStatus: boolean) => {
    if (favoriteStatus) {
      return saveFavoriteApi({
            id: widget.key,
            props: {contextProps: widget.contextProps, internalProps: {...widget.internalProps, favoriteStatus}},
            type: widget.widgetName,
        });
    } else {
        return removeFavoriteApi(widget.key);
    }
  };

const mapFavoriteToWidget = (favoriteWidget: UserFavoritedWidget): Widget => {
      const correspondingWidget = availableWidgets.find(x => x.type === favoriteWidget.type);
      return {
          key: favoriteWidget.id,
          widgetName: favoriteWidget.type,
          internalProps: favoriteWidget.props.internalProps,
          contextProps: favoriteWidget.props.contextProps,
          transientProps: {
              isFavoriting: false,
          },
      };
  };

export const getFavorites = (): Promise<Widget[]> => {
      return getUserFavoriteWidgets()
          .then(widgets => widgets.map(x => mapFavoriteToWidget(x)));
  };
