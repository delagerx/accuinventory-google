import * as Widgets from '../containers/configuredWidgets';
import {WidgetDefinition} from 'application/widget/entities/widgetDefinition';

export const availableWidgets: WidgetDefinition[] = [
    Widgets.AddressOverviewConfiguration,
    Widgets.LpnOverviewConfiguration,
    Widgets.OrderOverviewConfiguration,
    Widgets.ProductVisualizeConfiguration,
    Widgets.LotInfoConfiguration,
    Widgets.EntityOverviewConfiguration,
    Widgets.TaskOverviewConfiguration,
];
