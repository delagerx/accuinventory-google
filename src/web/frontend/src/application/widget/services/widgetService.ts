import { WidgetCreationRequest } from '../commands/updateWidgetList';
import * as React from 'react';
import * as hash from 'object-hash';

import { availableWidgets } from '../../widget/services/availableWidgets';
import { Widget, WidgetContextProps, WidgetKey } from '../entities/widget';
import { WidgetDefinition, WidgetName } from '../entities/widgetDefinition';
import { WidgetType } from '../entities/widgetType';

export function getWidgetsByType(widgetType: WidgetType): WidgetDefinition[] {
    return availableWidgets.filter(w => w.type === widgetType);
}

export function hasWidgetForType(widgetType: WidgetType): boolean {
    return availableWidgets.some(x => x.type === widgetType);
}

export function getWidgetsByKey(widgetKey: WidgetKey): WidgetDefinition {
    return availableWidgets.find(w => w.name === widgetKey);
}

export const createWidgetsForType = (creationRequest: WidgetCreationRequest): Widget[] => {
    return getWidgetsByType(creationRequest.type)
          .map((w, i) => createWidget(creationRequest, w.name));
};

export const createWidget = (creationRequest: WidgetCreationRequest, widgetName: WidgetName): Widget => {
    const widgetKey = hash(creationRequest.contextProps);
    return {
        key: widgetKey,
        widgetName: widgetName,
        contextProps: creationRequest.contextProps,
        internalProps: {
            favoriteStatus: false,
        },
        transientProps: {
            isFavoriting: false,
            actions: creationRequest.actions,
        },
    };
};
