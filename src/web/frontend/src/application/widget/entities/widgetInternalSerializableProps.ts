import { WidgetKey } from './widget';
export interface WidgetInternalSerializableProps {
    readonly favoriteStatus: boolean;
}
