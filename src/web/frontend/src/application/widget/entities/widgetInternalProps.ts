import { WidgetKey } from './widget';
export interface WidgetInternalProps {
    readonly favoriteStatus: boolean;
}
