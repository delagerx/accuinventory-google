import { Suggestion } from 'application/navigation/entities/suggestion';

export interface WidgetTransientProps {
    isFavoriting: boolean;
    readonly actions?: Suggestion[];
}
