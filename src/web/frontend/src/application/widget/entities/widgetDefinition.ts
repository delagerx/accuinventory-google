import { WidgetType } from './widgetType';
import { WidgetContextProps } from './widget';
import { ComponentDefinedProps } from './componentDefinedProps';
import * as React from 'react';

export type WidgetName = string;
export interface WidgetDefinition {
    readonly name: WidgetName;
    readonly type: WidgetType;
    readonly component: JSX.Element | React.SFC<any> | any;
    readonly widgetPropsMapper: (contextProps: WidgetContextProps) => ComponentDefinedProps;
}
