import { WidgetKey, WidgetMap } from './widget';
import { Map } from 'immutable';

export interface WidgetState {
    readonly widgets: WidgetMap;
    readonly widgetKeys: WidgetKey[];
}
