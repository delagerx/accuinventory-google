import { WidgetContextProps } from './widget';
import { WidgetInternalSerializableProps } from './widgetInternalSerializableProps';
export interface WidgetSerializableProps {
    readonly internalProps: WidgetInternalSerializableProps;
    readonly contextProps: WidgetContextProps;
}
