import { Map } from 'immutable';

import { WidgetName } from './widgetDefinition';
import { WidgetInternalSerializableProps } from './widgetInternalSerializableProps';
import { WidgetTransientProps } from './widgetTransientProps';
import { SuggestionContextProps } from 'application/navigation/entities/suggestionContextProps';

export type WidgetKey = string;
export type WidgetMap = Map<WidgetKey, Widget>;
export type WidgetContextProps = SuggestionContextProps;

export interface Widget {
    readonly key: WidgetKey;
    readonly widgetName: WidgetName;
    readonly internalProps: WidgetInternalSerializableProps;
    readonly contextProps: WidgetContextProps;
    readonly transientProps: WidgetTransientProps;
}
