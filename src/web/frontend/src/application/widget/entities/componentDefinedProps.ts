export interface ComponentDefinedProps {
    title: string;
    subtitle: string;
    avatar?: string;
    expandable: boolean;
    actions?: any[];
}
