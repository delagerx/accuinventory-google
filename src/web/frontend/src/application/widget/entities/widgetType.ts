import { SuggestionType } from 'application/navigation/entities/suggestionType';
export type WidgetType = SuggestionType;
