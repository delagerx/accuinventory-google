import { WidgetActionList } from './widgetActionList';
import { WidgetDefinition } from '../../entities/widgetDefinition';
import { getWidgetsByKey } from '../../services/widgetService';
import { WidgetKey, WidgetMap } from '../../entities/widget';
import * as React from 'react';
import * as ReactDom from 'react-dom';
import {connect} from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import {RootState} from 'infraestructure/redux/reducers/rootReducer';
import i18n from 'i18n';
import {Widget} from 'application/widget/entities/widget';
import {Props as WidgetProps, WidgetComponent} from '../../components/widget';
import {favoriteWidgetDispatcher} from 'application/widget/events/toggleFavorite';

export interface Props {
    widgets?: Widget[];
    widgetKeys: WidgetKey[];
    favorite?: (widget: Widget) => void;
}

function makeWidgetProps(widget: Widget, definition: WidgetDefinition, favorite: (widget: Widget) => void): WidgetProps {
  const componentDefinedProps = definition.widgetPropsMapper(widget.contextProps);
  return {
    internalProps: widget.internalProps,
    transientProps: widget.transientProps,
    componentDefinedProps: componentDefinedProps,
    favorite: () => favorite(widget),
  };
}

function renderWidget(widget: Widget, index: number, props: Props): JSX.Element {
    const definition = getWidgetsByKey(widget.widgetName);
    const Component: any = definition.component;
    const componentDefinedProps = definition.widgetPropsMapper(widget.contextProps);
    return (
        <div key={`widget-component-${widget.key}`}>
            <WidgetComponent
                {...makeWidgetProps(widget, definition, props.favorite)}
            >
                <Component {...widget.contextProps} toggleFavorite={() => props.favorite(widget)}/>
            </WidgetComponent>
            <WidgetActionList relatedAcions={widget.transientProps.actions}/>
        </div>
    );
}

@connect(mapStateToProps, mapDispatchToProps)
export class WidgetList extends React.Component<Props> {
    render() {
        return (
            <div className="row">
                {this.props.widgets.map((w, i) => renderWidget(w, i, this.props))}
            </div>
        );
    }
}

function mapStateToProps(state: RootState, ownProps: Props): Partial<Props> {
    const widgets = state
        .widget
        .widgets
        .filter(x => ownProps
            .widgetKeys
            .findIndex(y => y === x.key) !== -1,
        )
        .toArray();
    return {
        widgets: widgets,
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>, ownProps: Props): Partial<Props> {
    return {
        favorite: bindActionCreators(favoriteWidgetDispatcher, dispatch),
    };
}
