import { LinkList } from '../../../shared/components/linkList/linkList';
import { RelatedSuggestion } from 'application/navigation/entities/relatedSuggestion';
import * as React from 'react';

interface Props {
    relatedAcions?: RelatedSuggestion[];
}

export class WidgetActionList extends React.Component<Props> {
    render() {
        if (!this.props.relatedAcions) {
            return (
                <div/>
            );
        }
        return (
            <LinkList
                links={this.props.relatedAcions}
                total={this.props.relatedAcions.length}
            />
        );
    }
}
