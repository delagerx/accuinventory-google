export { AddressOverviewConfiguration } from '../../plugins/address/widgets/overview/definition';
export { LotInfoConfiguration } from '../../plugins/lot/widgets/info/definition';
export { LpnOverviewConfiguration } from '../../plugins/lpn/widgets/overview/definition';
export { ProductVisualizeConfiguration } from '../../plugins/products/widgets/info/definition';
export { OrderOverviewConfiguration } from '../../plugins/orders/widgets/overview/definition';
export { EntityOverviewConfiguration } from '../../plugins/entity/widgets/overview/definition';
export { TaskOverviewConfiguration} from '../../plugins/tasks/widgets/overview/definition';
