import i18n from 'i18n';
import Card from 'material-ui/Card';
import * as React from 'react';
import { WidgetHeader } from './widgetHeader';
import { WidgetFooter } from './widgetFooter';

import { WidgetInternalSerializableProps } from '../../entities/widgetInternalSerializableProps';
import { WidgetTransientProps } from '../../entities/widgetTransientProps';
import { ComponentDefinedProps } from 'application/widget/entities/componentDefinedProps';

export interface Props {
    transientProps: WidgetTransientProps;
    internalProps: WidgetInternalSerializableProps;
    componentDefinedProps: ComponentDefinedProps;
    children?: any;
    favorite: () => void;
}

const widgetComponent: React.SFC<Props> = (props) => {
  return (
      <Card className="widget">
        <div className="widget-header">
          <WidgetHeader
            isFavorite={props.internalProps.favoriteStatus}
            doFavorite={props.favorite}
            title={props.componentDefinedProps.title}
            subtitle={props.componentDefinedProps.subtitle}
          />
        </div>
        <div className="widget-body">
          {props.children}
        </div>
        <WidgetFooter actions={props.componentDefinedProps.actions}/>
      </Card>
    );
};

const defaultProps: Partial<Props> = {
  componentDefinedProps: {
    actions: [],
    expandable: false,
    subtitle: '',
    title: '',
  },
};
widgetComponent.defaultProps = defaultProps as Props;

export const WidgetComponent = widgetComponent;
