import * as React from 'react';
import { CardHeader } from 'material-ui/Card';
import { FavoriteAction } from './favoriteAction';

interface Props {
    isFavorite: boolean;
    doFavorite: () => void;
    title: string;
    subtitle?: string;
}

function headerProps(props: Props) {
    return {
      title: renderTitle(props),
      subheader: props.subtitle,
    };
}

function renderTitle(props: Props) {
    return (
      <div className="row">
        <div className="col s10">
          <h2 className="widget-header-heading">{props.title}</h2>
        </div>
        <div className="col s2">
          <FavoriteAction
            isFavorite={props.isFavorite}
            doFavorite={props.doFavorite}
          />
        </div>
      </div>
    );
}

export function WidgetHeader(props: Props) {
    return (
        <CardHeader {...headerProps(props)}/>
    );
}
