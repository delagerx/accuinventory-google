import * as React from 'react';
import IconButton from 'material-ui/IconButton';
import StarIcon from 'material-ui-icons/Star';
import StarBorderIcon from 'material-ui-icons/StarBorder';
import { i18n } from 'i18n';

interface Props {
    isFavorite: boolean;
    doFavorite: () => void;
}

export function FavoriteAction(props: Props) {
    const icon = props.isFavorite
        ? <div className="widget-fav active"><StarIcon/></div>
        : <div className="widget-fav"><StarBorderIcon/></div>;
    const tooltip = props.isFavorite
        ? i18n('WIDGET_ADD_TO_FAVORITES')
        : i18n('WIDGET_REMOVE_FAVORITE');
    return (
        <IconButton onClick={props.doFavorite} aria-label="Favorite">
            {icon}
        </IconButton>
    );
}