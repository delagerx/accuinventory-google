import * as React from 'react';
import { CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';

export interface Props {
    actions: any[];
}

function renderActions(actions: any[]) {
    if (!actions) {
      return <div/>;
    }
    return actions.map((Action, i) => <Button className="card-action" key={`action_${i}`} >{Action}</Button>);
}

export function WidgetFooter(props: Props) {
    if (props.actions && props.actions.length > 0) {
      return (
        <div className="widget-footer">
          <CardActions>
            {renderActions(props.actions)}
          </CardActions>
        </div>
      );
    } else {
      return <div/>;
    }
}
