import { SearchBox } from 'application/search/containers/searchBox';
import * as React from 'react';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import { Frequents } from 'application/home/frequents/containers/frequentsTab';
import { Dashboard } from 'application/home/dashboard/containers/dashboardTab';
import i18n from 'i18n';

interface State {
    currentTab: number;
}

export class HomePage extends React.Component<{}, State> {
    constructor() {
        super();
        this.state = {
            currentTab: 0,
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange = (event, value: number) => {
        this.setState({ currentTab: value });
    }
    render() {
        const classes = {
            rootInheritSelected: 'active',
            root: 'tab',
        };
        const rootClasses = {
            flexContainer: 'tabs',
            fixed: 'tabs-fixed',
        };
        return (
            <div>
                <section className="container">
                    <Tabs
                        classes={rootClasses}
                        value={this.state.currentTab}
                        onChange={this.handleChange}
                        centered={true}
                        indicatorClassName="tabs-indicator"
                    >
                        <Tab classes={classes} label={i18n('GENERAL_DASHBOARD')}/>
                        <Tab classes={classes} label={i18n('GENERAL_FREQUENTS')}/>
                    </Tabs>
                    {renderTabs(this.state.currentTab)}
                </section>
            </div>
        );
    }
}

function renderTabs(currentPage: number) {
    return currentPage === 0
        ? <section className="tab-dashboard"><Dashboard/></section>
        : <section className="tab-frequency"><Frequents/></section>;
}
