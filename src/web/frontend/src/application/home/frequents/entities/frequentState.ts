import { FrequentItem } from './frequentItem';
export interface FrequentsState {
    items: FrequentItem[];
}
