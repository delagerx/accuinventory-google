import { RouteName } from 'services/routes/routeNames';
export interface FrequentItem {
    name: RouteName;
    url: string;
    icon: string;
}
