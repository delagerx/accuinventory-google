import * as React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import { Dispatch } from 'redux';

interface Props {
    name: string;
    url: string;
    icon: string;
    onClick?: (url: string) =>  void;
}

export class FrequentItemComponent extends React.Component<Props> {
    render() {
        return (
            <a href={this.props.url} className="card-frequency">
                <div>
                        <i className="material-icons">{this.props.icon}</i>
                        <h3 className="title">
                        {this.props.name}
                        </h3>
                        <p className="description">{this.props.url}</p>
                </div>
            </a>
        );
    }
}
