import { FrequentItem } from '../entities/frequentItem';
import { FrequentItemComponent } from './frequentItem';
import * as React from 'react';

interface Props {
  items: FrequentItem[];
}

function renderItem(item: FrequentItem, index: number) {
  return (
    <div key={index} className="col s12 m6 l4 xl4">
      <FrequentItemComponent {...item}/>
    </div>
  );
}

export class FrequentList extends React.Component<Props> {
  render() {
    return (
      <div className="row">
          {this.props.items.map(renderItem)}
      </div>
    );
  }
}
