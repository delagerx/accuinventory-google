import { FrequentsState } from '../entities/frequentState';
import { eventNames, FetchFrequentResult } from './events';
import { handleActions, Action } from 'redux-actions';

const initialState: FrequentsState = {
  items: [],
};

export default handleActions<FrequentsState, any>({
  [eventNames.FREQUENTS_SUCCEEDED]: handleFetchCompleted,
}, initialState);

function handleFetchCompleted(state: FrequentsState, action: Action<FetchFrequentResult>): FrequentsState{
  return {
    ...state,
    items: action.payload.frequents,
  };
}
