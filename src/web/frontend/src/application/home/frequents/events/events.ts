import { FrequentItem } from '../entities/frequentItem';
import { createAction } from 'redux-actions';
import * as React from 'react';

export const eventNames = {
    FREQUENTS_REQUESTED: 'FREQUENTS_REQUESTED',
    FREQUENTS_SUCCEEDED: 'FREQUENTS_SUCCEEDED',
    FREQUENTS_FAILED: 'FREQUENTS_FAILED',
};

export interface FetchFrequentResult {
    frequents: FrequentItem[];
}

export const FetchFrequentsDispatcher = createAction<void, string>(eventNames.FREQUENTS_REQUESTED, () => ({}));
