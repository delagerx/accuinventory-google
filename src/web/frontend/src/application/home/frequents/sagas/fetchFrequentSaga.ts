import { call, put, takeLatest } from 'redux-saga/effects';
import { Action } from 'redux-actions';
import { fetchUserFrequents } from '../services/frequentService';
import { eventNames, FetchFrequentResult } from '../events/events';
import {ErrorEvent} from 'application/application/errors/events/errorEvent';

function* fetch(action: Action<void>) {
   try {
      const frequents = yield call(fetchUserFrequents);
      yield put<Action<FetchFrequentResult>>({type: eventNames.FREQUENTS_SUCCEEDED, payload: { frequents }});
   } catch (e) {
      yield put<Action<ErrorEvent>>({type: eventNames.FREQUENTS_FAILED, payload: {error: e.message, exception: e}});
   }
}

export default function* saga() {
    yield takeLatest(eventNames.FREQUENTS_REQUESTED, fetch);
}
