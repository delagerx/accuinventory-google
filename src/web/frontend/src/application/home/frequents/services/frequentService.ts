import { FrequentItem } from '../entities/frequentItem';
import { HistoryResponseItem } from '../../../../services/apis/history/dtos/HistoryResponseItem';
import { fetchUserHistory } from 'services/apis/history';
import i18n from 'i18n';

const mapResults = (historyItens: HistoryResponseItem[]): FrequentItem[] => {
    return historyItens.map(mapItem);
};

const mapItem = (historyItem: HistoryResponseItem): FrequentItem => {
    return {
        name: historyItem.name,
        url: historyItem.url,
        icon: historyItem.iconKey,
    };
};

export const fetchUserFrequents = (): Promise<FrequentItem[]> => {
    return fetchUserHistory().then(mapResults);
};
