import { FrequentItem } from '../../entities/frequentItem';
import { FrequentList } from '../../components/frequentList';
import { bindActionCreators, Dispatch } from 'redux';
import { FetchFrequentsDispatcher } from '../../events/events';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import {connect} from 'react-redux';

interface Props {
    frequentItems?: FrequentItem[];
    fetchFrequentItens?: () => void;
}

function mapStateToProps(state: RootState): Props {
    return {
        frequentItems: state.home.frequents.items,
    };
}

@connect(mapStateToProps, mapDispatchToProps)
export class Frequents extends React.Component<Props> {
    componentDidMount() {
        this.props.fetchFrequentItens();
    }
    render() {
        return (
            <FrequentList items={this.props.frequentItems}/>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        fetchFrequentItens: bindActionCreators(FetchFrequentsDispatcher, dispatch),
    };
}
