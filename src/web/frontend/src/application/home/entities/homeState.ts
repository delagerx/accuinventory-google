import { DashboardState } from '../dashboard/entities/dashboardState';
import { FrequentsState } from '../frequents/entities/frequentState';
export interface HomeState {
    frequents: FrequentsState;
    dashboard: DashboardState;
}
