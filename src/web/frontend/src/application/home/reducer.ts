import { WidgetKey } from '../widget/entities/widget';
import { HomeState } from './entities/homeState';
import { combineReducers } from 'redux';
import frequentReducer from './frequents/events/eventHandlers';
import dashboardReducer from './dashboard/events/eventHandlers';
import { Set } from 'immutable';

const combined = combineReducers<HomeState>({
  frequents: frequentReducer,
  dashboard: dashboardReducer,
});

const initialState: HomeState = {
  frequents: {
    items: [],
  },
  dashboard: {
    widgets: Set<WidgetKey>(),
  },
};

export default (state = initialState, action) => combined(state, action);
