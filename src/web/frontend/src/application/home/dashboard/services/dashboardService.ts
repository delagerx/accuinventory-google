import {WidgetDefinition} from 'application/widget/entities/widgetDefinition';
import {Widget} from 'application/widget/entities/widget';
import { getFavorites } from 'application/widget/services/favoriteService';
export const loadUserFavoriteWidgets = (availableWidgets: WidgetDefinition[]): Promise < Widget[] > => {
    return getFavorites();
};
