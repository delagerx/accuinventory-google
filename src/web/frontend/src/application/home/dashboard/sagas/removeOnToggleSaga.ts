import { RootState } from '../../../../infraestructure/redux/reducers/rootReducer';
import {
    eventNames,
    favoriteWidgetRemovedCreator,
    loadFavoriteFailedCreator,
    loadFavoriteWidgetsCompletedCreator,
} from '../events/events';
import { availableWidgets } from '../../../widget/services/availableWidgets';
import { loadUserFavoriteWidgets } from '../services/dashboardService';
import { Widget, WidgetKey } from '../../../widget/entities/widget';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { Action } from 'redux-actions';
import * as React from 'React';
import { Set } from 'immutable';
import {ToggleFavoriteSucceeded, toggleFavoriteSucceeded} from 'application/widget/events/toggleFavorite';

const getWidgets = (state: RootState): Set < WidgetKey > => state.home.dashboard.widgets;
function * updateWidgetList(action: Action<ToggleFavoriteSucceeded>) {
    if (!action.payload.favoriteStatus) {
      yield put(favoriteWidgetRemovedCreator(action.payload.key));
    }
}

export default function* saga(){
    yield takeLatest(toggleFavoriteSucceeded, updateWidgetList);
}
