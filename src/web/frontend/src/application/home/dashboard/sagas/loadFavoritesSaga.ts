import { eventNames, loadFavoriteFailedCreator, loadFavoriteWidgetsCompletedCreator } from '../events/events';
import { availableWidgets } from '../../../widget/services/availableWidgets';
import { loadUserFavoriteWidgets } from '../services/dashboardService';
import { Widget, WidgetKey } from '../../../widget/entities/widget';
import { call, put, takeLatest } from 'redux-saga/effects';
import { Action } from 'redux-actions';
import * as React from 'React';
import { Set } from 'immutable';
import {widgetsCreatedEventCreator} from 'application/widget/events/widgetsCreated';

function * updateWidgetList(action: Action < void >) {
    try {
      const widgets: Widget[] = yield call(() => loadUserFavoriteWidgets(availableWidgets));
      yield put(loadFavoriteWidgetsCompletedCreator(Set<WidgetKey>(widgets.map(x => x.key))));
      yield put(widgetsCreatedEventCreator(widgets));
    } catch (e) {
      yield put(loadFavoriteFailedCreator(e));
    }
}

export default function* saga() {
    yield takeLatest(eventNames.LOAD_WIDGET_REQUESTED, updateWidgetList);
}
