import { WidgetList } from 'application/widget/containers/widgetContainer';
import { WidgetKey } from '../../../../widget/entities/widget';
import { bindActionCreators, Dispatch } from 'redux';
import { loadFavoritedWidgetsDispatcher } from '../../events/events';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import {connect} from 'react-redux';

interface Props {
    favoriteWidgetKeys?: WidgetKey[];
    fetchFavorites?: any;
}

@connect(mapStateToProps, mapDispatchToProps)
export class Dashboard extends React.Component<Props> {
    componentWillMount() {
        this.props.fetchFavorites();
    }

    render() {
        return (
            <div className="row">
                 <WidgetList widgetKeys={this.props.favoriteWidgetKeys}/>
            </div>
        );
    }
}

function mapStateToProps(state: RootState): Props {
    return {
        favoriteWidgetKeys: state.home.dashboard.widgets.toArray(),
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>, ownProps: Props) {
    return {
        fetchFavorites: bindActionCreators(loadFavoritedWidgetsDispatcher, dispatch),
    };
}
