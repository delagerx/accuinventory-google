import { WidgetKey } from '../../../widget/entities/widget';
import { createAction, Action } from 'redux-actions';
import { Set } from 'immutable';

export const eventNames = {
    LOAD_WIDGET_REQUESTED: 'LOAD_WIDGET_REQUESTED',
    LOAD_WIDGET_SUCCEEDED: 'LOAD_WIDGET_SUCCEEDED',
    LOAD_WIDGET_FAILED: 'LOAD_WIDGET_FAILED',
    FAVORITE_WIDGET_REMOVED: 'FAVORITE_WIDGET_REMOVED',
};

export interface LoadWidgetsCompleted {
    widgetKeys: Set<WidgetKey>;
}

export interface FavoriteWidgetRemoved {
    widgetToRemove: WidgetKey;
}

export const loadFavoritedWidgetsDispatcher = createAction(eventNames.LOAD_WIDGET_REQUESTED);
export const loadFavoriteWidgetsCompletedCreator = (widgetKeys: Set<WidgetKey>): Action<LoadWidgetsCompleted> => ({type: eventNames.LOAD_WIDGET_SUCCEEDED, payload: {widgetKeys}});
export const favoriteWidgetRemovedCreator = (widgetToRemove: WidgetKey): Action<FavoriteWidgetRemoved> => ({type: eventNames.FAVORITE_WIDGET_REMOVED, payload: {widgetToRemove}});
export const loadFavoriteFailedCreator = (error: string) => ({type: eventNames.LOAD_WIDGET_FAILED, payload: {message: error}});
