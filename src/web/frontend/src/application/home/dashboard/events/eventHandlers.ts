import { DashboardState } from '../entities/dashboardState';
import { Widget, WidgetKey } from '../../../widget/entities/widget';
import { eventNames, FavoriteWidgetRemoved, LoadWidgetsCompleted } from './events';
import { handleActions, Action } from 'redux-actions';
import { Set } from 'immutable';

const initialState: DashboardState = {
  widgets: Set<WidgetKey>(),
};

export default handleActions<DashboardState, any>({
  [eventNames.LOAD_WIDGET_SUCCEEDED]: handleLoadCompleted,
  [eventNames.FAVORITE_WIDGET_REMOVED]: handleFavoriteRemoved,
}, initialState);

function handleLoadCompleted(state: DashboardState, action: Action<LoadWidgetsCompleted>): DashboardState{
    return {
        ...state,
        widgets: action.payload.widgetKeys,
    };
}

function handleFavoriteRemoved(state: DashboardState, action: Action<FavoriteWidgetRemoved>): DashboardState{
    return {
        ...state,
        widgets: state.widgets.remove(action.payload.widgetToRemove),
    };
}
