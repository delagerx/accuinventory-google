import { WidgetKey } from '../../../widget/entities/widget';
import { Set } from 'immutable';
export interface DashboardState {
    widgets: Set<WidgetKey>;
}
