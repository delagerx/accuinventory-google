import { InternalLinkComponent } from '../../../navigation/containers/internalLink';
import * as React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Dispatch } from 'redux';

interface Props {
    redirectToHome?: () => void;
}

export class Logo extends React.Component<Props> {
    render() {
        return (
            <InternalLinkComponent
                className="brand"
                routeName={'Home'}
            >
                <img src="/assets/img/ai_logo.png" alt=""/>
            </InternalLinkComponent>
        );
    }
}
