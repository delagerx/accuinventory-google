import Pagination from 'react-js-pagination';
import * as React from 'react';
import IconButton from 'material-ui/IconButton';
import ChevronRight from 'material-ui-icons/ChevronRight';
import ChevronLeft from 'material-ui-icons/ChevronLeft';

interface Props {
    total: number;
    pageSize: number;
    pageNumber: number;
    pageRangeDisplayed: number;
    onPageChange: (page: number) => void;
}

const prev = (
    <IconButton aria-label="Previous page" component="a">
        <ChevronLeft />
    </IconButton>
);
const next = (
    <IconButton aria-label="Next page" component="a">
        <ChevronRight />
    </IconButton>
);
export function AppPagination(props: Props) {
    return (
        <Pagination
            totalItemsCount={props.total}
            itemsCountPerPage={props.pageSize}
            pageRangeDisplayed={10}
            activePage={props.pageNumber}
            onChange={props.onPageChange}
            activeClass="active"
            prevPageText={prev}
            nextPageText={next}
            linkClassFirst="hide"
            linkClassLast="hide"
        />
    );
}
