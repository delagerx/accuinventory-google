import * as React from 'react';
import i18n from 'i18n';
import { LinkList, LinkListProps } from 'application/shared/components/linkList/linkList';
import { AppPagination } from 'application/shared/components/appPagination';

export interface PaginationProps extends LinkListProps {
    pageNumber?: number;
    pageSize?: number;
    changePage?: (page: number) => void;
    emptyListMessage?: string;
}

export class PaginatedLinkList extends React.Component<PaginationProps> {
    renderEmptyList(props: PaginationProps) {
        return props.emptyListMessage
            ? <h2 className="subtitle-page">{props.emptyListMessage}</h2>
            : <div/>;
    }

    renderList(props: PaginationProps) {
        return (
            <div>
                <h2 className="subtitle-page">{`${props.total} ${i18n('LINK_LIST_FOUND_RESULTS')}`}</h2>
                <LinkList {...props}/>
                <AppPagination
                    total={props.total}
                    pageSize={props.pageSize}
                    pageRangeDisplayed={10}
                    pageNumber={props.pageNumber}
                    onPageChange={props.changePage}
                />
            </div>
        );
    }
    render() {
        const props = this.props;
        return props.links && props.links.length > 0
            ? this.renderList(this.props)
            : this.renderEmptyList(this.props);
    }
}
