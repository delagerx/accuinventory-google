import { SearchResultItem } from 'application/results/entities/searchResultItem';
import { Suggestion } from 'application/navigation/entities/suggestion';
import * as React from 'react';
import { i18n, translateSuggestionType } from 'services/locales/i18n';
import { PermissionableLinkComponent } from 'application/shared/components/permissionableLink';

import { LinkRelatedSuggestions } from './linkRelatedSuggestions';

interface Props {
    item: SearchResultItem;
}

export class LinkItem extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this.renderRelatedActions = this.renderRelatedActions.bind(this);
    }
    renderRelatedAction(suggestion: Suggestion, index: number) {
        return (
            <LinkRelatedSuggestions key={`action_${index}`} suggestion={suggestion}/>
        );
    }
    renderRelatedActions(item: SearchResultItem) {
        const relatedActions = item.relatedSuggestions;
        const relatedLinks = relatedActions
            ? relatedActions.map(this.renderRelatedAction)
            : <div/>;
        return (
            <nav className="links">
                {relatedLinks}
            </nav>
        );
    }

    render() {
        const props = this.props;
        const category = `${i18n('LINK_LIST_ITEM_CATEGORY')}: ${translateSuggestionType(props.item.type)}`;
        return (
            <a href={this.props.item.url}>
                <article className={`result-link-list-item`}>
                    <h3 className="heading">
                        <PermissionableLinkComponent
                          url={this.props.item.url}
                          text={props.item.text}
                          hasPermission={this.props.item.hasPermission}
                        />
                    </h3>
                    <span className="category">{category}</span>
                    <nav className="links">
                        {this.renderRelatedActions(props.item)}
                    </nav>
                </article>
            </a>
        );
    }
}
