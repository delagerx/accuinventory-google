import { RelatedSuggestion } from 'application/navigation/entities/relatedSuggestion';
import * as React from 'react';

interface RelatedSuggestionProps {
    suggestion: RelatedSuggestion;
}

export class LinkRelatedSuggestions extends React.Component<RelatedSuggestionProps> {
    render() {
        return (
            <a href={this.props.suggestion.url}>
                {this.props.suggestion.text}
            </a>
        );
    }
}
