import { SearchResultItem } from 'application/results/entities/searchResultItem';
import { LinkItem } from 'application/shared/components/linkList/linkItem';
import * as React from 'react';

export interface LinkListProps {
    links?: SearchResultItem[];
    total: number;
}

function renderLinks(props: LinkListProps) {
    return (
        <div>
            {props.links.filter(x => !x.hide).map((x, i) => <LinkItem key={i} item={x}/>)}
        </div>
    );
}

export class LinkList extends React.Component<LinkListProps> {
    render() {
        return (
            <div className="result-link-list">
                {renderLinks(this.props)}
            </div>
        );
    }
}
