import * as React from 'react';
import { RouteComponentProps } from 'react-router';

export interface Props extends RouteComponentProps<void, void> {
    title: string;
    children?: any[];
}

export const Page = (props: Props) => {
    return (
            <div className="container">
                <h3>{props.title}</h3>
                {props.children}
            </div>
        );
};
