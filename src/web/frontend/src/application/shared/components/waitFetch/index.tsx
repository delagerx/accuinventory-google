import { LoadingSpinner } from '../loading/loading';
import * as React from 'react';

interface Props<TFetchResult, TProps> {
    fetch: (props: TProps) => Promise<TFetchResult>;
    render: (props: TFetchResult & TProps) => any;
}

interface State<TFetchResult> {
    fetching: boolean;
    fetchProps: TFetchResult;
}

export class WaitFetch<TFetchResult, TComponentProps> extends React.Component<Props<TFetchResult, TComponentProps>, State<TFetchResult>> {
    state = {
        fetching: true,
        fetchProps: undefined,
    };

    componentWillMount() {
        const props: TComponentProps = {...this.props as any};
        this.props.fetch(props).then(x =>
            this.setState({
                fetching: false,
                fetchProps: x,
            })
        );
    }

    render() {
        if (this.state.fetching) {
            return <LoadingSpinner isLoading={true}/>;
        }
        return this.props.render({...this.state.fetchProps, ...this.props as any});
    }
}
