import * as React from 'react';
import Lock from 'material-ui-icons/Lock';
import i18n from 'i18n';

interface Props {
  hasPermission: boolean;
  children?: any;
  text: string;
  url: string;
  className?: string;
}

export class PermissionableLinkComponent extends React.Component<Props> {
  render() {
    const content = this.props.hasPermission
      ? <div/>
      : <div className="permission-forbidden"><Lock/><span>{i18n('PERMISSION_FORBIDDEN')}</span></div>;
    return (
      <a href={this.props.url} className={this.props.className}>
        {this.props.text}
        {content}
      </a>
    );
  }
}
