import * as React from 'react';
const Spinner = require('react-spinkit');

interface Props {
    isLoading: boolean;
}

interface State {
    loading: boolean;
}

export class LoadingSpinner extends React.Component<Props, State> {
    state = {
        loading: this.props.isLoading,
    };

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.isLoading !== this.state.loading) {
            this.setState({loading: nextProps.isLoading});
        }
    }

    renderLoading() {
        return <div className="loading"><Spinner fadeIn="none" name="circle" color="orange" /></div>;
    }

    renderComponent() {
        return <div>{this.props.children}</div>;
    }

    render() {
        return this.state.loading
            ? this.renderLoading()
            : this.renderComponent();
    }
}
