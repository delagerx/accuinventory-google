export interface SearchQuery {
    query: string;
    page: number;
    pageSize: number;
}
