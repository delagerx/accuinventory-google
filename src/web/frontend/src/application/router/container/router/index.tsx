import { FullHeaderPage } from '../../../header/containers/fullHeader/fullHeaderPage';
import {callbackRouterUrlPattern, rootUrl} from 'services/routes/routeUrls';
import * as React from 'react';
import { Router, Route, IndexRedirect } from 'react-router';
import { History } from 'history';
import { CompactHeaderPage } from '../../../header/containers/compactHeader/compactHeaderPage';
import { ReactRoute, homeRoute, otherRoutes, errorRoute } from 'application/router/routes';
import {CallbackPage} from '../../../application/login/containers/callback';
import {AuthenticatedPage} from '../../../application/containers/authenticatedPage';

interface Props {
    history: History;
}

function compactHeaderPage(route: ReactRoute): any {
  return (props) => (
    <CompactHeaderPage {...props}>
      <route.component {...props}/>
    </CompactHeaderPage>
  );
}

function renderHome(): any {
    return (props) => (
      <FullHeaderPage {...props}>
        <homeRoute.component {...props}/>
      </FullHeaderPage>
    );
}

const renderRoute = (route: ReactRoute): JSX.Element => {
  return <Route key={route.name} path={route.routerUrlPattern} component={compactHeaderPage(route)}/>;
};

export class AppRouter extends React.Component<Props> {
  render() {
    const homeComponent = renderHome();
    return (
      <Router history={this.props.history}>
        <Route path={callbackRouterUrlPattern} component={CallbackPage}/>
        <Route path={errorRoute.routerUrlPattern} component={errorRoute.component}/>
        <Route path={rootUrl} component={AuthenticatedPage}>
          <IndexRedirect to={homeRoute.routerUrlPattern}/>
          <Route key={homeRoute.name} path={homeRoute.routerUrlPattern} component={homeComponent}/>
          {otherRoutes.map(renderRoute)}
        </Route>
      </Router>
    );
  }
}
