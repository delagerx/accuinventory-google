import { HomePage } from 'application/home/containers/homePage';
import { ResultPage } from 'application/results/containers/resultPage';
import { NotFoundPage } from 'application/application/errors/containers/notFoundPage';
import { ErrorPage } from 'application/application/errors/containers/errorPage';
import { SelectedPage } from 'application/selectedItem/containers/selectedPage/index';
import { RouteEntry } from 'services/routes/routeEntry';
import { Routes } from 'services/routes/routes';

export interface ReactRoute extends RouteEntry {
  component: any;
}

export const homeRoute: ReactRoute = {
  ...Routes.home,
  component: HomePage,
};

export const errorRoute: ReactRoute = {
  ...Routes.error,
  component: ErrorPage,
};

export const otherRoutes: ReactRoute[] = [
  {
    ...Routes.item,
    component: SelectedPage,
  },
  {
    ...Routes.search,
    component: ResultPage,
  },
  {
    ...Routes.notFound,
    component: NotFoundPage,
  },
];
