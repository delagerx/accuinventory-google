export interface RedirectLink {
    url: string;
    name: string;
    icon: string;
    save: boolean;
}
