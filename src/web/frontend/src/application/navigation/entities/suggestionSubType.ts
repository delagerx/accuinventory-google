type actionTypes =  'WmsRxLink' | 'ExternalLink' | 'InternalLink';

export type SuggestionSubType =
  actionTypes
  | 'None';
