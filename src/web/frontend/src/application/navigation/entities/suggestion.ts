import {SuggestionType} from 'application/navigation/entities/suggestionType';
import {SuggestionSubType} from 'application/navigation/entities/suggestionSubType';
import {SuggestionContextProps} from './suggestionContextProps';
export interface Suggestion {
    text: string;
    type: SuggestionType;
    subType: SuggestionSubType;
    contextProps: any;
    url: string;
    hasPermission: boolean;
}
