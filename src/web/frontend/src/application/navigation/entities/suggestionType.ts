export type SuggestionType =
    'Task'
    | 'Order'
    | 'Address'
    | 'Product'
    | 'Lpn'
    | 'Action'
    | 'Lot'
    | 'Entity';
