import { createRedirectLink } from 'application/navigation/services/navigationService';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';

import { RouteName } from '../../../services/routes/routeNames';
import { LogisticOperationId } from '../../application/session/entities/logisticOperation';

export interface Props {
    routeName: RouteName;
    className?: string;
    lo?: LogisticOperationId;
}
@connect(mapStateToProps)
export class InternalLinkComponent extends React.Component<Props> {
    url = '';
    componentWillMount() {
      this.url = createRedirectLink({
        text: this.props.routeName,
        type: 'Action',
        subType: 'InternalLink',
        contextProps: {
          routeName: this.props.routeName,
        },
        currentLogisticOperationId: this.props.lo,
      });
    }
    render() {
        return (
            <a className={this.props.className} href={this.url}>
                {this.props.children}
            </a>
        );
    }
}

function mapStateToProps(state: RootState, incomingProps: Props): Partial<Props> {
  return {
    lo: incomingProps.lo ? incomingProps.lo : state.session.currentLogisticOperation.id,
  };
}
