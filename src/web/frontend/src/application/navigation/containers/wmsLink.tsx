import { createRedirectLink } from 'application/navigation/services/navigationService';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';

import { RouteName } from '../../../services/routes/routeNames';
import { LogisticOperationId } from '../../application/session/entities/logisticOperation';
import { WmsRoutes, wmsRoutes } from 'constants/wmsRoutes';
import i18n from 'i18n';

export interface Props {
    className?: string;
    route: (keyof WmsRoutes);
    payload?: any;
    text?: string | JSX.Element;
    lo?: LogisticOperationId;
}
@connect(mapStateToProps)
export class WmsLinkComponent extends React.Component<Props> {
    url = '';
    text = '';
    componentWillMount() {
      const route = wmsRoutes[this.props.route];
      this.text = this.props.text || i18n(route.name);
      this.url = createRedirectLink({
        text: i18n(route.name),
        type: 'Action',
        subType: 'WmsRxLink',
        contextProps: {
          url: route.url,
          ...this.props.payload,
        },
        currentLogisticOperationId: this.props.lo,
      });
    }
    render() {
      if (this.props.children) {
        return (
          <a className={this.props.className} href={this.url}>
              {this.props.children}
          </a>
        );
      } else {
        return (
          <a className={this.props.className} href={this.url}>
            {this.text}
          </a>
        );
      }
    }
}

function mapStateToProps(state: RootState, incomingProps: Props): Partial<Props> {
  return {
    lo: incomingProps.lo ? incomingProps.lo : state.session.currentLogisticOperation.id,
  };
}
