import { LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { SuggestionContextProps } from 'application/navigation/entities/suggestionContextProps';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { createRedirectLink } from 'application/navigation/services/navigationService';
import { getCurrentLogisticOperationId } from 'application/application/session/events/eventHandlers';
import { connect } from 'react-redux';
import * as React from 'react';
export interface Props {
  className?: string;
  text: string;
  type: SuggestionType;
  contextProps: any;
  currentLogisticOperationId?: string;
}

@connect(mapStateToProps)
export class ItemDetailsLink extends React.Component<Props> {
  url = '';
  componentWillMount() {
    this.url = createRedirectLink({
      text: this.props.text,
      type: this.props.type,
      subType: 'None',
      contextProps: this.props.contextProps,
      currentLogisticOperationId: this.props.currentLogisticOperationId,
    });
  }
  render() {
    return (
      <a href={this.url} className={this.props.className}>
        {this.props.children}
      </a>
    );
  }
}

function mapStateToProps(state: RootState): Partial<Props> {
  return {
    currentLogisticOperationId: getCurrentLogisticOperationId(state),
  };
}
