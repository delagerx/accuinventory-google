import { RedirectApi } from 'constants/apis';
import * as queryString from 'query-string';

import { configuredRedirectLinkFactories } from './suggestionUrlGenerators';
import { RedirectLinkRequest } from './suggestionUrlGenerators/redirectLinkRequest';

export function createRedirectLink(request: RedirectLinkRequest): string {
    const factory = configuredRedirectLinkFactories.find(x => x.decider(request));
    const redirectLinkProps = factory.redirectLinkCreator(request);
    const redirectQueryString = queryString.stringify(redirectLinkProps);
    /*const fullUrl = `${RedirectApi}?${redirectQueryString}`;*/
    //Alterado temporariamente para não utilizar a api de redirect, consequentemente não salvando no histórico,
    //devido a autenticação em gets. É necessário separar o endpoint que faz esse redirect em um sistema que se integre
    //com a nova autenticação
    const fullUrl = redirectLinkProps.url;
    return fullUrl;
}
