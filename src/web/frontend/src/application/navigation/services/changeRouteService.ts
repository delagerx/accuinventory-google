import { RootState } from '../../../infraestructure/redux/reducers/rootReducer';
import {
  creator as createInternalLink } from 'application/navigation/services/suggestionUrlGenerators/internalLinkGenerator';
import { push } from 'react-router-redux';
import { select, put } from 'redux-saga/effects';
import { RedirectLinkRequest } from './suggestionUrlGenerators/redirectLinkRequest';
import { RouteName } from 'services/routes/routeNames';

export function* changeRoute(routeName: RouteName) {
  const logisticOperationId = yield select(
    (state: RootState) => state.session.currentLogisticOperation
      ? state.session.currentLogisticOperation.id
      : '',
  );
  const request: RedirectLinkRequest = {
    currentLogisticOperationId: logisticOperationId,
    type: 'Action',
    subType: 'InternalLink',
    text: '',
    contextProps: {
      routeName: routeName,
    },
  };
  const {url} = createInternalLink(request);
  yield put(push(url));
}
