import { RedirectLink } from 'application/navigation/entities/redirectLink';
import { RedirectLinkRequest } from './RedirectLinkRequest';

export type Decider = (request: RedirectLinkRequest) => boolean;
export type RedirectLinkCreator = (request: RedirectLinkRequest) => RedirectLink;
