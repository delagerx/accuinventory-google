import { RedirectLink } from '../../entities/redirectLink';
import { SuggestionRedirectLinkFactory } from './';
import { put } from 'redux-saga/effects';
import { Decider, RedirectLinkCreator } from './interfaces';
import { RedirectLinkRequest } from './redirectLinkRequest';
export const decider: Decider = (event: RedirectLinkRequest) => {
  return event.type === 'Action' && event.subType === 'ExternalLink';
};

export function creator(suggestion: RedirectLinkRequest): RedirectLink {
  return {
    url: suggestion.contextProps.url,
    name: suggestion.text,
    icon: 'ExternalLink',
    save: true,
  };
}

export const externalLinkGenerator = {
  decider: decider,
  redirectLinkCreator: creator,
};
