import { RedirectLink } from 'application/navigation/entities/redirectLink';
import { makeItemRoute } from 'services/routes/index';

import { Decider } from './interfaces';
import { RedirectLinkRequest } from './redirectLinkRequest';

export const decider: Decider = (event: RedirectLinkRequest) => {
  return event.type !== 'Action';
};

export function creator(suggestion: RedirectLinkRequest): RedirectLink {
  const lo = !suggestion.contextProps.lo || suggestion.contextProps.lo === 'all'
    ? suggestion.currentLogisticOperationId
    : suggestion.contextProps.lo;
  const route = makeItemRoute({id: suggestion.contextProps.id, type: suggestion.type, lo});
  return {
    icon: 'ItemLink',
    name: suggestion.text,
    save: true,
    url: route,
  };
}

export const itemDetailsGenerator = {
  decider: decider,
  redirectLinkCreator: creator,
};
