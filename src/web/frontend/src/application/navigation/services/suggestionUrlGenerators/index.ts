import { PutEffect } from 'redux-saga/effects';
import { Decider, RedirectLinkCreator } from './interfaces';
import { externalLinkGenerator } from './externalLinkGenerator';
import { wmsRxLinkGenerator } from './wmsRxLinkGenerator';
import { internalLinkGenerator } from './internalLinkGenerator';
import { itemDetailsGenerator } from './itemDetailsGenerator';
export interface SuggestionRedirectLinkFactory {
  decider: Decider;
  redirectLinkCreator: RedirectLinkCreator;
}

export const configuredRedirectLinkFactories = [
  externalLinkGenerator,
  wmsRxLinkGenerator,
  internalLinkGenerator,
  itemDetailsGenerator,
] as SuggestionRedirectLinkFactory[];
