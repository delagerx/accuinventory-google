import { RedirectLink } from 'application/navigation/entities/redirectLink';
import { getRouteByType } from 'services/routes/getRouteByType';
import { RouteName } from 'services/routes/routeNames';

import { formatString } from '../../../../utils/stringFormatter';
import { Decider } from './interfaces';
import { RedirectLinkRequest } from './redirectLinkRequest';

export const decider: Decider = (event: RedirectLinkRequest) => {
  return event.type === 'Action' && event.subType === 'InternalLink';
};

function shouldSaveUrl(routeName: string) {
  const routesNotToSave: RouteName[] = ['Home', 'Search'];
  if (routesNotToSave.find(item => item === routeName) === undefined) {
      return true;
  }
  return false;
}

export function creator(suggestion: RedirectLinkRequest): RedirectLink {
  const route = getRouteByType(suggestion.contextProps.routeName);
  const shouldSave = shouldSaveUrl(route.name);
  const url = formatString(route.routerUrlPattern, {lo: suggestion.currentLogisticOperationId});
  return {
    url: url,
    name: route.name,
    icon: 'InternalLink',
    save: shouldSave,
  };
}

export const internalLinkGenerator = {
  decider: decider,
  redirectLinkCreator: creator,
};
