import { RedirectLink } from 'application/navigation/entities/redirectLink';
import { formatString } from 'utils/stringFormatter';

import { Decider } from './interfaces';
import { RedirectLinkRequest } from './redirectLinkRequest';

export const decider: Decider = (event: RedirectLinkRequest) => {
  return event.type === 'Action' && event.subType === 'WmsRxLink';
};

export function creator(suggestion: RedirectLinkRequest): RedirectLink {
  const formatParameters = {...suggestion.contextProps, lo: suggestion.currentLogisticOperationId};
  const formattedUrl = formatString(suggestion.contextProps.url, formatParameters);
  return {
    icon: 'WmsLink',
    name: suggestion.text,
    save: true,
    url: formattedUrl,
  };
}

export const wmsRxLinkGenerator = {
  decider: decider,
  redirectLinkCreator: creator,
};
