import { LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { SuggestionSubType } from 'application/navigation/entities/suggestionSubType';
import { SuggestionContextProps } from 'application/navigation/entities/suggestionContextProps';

export interface RedirectLinkRequest {
    type: SuggestionType;
    subType: SuggestionSubType;
    contextProps: any;
    text: string;
    currentLogisticOperationId: LogisticOperationId;
}
