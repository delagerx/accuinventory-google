import { itemFetchRequestDefs, ItemFetchSucceeded } from '../../selectedItem/events/itemFetchRequest/index';
import { updateWidgetListCommandCreator } from 'application/widget/commands/updateWidgetList';
import { Action } from 'redux-actions';
import { put, takeLatest } from 'redux-saga/effects';

function * showWidgetsForSelectedSuggestionSaga(action: Action <ItemFetchSucceeded>) {
    const widget = {
        type: action.payload.item.type,
        contextProps: action.payload.item.contextProps,
        actions: action.payload.item.relatedSuggestions,
    };
    const updateWidgetListCommand = updateWidgetListCommandCreator([widget]);
    yield put(updateWidgetListCommand);
}

export default function* saga() {
    yield takeLatest(itemFetchRequestDefs.succ.name, showWidgetsForSelectedSuggestionSaga);
}
