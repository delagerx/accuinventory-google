import { all } from 'redux-saga/effects';
import { itemFetchRequestDefs } from '../events/itemFetchRequest/index';
import saga from './updateWidgetListOnSelectedSaga';

export default function* searchSagas() {
    yield all([
        ...itemFetchRequestDefs.saga(),
        ...saga(),
    ]);
}
