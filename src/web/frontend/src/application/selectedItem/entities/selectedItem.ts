import { LogisticOperationId } from '../../application/session/entities/logisticOperation';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
export interface SelectedItem {
    id: string;
    type: SuggestionType;
    lo: LogisticOperationId;
}
