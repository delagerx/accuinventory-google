import { Suggestion } from 'application/navigation/entities/suggestion';
import { SearchResultItem } from 'application/results/entities/searchResultItem';
export interface SelectedItemState {
    item: Suggestion;
    links: SearchResultItem[];
    pageSize: number;
    pageNumber: number;
    totalHits: number;
    isFetching: boolean;
}
