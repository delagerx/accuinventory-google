import { LoadingSpinner } from '../../../shared/components/loading/loading';
import { ResultWidgets } from 'application/results/containers/resultWidgets';
import { SelectedItemLinks } from 'application/selectedItem/containers/selectedItemRelatedLinks';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { itemFetchRequestDefs, ItemFetch } from '../../events/itemFetchRequest/index';

interface Item {
    type: SuggestionType;
    id: string;
}

interface Props {
    isLoading?: boolean;
    fetchItem?: (item: ItemFetch) => void;
    params: Item;
}

class SelectedPageHoc extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchItem({type: this.props.params.type, id: this.props.params.id});
    }

    componentWillReceiveProps(nextProps: Props) {
        if (this.props.params.type !== nextProps.params.type || this.props.params.id !== nextProps.params.id) {
            this.props.fetchItem({type: nextProps.params.type, id: nextProps.params.id});
        }
    }

    render() {
        return (
            <div className="container result-area">
                <LoadingSpinner isLoading={this.props.isLoading}>
                    <ResultWidgets/>
                    <SelectedItemLinks/>
                </LoadingSpinner>
            </div>
        );
    }
}

function mapStateToProps(state: RootState): Partial<Props> {
    return {
        isLoading: state.selectedItem.isFetching,
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
    return {
        fetchItem: itemFetchRequestDefs.com.dispatcher(dispatch),
    };
}

export const SelectedPage = connect(mapStateToProps, mapDispatchToProps)(SelectedPageHoc);
