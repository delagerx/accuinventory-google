import { PaginatedLinkList, PaginationProps } from '../../../shared/components/linkList/paginatedLinkList';
import * as React from 'react';
import i18n from 'i18n';
import Pagination from 'react-js-pagination';
import { AutoCompleteSuggestion } from 'application/search/entities/autoCompleteSuggestion';
import { bindActionCreators, Dispatch } from 'redux';
import { changePageCommandDispatcher } from 'application/results/events/changePageCommand';
import { connect } from 'react-redux';
import { LinkItem } from 'application/shared/components/linkList/linkItem';
import { LinkList, LinkListProps } from 'application/shared/components/linkList/linkList';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { formatString } from '../../../../utils/stringFormatter';

function mapStateToProps(state: RootState): Partial<PaginationProps & any> {
    return {
        links: state.selectedItem.links,
        pageNumber: state.selectedItem.pageNumber,
        pageSize: state.selectedItem.pageSize,
        total: state.selectedItem.totalHits,
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<PaginationProps> {
    return {
        changePage: bindActionCreators(changePageCommandDispatcher, dispatch),
    };
}

@connect(mapStateToProps, mapDispatchToProps)
export class SelectedItemLinks extends React.Component<any> {
    render() {
        const emptyListMessage = i18n('ITEM_RELATED_ACTION_EMPTY_LIST');
        return (
            <PaginatedLinkList {...this.props} emptyListMessage={emptyListMessage}/>
        );
    }
}
