import { Action, createAction } from 'redux-actions';
import {Suggestion} from 'application/navigation/entities/suggestion';

export const itemSelectedEventName = 'SEARCH_ITEM_SELECTED';

export const itemSelectedDispatcher = createAction<Suggestion, Suggestion>
    (itemSelectedEventName, (item: Suggestion) => item);
