import { call, select } from 'redux-saga/effects';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { apiReduxBoilerplateCreator } from 'utils/apiHandlerCreator';
import { Action, createAction } from 'redux-actions';
import { getItemApi } from 'services/apis/item';
import { mapItem } from 'application/results/services/resultService';
import { SearchResultItem } from 'application/results/entities/searchResultItem';
import { getCurrentLogisticOperationId } from 'application/application/session/events/eventHandlers';

export const ItemFetchRequest = 'ITEM_FETCH';

export interface ItemFetch {
  type: SuggestionType;
  id: string;
}

export interface ItemFetchSucceeded {
  item: SearchResultItem;
}

function* apiCall(payload: ItemFetch) {
  const logisticOperationId = yield select(getCurrentLogisticOperationId);
  const filter = {
    type: payload.type,
    id: payload.id,
    logisticOperation: logisticOperationId,
  };
  const result = yield call(() => getItemApi(filter));
  const mappedResult = mapItem(result, logisticOperationId);
  return {item: mappedResult};
}

const apiOptions = {
  apiCall: apiCall,
  redirectToCorrespondingResponse: true,
};

export const itemFetchRequestDefs =
  apiReduxBoilerplateCreator<ItemFetch, ItemFetchSucceeded>(ItemFetchRequest, apiOptions);
