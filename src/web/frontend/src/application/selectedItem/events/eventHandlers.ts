import { itemSelectedEventName } from './itemSelected';
import { access } from 'fs';
import { Action, handleActions } from 'redux-actions';

import { Suggestion } from 'application/navigation/entities/suggestion';
import { SelectedItemState } from '../entities/selectedItemState';
import { ResultsFetchSucceeded } from 'application/results/events/resultsFetchSucceeded';
import { itemFetchRequestDefs, ItemFetchSucceeded } from './itemFetchRequest/index';

const initialState: SelectedItemState = {
    links: [],
    item: undefined,
    pageSize: 10,
    pageNumber: 1,
    totalHits: 0,
    isFetching: false,
};

function handleItemSelected(state: SelectedItemState, action: Action<Suggestion>): SelectedItemState {
    return {
      ...state,
      item: action.payload,
    };
}

function handleResultsFetchRequested(state: SelectedItemState, action: Action<ItemFetchSucceeded>)
: SelectedItemState {
    return {
      ...initialState,
      isFetching: true,
    };
}

function handleResultsFetchSucceeded(state: SelectedItemState, action: Action<ItemFetchSucceeded>)
: SelectedItemState {
    return {
      ...state,
      item: action.payload.item,
      isFetching: false,
    };
}

function handleResultsFetchFailed(state: SelectedItemState, action: Action<ItemFetchSucceeded>)
: SelectedItemState {
    return {
      ...state,
      isFetching: false,
    };
}

export default handleActions<SelectedItemState, any>({
    [itemSelectedEventName]: handleItemSelected,
    [itemFetchRequestDefs.req.name]: handleResultsFetchRequested,
    [itemFetchRequestDefs.succ.name]: handleResultsFetchSucceeded,
    [itemFetchRequestDefs.fail.name]: handleResultsFetchFailed,
  }, initialState);
