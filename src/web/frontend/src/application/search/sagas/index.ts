import { all } from 'redux-saga/effects';
import { autoCompleteRequestDefs } from '../events/autoCompleteFetchRequested';

export function* searchSagas() {
    yield all([
        ...autoCompleteRequestDefs.saga(),
    ]);
}
