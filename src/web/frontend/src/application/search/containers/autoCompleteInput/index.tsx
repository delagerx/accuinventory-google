import { Suggestion } from 'application/navigation/entities/suggestion';
import { AutoCompleteSuggestion } from 'application/search/entities/autoCompleteSuggestion';
import { autoCompleteRequestDefs } from 'application/search/events/autoCompleteFetchRequested';
import { SearchExecuted, searchExecutedDefs } from 'application/search/events/searchExecuted';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import Autosuggest from '../../../../../node_modules/react-autosuggest/dist/Autosuggest';
import { itemSelectedDispatcher } from '../../../selectedItem/events/itemSelected/index';
import { renderAutoCompleteItem } from '../../components/autoCompleteItem';
import { SearchRequested } from '../../events/autoCompleteFetchRequested/index';

interface Props {
  query?: string;
  selectSuggestion?: (query: Suggestion) => void;
  fetchAutoCompleteSuggestions?: (query: SearchRequested) => void;
  executeSearch?: (query: SearchExecuted) => void;
  suggestions?: AutoCompleteSuggestion[];
}

function getSuggestionValue(suggestion: AutoCompleteSuggestion) {
  return suggestion.text;
}

class AutoCompleteComponent extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props);
    this.onUserInput = this.onUserInput.bind(this);
    this.fetchAutoCompleteSuggestions = this.fetchAutoCompleteSuggestions.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.state = {
        query: this.props.query,
    };
}

  componentWillReceiveProps(nextProps: Props) {
      if (this.props.query !== nextProps.query) {
          this.setState({
              query: nextProps.query,
          });
      }
  }
  fetchAutoCompleteSuggestions(input: {value: string, reason: string}) {
    if (input.reason === 'input-changed') {
      this.props.fetchAutoCompleteSuggestions({query: input.value });
    }
  }
  onUserInput(event: any, input: { newValue: string, method: any }) {
      this.setState({
          query: input.newValue,
      });
      if (input.method === 'enter') {
        this.props.executeSearch({query: input.newValue});
      }
      if (input.method === 'click') {
        window.location.href = (this.props.suggestions.find(x => x.text === input.newValue)).url;
      }
  }
  renderSuggestion(suggestion: AutoCompleteSuggestion) {
      return renderAutoCompleteItem(suggestion);
  }
  onKeyDown(event: any) {
    if (event.key === 'Enter') {
        this.props.executeSearch({query: this.state.query});
    }
  }
  ignoreClear = () => ({});
  render() {
    const inputProps = {
      value: this.state.query,
      onChange: this.onUserInput,
      onKeyDown: this.onKeyDown,
      autoFocus: true,
    };
    const classes = {
      input: 'input-default',
      suggestionsContainer: 'input-suggestion-container',
      suggestionsContainerOpen: 'input-suggestion-container-open',
    };
    return (
      <Autosuggest
          renderSuggestion={this.renderSuggestion}
          suggestions={this.props.suggestions}
          getSuggestionValue={getSuggestionValue}
          inputProps={inputProps}
          onSuggestionsFetchRequested={this.fetchAutoCompleteSuggestions}
          onSuggestionsClearRequested={this.ignoreClear}
          theme={classes}
      />
    );
  }
}

function mapDispatchToProps(dispatch: Dispatch<any>, ownProps: Props): Partial<Props> {
  return {
      fetchAutoCompleteSuggestions: autoCompleteRequestDefs.com.dispatcher(dispatch),
      selectSuggestion: bindActionCreators(itemSelectedDispatcher, dispatch),
      executeSearch: searchExecutedDefs.dispatcher(dispatch),
  };
}

function mapStateToProps(state: RootState): Partial<Props> {
  return {
    suggestions: state.autoComplete.suggestions,
    query: state.autoComplete.query,
  };
}

export const AutoComplete = connect(mapStateToProps, mapDispatchToProps)(AutoCompleteComponent);
