import i18n from 'i18n';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import SearchIcon from 'material-ui-icons/Search';
import Button from 'material-ui/Button';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { AutoComplete } from '../autoCompleteInput';
import { SearchExecuted, searchExecutedDefs } from '../../events/searchExecuted/index';

interface Props {
    executeSearch?: (query: SearchExecuted) => void;
    query?: string;
}

interface State {
    query: string;
}

class Component extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.executeQuery = this.executeQuery.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            query: this.props.query,
        };
    }

    componentWillReceiveProps(nextProps: Props) {
        if (this.props.query !== nextProps.query) {
            this.setState({
                query: nextProps.query,
            });
        }
    }

    executeQuery() {
        this.props.executeSearch({query: this.state.query});
    }

    onSubmit(event: any) {
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} className="search">
                <fieldset>
                    <label className="label-default">{i18n('VIEW_SEARCH_TITLE')}</label>
                    <div>
                        <AutoComplete/>
                        <Button onClick={this.executeQuery} classes={{root: 'btn-submit'}}>
                            <SearchIcon/>
                            {i18n('VIEW_SEARCH_BUTTON')}
                        </Button>
                    </div>
                </fieldset>
            </form>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>, ownProps: Props): Partial<Props> {
    return {
        executeSearch: searchExecutedDefs.dispatcher(dispatch),
    };
}

function mapStateToProps(state: RootState): Partial<Props> {
    return {
        query: state.autoComplete.query,
    };
}

export const SearchBox = connect(mapStateToProps, mapDispatchToProps)(Component);
