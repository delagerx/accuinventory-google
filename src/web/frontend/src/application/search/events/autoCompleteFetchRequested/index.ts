import { LogisticOperation, LogisticOperationId } from 'application/application/session/entities/logisticOperation';
import { getCurrentLogisticOperationId } from 'application/application/session/events/eventHandlers';
import { SearchFilter } from 'services/apis/search';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { call, select } from 'redux-saga/effects';
import { ApiOptions, apiReduxBoilerplateCreator } from '../../../../utils/apiHandlerCreator';
import { AutoCompleteQueryResult } from 'application/search/entities/autoCompleteQueryResult';
import { fetchAutocompleteSuggestions } from 'application/search/services/searchService';
import { makeCancelableCall } from 'utils/sagaUtils';

export const autoCompleteSearchRequested = 'AUTOCOMPLETE_SEARCH';

export interface SearchRequested {
    query: string;
}

export interface SearchSucceeded {
    result: AutoCompleteQueryResult;
}

function* apiCall(payload: SearchRequested): any {
    const logisticOperaton: LogisticOperationId = yield select(getCurrentLogisticOperationId);
    const searchFilter: SearchFilter = {
        query: payload.query,
        page: 1,
        pageSize: 10,
        logisticOperation: logisticOperaton,
    };
    const cancelableApiCall =
        makeCancelableCall((cancellationToken) => fetchAutocompleteSuggestions(searchFilter, cancellationToken));
    const result = yield call(() => fetchAutocompleteSuggestions(searchFilter));
    return {result};
}

const apiOptions: ApiOptions<SearchRequested> = {
    debounceTime: 250,
    apiCall: apiCall,
};

export const autoCompleteRequestDefs =
    apiReduxBoilerplateCreator<SearchRequested, SearchSucceeded>(autoCompleteSearchRequested, apiOptions);
