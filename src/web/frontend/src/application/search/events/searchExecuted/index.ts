import { createEventDefs } from '../../../../utils/eventCreator';
import { createAction } from 'redux-actions';

export const searchExecutedEvent = 'SEARCH_EXECUTED';
export interface SearchExecuted {
    query: string;
}

export const searchExecutedDefs = createEventDefs<SearchExecuted>(searchExecutedEvent);
