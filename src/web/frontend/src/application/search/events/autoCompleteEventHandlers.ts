import { SearchRequested } from 'application/search/events/autoCompleteFetchRequested';
import { Action, handleActions } from 'redux-actions';

import { resultsFetchRequestDefs } from '../../results/events/resultsFetchRequested/index';
import { AutoCompleteState } from '../entities/AutoCompleteState';
import { autoCompleteRequestDefs, SearchSucceeded } from './autoCompleteFetchRequested/index';
import { ResultsFetchRequested } from 'application/results/events/resultsFetchRequested';

const initialState: AutoCompleteState = {
  suggestions: [],
  query: '',
};

export default handleActions<AutoCompleteState, any>({
  [autoCompleteRequestDefs.com.name]: handleAutoCompleteCommand,
  [autoCompleteRequestDefs.succ.name]: handleAutocompleteSucceeded,
  [resultsFetchRequestDefs.req.name]: syncQueryWithResults,
}, initialState);

function handleAutoCompleteCommand(state: AutoCompleteState, action: Action<SearchRequested>): AutoCompleteState {
  return {
    ...state,
    query: action.payload.query,
  };
}

function handleAutocompleteSucceeded(state: AutoCompleteState, action: Action<SearchSucceeded>): AutoCompleteState {
  return {
    ...state,
    suggestions: action.payload.result.items,
  };
}

function syncQueryWithResults(state: AutoCompleteState, action: Action<ResultsFetchRequested>): AutoCompleteState {
  return {
    ...state,
    query: action.payload.searchQuery.query,
  };
}
