import { AutoCompleteSuggestion } from './autoCompleteSuggestion';
export interface AutoCompleteState {
    query: string;
    suggestions: AutoCompleteSuggestion[];
    selected?: AutoCompleteSuggestion;
}
