import { AutoCompleteSuggestion } from 'application/search/entities/autoCompleteSuggestion';

export interface AutoCompleteQueryResult {
    items: AutoCompleteSuggestion[];
    total: number;
}