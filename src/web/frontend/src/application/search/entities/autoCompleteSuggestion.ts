import { Suggestion } from 'application/navigation/entities/suggestion';

export interface AutoCompleteSuggestion extends Suggestion {
    value: string;
}
