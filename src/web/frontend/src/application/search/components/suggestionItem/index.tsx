import * as React from 'react';
import { MenuItem } from 'material-ui/Menu';
import { AutoCompleteSuggestion } from '../../entities/autoCompleteSuggestion';
import Button from 'material-ui/Button';

export function renderSuggestionItem(suggestion: AutoCompleteSuggestion) {
  return (
    <div className="input-suggestion-container-item">
      <a href={suggestion.url}>
        {suggestion.text}
      </a>
    </div>
  );
}
