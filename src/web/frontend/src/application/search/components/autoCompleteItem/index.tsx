import * as React from 'react';
import { MenuItem } from 'material-ui/Menu';
import { AutoCompleteSuggestion } from '../../entities/autoCompleteSuggestion';
import { PermissionableLinkComponent } from 'application/shared/components/permissionableLink';
import Button from 'material-ui/Button';

export function renderAutoCompleteItem(suggestion: AutoCompleteSuggestion) {
  return (
      <PermissionableLinkComponent
        className={`input-suggestion-container-item`}
        hasPermission={suggestion.hasPermission}
        text={suggestion.text}
        url={suggestion.url}
      />
  );
}
