import { LogisticOperationId } from '../../application/session/entities/logisticOperation';
import { createRedirectLink } from '../../navigation/services/navigationService';
import { SearchFilter } from '../../../services/apis/search';
import { SearchResultItemApiDto, searchApi } from 'services/apis/search';
import { AutoCompleteSuggestion } from '../entities/autoCompleteSuggestion';
import { AutoCompleteQueryResult } from '../entities/autoCompleteQueryResult';
import * as uuidv4 from 'uuid/v4';
import { SearchResultApiDto } from 'services/apis/search/dtos/UserAction';

export function fetchAutocompleteSuggestions(search: SearchFilter, cancelToken?: any): Promise <AutoCompleteQueryResult> {
    return searchApi(search, cancelToken)
            .then(x => handleResult(x, search.logisticOperation));
}

function handleResult(response: SearchResultApiDto, lo: LogisticOperationId): AutoCompleteQueryResult {
    const items = response.items
        .sort((a1, a2) => a1.probability > a2.probability ? -1 : a1.probability === a2.probability ? 0 : 1)
        .map(x => mapResults(x, lo));
    return {items: items, total: response.total};
}

function mapResults(action: SearchResultItemApiDto, lo: LogisticOperationId): AutoCompleteSuggestion {
    const redirectRequest = {
        text: action.label,
        type: action.type,
        subType: action.subType,
        contextProps: action.contextProps,
    };

    return {
        ...redirectRequest,
        value: action.label,
        hasPermission: action.hasPermission,
        url: createRedirectLink({...redirectRequest, currentLogisticOperationId: lo}),
    };
}
