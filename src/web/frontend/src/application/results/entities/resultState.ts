import { WidgetKey } from '../../widget/entities/widget';
import {SearchResultItem} from 'application/results/entities/searchResultItem';
export interface ResultState {
    executedQuery: string;
    results: SearchResultItem[];
    isFetching: boolean;

    pageSize: number;
    pageNumber: number;
    totalHits: number;
}
