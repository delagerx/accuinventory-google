import { RelatedSuggestion } from 'application/navigation/entities/relatedSuggestion';
import { SuggestionType } from 'application/navigation/entities/suggestionType';
import { Suggestion } from 'application/navigation/entities/suggestion';
export interface SearchResultItem extends Suggestion {
    relatedSuggestions?: RelatedSuggestion[];
    hide?: boolean;
}
