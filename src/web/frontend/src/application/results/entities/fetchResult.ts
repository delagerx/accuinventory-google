import { SearchResultItem } from './searchResultItem';
export interface FetchResult {
    total: number;
    items: SearchResultItem[];
}
