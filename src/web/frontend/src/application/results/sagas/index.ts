import { all } from 'redux-saga/effects';
import { resultsFetchRequestDefs } from '../events/resultsFetchRequested';
import changePageSaga from './changeQueryUrlOnPageChangeSaga';
import fetchResultsOnSearchSaga from './changeQueryUrlOnSearchSaga';
import updateUrlWithQuerySaga from './updateUrlWithQuerySaga';

export default function* resultSagas() {
    yield all([
      ...resultsFetchRequestDefs.saga(),
      ...changePageSaga(),
      ...fetchResultsOnSearchSaga(),
      ...updateUrlWithQuerySaga(),
    ]);
  }
