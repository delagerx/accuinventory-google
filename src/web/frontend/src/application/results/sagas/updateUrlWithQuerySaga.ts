import { getCurrentLogisticOperationId } from '../../application/session/events/eventHandlers';
import { push } from 'react-router-redux';
import { Action } from 'redux-actions';
import { put, takeLatest, select } from 'redux-saga/effects';
import { makeSearchRoute } from 'services/routes';

import { ChangeUrlWithQueryCommand, changeUrlWithQueryCommand } from '../events/changeUrlWithQueryCommand/index';
import { SearchQuery } from 'application/shared/entities/SearchQuery';

export function* changeToResultDetailsRoute(search: SearchQuery) {
    const logisticOperationId = yield select(getCurrentLogisticOperationId);
    yield put(push(makeSearchRoute(search, logisticOperationId)));
  }

function* searchExecuted(action: Action<ChangeUrlWithQueryCommand>) {
    yield changeToResultDetailsRoute(action.payload.searchQuery);
}

export default function* saga() {
    yield takeLatest(changeUrlWithQueryCommand, searchExecuted);
}
