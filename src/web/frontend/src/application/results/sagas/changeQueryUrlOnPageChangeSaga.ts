import { ChangePageCommand, changePageCommand } from '../events/changePageCommand/index';
import {RootState} from 'infraestructure/redux/reducers/rootReducer';
import {call, takeLatest, select, put} from 'redux-saga/effects';
import {Action} from 'redux-actions';
import {fetchResults} from 'application/results/services/resultService';
import { getPageInfo } from 'application/results/events/searchResultEventHandlers';
import { changeUrlWithQueryCommandCreator } from '../events/changeUrlWithQueryCommand/index';

function * changePage(action: Action<ChangePageCommand>) {
    const {query, pageSize} = yield (select(getPageInfo));
    yield put(changeUrlWithQueryCommandCreator({query, page: action.payload.page, pageSize}));
}

export default function* saga() {
    yield takeLatest(changePageCommand, changePage);
}
