import { getPageInfo } from 'application/results/events/searchResultEventHandlers';
import { SearchExecuted, searchExecutedEvent } from 'application/search/events/searchExecuted';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { Action } from 'redux-actions';
import { put, select, takeLatest } from 'redux-saga/effects';

import { changeUrlWithQueryCommandCreator } from '../events/changeUrlWithQueryCommand/index';

function * search(action: Action<SearchExecuted>) {
    const pageNumber = 1;
    const {pageSize} = yield(select(getPageInfo));
    const query: SearchQuery = {
        query: action.payload.query,
        page: pageNumber,
        pageSize: pageSize,
    };
    yield put(changeUrlWithQueryCommandCreator(query));
}

export default function* saga() {
    yield takeLatest(searchExecutedEvent, search);
}
