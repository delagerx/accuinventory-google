import { SearchResultItem } from 'application/results/entities/searchResultItem';
import { ResultState } from 'application/results/entities/resultState';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { Action } from 'redux-actions';

import { resultsFetchRequestDefs } from '../resultsFetchRequested/index';

export interface ResultsFetchSucceeded {
    query: SearchQuery;
    total: number;
    items: SearchResultItem[];
}

function resultsFetchSucceededHandler(state: ResultState, action: Action<ResultsFetchSucceeded>): ResultState {
    return {
        ...state,
        isFetching: false,
        results: action.payload.items,
        totalHits: action.payload.total,
        pageNumber: action.payload.query.page,
        executedQuery: action.payload.query.query,
    };
}
export const handleResultsFetchSucceeded = {[resultsFetchRequestDefs.succ.name]: resultsFetchSucceededHandler};
