import { createAction } from 'redux-actions';

export const changePageCommand = 'command/CHANGE_PAGE_COMMAND';

export interface ChangePageCommand {
    page: number;
}

export const changePageCommandDispatcher =
    createAction<ChangePageCommand, number>(changePageCommand, (page: number) => ({page}));
