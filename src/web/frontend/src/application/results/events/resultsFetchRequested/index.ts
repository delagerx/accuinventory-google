import { FetchResult } from 'application/results/entities/fetchResult';
import { ResultState } from 'application/results/entities/resultState';
import { ResultsFetchSucceeded } from 'application/results/events/resultsFetchSucceeded';
import { fetchResults } from 'application/results/services/resultService';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { updateWidgetListCommandCreator } from 'application/widget/commands/updateWidgetList';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { Action } from 'redux-actions';
import { call, put, select } from 'redux-saga/effects';
import { apiReduxBoilerplateCreator } from 'utils/apiHandlerCreator';

import { LogisticOperation } from '../../../application/session/entities/logisticOperation';
import { getWidgetToShow } from './getWidgetToShow';

export const resultsFetchRequestedEvent = 'RESULTS_FETCH';
export interface ResultsFetchRequested {
    searchQuery: SearchQuery;
}

function* apiCall(payload: ResultsFetchRequested) {
    const logisticOperation: LogisticOperation =
        yield select((state: RootState) => state.session.currentLogisticOperation);
    const query = {
        ...payload.searchQuery,
        logisticOperation: logisticOperation.id,
    };
    const result: FetchResult = yield call(() => fetchResults(query));
    const widgetToShowRequest = getWidgetToShow(result.items, query.page);
    yield put(updateWidgetListCommandCreator(widgetToShowRequest));
    return {items: result.items, total: result.total, query: query};
}

const apiOptions = {
    apiCall: apiCall,
};

export const resultsFetchRequestDefs =
    apiReduxBoilerplateCreator<ResultsFetchRequested, ResultsFetchSucceeded>(resultsFetchRequestedEvent, apiOptions);

function resultsFetchRequestedHandler(state: ResultState, action: Action<ResultsFetchRequested>): ResultState {
    return {
        ...state,
        isFetching: true,
        results: [],
        totalHits: 0,
    };
}
export const handleResultsFetchRequested = {[resultsFetchRequestDefs.req.name]: resultsFetchRequestedHandler};
