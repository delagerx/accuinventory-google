import { SearchResultItem } from 'application/results/entities/searchResultItem';
import { WidgetCreationRequest } from 'application/widget/commands/updateWidgetList';
import { hasWidgetForType } from 'application/widget/services/widgetService';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';

export function getWidgetToShow(searchResults: SearchResultItem[], page: number): WidgetCreationRequest[] {
    const resultsWithWidgets = searchResults.slice(0, 5).filter(x => hasWidgetForType(x.type));
    if (resultsWithWidgets.length === 1 && page === 1) {
        const onlyResultWithWidgets = resultsWithWidgets[0];
        const createWidgetRequest = {
            type: onlyResultWithWidgets.type,
            contextProps: onlyResultWithWidgets.contextProps,
            actions: onlyResultWithWidgets.relatedSuggestions,
        };
        onlyResultWithWidgets.hide = true;
        return [createWidgetRequest];
    }
    return [];
}
