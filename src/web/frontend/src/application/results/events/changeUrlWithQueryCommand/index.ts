import { createAction, Action } from 'redux-actions';
import { ResultState } from 'application/results/entities/resultState';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { createEventDefs } from 'utils/eventCreator';

export const changeUrlWithQueryCommand = 'command/CHANGE_URL_WITH_QUERY';
export interface ChangeUrlWithQueryCommand {
    searchQuery: SearchQuery;
}

export function changeUrlWithQueryCommandCreator(searchQuery: SearchQuery): Action<ChangeUrlWithQueryCommand> {
    return {
        type: changeUrlWithQueryCommand,
        payload: {
            searchQuery,
        },
    };
}
