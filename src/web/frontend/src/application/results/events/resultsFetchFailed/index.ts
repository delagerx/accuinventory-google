import { resultsFetchRequestDefs } from '../resultsFetchRequested/index';
import { ResultState } from 'application/results/entities/resultState';
function resultsFetchFailedHandler(state: ResultState): ResultState {
    return {
        ...state,
        isFetching: false,
    };
}
export const handleResultsFetchFailed = {[resultsFetchRequestDefs.fail.name]: resultsFetchFailedHandler};
