import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { handleActions } from 'redux-actions';

import { RootState } from '../../../infraestructure/redux/reducers/rootReducer';
import { ResultState } from '../entities/resultState';
import { handleResultsFetchFailed } from './resultsFetchFailed';
import { handleResultsFetchRequested } from './resultsFetchRequested';
import { handleResultsFetchSucceeded } from './resultsFetchSucceeded';

const initialState: ResultState = {
    executedQuery: '',
    isFetching: false,
    totalHits: 0,
    pageSize: 10,
    pageNumber: 1,
    results: [],
};

export default handleActions<ResultState, any>({
    ...handleResultsFetchRequested,
    ...handleResultsFetchSucceeded,
    ...handleResultsFetchFailed,
  }, initialState);

export function getPageInfo(state: RootState): SearchQuery {
    return {
        page: state.result.pageNumber,
        pageSize: state.result.pageSize,
        query: state.result.executedQuery,
    };
}
