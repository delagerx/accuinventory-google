import { formatString } from '../../../../utils/stringFormatter';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { PaginatedLinkList, PaginationProps } from '../../../shared/components/linkList/paginatedLinkList';
import { changePageCommandDispatcher } from '../../events/changePageCommand/index';
import i18n from 'i18n';

function mapStateToProps(state: RootState): Partial<PaginationProps & any> {
    return {
        links: state.result.results,
        pageNumber: state.result.pageNumber,
        pageSize: state.result.pageSize,
        total: state.result.totalHits,
        query: state.result.executedQuery,
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<PaginationProps> {
    return {
        changePage: bindActionCreators(changePageCommandDispatcher, dispatch),
    };
}

@connect(mapStateToProps, mapDispatchToProps)
export class ResultPageLinksHoc extends React.Component<any> {
    render() {
        const emptyListMessage = formatString(i18n('RESULTS_EMPTY_LIST'), {0: this.props.query});
        return (
            <PaginatedLinkList {...this.props} emptyListMessage={emptyListMessage}/>
        );
    }
}
