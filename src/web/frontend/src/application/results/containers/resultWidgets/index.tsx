import { WidgetList } from 'application/widget/containers/widgetContainer';
import { WidgetKey } from 'application/widget/entities/widget';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';

interface Props {
    widgetKeys?: WidgetKey[];
}

const component = (props: Props) => {
    return <WidgetList widgetKeys={props.widgetKeys}/>;
};

const mapStateToProps = (state: RootState): Props => {
    return {
        widgetKeys: state.widget.widgetKeys,
    };
};

export const ResultWidgets = connect(mapStateToProps)(component);
