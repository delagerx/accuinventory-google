import { LoadingSpinner } from '../../../shared/components/loading/loading';
import { ResultPageLinksHoc } from 'application/results/containers/resultLinks';
import { ResultWidgets } from 'application/results/containers/resultWidgets';
import * as React from 'react';
import { SearchQuery } from 'application/shared/entities/SearchQuery';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { Dispatch, connect } from 'react-redux';
import { resultsFetchRequestDefs } from '../../events/resultsFetchRequested/index';
import { ResultsFetchRequested } from 'application/results/events/resultsFetchRequested';
import { getSearchQueryFromQueryString } from 'services/routes';

interface Props {
    isLoading?: boolean;
    fetchResults?: (searchQuery: ResultsFetchRequested) => void;
    searchQuery?: SearchQuery;
    location: any;
}

function renderResultArea() {
    return (
        <div>
            <ResultWidgets/>
            <ResultPageLinksHoc/>
        </div>
    );
}

const ResultPageComponent = (isLoading: boolean) => {
    return (
        <div className="container result-area">
            <LoadingSpinner isLoading={isLoading}>
                {renderResultArea()}
            </LoadingSpinner>
        </div>
    );
};

class ResultPageHoc extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchResults({searchQuery: this.props.searchQuery});
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.location && (
            (nextProps.searchQuery.query !== this.props.searchQuery.query)
            || (nextProps.searchQuery.page !== this.props.searchQuery.page)
            || (nextProps.searchQuery.pageSize !== this.props.searchQuery.pageSize)
        )) {
            this.props.fetchResults({searchQuery: nextProps.searchQuery});
        }
    }

    render() {
        return ResultPageComponent(this.props.isLoading);
    }
}

function mapStateToProps(state: RootState, ownProps: Props): Partial<Props> {
    const searchQuery = getSearchQueryFromQueryString(ownProps.location.search);
    return {
        isLoading: state.result.isFetching,
        searchQuery: searchQuery,
    };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
    return {
        fetchResults: resultsFetchRequestDefs.com.dispatcher(dispatch),
    };
}

export const ResultPage = connect(mapStateToProps, mapDispatchToProps)(ResultPageHoc);
