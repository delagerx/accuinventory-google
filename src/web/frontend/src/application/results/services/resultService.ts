import { createRedirectLink } from '../../navigation/services/navigationService';
import { RelatedSuggestion } from 'application/navigation/entities/relatedSuggestion';
import { SearchResultItem } from '../entities/searchResultItem';
import { SearchResultApiDto, RelatedSuggestionApiDto } from '../../../services/apis/search/dtos/UserAction';
import { FetchResult } from '../entities/fetchResult';
import { searchApi, SearchFilter, SearchResultItemApiDto } from '../../../services/apis/search';
import { Suggestion } from 'application/navigation/entities/suggestion';
import { LogisticOperationId } from 'application/application/session/entities/logisticOperation';
export function fetchResults(query: SearchFilter): Promise < FetchResult > {
    return searchApi(query).then(x => mapResult(x, query.logisticOperation));
}

function mapResult(searchResult: SearchResultApiDto, currentLo: LogisticOperationId): FetchResult {
    return {
        items: searchResult.items.map(x => mapItem(x, currentLo)),
        total: searchResult.total,
    };
}

export function mapItem(searchResultItem: SearchResultItemApiDto, currentLo: LogisticOperationId): SearchResultItem {
    const relatedSuggestions = searchResultItem.relatedSuggestions
        ? searchResultItem.relatedSuggestions.map(x => mapRelatedAction(x, currentLo))
        : [];
    const redirectRequest = {
        contextProps: searchResultItem.contextProps,
        text: searchResultItem.label,
        type: searchResultItem.type,
        subType: searchResultItem.subType,
    };
    return {
        ...redirectRequest,
        relatedSuggestions: relatedSuggestions,
        hasPermission: searchResultItem.hasPermission,
        url: createRedirectLink({...redirectRequest, currentLogisticOperationId: currentLo}),
    };
}

function mapRelatedAction(relatedAction: RelatedSuggestionApiDto, currentLo: LogisticOperationId): RelatedSuggestion {
    const redirectRequest = {
        text: relatedAction.label,
        contextProps: relatedAction.contextProps,
        type: relatedAction.type,
        subType: relatedAction.subType,
    };
    return {
        ...redirectRequest,
        hasPermission: relatedAction.hasPermission,
        url: createRedirectLink({...redirectRequest, currentLogisticOperationId: currentLo}),
    };
}
