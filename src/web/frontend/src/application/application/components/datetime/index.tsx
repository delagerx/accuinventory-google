import * as React from 'react';
import * as realize from 'realizejs';

export interface DateTimeComponentProps {
    dateAsObject?: Date;
    dateAsString?: string;
    showTime?: boolean;
    twelveHoursFormat?: boolean;
}

export class DateTimeComponent extends React.Component<DateTimeComponentProps> {
    render() {
        return (
            <span>
                {formatDateTime(this.props)}
            </span>
        );
    }
}

function formatDateTime(props: DateTimeComponentProps) {

    let dateObject;

    if (!!props.dateAsString) {
        dateObject = new Date(props.dateAsString);
    } else if (!!props.dateAsObject) {
        dateObject = new Date(props.dateAsObject);
    } else {
        dateObject = new Date();
    }

    if (props.showTime) {
        const currentLocale = realize.i18n.currentLocale;
        return dateObject.toLocaleString(currentLocale, { hour12: props.twelveHoursFormat });
    }

    return dateObject.toLocaleDateString(realize.i18n.currentLocale);
}
