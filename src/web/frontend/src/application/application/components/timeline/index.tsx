import * as realize from 'realizejs';
import * as React from 'react';
import * as style from './style.css';
import { DateTimeComponent } from '../datetime/index';

export interface TimelineProps {
    entries: TimelineEntryProps[];
    maxVisibleEntries?: number;
    title?: string;
    entryClassName?: string;
}

export interface TimelineEntryProps {
    title: string | JSX.Element;
    timestamp: Date;
    isActive: boolean;
    icon?: string | JSX.Element;
    showTime?: boolean;
}

export class TimelineComponent extends React.Component<TimelineProps> {
  render() {
    return(
      <div className="timeline">
          {!!this.props.title && this.renderTimelineTitle()}
          {this.renderTimelineEntries()}
      </div>
    );
  }
  renderTimelineTitle() {
    if (typeof this.props.title === 'string') {
      return (
        <span className="title">
          {this.props.title}
        </span>
      );
    } else {
      return this.props.title as JSX.Element;
    }
  }

  renderTimelineEntries() {
      if (this.props.entries.length > this.props.maxVisibleEntries) {
          const maxInitialItems = (this.props.maxVisibleEntries / 2) + (this.props.maxVisibleEntries % 2);
          const maxFinalItems = this.props.maxVisibleEntries / 2;

          const rows = [];

          for (let i = 0; i < maxInitialItems; i++) {
              rows.push(this.renderTimelineEntry(this.props.entries[i], this.props.entryClassName, rows.length));
          }

          const collapsedEntry: TimelineEntryProps = {
              title: '...',
              timestamp: null,
              isActive: false,
          };

          rows.push(this.renderTimelineEntry(collapsedEntry, this.props.entryClassName, rows.length));

          for (let i = (this.props.entries.length - maxFinalItems); i < this.props.entries.length; i++) {
              rows.push(this.renderTimelineEntry(this.props.entries[i], this.props.entryClassName, rows.length));
          }

          return rows;
      }

      return this.props.entries.map((entry, i) => this.renderTimelineEntry(entry, this.props.entryClassName, i));
  }

  renderTimelineEntry(entryProps: TimelineEntryProps, entryClassName: string, index: number) {

      function renderEntryDate() {
        if (!!entryProps.timestamp) {
            return (<DateTimeComponent dateAsObject={entryProps.timestamp} showTime={!!entryProps.showTime} />);
        } else {
            return '';
        }
      }

      function renderIcon() {
        if (entryProps.icon) {
          if (typeof entryProps.icon === 'string') {
            return <i className="material-icons">{entryProps.icon}</i>;
          } else {
            return entryProps.icon;
          }
        }
        return <div/>;
      }

      const timelineClassName = entryClassName === undefined
        ? 'timeline__group'
        : entryClassName;
      return (
          <div key={index} className={`${timelineClassName} ${entryProps.isActive ? 'current' : ''}`}>
            <div className="timeline__group--content">
              <div className="timeline__group--icon">
                {renderIcon()}
              </div>
              <div className="timeline__group--description">
                <span className="timeline__group--status">{entryProps.title}</span>
                <time className="timeline__group--datetime">{renderEntryDate()}</time>
              </div>
            </div>
          </div>
      );
  }
}
