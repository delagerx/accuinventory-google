import * as React from 'react';
import i18n from 'i18n';
import Tabs, { Tab } from 'material-ui/Tabs';

export interface TabsContainerProps {
  currentTab?: number;
  onChange?: any;
  tabs: TabProps[];
}

export interface TabProps {
  label: string;
  tabContent: any;
}

const Component = (props: TabsContainerProps) => {
  const classes = {
    rootInheritSelected: 'active',
    root: 'tab',
  };

  const rootClasses = {
      flexContainer: 'tabs',
      fixed: 'tabs-fixed',
  };

  const tabLabels = props.tabs.map((tab, i) => <Tab key={i} classes={classes} label={tab.label} />);

  function renderTabContent() {
    return(
      <section>
        {props.tabs[props.currentTab].tabContent}
      </section>
    );
  }

  return (
    <section>
      <Tabs
        classes={rootClasses}
        value={props.currentTab}
        onChange={props.onChange}
        centered={true}
        indicatorClassName="tabs-indicator"
      >
        {tabLabels}
      </Tabs>
      {renderTabContent()}
    </section>
  );
};

export class HOCTabbedContent extends React.Component<TabsContainerProps, any> {
  constructor() {
    super();

    this.state = { currentTab: 0 };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event, value: number) => {
      this.setState({ currentTab: value }, () => this.props.onChange);
  }

  render() {

    return(
      <Component {...this.props} currentTab={this.state.currentTab} onChange={this.handleChange} />
    );
  }
}

export const TabbedContent = HOCTabbedContent;
