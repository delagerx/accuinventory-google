import * as React from 'react';
import i18n from 'i18n';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Collapse from 'material-ui/transitions/Collapse';
import FlatButton from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import ExpandLessIcon from 'material-ui-icons/ExpandLess';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import { StandardProps } from 'material-ui';

export interface ExpandableCardProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>, any> {
  content: React.ReactNode;
  collapsedContent: React.ReactNode;
  expanded?: boolean;
  onExpandClick?: () => void;
}

const Component = (props: ExpandableCardProps) => {
  return (
    <div className="expandable-card">
        <CardContent className="row expandable-card-content">
            {props.content}
        </CardContent>
        <Collapse in={props.expanded} unmountOnExit={true}>
            <CardContent className="expandable-card-content">
                {props.collapsedContent}
            </CardContent>
        </Collapse>
        <div className="widget-footer">
            {renderIconButton(props)}
        </div>
    </div>
  );
};

function renderIconButton(props: ExpandableCardProps) {
  return(
    <button onClick={props.onExpandClick} className="btn-slide-toggle" aria-label={i18n('EXPANDABLE_CARD_MORE_INFO')} >
      {props.expanded ? renderCollpaseButton() : renderExpandButton()}
    </button>
  );
}

function renderCollpaseButton() {
  return (
    <span>
      <ExpandLessIcon className="icon" />
      {i18n('EXPANDABLE_CARD_HIDE')}
    </span>
  );
}

function renderExpandButton() {
  return (
    <span>
      <ExpandMoreIcon className="icon" />
      {i18n('EXPANDABLE_CARD_SHOW')}
    </span>
  );
}

export class HOCExpandableCard extends React.Component<ExpandableCardProps, any> {
  constructor() {
      super();
      this.state = { expanded: false };
  }

  onExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    return(
      <Component {...this.props} expanded={...this.state.expanded} onExpandClick={this.onExpandClick} />
    );
  }
}

export const ExpandableCard = HOCExpandableCard;
