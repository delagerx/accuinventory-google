import * as React from 'react';
import * as realize from 'realizejs';
import i18n from 'i18n';

export interface TableComponentProps {
    columns: TableColumns;
    externalLink?: string;
    maxRowsDisplayed?: number;
    items: TableRows[];
    title?: string;
    totalItemsCount?: number;
}

export interface TableColumns {
    [key: string]: TableColumnProps;
}

export interface TableRows {
    [key: string]: TableRowProps;
}

export interface TableColumnProps {
    title: string;
}

export interface TableRowProps {
    value: any;
}

export class TableComponent extends React.Component<TableComponentProps> {
    render() {
        return (
            <table className="table">
                <caption>{this.props.title}</caption>
                {renderTableHeader(this.props)}
                {renderTableBody(this.props)}
            </table>
        );
    }
}

function renderTableHeader(props: TableComponentProps) {

    const tableHeaderList = Object.keys(props.columns).map((columnKey, columnIndex) => {
        return (
            <th key={columnIndex} className="table-collapsible--text">
                {props.columns[columnKey].title}
            </th>
        );
    });

    return (
        <thead className="table-collapsible__header">
            <tr role="row">
                {tableHeaderList}
            </tr>
        </thead>
    );
}

function renderTableBody(props: TableComponentProps) {
    const totalItemsCount = props.totalItemsCount || props.items.length;
    const rowsLimitReached = totalItemsCount > props.maxRowsDisplayed;
    const rowsCount = Math.min(props.items.length, props.maxRowsDisplayed);
    const tableIsEmpty = props.items.length === 0;

    return (
        <tbody>
            {renderTableRows(props.items, props.columns, props.maxRowsDisplayed)}
            {tableIsEmpty && renderEmptyTableMessage(props.columns)}
            {rowsLimitReached && renderShowMoreItemsMessage(props, totalItemsCount, rowsCount)}
        </tbody>
    );
}

function renderTableRows(rows: TableRows[], columns: TableColumns, maxRowsDisplayed: number) {

    if (maxRowsDisplayed > 0) {
        rows = rows.slice(0, maxRowsDisplayed);
    }

    const tableRowsList = rows.map((row, rowIndex) => {
        return (
            <tr key={rowIndex} className="row">
                {renderTableRowCells(row, columns, rowIndex.toString())}
            </tr>
        );
    });

    return tableRowsList;
}

function renderTableRowCells(row: TableRows, columns: TableColumns, rowIdentifier: string) {

    const tableRowCellsList = Object.keys(columns).map((columnKey, columnIndex) => {
        const formattedRowIdentifier = `${rowIdentifier}[${columnIndex}]`;

        return (
            <td
                key={formattedRowIdentifier}
                className="table-collapsible--text"
                data-title={columns[columnKey].title}
            >
                {row[columnKey].value}
            </td>
        );
    });

    return tableRowCellsList;
}

function renderEmptyTableMessage(columns: TableColumns) {
    const columnsCount = Object.keys(columns).length;

    return (
        <tr>
            <td className="empty-table-message" colSpan={columnsCount}>
                {i18n('GENERAL_EMPTY_TABLE')}
            </td>
        </tr>
    );
}

function renderShowMoreItemsMessage(props: TableComponentProps, totalItemsCount: number, rowsCount: number) {
    const columnsCount = Object.keys(props.columns).length;
    const linkText = buildShowMoreItemsMessage(totalItemsCount, rowsCount);

    if (!!props.externalLink) {
        return (
            <tr>
                <td className="show-more-items-message" colSpan={columnsCount}>
                    <a href={props.externalLink}>
                        <span>{linkText}</span>
                        <i className="material-icons">exit_to_app</i>
                    </a>
                </td>
            </tr>
        );
    }

    return (
        <tr>
            <td className="show-more-items-message" colSpan={columnsCount}>
                <span>{linkText}</span>
            </td>
        </tr>
    );
}

function buildShowMoreItemsMessage(totalItemsCount: number, rowsCount: number) {
    if (!!totalItemsCount) {
        const extraItemsCount = totalItemsCount - rowsCount;

        if (extraItemsCount > 1) {
            return i18n('GENERAL_SHOW_MORE_X_ITEMS').replace('{count}', (extraItemsCount));
        } else {
            return i18n('GENERAL_SHOW_MORE_X_ITEM');
        }
    } else {
        return i18n('GENERAL_SHOW_MORE_ITEMS');
    }
}
