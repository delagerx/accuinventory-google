import { version } from 'constants/version';
import * as React from 'react';

export function FooterComponent() {
    return(
        <footer>
            <div className="container">
            <div className="row">
                <div className="col s6">
                    <img src="/assets/img/delage.png" alt=""/>
                </div>
                <div className="col s6">
                    <p className="version">Accuinventory {version}</p>
                </div>
          </div>
        </div>
      </footer>
    );
}
