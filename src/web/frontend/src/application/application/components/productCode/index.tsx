import * as React from 'react';
import * as realize from 'realizejs';

export interface ProductCodeComponentProps {
    productCode: string;
    productDigit: string;
}

export class ProductCodeComponent extends React.Component<ProductCodeComponentProps> {
    render() {
        const formattedCode = !!this.props.productDigit
            ? `${this.props.productCode}-${this.props.productDigit}`
            : this.props.productCode;

        return (
            <span>
                {formattedCode}
            </span>
        );
    }
}