import { Action } from 'redux-actions';
import { ErrorState } from '../entities/errorState';
import {errorEvent, ErrorEvent} from './errorEvent';
const initialState: ErrorState = {
  message: '',
};

export function errorReducer(state: ErrorState = initialState, action: Action<ErrorEvent>) {
  if (errorEvent === action.type) {
    return {
      message: action.payload.error,
    };
  }
  return state;
}
