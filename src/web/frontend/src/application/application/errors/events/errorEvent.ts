import {Action} from 'redux-actions';

export const errorEvent = 'ERROR_OCCURRED';
export interface ErrorEvent {
    error: string;
    exception: any;
}

export function errorEventCreator(error: string, exception: any): Action<ErrorEvent> {
  return {
    type: errorEvent,
    payload: {
      error: error,
      exception: exception,
    },
  };
}
