import * as React from 'react';
import i18n from 'i18n';
import { InternalLinkComponent } from 'application/navigation/containers/internalLink';
export class NotFoundPage extends React.Component {
  render() {
    return (
      <div className="container">
          <div className="row">
            <div className="col s12">
              <div className="error-page">
                <h3>
                  <span>Error 404</span>
                  <i className="material-icons">sentiment_very_dissatisfied</i>
                </h3>
                <h4>{i18n('NOT_FOUND_PAGE_TITLE')}</h4>
                <p>
                  {i18n('NOT_FOUND_PAGE_DESCRIPTION')}
                  <br/>
                  <InternalLinkComponent
                    routeName={'Home'}
                  >
                    {i18n('NOT_FOUND_PAGE_LINK')}
                  </InternalLinkComponent>
                </p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
