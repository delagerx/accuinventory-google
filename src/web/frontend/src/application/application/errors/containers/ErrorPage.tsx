import * as React from 'react';
import i18n from 'i18n';

export class ErrorPage extends React.Component {
  render() {
    return (
      <div className="container">
          <div className="row">
            <div className="col s12">
              <div className="error-page">
                <h3>
                  <span>Error 500</span>
                  <i className="material-icons">settings</i>
                </h3>
                <h4>{i18n('SERVER_ERROR_PAGE_TITLE')}</h4>
                <p>
                  {i18n('SERVER_ERROR_PAGE_DESCRIPTION')}
                </p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
