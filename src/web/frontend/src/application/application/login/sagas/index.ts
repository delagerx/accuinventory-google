import { all } from 'redux-saga/effects';
import {loginSaga} from './loginSaga';
export function* loginSagas() {
  yield all([
    ...loginSaga(),
  ]);
}
