import {call, takeLatest, put, select} from 'redux-saga/effects';
import {USER_EXPIRED, USER_FOUND, USER_SIGNED_OUT} from 'redux-oidc';
import {userManager} from 'services/login';
import {errorEventCreator} from '../../errors/events/errorEvent';
import i18n from 'i18n';
import {push} from 'react-router-redux';
import {errorRouterUrlPattern} from 'services/routes/routeUrls';
import {delay, takeEvery} from 'redux-saga';
import {Action} from 'redux';

function* startLoginFlow(action: Action) {
  if (action.type === USER_FOUND) {
    return;
  }
  //User expired ocorre após um signin, caso já exista outro usuário previamente logado, portanto
  //é necessário esperar o evento do usuário corrente logado
  yield delay(500);
  yield* trySignIn();
}

function* userSignedOut() {
  yield* trySignIn();
}

function* trySignIn() {
  try {
    yield call(() => userManager.signinRedirect());
  } catch (e) {
    yield put(errorEventCreator(i18n('FAILED_TO_CONTACT_LOGIN_SERVER'), e));
    yield put(push(errorRouterUrlPattern));
  }
}

export function* loginSaga() {
  yield takeLatest([USER_EXPIRED, USER_FOUND], startLoginFlow);
  yield takeLatest(USER_SIGNED_OUT, userSignedOut);
}
