import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import { CallbackComponent } from 'redux-oidc';
import { push } from 'react-router-redux';
import {userManager} from 'services/login';
import {homeUrl} from 'services/routes/routeUrls';

interface Props {
  changeToSuccessPage?: () => void;
  changeToFailPage?: () => void;
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
  return {
    changeToFailPage: () => dispatch(push(homeUrl)),
    changeToSuccessPage: () => dispatch(push(homeUrl)),
  };
}

@connect(() => ({}), mapDispatchToProps)
export class CallbackPage extends React.Component<Props> {
  render() {
    return (
      <CallbackComponent
        userManager={userManager}
        successCallback={this.props.changeToSuccessPage}
        errorCallback={this.props.changeToFailPage}
      >
        <div>Redirecting...</div>
      </CallbackComponent>
    );
  }
}
