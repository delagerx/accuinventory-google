import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch } from 'redux';

import { LoadingSpinner } from '../../../shared/components/loading/loading';
import { User } from '../../session/entities/user';
import { loadUserEventDefs, LoadUserRequested } from '../../session/events/loadUserInfoEvent';
import { FooterComponent } from 'application/application/components/footer';
import { SystemAlert } from 'application/application/containers/alerts';

export interface Props extends RouteComponentProps<{lo?: string}, void> {
  children?: any;
  loadUser?: (request: LoadUserRequested) => void;
  user?: User;
}

@connect(mapStateToProps, mapDispatchToProps)
export class AppPage extends React.Component<Props> {
  componentWillMount() {
    this.props.loadUser({lo: this.props.params.lo});
  }
  render() {
    return (
        <div>
          <LoadingSpinner isLoading={!this.props.user}>
            {this.props.children}
            <FooterComponent/>
          </LoadingSpinner>
          <SystemAlert/>
        </div>
      );
  }
}

function mapStateToProps(state: RootState) {
  return {
    user: state.session.user,
  };
}

function mapDispatchToProps(dispatch: Dispatch<any>): Partial<Props> {
  return {
    loadUser: loadUserEventDefs.com.dispatcher(dispatch),
  };
}
