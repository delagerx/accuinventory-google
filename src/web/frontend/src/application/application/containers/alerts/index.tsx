import { RootState } from '../../../../infraestructure/redux/reducers/rootReducer';
import { connect } from 'react-redux';
import * as React from 'react';
import Snackbar from 'material-ui/Snackbar';

export interface Props {
  message?: string;
}

interface State {
  open: boolean;
}

@connect(mapStateToProps)
export class SystemAlert extends React.Component<Props, State> {
  state = {
    open: false,
  };
  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.message) {
      this.setState({
        open: true,
      });
    }
  }

  handleRequestClose = () => {
    this.setState({ open: false });
  }
  render() {
    return (
      <Snackbar
        open={this.state.open}
        onRequestClose={this.handleRequestClose}
        message={<span>{this.props.message}</span>}
        autoHideDuration={5000}
      />
    );
  }
}

function mapStateToProps(state: RootState) {
  return {
    message: state.errors.message,
  };
}
