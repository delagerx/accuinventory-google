import {RootState} from 'infraestructure/redux/reducers/rootReducer';
import * as React from 'react';
import {isAuthenticated, userManager} from '../../../../services/login';
import {AppPage} from '../appPage';
import {connect} from 'react-redux';
import {RouteComponentProps} from 'react-router';
import {LoadingSpinner} from '../../../shared/components/loading/loading';

interface Props extends RouteComponentProps<{lo?: string}, void> {
  user: any;
  isLoadingUser: boolean;
}

function mapStateToProps(state: RootState): Partial<Props> {
  return {
    user: state.oidc.user,
    isLoadingUser: state.oidc.isLoadingUser,
  };
}

@connect(mapStateToProps)
export class AuthenticatedPage extends React.Component<Props> {
  render() {
    return isAuthenticated(this.props.user)
      ? <AppPage {...this.props}>{this.props.children}</AppPage>
      : <LoadingSpinner isLoading={true}/>;
  }
}
