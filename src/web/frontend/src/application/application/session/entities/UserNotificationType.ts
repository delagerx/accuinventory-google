export enum UserNotificationType {
  Default = 1,
  Success = 2,
  Danger = 3,
  Warning = 4,
  Information = 5,
}
