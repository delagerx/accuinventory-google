import { LogisticOperation } from './logisticOperation';
import { User } from './user';
import {UserNotificationItem} from './UserNotificationItem';
export interface SessionState {
    user?: User;
    currentLogisticOperation?: LogisticOperation;
    notifications: UserNotificationItem[];
    notificationCount: number;
}
