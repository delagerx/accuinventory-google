export interface LogisticOperation {
  id: LogisticOperationId;
  name: string;
}

export type LogisticOperationId = string;
