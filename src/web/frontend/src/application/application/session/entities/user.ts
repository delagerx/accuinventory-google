import { LogisticOperation } from './logisticOperation';

export interface User {
    preferredLanguage: string;
    name: string;
    function: string;
    email: string;
    logisticOperations: LogisticOperation[];
}
