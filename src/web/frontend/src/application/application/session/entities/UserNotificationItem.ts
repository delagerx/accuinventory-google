import {UserNotificationType} from './UserNotificationType';

export interface UserNotificationItem {
  id: number;
  title: string;
  message: string;
  createdAt: string;
  sender: string;
  isRead: boolean;
  type: UserNotificationType;
}
