import { LogisticOperationId } from '../entities/logisticOperation';
import { Dispatch } from 'redux';
import { LogisticOperation } from 'application/application/session/entities/logisticOperation';
import { push } from 'react-router-redux';
import { makeUrl } from 'services/routes';

export const logisticOperationChanged = 'LOGISTIC_OPERATION_CHANGED';
export interface LogisticOperationChanged {
  id: LogisticOperationId;
}

export function logisticOperationChangedDispatcher(dispatch: Dispatch<any>) {
  return (id: LogisticOperationId) => {
    dispatch(push(makeUrl('Home', id)));
    dispatch({
      type: logisticOperationChanged,
      payload: {
        id: id,
      },
    });
  };
}
