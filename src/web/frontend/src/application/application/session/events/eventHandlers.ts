import { SessionState } from '../entities/sessionState';
import { handleActions, Action } from 'redux-actions';
import { loadUserEventDefs, LoadUserSucceeded } from 'application/application/session/events/loadUserInfoEvent';
import {
  logisticOperationChanged,
  LogisticOperationChanged } from 'application/application/session/events/changeLogisticOperation';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import {NotificationsRefreshed, notificationsRefreshed} from './notificationsRefreshedEvent';
import {ReadNotificationSucceeded, readUserNotificationDefs} from './readNotificationsCommand';

const initialState = {
  notifications: [],
  notificationCount: 0,
};

export default handleActions<SessionState, any>({
  [loadUserEventDefs.succ.name]: handleLoadUserSuccess,
  [logisticOperationChanged]: handleLogisticOperationChanged,
  [notificationsRefreshed]: handleNotificationsRefreshed,
  [readUserNotificationDefs.succ.name]: handleNotificationsRead,
}, initialState);

function handleNotificationsRead(state: SessionState, action: Action<ReadNotificationSucceeded>): SessionState {
  const unreadNotifications =
    state
      .notifications
      .filter(x => action.payload.notificationsIds.indexOf(x.id) === -1);
  const readNotifications = state
    .notifications
    .filter(x => action.payload.notificationsIds.indexOf(x.id) !== -1)
    .map(x => ({...x, isRead: true}));
  return {
    ...state,
    notifications: unreadNotifications.concat(readNotifications),
    notificationCount: state.notificationCount - action.payload.notificationsIds.length,
  };
}

function handleNotificationsRefreshed(state: SessionState, action: Action<NotificationsRefreshed>): SessionState {
  return {
    ...state,
    notifications: action.payload.notifications,
    notificationCount: action.payload.notificationCount,
  };
}

function handleLoadUserSuccess(state: SessionState, action: Action<LoadUserSucceeded>): SessionState {
  return {
    ...state,
    user: action.payload.user,
    currentLogisticOperation: action.payload.logisticOperation,
  };
}

function handleLogisticOperationChanged(state: SessionState, action: Action<LogisticOperationChanged>): SessionState {
  return {
    ...state,
    currentLogisticOperation: state.user.logisticOperations.find(x => x.id === action.payload.id),
  };
}

export function getCurrentLogisticOperationId(state: RootState) {
  return state.session.currentLogisticOperation.id;
}

export function getNotifications(state: RootState) {
  return {
    notifications: state.session.notifications,
    count: state.session.notificationCount,
  };
}
