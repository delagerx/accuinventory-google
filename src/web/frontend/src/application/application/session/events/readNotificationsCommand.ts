import {readUserNotifications} from '../../../../services/apis/user/readUserNotifications';
import {call} from 'redux-saga/effects';
import {apiReduxBoilerplateCreator} from '../../../../utils/apiHandlerCreator';

export const notificationsReadNamespace = 'READ_NOTIFICATIONS';
export interface ReadNotificationCommand {
  notificationsIds: number[];
}
export interface ReadNotificationSucceeded {
  notificationsIds: number[];
}

function* readNotificationApiCall(payload: ReadNotificationCommand) {
  yield call(() => readUserNotifications(payload.notificationsIds));
  return {notificationsIds: payload.notificationsIds};
}

const apiOptions = {
  apiCall: readNotificationApiCall,
};

export const readUserNotificationDefs =
apiReduxBoilerplateCreator<ReadNotificationCommand, ReadNotificationSucceeded>(notificationsReadNamespace, apiOptions);
