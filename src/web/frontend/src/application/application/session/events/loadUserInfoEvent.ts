import { LogisticOperation } from '../entities/logisticOperation';
import { call, put } from 'redux-saga/effects';
import { setLocale } from 'services/locales/i18n';
import { apiReduxBoilerplateCreator } from 'utils/apiHandlerCreator';
import { fetchCurrentUserInfo } from 'services/apis/user/fetchCurrentUserInfo';
import { User } from 'application/application/session/entities/user';
import { push } from 'react-router-redux';
import { makeUrl } from 'services/routes';

export const loadUserRequested = 'LOAD_USER';
export interface LoadUserRequested {
    lo: string | undefined;
}

export interface LoadUserSucceeded {
    user: User;
    logisticOperation: LogisticOperation;
}

function* apiCall(payload: LoadUserRequested) {
    const user: User = yield call(() => fetchCurrentUserInfo());
    let logisticOperation: LogisticOperation;
    const existingLo = user.logisticOperations.find(x => x.id === payload.lo);
    if (existingLo) {
        logisticOperation = existingLo;
    } else {
        logisticOperation = user.logisticOperations[0];
        yield put(push(makeUrl('Home', logisticOperation.id)));
    }
    return {
        user,
        logisticOperation: logisticOperation,
    };
}

const apiOptions = {
    apiCall: apiCall,
    redirectToCorrespondingResponse: true,
};

export const loadUserEventDefs =
    apiReduxBoilerplateCreator<LoadUserRequested, LoadUserSucceeded>(loadUserRequested, apiOptions);
