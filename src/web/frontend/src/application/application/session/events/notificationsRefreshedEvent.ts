import {UserNotificationItem} from '../entities/userNotificationItem';

export const notificationsRefreshed = 'NOTIFICATIONS_REFRESHED';
export interface NotificationsRefreshed {
  notifications: UserNotificationItem[];
  notificationCount: number;
}

export function notificationRefreshedCreator(notifications: UserNotificationItem[], count: number) {
  return {
    type: notificationsRefreshed,
    payload: {
      notifications: notifications,
      notificationCount: count,
    },
  };
}
