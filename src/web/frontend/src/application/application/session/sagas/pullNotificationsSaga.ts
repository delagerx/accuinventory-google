import {call, put, select, takeLatest} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import {fetchCurrentUserNotifications} from 'services/apis/user/fetchUserNotifications';
import {notificationRefreshedCreator} from '../events/notificationsRefreshedEvent';
import {loadUserEventDefs} from '../events/loadUserInfoEvent';
import {UserNotificationItem} from '../entities/UserNotificationItem';
import {getNotifications} from '../events/eventHandlers';
import {UserNotificationsDto} from 'services/apis/user/dtos/userNotificationsDto';

function* pullNotifications() {
  function sortNotificationByDate(n1: UserNotificationItem, n2: UserNotificationItem) {
    return n1.createdAt > n2.createdAt ? -1 : n1.createdAt === n2.createdAt ? 0 : 1;
  }

  function repeatedOldNotifications(newNotificationsId: number[], n) {
    return newNotificationsId.indexOf(n.id) === -1;
  }

  while (true) {
    const notificationsResponse: UserNotificationsDto = yield call(fetchCurrentUserNotifications);
    const {newNotifications, notificationCount} = notificationsResponse;
    const oldNotifications = yield select(getNotifications);
    const newNotificationsId = newNotifications.map(x => x.id);
    const notifications = oldNotifications.notifications
      .filter(n => repeatedOldNotifications(newNotificationsId, n))
      .concat(newNotifications)
      .sort(sortNotificationByDate)
      .slice(0, 5);
    yield put(notificationRefreshedCreator(notifications, notificationCount));
    yield delay(10000);
  }
}

export function* pullNotificationsSaga() {
  yield takeLatest(loadUserEventDefs.succ.name, pullNotifications);
}
