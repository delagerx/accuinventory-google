import {takeLatest} from 'redux-saga/effects';
import {loadUserEventDefs} from '../events/loadUserInfoEvent';
import {delay} from 'redux-saga';
import * as cookies from 'browser-cookies';
import {getCurrentLanguage, isLanguageRegistred} from 'i18n';

function* pullCookieLanguage() {
  while (true) {
    const cookieLanguage = cookies.get('_culture');
    const currentLanguage = getCurrentLanguage();
    if (cookieLanguage && cookieLanguage !== currentLanguage && isLanguageRegistred(cookieLanguage)) {
      location.reload();
    }
    yield delay(3000);
  }
}

export function* pullCookieLanguageSaga() {
  yield takeLatest(loadUserEventDefs.succ.name, pullCookieLanguage);
}
