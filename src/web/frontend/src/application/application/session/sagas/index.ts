import { loadUserEventDefs } from '../events/loadUserInfoEvent';
import { all } from 'redux-saga/effects';
import {pullNotificationsSaga} from './pullNotificationsSaga';
import {readUserNotificationDefs} from '../events/readNotificationsCommand';
import {pullCookieLanguageSaga} from './pullCookieLanguageSaga';
export function* sessionSagas() {
    yield all([
      loadUserEventDefs.saga(),
      ...pullNotificationsSaga(),
      readUserNotificationDefs.saga(),
      ...pullCookieLanguageSaga(),
    ]);
}
