import { all } from 'redux-saga/effects';
import loadUserFavorites from 'application/home/dashboard/sagas/loadFavoritesSaga';
import fetchFrequents from 'application/home/frequents/sagas/fetchFrequentSaga';
import removeOnToggle from 'application/home/dashboard/sagas/removeOnToggleSaga';

import { sessionSagas } from 'application/application/session/sagas';
import widgetSagas from 'application/widget/sagas';
import resultsSagas from 'application/results/sagas';
import itemSagas from 'application/selectedItem/sagas';
import { searchSagas } from 'application/search/sagas';
import {loginSagas} from '../../../application/application/login/sagas';

export default function* rootSaga() {
  yield all([
    ...loadUserFavorites(),
    ...fetchFrequents(),
    ...removeOnToggle(),

    ...sessionSagas(),
    ...widgetSagas(),
    ...resultsSagas(),
    ...itemSagas(),
    ...searchSagas(),
    ...loginSagas(),
  ]);
}
