import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import { createUserManager, loadUser } from 'redux-oidc';
import { RootState } from 'infraestructure/redux/reducers/rootReducer';
import { reducers } from 'infraestructure/redux/reducers/store';
import rootSaga from '../saga';
import thunk from 'redux-thunk';
import { History } from 'history';
import {userManager} from '../../../services/login';

export function configureStore(history: History, initialState?: RootState): Store<RootState> {
  const create = window.devToolsExtension
    ? window.devToolsExtension()(createStore)
    : createStore;

  const sagaMiddleware = createSagaMiddleware();
  const loadingMiddleware: any = loadingBarMiddleware({
      promiseTypeSuffixes: ['REQUESTED', 'SUCCEEDED', 'FAILED'],
    });
  const createStoreWithMiddleware = applyMiddleware(
    sagaMiddleware,
    loadingMiddleware,
    routerMiddleware(history),
    thunk)(create);
  const store = createStoreWithMiddleware(reducers, initialState) as Store<RootState>;
  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('../reducers/rootReducer', () => {
      const nextReducer = require('../reducers/rootReducer');
      store.replaceReducer(nextReducer);
    });
  }
  loadUser(store, userManager);
  return store;
}
