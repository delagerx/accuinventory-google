import { ErrorState } from '../../../application/application/errors/entities/errorState';
import { SessionState } from 'application/application/session/entities/sessionState';
import { HomeState } from 'application/home/entities/homeState';
import { ResultState } from 'application/results/entities/resultState';
import { AutoCompleteState } from 'application/search/entities/autoCompleteState';
import { SelectedItemState } from 'application/selectedItem/entities/selectedItemState';
import { WidgetState } from 'application/widget/entities/widgetState';

export interface RootState {
  oidc: any;
  autoComplete: AutoCompleteState;
  session: SessionState;
  home: HomeState;
  widget: WidgetState;
  result: ResultState;
  selectedItem: SelectedItemState;
  errors: ErrorState;
}
