import { errorReducer } from 'application/application/errors/events/errorEventHandlers';
import SessionReducer from 'application/application/session/events/eventHandlers';
import HomeReducer from 'application/home/reducer';
import ResultReducer from 'application/results/events/searchResultEventHandlers';
import SearchReducer from 'application/search/events/autoCompleteEventHandlers';
import SelectedItemReducer from 'application/selectedItem/events/eventHandlers';
import WidgetReducer from 'application/widget/events/eventHandlers';
import { loadingBarReducer } from 'react-redux-loading-bar';
import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { reducer as oidcReducer } from 'redux-oidc';
import { RootState } from './rootReducer';

export const reducers = combineReducers<RootState>({
  routing: routerReducer,
  loadingBar: loadingBarReducer,
  oidc: oidcReducer,

  errors: errorReducer,
  autoComplete: SearchReducer,
  session: SessionReducer,
  home: HomeReducer,
  widget: WidgetReducer,
  result: ResultReducer,
  selectedItem: SelectedItemReducer,
});
