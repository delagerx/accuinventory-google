var webpack = require('webpack');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');
var path = require('path');

// variables
var isProduction = process.argv.indexOf('-p') >= 0;
var sourcePath = path.join(__dirname, './src');
var outPath = path.join(__dirname, './dist');

// plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const environmentSettings = path.resolve(sourcePath, 'config', process.env.NODE_ENV.trim() + '.js');
console.log(environmentSettings)

module.exports = {
  context: sourcePath,
  entry: {
    main: './index.tsx',
  },
  output: {
    path: outPath,
    publicPath: '/',
    filename: '[name].bundle.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    mainFields: ['browser', 'main'],
    plugins: [new TsConfigPathsPlugin(/* { tsconfig, compiler } */)],
    alias: {
        environment: environmentSettings
    }
  },
  module: {
    loaders: [
      // css
      {
        test: /\.css$/,
        include: [/node_modules/],
        use: [
            {
                loader: 'style-loader'
            },
            {
                loader: 'css-loader'
            }]
      },
      {
        test: /\.scss$/,
        use: [
            {
                loader: 'style-loader'
            },
            {
                loader: 'css-loader',
            },
            {
                loader: "sass-loader",
                options: {
                    includePaths: [path.join(sourcePath, "./sass"), path.join(__dirname, "./node_modules/materialize-css/sass")]
                }
            }],
      },
      {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 80000
              }
            }
        ]
      },
      {
        test: /\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader",
        options: {
          limit: 80000
        }
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ 
      template: 'views/index.html' 
    }),
    new ExtractTextPlugin("style.css"),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'node-static',
      filename: 'node-static.js',
      minChunks(module, count) {
          var context = module.context;
          return context && context.indexOf('node_modules') >= 0;
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'react-build',
      minChunks(module, count) {
          var context = module.context;
          return context && (context.indexOf('node_modules\\react\\') >= 0 || context.indexOf('node_modules\\react-dom\\') >= 0);
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'manifest'
    }),        
    new webpack.optimize.CommonsChunkPlugin({
        async: 'used-twice',
        minChunks(module, count) {
            return count >= 2;
        },
    }),
  ]
};
