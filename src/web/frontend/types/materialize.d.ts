declare interface MaterializeColors {
    red: string,
    blue: string,
    yellow: string,
    green: string
}