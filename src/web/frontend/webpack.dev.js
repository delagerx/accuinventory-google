const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const DashboardPlugin = require('webpack-dashboard/plugin');
var path = require('path');
var sourcePath = path.join(__dirname, './src');

module.exports = merge(common, {
  module: {
    loaders: [
      {
        test: /\.tsx?$/,
        use: [ 
          'react-hot-loader', 
          'awesome-typescript-loader' 
        ] 
      },
    ]
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: sourcePath,
    hot: true,
    stats: {
      warnings: false
    },
  },
  plugins: [
    new DashboardPlugin(),
    //new BundleAnalyzerPlugin({
    //  analyzerMode: 'static'
    //})
  ]
});
